﻿using FluentValidation;
using Nop.Core.Domain.Common;
using Nop.Services.Localization;
using Nop.Web.Framework.Validators;
using Nop.Web.Models.Checkout;
using System;

namespace Nop.Web.Validators.Checkout
{
    public partial class CheckoutAddressValidator : BaseNopValidator<CheckoutAddressModel>
    {
        public CheckoutAddressValidator(ILocalizationService localizationService)
        {
            RuleFor(x => x.PartyDate).NotNull().WithMessage(localizationService.GetResource("CheckoutAddresss.PartyDate.Required"));
            RuleFor(x => x.PartyDate).Must(x => x.HasValue && x.Value >= DateTime.Today).WithMessage(localizationService.GetResource("CheckoutAddresss.PartyDate.MinDate"));
        }
    }
}