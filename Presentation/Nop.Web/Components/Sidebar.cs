﻿using Microsoft.AspNetCore.Mvc;
using Nop.Web.Factories;
using Nop.Web.Framework.Components;

namespace Nop.Web.Components
{
    public class SidebarViewComponent : NopViewComponent
    {
        private readonly ICatalogModelFactory _catalogModelFactory;

        public SidebarViewComponent(ICatalogModelFactory catalogModelFactory)
        {
            _catalogModelFactory = catalogModelFactory;
        }

        public IViewComponentResult Invoke(int? currentCategoryId, int? currentProductId)
        {
            var model = _catalogModelFactory.PrepareSidebarOverviewModel(Url, currentCategoryId, currentProductId);
            return View(model);
        }
    }
}
