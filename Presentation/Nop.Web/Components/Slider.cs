﻿using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Web.Factories;
using Nop.Web.Framework.Components;

namespace Nop.Web.Components
{
    public class SliderViewComponent : NopViewComponent
    {
        private readonly ICatalogModelFactory _catalogModelFactory;

        public SliderViewComponent(ICatalogModelFactory catalogModelFactory,
            IStoreContext storeContext,
            IWorkContext workContext)
        {
            _catalogModelFactory = catalogModelFactory;
        }

        public IViewComponentResult Invoke(int sliderId)
        {
            var model = _catalogModelFactory.PrepareSliderOverviewModel(sliderId, Url);
            return View(model);
        }
    }
}
