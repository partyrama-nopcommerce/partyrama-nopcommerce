﻿using Microsoft.AspNetCore.Mvc;
using Nop.Web.Factories;
using Nop.Web.Framework.Components;

namespace Nop.Web.Components
{
    public class ShippingInfoViewComponent : NopViewComponent
    {
        private readonly ICommonModelFactory _commonModelFactory;

        public ShippingInfoViewComponent(ICommonModelFactory commonModelFactory)
        {
            _commonModelFactory = commonModelFactory;
        }

        public IViewComponentResult Invoke(int? productThumbPictureSize)
        {
            var model = _commonModelFactory.PrepareShippingInfoModel();
            return View(model);
        }
    }
}