﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Nop.Core.Domain.Catalog;
using Nop.Services.Catalog;
using Nop.Services.Security;
using Nop.Services.Stores;
using Nop.Web.Factories;
using Nop.Web.Framework.Components;

namespace Nop.Web.Components
{
    public class VfrRelatedProductsViewComponent : NopViewComponent
    {
        #region Const

        private const int PRODUCT_LIMIT = 10;

        #endregion

        private readonly IConnectionsService _connectionService;
        private readonly IProductModelFactory _productModelFactory;
        private readonly IProductService _productService;
        private readonly IAclService _aclService;
        private readonly IStoreMappingService _storeMappingService;

        public VfrRelatedProductsViewComponent(IConnectionsService connectionService,
            IProductModelFactory productModelFactory,
            IProductService productService, 
            IAclService aclService,
            IStoreMappingService storeMappingService)
        {
            _productModelFactory = productModelFactory;
            _productService = productService;
            _connectionService = connectionService;
            _aclService = aclService;
            _storeMappingService = storeMappingService;
        }

        public IViewComponentResult Invoke(int productId, int? categoryId)
        {
            if (categoryId.GetValueOrDefault() <= 0)
                return Content("");

            var productIds = _connectionService.GetProductIdsWithIndexByObjectId(categoryId.Value, ConnectionFromType.cat, PRODUCT_LIMIT, productId);

            //load products
            var products = _productService.GetProductsByIds(productIds.Keys.ToArray());
            //exlude separators
            products = products.Where(p => p.ProductType != ProductType.Separator).ToList();
            //ACL and store mapping
            products = products.Where(p => _aclService.Authorize(p) && _storeMappingService.Authorize(p)).ToList();
            //availability dates
            products = products.Where(p => _productService.ProductIsAvailable(p)).ToList();
            //visible individually
            products = products.Where(p => p.VisibleIndividually).ToList();

            if (!products.Any())
                return Content("");

            IList<Product> productsToReturn = new List<Product>();

            if (products.Count <= PRODUCT_LIMIT)
            {
                productsToReturn = products;
            }
            else
            {
                foreach (var item in productIds.OrderBy(p => p.Value))
                {
                    if (productsToReturn.Count == PRODUCT_LIMIT)
                    {
                        break;
                    }

                    var product = products.FirstOrDefault(p => p.Id == item.Key);
                    if (product != null)
                    {
                        productsToReturn.Add(product);
                    }
                }
            }

            var model = _productModelFactory.PrepareProductOverviewModels(productsToReturn, true, true).ToList();
            return View(model);
        }
    }
}