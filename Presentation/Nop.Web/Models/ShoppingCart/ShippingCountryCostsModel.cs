﻿using System.Collections.Generic;
using Nop.Web.Framework.Models;

namespace Nop.Web.Models.ShoppingCart
{
    public partial class ShippingCountryCostsModel : BaseNopModel
    {
        public ShippingCountryCostsModel()
        {
            Items = new List<ShippingCountryCostModel>();
        }
        
        public IList<ShippingCountryCostModel> Items { get; set; }
    }

    public partial class ShippingCountryCostModel : BaseNopModel
    {
        public int CountryId { get; set; }

        public string Time { get; set; }

        public string Cost { get; set; }
    }
}