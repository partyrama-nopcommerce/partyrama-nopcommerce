﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Framework.Mvc.ModelBinding;
using Nop.Web.Framework.Models;
using Nop.Core.Domain.Shipping;

namespace Nop.Web.Models.ShoppingCart
{
    public partial class ShippingCountryModel : BaseNopModel
    {
        public ShippingCountryModel()
        {
            AvailableCountries = new List<SelectListItem>();
        }

        public bool Enabled { get; set; }

        [NopResourceDisplayName("ShoppingCart.EstimateShipping.Country")]
        public int? CountryId { get; set; }
        
        public IList<SelectListItem> AvailableCountries { get; set; }
    }

    public partial class ShippingCountryResultModel : BaseNopModel
    {
        public ShippingCountryResultModel()
        {
            ShippingOptions = new List<ShippingOption>();
            Warnings = new List<string>();
        }

        public IList<ShippingOption> ShippingOptions { get; set; }

        public IList<string> Warnings { get; set; }
    }
}