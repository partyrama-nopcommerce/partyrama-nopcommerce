﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentValidation.Attributes;
using Nop.Web.Framework.Models;
using Nop.Web.Models.Common;
using Nop.Web.Validators.Checkout;

namespace Nop.Web.Models.Checkout
{
    [Validator(typeof(CheckoutAddressValidator))]
    public partial class CheckoutAddressModel : BaseNopModel
    {
        public CheckoutAddressModel()
        {
            ExistingAddresses = new List<AddressModel>();
            BillingNewAddress = new AddressModel();
            ShippingNewAddress = new AddressModel();
        }
        
        public IList<AddressModel> ExistingAddresses { get; set; }

        public AddressModel BillingNewAddress { get; set; }
        public AddressModel ShippingNewAddress { get; set; }

        public bool ShipToSameAddress { get; set; }
        public bool ShipToSameAddressAllowed { get; set; }

        public DateTime? PartyDate { get; set; }

        public IEnumerable<AddressModel> BillingAddresses => ExistingAddresses.Where(a => a.CountryAllowsBilling);
        public IEnumerable<AddressModel> ShippingAddresses => ExistingAddresses.Where(a => a.CountryAllowsShipping);
    }
}