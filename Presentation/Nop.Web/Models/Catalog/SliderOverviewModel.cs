﻿using System;
using System.Collections.Generic;
using Nop.Core.Domain.Catalog;
using Nop.Web.Framework.Models;

namespace Nop.Web.Models.Catalog
{
    public class SliderOverviewModel : BaseNopEntityModel
    {
        public SliderOverviewModel()
        {
            Items = new List<SliderItemOverviewModel>();
        }

        public SliderType SliderType { get; set; }

        public int? Speed { get; set; }

        public bool VisibleArrows { get; set; }

        public bool VisibleDots { get; set; }

        public string CssClass { get; set; }

        public BorderStyle BorderStyle { get; set; }

        public int? BorderHeight { get; set; }

        public string BorderColor { get; set; }

        public string Ride => Speed.HasValue ? "carousel": "false";

        public string Interval => Speed.HasValue ? Speed.Value.ToString() : "false";

        public BorderPlacement BorderPlacement { get; set; }

        public string BorderTopWidth => BorderPlacement == BorderPlacement.Top || BorderPlacement == BorderPlacement.Both ? $"{BorderHeight.GetValueOrDefault()}px" : "0";

        public string BorderBottomWidth => BorderPlacement == BorderPlacement.Bottom || BorderPlacement == BorderPlacement.Both ? $"{BorderHeight.GetValueOrDefault()}px" : "0";

        public string BorderStyleText => BorderStyle.ToString().ToLower();

        public int? Number { get; set; }

        public IList<SliderItemOverviewModel> Items { get; set; }

        public Guid SliderNumber { get; set; }
    }
}