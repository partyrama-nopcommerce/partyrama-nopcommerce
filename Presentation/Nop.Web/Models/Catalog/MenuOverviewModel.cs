﻿using System.Collections.Generic;
using Nop.Web.Framework.Models;

namespace Nop.Web.Models.Catalog
{
    public class MenuOverviewModel : BaseNopEntityModel
    {
        public MenuOverviewModel()
        {
            Items = new List<MenuItemOverviewModel>();
        }

        public IList<MenuItemOverviewModel> Items { get; set; }
    }
}