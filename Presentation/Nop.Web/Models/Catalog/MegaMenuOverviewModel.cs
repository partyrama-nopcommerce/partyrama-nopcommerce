﻿using System.Collections.Generic;
using Nop.Web.Framework.Models;

namespace Nop.Web.Models.Catalog
{
    public class MegaMenuOverviewModel : BaseNopEntityModel
    {
        public MegaMenuOverviewModel()
        {
            Items = new List<MegaMenuItemOverviewModel>();
        }

        public IList<MegaMenuItemOverviewModel> Items { get; set; }
    }
}