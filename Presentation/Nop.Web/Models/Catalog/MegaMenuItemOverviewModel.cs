﻿using Nop.Core.Domain.Catalog;
using Nop.Web.Framework.Models;
using Nop.Web.Models.Media;
using System.Collections.Generic;

namespace Nop.Web.Models.Catalog
{
    public class MegaMenuItemOverviewModel : BaseNopEntityModel
    {
        public MegaMenuItemOverviewModel()
        {
            Rows = new List<MegaMenuItemRowModel>();
        }

        public string Name { get; set; }

        public string Link { get; set; }

        public string CssClass { get; set; }

        public string Description { get; set; }

        public RowType RowType { get; set; }

        public RowConnectionType RowConnectionType { get; set; }

        public PictureModel PictureModel { get; set; }

        public SliderOverviewModel SliderModel { get; set; }

        public IList<MegaMenuItemRowModel> Rows { get; set; }

        #region Nested Classes

        public partial class MegaMenuItemRowModel : BaseNopEntityModel
        {
            public string Name { get; set; }

            public string Link { get; set; }

            public string CssClass { get; set; }

            public string Color { get; set; }

            public PictureModel PictureModel { get; set; }
        }

        #endregion
    }
}