﻿using Nop.Core.Domain.Catalog;
using Nop.Web.Framework.Models;
using Nop.Web.Models.Media;

namespace Nop.Web.Models.Catalog
{
    public partial class ConnectionModel : BaseNopEntityModel
    {
        public ConnectionModel() { }

        public ConnectionToType ToType { get; set; }

        public string CssClass { get; set; }

        public bool ShowDescription { get; set; }

        public ProductOverviewModel Product { get; set; }

        public ConnectionCategoryModel Category { get; set; }

        public ConnectionElementModel Element { get; set; }

        public ConnectionSliderModel Slider { get; set; }

        #region Nested Classes

        public partial class ConnectionCategoryModel : BaseNopEntityModel
        {
            public ConnectionCategoryModel()
            {
                PictureModel = new PictureModel();
            }

            public string Name { get; set; }

            public string SeName { get; set; }

            public string ShortDescription { get; set; }

            public PictureModel PictureModel { get; set; }
        }

        public partial class ConnectionElementModel : BaseNopEntityModel
        {
            public ConnectionElementModel() { }

            public string Name { get; set; }

            public string Description { get; set; }

        }

        public partial class ConnectionSliderModel : BaseNopEntityModel
        {
            public ConnectionSliderModel() { }

            public string Name { get; set; }
        }

        #endregion
    }
}
