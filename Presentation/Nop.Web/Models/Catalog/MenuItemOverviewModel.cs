﻿using Nop.Core.Domain.Catalog;
using Nop.Web.Framework.Models;
using Nop.Web.Models.Media;
using System.Collections.Generic;

namespace Nop.Web.Models.Catalog
{
    public class MenuItemOverviewModel : BaseNopEntityModel
    {
        public MenuItemOverviewModel()
        {
            Rows = new List<MenuItemRowModel>();
        }

        public string Name { get; set; }

        public string Link { get; set; }

        public string CssClass { get; set; }

        public string Color { get; set; }

        public RowType RowType { get; set; }

        public int? MegaMenuTabId { get; set; }

        public IList<MenuItemRowModel> Rows { get; set; }

        #region Nested Classes

        public partial class MenuItemRowModel : BaseNopEntityModel
        {
            public string Name { get; set; }

            public string Link { get; set; }

            public string CssClass { get; set; }

            public string Color { get; set; }

            public PictureModel PictureModel { get; set; }
        }

        #endregion
    }
}