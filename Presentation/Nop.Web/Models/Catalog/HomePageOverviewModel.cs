﻿using System.Collections.Generic;
using Nop.Web.Framework.Models;

namespace Nop.Web.Models.Catalog
{
    public class HomePageOverviewModel : BaseNopEntityModel
    {
        public HomePageOverviewModel()
        {
            Items = new List<HomePageItemOverviewModel>();
        }

        public IList<HomePageItemOverviewModel> Items { get; set; }
    }
}