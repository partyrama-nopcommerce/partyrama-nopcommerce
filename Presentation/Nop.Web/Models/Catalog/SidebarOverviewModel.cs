﻿using System.Collections.Generic;
using Nop.Web.Framework.Models;

namespace Nop.Web.Models.Catalog
{
    public class SidebarOverviewModel : BaseNopEntityModel
    {
        public SidebarOverviewModel()
        {
            Items = new List<SidebarItemOverviewModel>();
        }

        public IList<SidebarItemOverviewModel> Items { get; set; }
    }
}