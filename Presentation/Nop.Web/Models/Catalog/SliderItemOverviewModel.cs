﻿using Nop.Core.Domain.Catalog;
using Nop.Web.Framework.Models;
using Nop.Web.Models.Media;
using System;
using System.Collections.Generic;

namespace Nop.Web.Models.Catalog
{
    public class SliderItemOverviewModel : BaseNopEntityModel
    {
        public SliderItemOverviewModel()
        {
            Rows = new List<SliderItemRowModel>();
        }

        public string Name { get; set; }

        public string Link { get; set; }

        public string CssClass { get; set; }

        public string DotCssClass { get; set; }

        public int Index { get; set; }

        public Guid ParentNumber { get; set; }

        public string Description { get; set; }

        public bool ShowDescription { get; set; }

        public bool ShowBuyButton { get; set; }

        public RowType RowType { get; set; }

        public PictureModel PictureModel { get; set; }

        public IList<SliderItemRowModel> Rows { get; set; }

        #region Nested Classes

        public partial class SliderItemRowModel : BaseNopEntityModel
        {
            public string Name { get; set; }

            public string Link { get; set; }

            public string CssClass { get; set; }

            public string Color { get; set; }

            public PictureModel PictureModel { get; set; }
        }

        #endregion
    }
}