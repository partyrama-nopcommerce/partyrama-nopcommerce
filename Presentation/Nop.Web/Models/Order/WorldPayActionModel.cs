﻿using System;

namespace Nop.Web.Models.Order
{
    public class WorldPayActionModel
    {
        private string _orderKey;

        public string OrderKey
        {   get
            {
                return _orderKey;
            }
            set
            {
                _orderKey = value;
                OrderGuid = Guid.Parse(_orderKey.Split('^')[2]);
            }
        }
        public WorldPayPaymentStatus PaymentStatus { get; set; }
        public int PaymentAmount { get; set; }
        public string PaymentCurrency { get; set; }
        public string MAC { get; set; }
        public Guid OrderGuid { get; set; }
    }

    public enum WorldPayPaymentStatus
    {
        AUTHORISED,
        SHOPPER_REDIRECTED,
        SENT_FOR_AUTHORISATION,
        REFUSED,
        ERROR
    }
}
