﻿using Nop.Web.Framework.Models;

namespace Nop.Web.Models.Common
{
    public partial class ShippingInfoModel : BaseNopModel
    {
        public string CurrencySign { get; set; }
    }
}