﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Vendors;
using Nop.Web.Models.Catalog;

namespace Nop.Web.Factories
{
    public partial interface ICatalogModelFactory
    {
        #region Common

        /// <summary>
        /// Prepare sorting options
        /// </summary>
        /// <param name="pagingFilteringModel">Catalog paging filtering model</param>
        /// <param name="command">Catalog paging filtering command</param>
        void PrepareSortingOptions(CatalogPagingFilteringModel pagingFilteringModel, CatalogPagingFilteringModel command);

        /// <summary>
        /// Prepare view modes
        /// </summary>
        /// <param name="pagingFilteringModel">Catalog paging filtering model</param>
        /// <param name="command">Catalog paging filtering command</param>
        void PrepareViewModes(CatalogPagingFilteringModel pagingFilteringModel, CatalogPagingFilteringModel command);

        /// <summary>
        /// Prepare page size options
        /// </summary>
        /// <param name="pagingFilteringModel">Catalog paging filtering model</param>
        /// <param name="command">Catalog paging filtering command</param>
        /// <param name="allowCustomersToSelectPageSize">Are customers allowed to select page size?</param>
        /// <param name="pageSizeOptions">Page size options</param>
        /// <param name="fixedPageSize">Fixed page size</param>
        void PreparePageSizeOptions(CatalogPagingFilteringModel pagingFilteringModel, CatalogPagingFilteringModel command,
            bool allowCustomersToSelectPageSize, string pageSizeOptions, int fixedPageSize);

        #endregion

        #region Categories

        /// <summary>
        /// Prepare category model
        /// </summary>
        /// <param name="category">Category</param>
        /// <param name="command">Catalog paging filtering command</param>
        /// <returns>Category model</returns>
        CategoryModel PrepareCategoryModel(Category category, CatalogPagingFilteringModel command);

        /// <summary>
        /// Prepare category model for views with connections
        /// </summary>
        /// <param name="category">Category</param>
        /// <returns>Category model</returns>
        CategoryModel PreparePartyramaCategoryModel(Category category);

        /// <summary>
        /// Prepare category template view path
        /// </summary>
        /// <param name="templateId">Template identifier</param>
        /// <returns>Category template view path</returns>
        string PrepareCategoryTemplateViewPath(int templateId);

        /// <summary>
        /// Prepare category navigation model
        /// </summary>
        /// <param name="currentCategoryId">Current category identifier</param>
        /// <param name="currentProductId">Current product identifier</param>
        /// <returns>Category navigation model</returns>
        CategoryNavigationModel PrepareCategoryNavigationModel(int currentCategoryId,
            int currentProductId);

        /// <summary>
        /// Prepare top menu model
        /// </summary>
        /// <returns>Top menu model</returns>
        TopMenuModel PrepareTopMenuModel();

        /// <summary>
        /// Prepare homepage category models
        /// </summary>
        /// <returns>List of homepage category models</returns>
        List<CategoryModel> PrepareHomepageCategoryModels();

        /// <summary>
        /// Prepare category (simple) models
        /// </summary>
        /// <returns>List of category (simple) models</returns>
        List<CategorySimpleModel> PrepareCategorySimpleModels();

        /// <summary>
        /// Prepare category (simple) models
        /// </summary>
        /// <param name="rootCategoryId">Root category identifier</param>
        /// <param name="loadSubCategories">A value indicating whether subcategories should be loaded</param>
        /// <returns>List of category (simple) models</returns>
        List<CategorySimpleModel> PrepareCategorySimpleModels(int rootCategoryId, bool loadSubCategories = true);

        #endregion

        #region Manufacturers

        /// <summary>
        /// Prepare manufacturer model
        /// </summary>
        /// <param name="manufacturer">Manufacturer identifier</param>
        /// <param name="command">Catalog paging filtering command</param>
        /// <returns>Manufacturer model</returns>
        ManufacturerModel PrepareManufacturerModel(Manufacturer manufacturer, CatalogPagingFilteringModel command);

        /// <summary>
        /// Prepare manufacturer template view path
        /// </summary>
        /// <param name="templateId">Template identifier</param>
        /// <returns>Manufacturer template view path</returns>
        string PrepareManufacturerTemplateViewPath(int templateId);

        /// <summary>
        /// Prepare manufacturer all models
        /// </summary>
        /// <returns>List of manufacturer models</returns>
        List<ManufacturerModel> PrepareManufacturerAllModels();

        /// <summary>
        /// Prepare manufacturer navigation model
        /// </summary>
        /// <param name="currentManufacturerId">Current manufacturer identifier</param>
        /// <returns>Manufacturer navigation model</returns>
        ManufacturerNavigationModel PrepareManufacturerNavigationModel(int currentManufacturerId);

        #endregion

        #region Vendors

        /// <summary>
        /// Prepare vendor model
        /// </summary>
        /// <param name="vendor">Vendor</param>
        /// <param name="command">Catalog paging filtering command</param>
        /// <returns>Vendor model</returns>
        VendorModel PrepareVendorModel(Vendor vendor, CatalogPagingFilteringModel command);

        /// <summary>
        /// Prepare vendor all models
        /// </summary>
        /// <returns>List of vendor models</returns>
        List<VendorModel> PrepareVendorAllModels();

        /// <summary>
        /// Prepare vendor navigation model
        /// </summary>
        /// <returns>Vendor navigation model</returns>
        VendorNavigationModel PrepareVendorNavigationModel();

        #endregion

        #region Product tags

        /// <summary>
        /// Prepare popular product tags model
        /// </summary>
        /// <returns>Product tags model</returns>
        PopularProductTagsModel PreparePopularProductTagsModel();

        /// <summary>
        /// Prepare products by tag model
        /// </summary>
        /// <param name="productTag">Product tag</param>
        /// <param name="command">Catalog paging filtering command</param>
        /// <returns>Products by tag model</returns>
        ProductsByTagModel PrepareProductsByTagModel(ProductTag productTag,
            CatalogPagingFilteringModel command);

        /// <summary>
        /// Prepare product tags all model
        /// </summary>
        /// <returns>Popular product tags model</returns>
        PopularProductTagsModel PrepareProductTagsAllModel();

        #endregion

        #region Searching

        /// <summary>
        /// Prepare search model
        /// </summary>
        /// <param name="model">Search model</param>
        /// <param name="command">Catalog paging filtering command</param>
        /// <returns>Search model</returns>
        SearchModel PrepareSearchModel(SearchModel model, CatalogPagingFilteringModel command);

        /// <summary>
        /// Prepare search box model
        /// </summary>
        /// <returns>Search box model</returns>
        SearchBoxModel PrepareSearchBoxModel();

        #endregion

        #region MegaMenu

        /// <summary>
        /// Prepare mega menu overview with position model
        /// </summary>
        /// <param name="megaMenuTabId">Mega menu tab identifier</param>
        /// <param name="url">Url helper</param>
        /// <returns>Mega menu overview model</returns>
        MegaMenuOverviewModel PrepareMegaMenuOverviewModel(int megaMenuTabId, IUrlHelper url);

        /// <summary>
        /// Prepare cache mega menu item overview models
        /// </summary>
        /// <param name="megaMenuTabId">Mega menu tab identifier</param>
        /// <param name="url">Url helper</param>
        /// <returns>List of mega menu item overview models</returns>
        List<MegaMenuItemOverviewModel> PrepareCacheMegaMenuItemOverviewModels(int megaMenuTabId, IUrlHelper url);

        /// <summary>
        /// Prepare mega menu item overview models
        /// </summary>
        /// <param name="megaMenuTabId">Mega menu tab identifier</param>
        /// <param name="url">Url helper</param>
        /// <returns>List of mega menu item overview models</returns>
        List<MegaMenuItemOverviewModel> PrepareMegaMenuItemOverviewModels(int megaMenuTabId, IUrlHelper url);

        #endregion MegaMenu

        #region Menu

        /// <summary>
        /// Prepare menu overview with position model
        /// </summary>
        /// <param name="menuId">Menu tab identifier</param>
        /// <param name="url">Url helper</param>
        /// <returns>Menu overview model</returns>
        MenuOverviewModel PrepareMenuOverviewModel(int menuId, IUrlHelper url);

        /// <summary>
        /// Prepare cache menu item overview models
        /// </summary>
        /// <param name="menuId">Menu tab identifier</param>
        /// <param name="url">Url helper</param>
        /// <returns>List of menu item overview models</returns>
        List<MenuItemOverviewModel> PrepareCacheMenuItemOverviewModels(int menuId, IUrlHelper url);

        /// <summary>
        /// Prepare menu item overview models
        /// </summary>
        /// <param name="menuId">Menu tab identifier</param>
        /// <param name="url">Url helper</param>
        /// <returns>List of menu item overview models</returns>
        List<MenuItemOverviewModel> PrepareMenuItemOverviewModels(int menuId, IUrlHelper url);

        /// <summary>
        /// Prepare main menu model
        /// </summary>
        /// <param name="url">Url helper</param>
        /// <returns>Main menu model</returns>
        MenuOverviewModel PrepareMainManu(IUrlHelper url);

        #endregion Menu

        #region Sidebar 

        /// <summary>
        /// Prepare sidebar overview with position model
        /// </summary>
        /// <param name="sidebarId">Sidebar tab identifier</param>
        /// <param name="url">Url helper</param>
        /// <returns>Sidebar overview model</returns>
        SidebarOverviewModel PrepareSidebarOverviewModel(int sidebarId, IUrlHelper url);

        /// <summary>
        /// Prepare cache sidebar item overview models
        /// </summary>
        /// <param name="sidebarId">Sidebar tab identifier</param>
        /// <param name="url">Url helper</param>
        /// <returns>List of sidebar item overview models</returns>
        List<SidebarItemOverviewModel> PrepareCacheSidebarItemOverviewModels(int sidebarId, IUrlHelper url);

        /// <summary>
        /// Prepare sidebar item overview models
        /// </summary>
        /// <param name="sidebarId">Sidebar identifier</param>
        /// <param name="url">Url helper</param>
        /// <returns>List of sidebar item overview models</returns>
        List<SidebarItemOverviewModel> PrepareSidebarItemOverviewModels(int sidebarId, IUrlHelper url);

        /// <summary>
        /// Prepare sidebar overview with position model
        /// </summary>
        /// <param name="currentCategoryId">Current category identifier</param>
        /// <param name="currentProductId">Current product identifier</param>
        /// <param name="url">Url helper</param>
        /// <returns>Sidebar overview model</returns>
        SidebarOverviewModel PrepareSidebarOverviewModel(IUrlHelper url, int? currentCategoryId,
            int? currentProductId);
        #endregion Sidebar

        #region Slider 

        /// <summary>
        /// Prepare slider overview with position model
        /// </summary>
        /// <param name="sliderId">Slider tab identifier</param>
        /// <param name="url">Url helper</param>
        /// <returns>Slider overview model</returns>
        SliderOverviewModel PrepareSliderOverviewModel(int sliderId, IUrlHelper url);

        /// <summary>
        /// Prepare cache slider item overview models
        /// </summary>
        /// <param name="slider">Slider</param>
        /// <param name="url">Url helper</param>
        /// <param name="sliderNumber">Unique slider number</param>
        /// <returns>List of slider item overview models</returns>
        List<SliderItemOverviewModel> PrepareCacheSliderItemOverviewModels(Slider slider, IUrlHelper url, Guid sliderNumber);

        /// <summary>
        /// Prepare slider item overview models
        /// </summary>
        /// <param name="slider">Slider</param>
        /// <param name="url">Url helper</param>
        /// <param name="sliderNumber">Unique slider number</param>
        /// <returns>List of slider item overview models</returns>
        List<SliderItemOverviewModel> PrepareSliderItemOverviewModels(Slider slider, IUrlHelper url, Guid sliderNumber);

        #endregion Slider

        #region HomePage 

        /// <summary>
        /// Prepare home page overview with position model
        /// </summary>
        /// <param name="homePageId">Home page tab identifier</param>
        /// <param name="url">Url helper</param>
        /// <returns>Home page overview model</returns>
        HomePageOverviewModel PrepareHomePageOverviewModel(int homePageId, IUrlHelper url);

        /// <summary>
        /// Prepare cache home page item overview models
        /// </summary>
        /// <param name="homePageId">Home page identifier</param>
        /// <param name="url">Url helper</param>
        /// <returns>List of home page item overview models</returns>
        List<HomePageItemOverviewModel> PrepareCacheHomePageItemOverviewModels(int homePageId, IUrlHelper url);

        /// <summary>
        /// Prepare home page item overview models
        /// </summary>
        /// <param name="homePageId">Home page identifier</param>
        /// <param name="url">Url helper</param>
        /// <returns>List of home page item overview models</returns>
        List<HomePageItemOverviewModel> PrepareHomePageItemOverviewModels(int homePageId, IUrlHelper url);


        /// <summary>
        /// Prepare default home page overview with position model
        /// </summary>
        /// <param name="url">Url helper</param>
        /// <returns>Default home page overview model</returns>
        HomePageOverviewModel PrepareDefaultHomepage(IUrlHelper url);
        #endregion HomePage
    }
}
