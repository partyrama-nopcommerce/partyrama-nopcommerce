﻿using Nop.Core.Domain.Catalog;
using Nop.Web.Models.Catalog;
using System.Collections.Generic;

namespace Nop.Web.Factories
{
    /// <summary>
    /// Represents the connection model factory
    /// </summary>
    public partial interface IConnectionModelFactory
    {
        /// <summary>
        /// Prepare the connection models
        /// </summary>
        /// <param name="connections">Collection of connections</param>
        /// <returns>Collection of connection model</returns>
        IEnumerable<ConnectionModel> PrepareConnectionModels(IEnumerable<Connections> connections);
    }
}