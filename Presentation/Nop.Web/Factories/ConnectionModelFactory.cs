﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Media;
using Nop.Services.Catalog;
using Nop.Services.Localization;
using Nop.Services.Media;
using Nop.Services.Seo;
using Nop.Web.Infrastructure.Cache;
using Nop.Web.Models.Catalog;
using Nop.Web.Models.Media;

namespace Nop.Web.Factories
{
    /// <summary>
    /// Represents the connection model factory implementation
    /// </summary>
    public partial class ConnectionModelFactory : IConnectionModelFactory
    {
        #region Fields

        private readonly IProductService _productService;
        private readonly ICategoryService _categoryService;
        private readonly ILocalizationService _localizationService;
        private readonly IUrlRecordService _urlRecordService;
        private readonly IStaticCacheManager _cacheManager;
        private readonly IPictureService _pictureService;
        private readonly MediaSettings _mediaSettings;
        private readonly IWebHelper _webHelper;
        private readonly IWorkContext _workContext;
        private readonly IStoreContext _storeContext;
        private readonly IProductModelFactory _productModelFactory;
        private readonly IElementService _elementService;
        private readonly ISliderService _sliderService;

        #endregion

        #region Ctor

        public ConnectionModelFactory(
            IProductService productService,
            ICategoryService categoryService,
            ILocalizationService localizationService,
            IUrlRecordService urlRecordService,
            IStaticCacheManager cacheManager,
            IPictureService pictureService,
            MediaSettings mediaSettings,
            IWebHelper webHelper,
            IWorkContext workContext,
            IStoreContext storeContext,
            IProductModelFactory productModelFactory,
            IElementService elementService,
            ISliderService sliderService)
        {
            _productService = productService;
            _categoryService = categoryService;
            _localizationService = localizationService;
            _urlRecordService = urlRecordService;
            _cacheManager = cacheManager;
            _pictureService = pictureService;
            _mediaSettings = mediaSettings;
            _webHelper = webHelper;
            _workContext = workContext;
            _storeContext = storeContext;
            _productModelFactory = productModelFactory;
            _elementService = elementService;
            _sliderService = sliderService;
        }

        #endregion

        #region Utilities

        #endregion

        #region Methods

        /// <summary>
        /// Prepare the connection models
        /// </summary>
        /// <param name="connections">Collection of connections</param>
        /// <returns>Collection of connection model</returns>
        public IEnumerable<ConnectionModel> PrepareConnectionModels(IEnumerable<Connections> connections)
        {
            if (connections == null)
                throw new ArgumentNullException(nameof(connections));

            var productIds = connections.Where(cc => cc.ConnectionToType == ConnectionToType.prod).Select(cc => cc.To).ToArray();
            var productsFromDb = _productService.GetProductsByIds(productIds);
            var products = _productModelFactory.PrepareProductOverviewModels(productsFromDb, preparePictureModel: false, showCartAmount: true);
            PrepareProductsPictures(products, connections.Where(cc => cc.ConnectionToType == ConnectionToType.prod));

            var pictureSize = _mediaSettings.CategoryThumbPictureSize;
            var categoryIds = connections.Where(cc => cc.ConnectionToType == ConnectionToType.cat).Select(cc => cc.To).ToArray();
            var categories = _categoryService.GetCategoriesByIds(categoryIds).Select(x =>
            {
                var connCatModel = new ConnectionModel.ConnectionCategoryModel
                {
                    Id = x.Id,
                    Name = _localizationService.GetLocalized(x, y => y.Name),
                    SeName = _urlRecordService.GetSeName(x),
                    ShortDescription = _localizationService.GetLocalized(x, y => y.ShortDescription)
                };

                //prepare picture model
                connCatModel.PictureModel = GetCategoryItemPicture(ConnectionToType.cat, connCatModel, connections.First(cc => cc.ConnectionToType == ConnectionToType.cat && cc.To == x.Id).Picture);

                return connCatModel;
            }).ToList();

            var elementsIds = connections.Where(cc => 
                cc.ConnectionToType == ConnectionToType.sep 
                || cc.ConnectionToType == ConnectionToType.desc).Select(cc => cc.To).ToArray();

            var elements = _elementService.GetElementsByIds(elementsIds).Select(x =>
            {
                var connElementModel = new ConnectionModel.ConnectionElementModel
                {
                    Id = x.Id,
                    Name = _localizationService.GetLocalized(x, y => y.Name),
                    Description = _localizationService.GetLocalized(x, y => y.Description)
                };

                return connElementModel;
            }).ToList();

            var sliderIds = connections.Where(cc => cc.ConnectionToType == ConnectionToType.slider).Select(cc => cc.To).ToArray();

            var sliders = _sliderService.GetSlidersByIds(sliderIds).Select(x =>
            {
                var connSliderModel = new ConnectionModel.ConnectionSliderModel
                {
                    Id = x.Id,
                    Name = x.Name
                };

                return connSliderModel;
            }).ToList();

            var models = new List<ConnectionModel>();
            foreach (var conn in connections)
            {
                var connModel = new ConnectionModel
                {
                    CssClass = conn.CssClass,
                    ToType = conn.ConnectionToType,
                    ShowDescription = conn.ShowDescription
                };

                switch (conn.ConnectionToType)
                {
                    case ConnectionToType.cat:
                        connModel.Category = categories.First(c => c.Id == conn.To);
                        break;
                    case ConnectionToType.prod:
                        connModel.Product = products.First(p => p.Id == conn.To);
                        break;
                    case ConnectionToType.sep:
                    case ConnectionToType.desc:
                        connModel.Element = elements.First(el => el.Id == conn.To);
                        break;
                    case ConnectionToType.slider:
                        connModel.Slider = sliders.First(sl => sl.Id == conn.To);
                        break;
                    default:
                        break;
                }

                models.Add(connModel);
            }

            return models;
        }

        private void PrepareProductsPictures(IEnumerable<ProductOverviewModel> products, IEnumerable<Connections> proConnections, int? pictureSize = null)
        {
            if (pictureSize == null)
                pictureSize = _mediaSettings.ProductThumbPictureSize;

            foreach (var product in products)
            {
                var picture = proConnections.First(cc => cc.To == product.Id).Picture;
                if (picture != null)
                {
                    //TO DO Cache
                    product.DefaultPictureModel = new PictureModel
                    {
                        FullSizeImageUrl = _pictureService.GetPictureUrl(picture),
                        ImageUrl = _pictureService.GetPictureUrl(picture, pictureSize.Value),
                        Title = !string.IsNullOrEmpty(picture.TitleAttribute) ? picture.TitleAttribute : string.Format(_localizationService.GetResource("Media.Product.ImageLinkTitleFormat"), product.Name),
                        AlternateText = !string.IsNullOrEmpty(picture.AltAttribute) ? picture.AltAttribute : string.Format(_localizationService.GetResource("Media.Product.ImageAlternateTextFormat"), product.Name)
                    };
                }
                else
                {
                    var cacheKey = string.Format(ModelCacheEventConsumer.PRODUCT_DEFAULTPICTURE_MODEL_KEY,
                        product.Id, pictureSize, true, _workContext.WorkingLanguage.Id, _webHelper.IsCurrentConnectionSecured(),
                        _storeContext.CurrentStore.Id);

                    product.DefaultPictureModel = _cacheManager.Get(cacheKey, () =>
                    {
                        var pictureProduct = _pictureService.GetPicturesByProductId(product.Id, 1).FirstOrDefault();
                        var pictureModel = new PictureModel
                        {
                            ImageUrl = _pictureService.GetPictureUrl(pictureProduct, pictureSize.Value),
                            FullSizeImageUrl = _pictureService.GetPictureUrl(pictureProduct),
                            //"title" attribute
                            Title = (pictureProduct != null && !string.IsNullOrEmpty(pictureProduct.TitleAttribute))
                                ? pictureProduct.TitleAttribute
                                : string.Format(_localizationService.GetResource("Media.Product.ImageLinkTitleFormat"),
                                    product.Name),
                            //"alt" attribute
                            AlternateText = (pictureProduct != null && !string.IsNullOrEmpty(pictureProduct.AltAttribute))
                                ? pictureProduct.AltAttribute
                                : string.Format(_localizationService.GetResource("Media.Product.ImageAlternateTextFormat"),
                                    product.Name)
                        };

                        return pictureModel;
                    });
                }
            }
        }

        private PictureModel GetCategoryItemPicture(ConnectionToType cat, ConnectionModel.ConnectionCategoryModel model, Picture picture, int pictureSize = 0)
        {
            PictureModel pictureModel = null;

            if (picture != null)
            {
                //TO DO cache
                pictureModel = new PictureModel
                {
                    FullSizeImageUrl = _pictureService.GetPictureUrl(picture),
                    ImageUrl = _pictureService.GetPictureUrl(picture, pictureSize),
                    Title = !string.IsNullOrEmpty(picture.TitleAttribute) ? picture.TitleAttribute : string.Format(_localizationService.GetResource("Media.Category.ImageLinkTitleFormat"), model.Name),
                    AlternateText = !string.IsNullOrEmpty(picture.AltAttribute) ? picture.AltAttribute : string.Format(_localizationService.GetResource("Media.Category.ImageAlternateTextFormat"), model.Name)
                };

                return pictureModel;
            }
            else
            {
                var categoryPictureCacheKey = string.Format(ModelCacheEventConsumer.CATEGORY_PICTURE_MODEL_KEY, model.Id, pictureSize, true, _workContext.WorkingLanguage.Id, _webHelper.IsCurrentConnectionSecured(), _storeContext.CurrentStore.Id);
                pictureModel = _cacheManager.Get(categoryPictureCacheKey, () =>
                {
                    var pictureCategory = _pictureService.GetPicturesByCategoryId(model.Id, 1).FirstOrDefault();
                    var pictureCategoryModel = new PictureModel
                    {
                        FullSizeImageUrl = _pictureService.GetPictureUrl(pictureCategory),
                        ImageUrl = _pictureService.GetPictureUrl(pictureCategory, pictureSize),
                        Title = (pictureCategory != null && !string.IsNullOrEmpty(pictureCategory.TitleAttribute)) ? pictureCategory.TitleAttribute : string.Format(_localizationService.GetResource("Media.Category.ImageLinkTitleFormat"), model.Name),
                        AlternateText = (pictureCategory != null && !string.IsNullOrEmpty(pictureCategory.AltAttribute)) ? pictureCategory.AltAttribute : string.Format(_localizationService.GetResource("Media.Category.ImageAlternateTextFormat"), model.Name)
                    };

                    return pictureCategoryModel;
                });
            }

            return pictureModel;
        }
        #endregion
    }
}