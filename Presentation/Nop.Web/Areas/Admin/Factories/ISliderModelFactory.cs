﻿using Nop.Core.Domain.Catalog;
using Nop.Web.Areas.Admin.Models.Catalog;
using System.Collections.Generic;

namespace Nop.Web.Areas.Admin.Factories
{
    /// <summary>
    /// Represents the slider model factory
    /// </summary>
    public partial interface ISliderModelFactory
    {
        /// <summary>
        /// Prepare slider edit model
        /// </summary>
        /// <returns>Slider model</returns>
        SliderEditModel PrepareSliderEditModel();

        /// <summary>
        /// Prepare slider model
        /// </summary>
        /// <param name="model">Slider model</param>
        /// <param name="slider">Slider</param>
        /// <returns>Slider model</returns>
        SliderModel PrepareSliderModel(SliderModel model, Slider slider);

        /// <summary>
        /// Prepare paged slider item list model
        /// </summary>
        /// <param name="searchModel">Slider item search model</param>
        /// <returns>SliderItem list model</returns>
        SliderItemListModel PrepareSliderItemListModel(SliderItemSearchModel searchModel);

        /// <summary>
        /// Prepare paged slider item row list model
        /// </summary>
        /// <param name="searchModel">Slider item row search model</param>
        /// <returns>SliderItem row list model</returns>
        SliderItemListModel PrepareSliderItemRowListModel(SliderItemRowSearchModel searchModel);

        /// <summary>
        /// Prepare slider item model
        /// </summary>
        /// <param name="model">Slider item model</param>
        /// <returns>Slider item model</returns>
        SliderItemModel PrepareSliderItemModel(SliderItemModel model);

        /// <summary>
        /// Prepare category search model to add slider category row.
        /// </summary>
        /// <param name="searchModel">Category search model to add slider category row</param>
        /// <returns>Category search model to add slider category row</returns>
        SliderAddRowCategorySearchModel PrepareAddRowCategorySearchModel(SliderAddRowCategorySearchModel searchModel);

        /// <summary>
        /// Prepare paged category list model to add slider category row.
        /// </summary>
        /// <param name="searchModel">Category search model to add slider category row.</param>
        /// <returns>Category list model to add to add slider category row.</returns>
        SliderAddRowCategoryListModel PrepareSliderAddRowCategoryListModel(SliderAddRowCategorySearchModel searchModel);

        /// <summary>
        /// Prepare product search model to add slider product row.
        /// </summary>
        /// <param name="searchModel">Product search model to add slider product row</param>
        /// <returns>Product search model to add slider product row</returns>
        SliderAddRowProductSearchModel PrepareAddRowProductSearchModel(SliderAddRowProductSearchModel searchModel);

        /// <summary>
        /// Prepare paged product list model to add slider product row.
        /// </summary>
        /// <param name="searchModel">Product search model to add slider product row.</param>
        /// <returns>Product list model to add to add slider product row.</returns>
        SliderAddRowProductListModel PrepareSliderAddRowProductListModel(SliderAddRowProductSearchModel searchModel);

        /// <summary>
        /// Prepare element search model to add slider element row.
        /// </summary>
        /// <param name="searchModel">Element search model to add slider element row</param>
        /// <returns>Element search model to add slider element row</returns>
        SliderAddRowElementSearchModel PrepareAddRowElementSearchModel(SliderAddRowElementSearchModel searchModel);

        /// <summary>
        /// Prepare paged product list model to add slider element row.
        /// </summary>
        /// <param name="searchModel">Element search model to add slider element row.</param>
        /// <returns>Element list model to add to add slider element row.</returns>
        SliderAddRowElementListModel PrepareSliderAddRowElementListModel(SliderAddRowElementSearchModel searchModel);

        /// <summary>
        /// Prepare topic search model to add slider topic row.
        /// </summary>
        /// <param name="searchModel">Topic search model to add slider topic row</param>
        /// <returns>Topic search model to add slider topic row</returns>
        SliderAddRowTopicSearchModel PrepareAddRowTopicSearchModel(SliderAddRowTopicSearchModel searchModel);

        /// <summary>
        /// Prepare paged topic list model to add slider topic row.
        /// </summary>
        /// <param name="searchModel">Element search model to add slider topic row.</param>
        /// <returns>Topic list model to add to add slider topic row.</returns>
        SliderAddRowTopicListModel PrepareSliderAddRowTopicListModel(SliderAddRowTopicSearchModel searchModel);

        /// <summary>
        /// Get picture list.
        /// </summary>
        /// <param name="connectionId">Connection identifier.</param>
        /// <param name="connectionType">Connection type</param>
        /// <returns>Get short picture list.</returns>
        IList<PictureShortModel> GetAvailablePicture(int connectionId, RowConnectionType connectionType);
    }
}