﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Media;
using Nop.Services.Catalog;
using Nop.Services.Localization;
using Nop.Services.Media;
using Nop.Services.Topics;
using Nop.Web.Areas.Admin.Infrastructure.Mapper.Extensions;
using Nop.Web.Areas.Admin.Models.Catalog;
using Nop.Web.Areas.Admin.Models.Topics;

namespace Nop.Web.Areas.Admin.Factories
{
    /// <summary>
    /// Represents the mega menu model factory implementation
    /// </summary>
    public partial class MegaMenuModelFactory : IMegaMenuModelFactory
    {
        #region Fields

        private readonly IMegaMenuTabService _megaMenuTabService;
        private readonly IMegaMenuTabItemService _megaMenuTabItemService;
        private readonly ILocalizationService _localizationService;
        private readonly IBaseAdminModelFactory _baseAdminModelFactory;
        private readonly ICategoryService _categoryService;
        private readonly IProductService _productService;
        private readonly IElementService _elementService;
        private readonly ITopicService _topicService;
        private readonly ISliderService _sliderService;
        private readonly IPictureService _pictureService;

        #endregion

        #region Ctor

        public MegaMenuModelFactory(IMegaMenuTabService megaMenuTabService,
            IMegaMenuTabItemService megaMenuTabItemService,
            ILocalizationService localizationService,
            IBaseAdminModelFactory baseAdminModelFactory,
            ICategoryService categoryService,
            IProductService productService,
            IElementService elementService,
            ITopicService topicService,
            ISliderService sliderService,
            IPictureService pictureService)
        {
            _megaMenuTabService = megaMenuTabService;
            _megaMenuTabItemService = megaMenuTabItemService;
            _localizationService = localizationService;
            _baseAdminModelFactory = baseAdminModelFactory;
            _categoryService = categoryService;
            _productService = productService;
            _elementService = elementService;
            _topicService = topicService;
            _sliderService = sliderService;
            _pictureService = pictureService;
        }

        #endregion

        #region Utilities

        private IList<MegaMenuShortTabModel> PrepareMegaMenuTabs()
        {
            return _megaMenuTabService.GetAll().OrderByDescending(tab => tab.CreatedOnUtc).Select(tab =>
                new MegaMenuShortTabModel
                {
                    Id = tab.Id,
                    Name = tab.Name
                }).ToList();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Prepare megam menu model
        /// </summary>
        /// <param name="model">Mega menu model</param>
        /// <returns>Mega menu model</returns>
        public MegaMenuModel PrepareMegaMenuModel()
        {
            var model = new MegaMenuModel();

            model.Tabs = PrepareMegaMenuTabs();

            return model;
        }

        /// <summary>
        /// Prepare mega menu tab model
        /// </summary>
        /// <param name="model">Mega menu tab model</param>
        /// <param name="megaMenuTab">Mega menu tab</param>
        /// <returns>Mega menu tab model</returns>
        public MegaMenuTabModel PrepareMegaMenuTabModel(MegaMenuTabModel model, MegaMenuTab megaMenuTab)
        {
            if (megaMenuTab != null)
            {
                //fill in model values from the entity
                model = model ?? megaMenuTab.ToModel<MegaMenuTabModel>();
            }

            return model;
        }

        /// <summary>
        /// Prepare paged mega menu tab item list model
        /// </summary>
        /// <param name="searchModel">Mega menu tab item search model</param>
        /// <returns>MegaMenuTabItem list model</returns>
        public MegaMenuTabItemListModel PrepareMegaMenuTabItemListModel(MegaMenuTabItemSearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            //get items
            var items = _megaMenuTabItemService.GetColumnsHeadersByMegaMenuTabId(searchModel.MegaMenuTabId);

            //prepare grid model
            var model = new MegaMenuTabItemListModel
            {
                Data = items.Select(megaMenuTabItem =>
                {
                    //fill in model values from the entity
                    var megaMenuTabItemModel = megaMenuTabItem.ToModel<MegaMenuTabItemModel>();

                    megaMenuTabItemModel.ItemName = GetMenuTabItemName(megaMenuTabItem);
                    megaMenuTabItemModel.ItemColor = string.IsNullOrWhiteSpace(megaMenuTabItem.Color) ? null : megaMenuTabItem.Color;
                    //fill in additional values (not existing in the entity)
                    return megaMenuTabItemModel;
                }),
                Total = items.TotalCount
            };

            return model;
        }

        /// <summary>
        /// Prepare paged mega menu tab item row list model
        /// </summary>
        /// <param name="searchModel">Mega menu tab item row search model</param>
        /// <returns>MegaMenuTabItem row list model</returns>
        public MegaMenuTabItemListModel PrepareMegaMenuTabItemRowListModel(MegaMenuTabItemRowSearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            var column = _megaMenuTabItemService.GetMegaMenuTabItemById(searchModel.MegaMenuTabItemId);
            if (column == null)
                throw new ArgumentNullException(nameof(column));

            //get items
            var items = _megaMenuTabItemService.GetRowsByMegaMenuTabItem(column);

            //prepare grid model
            var model = new MegaMenuTabItemListModel
            {
                Data = items.Select(megaMenuTabItem =>
                {
                    //fill in model values from the entity
                    var megaMenuTabItemModel = megaMenuTabItem.ToModel<MegaMenuTabItemModel>();

                    megaMenuTabItemModel.ItemName = GetMenuTabItemName(megaMenuTabItem);
                    megaMenuTabItemModel.ItemColor = string.IsNullOrWhiteSpace(megaMenuTabItem.Color) ? null : megaMenuTabItem.Color;
                    megaMenuTabItemModel.ItemParentRowType = column.RowType;
                    if (column.RowType == RowType.imagecolumn)
                    {
                        PrepareMegaMenuPicture(megaMenuTabItemModel, megaMenuTabItem);
                    }
                    
                    //fill in additional values (not existing in the entity)
                    return megaMenuTabItemModel;
                }),
                Total = items.TotalCount
            };

            return model;
        }

        private void PrepareMegaMenuPicture(MegaMenuTabItemModel megaMenuTabItemModel, MegaMenuTabItem megaMenuTabItem)
        {
            var picture = megaMenuTabItem.Picture;

            if (picture == null)
            {
                switch (megaMenuTabItem.RowConnectionType)
                {
                    case RowConnectionType.category:
                        picture = _pictureService.GetPluginDefaultByCategoryId(megaMenuTabItem.RowConnectionId.Value, PictureKind.MegaMenu);
                        break;
                    case RowConnectionType.product:
                        picture = _pictureService.GetPluginDefaultByProductId(megaMenuTabItem.RowConnectionId.Value, PictureKind.MegaMenu);
                        break;
                }
            }

            if (picture != null)
            {
                megaMenuTabItemModel.ItemPicture.ItemPictureId = (megaMenuTabItem.Picture == null) ? -1 : picture.Id;
                megaMenuTabItemModel.ItemPicture.ItemPictureOriginalName = (megaMenuTabItem.Picture == null) ? "default" : picture.OriginalName ?? string.Empty;
                megaMenuTabItemModel.ItemPicture.ItemPictureUrl = _pictureService.GetPictureUrl(picture, 75);
            }
        }

        private string GetMenuTabItemName(MegaMenuTabItem megaMenuTabItem)
        {
            switch (megaMenuTabItem.RowConnectionType)
            {
                case RowConnectionType.category:
                    return _categoryService.GetCategoryById(megaMenuTabItem.RowConnectionId.GetValueOrDefault())?.ShortName;
                case RowConnectionType.product:
                    return _productService.GetProductById(megaMenuTabItem.RowConnectionId.GetValueOrDefault())?.Name;
                case RowConnectionType.element:
                    return _elementService.GetElementById(megaMenuTabItem.RowConnectionId.GetValueOrDefault())?.Name;
                case RowConnectionType.topic:
                    return _topicService.GetTopicById(megaMenuTabItem.RowConnectionId.GetValueOrDefault())?.SystemName;
                case RowConnectionType.slider:
                    return _sliderService.GetSliderById(megaMenuTabItem.RowConnectionId.GetValueOrDefault())?.Name;
                default:
                    return string.Empty;
            }
        }

        /// <summary>
        /// Prepare mega menu tab item model
        /// </summary>
        /// <param name="model">Mega menu tab item model</param>
        /// <returns>Mega menu tab item model</returns>
        public MegaMenuTabItemModel PrepareMegaMenuTabItemModel(MegaMenuTabItemModel model)
        {
            model.ItemRowConnectionType = RowConnectionType.category;
            switch (model.ItemRowType)
            {
                case RowType.header:
                    model.ItemCssClass = "mm_header";
                    break;
                case RowType.textcolumn:
                    model.ItemCssClass = "mm_column_title";
                    break;
                case RowType.imagecolumn:
                    model.ItemCssClass = "mm_column_image";
                    break;
            }

            return model;
        }

        /// <summary>
        /// Prepare category search model to add mega menu category row.
        /// </summary>
        /// <param name="searchModel">Category search model to add mega menu category row</param>
        /// <returns>Category search model to add mega menu category row</returns>
        public MegaMenuAddRowCategorySearchModel PrepareAddRowCategorySearchModel(MegaMenuAddRowCategorySearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            //prepare page parameters
            searchModel.SetPopupGridPageSize();
            _baseAdminModelFactory.PrepareMegaMenuColumns(searchModel.AvailableColumns, searchModel.MegaMenuTabId, true, _localizationService.GetResource("Admin.Common.RowAsTitle"));

            return searchModel;
        }

        /// <summary>
        /// Prepare paged category list model to add mega menu category row.
        /// </summary>
        /// <param name="searchModel">Category search model to add mega menu category row.</param>
        /// <returns>Category list model to add to add mega menu category row.</returns>
        public MegaMenuAddRowCategoryListModel PrepareMegaMenuAddRowCategoryListModel(MegaMenuAddRowCategorySearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            //get categories
            var categories = _categoryService.GetAllCategories(showHidden: true,
                categoryName: searchModel.SearchCategoryName,
                pageIndex: searchModel.Page - 1, pageSize: searchModel.PageSize);

            //prepare grid model
            var model = new MegaMenuAddRowCategoryListModel
            {
                Data = categories.Select(category =>
                {
                    //fill in model values from the entity
                    var categoryModel = category.ToModel<CategoryModel>();

                    //fill in additional values (not existing in the entity)
                    categoryModel.Breadcrumb = _categoryService.GetFormattedBreadCrumb(category);

                    return categoryModel;
                }),
                Total = categories.TotalCount
            };

            return model;
        }

        /// <summary>
        /// Prepare product search model to add mega menu product row.
        /// </summary>
        /// <param name="searchModel">Product search model to add mega menu product row</param>
        /// <returns>Product search model to add mega menu product row</returns>
        public MegaMenuAddRowProductSearchModel PrepareAddRowProductSearchModel(MegaMenuAddRowProductSearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            //prepare page parameters
            searchModel.SetPopupGridPageSize();
            _baseAdminModelFactory.PrepareMegaMenuColumns(searchModel.AvailableColumns, searchModel.MegaMenuTabId, true, _localizationService.GetResource("Admin.Common.RowAsTitle"));

            //prepare available product types
            _baseAdminModelFactory.PrepareProductTypes(searchModel.AvailableProductTypes);

            return searchModel;
        }

        /// <summary>
        /// Prepare paged product list model to add mega menu product row.
        /// </summary>
        /// <param name="searchModel">Product search model to add mega menu product row.</param>
        /// <returns>Product list model to add to add mega menu product row.</returns>
        public MegaMenuAddRowProductListModel PrepareMegaMenuAddRowProductListModel(MegaMenuAddRowProductSearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            //get products
            var products = _productService.SearchProducts(showHidden: true,
                productType: searchModel.SearchProductTypeId > 0 ? (ProductType?)searchModel.SearchProductTypeId : null,
                keywords: searchModel.SearchProductName,
                pageIndex: searchModel.Page - 1, pageSize: searchModel.PageSize);

            //prepare grid model
            var model = new MegaMenuAddRowProductListModel
            {
                //fill in model values from the entity
                Data = products.Select(product => product.ToModel<ProductModel>()),
                Total = products.TotalCount
            };

            return model;
        }

        /// <summary>
        /// Prepare element search model to add mega menu element row.
        /// </summary>
        /// <param name="searchModel">Element search model to add mega menu element row</param>
        /// <returns>Element search model to add mega menu element row</returns>
        public MegaMenuAddRowElementSearchModel PrepareAddRowElementSearchModel(MegaMenuAddRowElementSearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            //prepare page parameters
            searchModel.SetPopupGridPageSize();
            _baseAdminModelFactory.PrepareMegaMenuColumns(searchModel.AvailableColumns, searchModel.MegaMenuTabId, true, _localizationService.GetResource("Admin.Common.RowAsTitle"));

            //prepare available product types
            _baseAdminModelFactory.PrepareElementTypes(searchModel.AvailableElementTypes);

            return searchModel;
        }

        /// <summary>
        /// Prepare paged element list model to add mega menu element row.
        /// </summary>
        /// <param name="searchModel">Element search model to add mega menu element row.</param>
        /// <returns>Element list model to add to add mega menu element row.</returns>
        public MegaMenuAddRowElementListModel PrepareMegaMenuAddRowElementListModel(MegaMenuAddRowElementSearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            //get elements
            var elements = _elementService.GetAllElements(
                elementType: searchModel.SearchElementTypeId > 0 ? (ElementType?)searchModel.SearchElementTypeId : null,
                elementName: searchModel.SearchElementName,
                pageIndex: searchModel.Page - 1,
                pageSize: searchModel.PageSize);

            //prepare grid model
            var model = new MegaMenuAddRowElementListModel
            {
                Data = elements.Select(element =>
                {
                    //fill in model values from the entity
                    var elementModel = element.ToModel<ElementModel>();
                    return elementModel;
                }),
                Total = elements.TotalCount
            };

            return model;
        }

        /// <summary>
        /// Prepare topic search model to add mega menu topic row.
        /// </summary>
        /// <param name="searchModel">Topic search model to add mega menu topic row</param>
        /// <returns>Topic search model to add mega menu topic row</returns>
        public MegaMenuAddRowTopicSearchModel PrepareAddRowTopicSearchModel(MegaMenuAddRowTopicSearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            //prepare page parameters
            searchModel.SetPopupGridPageSize();
            _baseAdminModelFactory.PrepareMegaMenuColumns(searchModel.AvailableColumns, searchModel.MegaMenuTabId, true, _localizationService.GetResource("Admin.Common.RowAsTitle"));

            return searchModel;
        }

        /// <summary>
        /// Prepare paged topic list model to add mega menu topic row.
        /// </summary>
        /// <param name="searchModel">Element search model to add mega menu topic row.</param>
        /// <returns>Topic list model to add to add mega menu topic row.</returns>
        public MegaMenuAddRowTopicListModel PrepareMegaMenuAddRowTopicListModel(MegaMenuAddRowTopicSearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            //get topics
            var topics = _topicService.GetAllTopics(searchModel.SearchKeywords, searchModel.Page - 1, searchModel.PageSize);

            //prepare grid model
            var model = new MegaMenuAddRowTopicListModel
            {
                Data = topics.Select(topic =>
                {
                    //fill in model values from the entity
                    var topicModel = topic.ToModel<TopicModel>();
                    return topicModel;
                }),
                Total = topics.TotalCount
            };

            return model;
        }

        /// <summary>
        /// Prepare slider search model to add mega menu category row.
        /// </summary>
        /// <param name="searchModel">Slider search model to add mega menu slider row</param>
        /// <returns>Slider search model to add mega menu slider row</returns>
        public MegaMenuAddRowSliderSearchModel PrepareAddRowSliderSearchModel(MegaMenuAddRowSliderSearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            //prepare page parameters
            searchModel.SetPopupGridPageSize();
            _baseAdminModelFactory.PrepareMegaMenuColumns(searchModel.AvailableColumns, searchModel.MegaMenuTabId, true, _localizationService.GetResource("Admin.Common.RowAsTitle"));

            return searchModel;
        }

        /// <summary>
        /// Prepare paged slider list model to add mega menu slider row.
        /// </summary>
        /// <param name="searchModel">Slider search model to add mega menu slider row.</param>
        /// <returns>Slider list model to add to add mega menu slider row.</returns>
        public MegaMenuAddRowSliderListModel PrepareMegaMenuAddRowSliderListModel(MegaMenuAddRowSliderSearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            //get sliders
            var sliders = _sliderService.GetAllSliders(sliderName: searchModel.SearchSliderName,
                pageIndex: searchModel.Page - 1, pageSize: searchModel.PageSize);

            //prepare grid model
            var model = new MegaMenuAddRowSliderListModel
            {
                Data = sliders.Select(slider =>
                {
                    //fill in model values from the entity
                    var sliderModel = slider.ToModel<SliderModel>();

                    return sliderModel;
                }),
                Total = sliders.TotalCount
            };

            return model;
        }

        /// <summary>
        /// Get picture list.
        /// </summary>
        /// <param name="connectionId">Connection identifier.</param>
        /// <param name="connectionType">Connection type</param>
        /// <returns>Get short picture list.</returns>
        public IList<PictureShortModel> GetAvailablePicture(int connectionId, RowConnectionType connectionType)
        {
            var pictures = _pictureService.GetAvailablePictureForPlugin(connectionId, connectionType, PictureKind.MegaMenu);
            var model = new List<PictureShortModel>();

            _baseAdminModelFactory.AddDefaultPicture(model, connectionId, connectionType, PictureKind.MegaMenu);

            foreach (var picture in pictures)
            {
                model.Add(new PictureShortModel
                {
                    ItemPictureId = picture.Id,
                    ItemPictureOriginalName = picture.OriginalName ?? string.Empty,
                    ItemPictureUrl = _pictureService.GetPictureUrl(picture, 75)
                });
            }

            return model;
        }


        #endregion
    }
}