﻿using Nop.Core.Domain.Catalog;
using Nop.Web.Areas.Admin.Models.Catalog;
using System.Collections.Generic;

namespace Nop.Web.Areas.Admin.Factories
{
    /// <summary>
    /// Represents the home page model factory
    /// </summary>
    public partial interface IHomePageModelFactory
    {
        /// <summary>
        /// Prepare home page edit model
        /// </summary>
        /// <returns>Home page model</returns>
        HomePageEditModel PrepareHomePageEditModel();

        /// <summary>
        /// Prepare home page model
        /// </summary>
        /// <param name="model">Home page model</param>
        /// <param name="homePage">Home page</param>
        /// <returns>Home page model</returns>
        HomePageModel PrepareHomePageModel(HomePageModel model, HomePage homePage);

        /// <summary>
        /// Prepare paged home page item list model
        /// </summary>
        /// <param name="searchModel">Home page item search model</param>
        /// <returns>Home page item list model</returns>
        HomePageItemListModel PrepareHomePageItemListModel(HomePageItemSearchModel searchModel);

        /// <summary>
        /// Prepare paged home page item row list model
        /// </summary>
        /// <param name="searchModel">Home page item row search model</param>
        /// <returns>Home page item row list model</returns>
        HomePageItemListModel PrepareHomePageItemRowListModel(HomePageItemRowSearchModel searchModel);

        /// <summary>
        /// Prepare home page item model
        /// </summary>
        /// <param name="model">Home page item model</param>
        /// <returns>Home page item model</returns>
        HomePageItemModel PrepareHomePageItemModel(HomePageItemModel model);

        /// <summary>
        /// Prepare category search model to add home page category row.
        /// </summary>
        /// <param name="searchModel">Category search model to add home page category row</param>
        /// <returns>Category search model to add home page category row</returns>
        HomePageAddRowCategorySearchModel PrepareAddRowCategorySearchModel(HomePageAddRowCategorySearchModel searchModel);

        /// <summary>
        /// Prepare paged category list model to add home page category row.
        /// </summary>
        /// <param name="searchModel">Category search model to add home page category row.</param>
        /// <returns>Category list model to add to add home page category row.</returns>
        HomePageAddRowCategoryListModel PrepareHomePageAddRowCategoryListModel(HomePageAddRowCategorySearchModel searchModel);

        /// <summary>
        /// Prepare product search model to add home page product row.
        /// </summary>
        /// <param name="searchModel">Product search model to add home page product row</param>
        /// <returns>Product search model to add home page product row</returns>
        HomePageAddRowProductSearchModel PrepareAddRowProductSearchModel(HomePageAddRowProductSearchModel searchModel);

        /// <summary>
        /// Prepare paged product list model to add home page product row.
        /// </summary>
        /// <param name="searchModel">Product search model to add home page product row.</param>
        /// <returns>Product list model to add to add home page product row.</returns>
        HomePageAddRowProductListModel PrepareHomePageAddRowProductListModel(HomePageAddRowProductSearchModel searchModel);

        /// <summary>
        /// Prepare element search model to add home page element row.
        /// </summary>
        /// <param name="searchModel">Element search model to add home page element row</param>
        /// <returns>Element search model to add home page element row</returns>
        HomePageAddRowElementSearchModel PrepareAddRowElementSearchModel(HomePageAddRowElementSearchModel searchModel);

        /// <summary>
        /// Prepare paged product list model to add home page element row.
        /// </summary>
        /// <param name="searchModel">Element search model to add home page element row.</param>
        /// <returns>Element list model to add to add home page element row.</returns>
        HomePageAddRowElementListModel PrepareHomePageAddRowElementListModel(HomePageAddRowElementSearchModel searchModel);

        /// <summary>
        /// Prepare topic search model to add home page topic row.
        /// </summary>
        /// <param name="searchModel">Topic search model to add home page topic row</param>
        /// <returns>Topic search model to add home page topic row</returns>
        HomePageAddRowTopicSearchModel PrepareAddRowTopicSearchModel(HomePageAddRowTopicSearchModel searchModel);

        /// <summary>
        /// Prepare paged topic list model to add home page topic row.
        /// </summary>
        /// <param name="searchModel">Element search model to add home page topic row.</param>
        /// <returns>Topic list model to add to add home page topic row.</returns>
        HomePageAddRowTopicListModel PrepareHomePageAddRowTopicListModel(HomePageAddRowTopicSearchModel searchModel);

        /// <summary>
        /// Prepare slider search model to add home page category row.
        /// </summary>
        /// <param name="searchModel">Slider search model to add home page slider row</param>
        /// <returns>Slider search model to add home page slider row</returns>
        HomePageAddRowSliderSearchModel PrepareAddRowSliderSearchModel(HomePageAddRowSliderSearchModel searchModel);

        /// <summary>
        /// Prepare paged slider list model to add home page slider row.
        /// </summary>
        /// <param name="searchModel">Slider search model to add home page slider row.</param>
        /// <returns>Slider list model to add to add home page slider row.</returns>
        HomePageAddRowSliderListModel PrepareHomePageAddRowSliderListModel(HomePageAddRowSliderSearchModel searchModel);

        /// <summary>
        /// Get picture list.
        /// </summary>
        /// <param name="connectionId">Connection identifier.</param>
        /// <param name="connectionType">Connection type</param>
        /// <returns>Get short picture list.</returns>
        IList<PictureShortModel> GetAvailablePicture(int connectionId, RowConnectionType connectionType);
    }
}