﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Media;
using Nop.Services.Catalog;
using Nop.Services.Localization;
using Nop.Services.Media;
using Nop.Services.Topics;
using Nop.Web.Areas.Admin.Infrastructure.Mapper.Extensions;
using Nop.Web.Areas.Admin.Models.Catalog;
using Nop.Web.Areas.Admin.Models.Topics;

namespace Nop.Web.Areas.Admin.Factories
{
    /// <summary>
    /// Represents the sidebar model factory implementation
    /// </summary>
    public partial class SidebarModelFactory : ISidebarModelFactory
    {
        #region Fields

        private readonly ISidebarService _sidebarService;
        private readonly ISidebarItemService _sidebarItemService;
        private readonly ILocalizationService _localizationService;
        private readonly IBaseAdminModelFactory _baseAdminModelFactory;
        private readonly ICategoryService _categoryService;
        private readonly IProductService _productService;
        private readonly IElementService _elementService;
        private readonly ITopicService _topicService;
        private readonly ISliderService _sliderService;
        private readonly IPictureService _pictureService;

        #endregion

        #region Ctor

        public SidebarModelFactory(ISidebarService sidebarService,
            ISidebarItemService sidebarItemService,
            ILocalizationService localizationService,
            IBaseAdminModelFactory baseAdminModelFactory,
            ICategoryService categoryService,
            IProductService productService,
            IElementService elementService,
            ITopicService topicService,
            ISliderService sliderService,
            IPictureService pictureService)
        {
            _sidebarService = sidebarService;
            _sidebarItemService = sidebarItemService;
            _localizationService = localizationService;
            _baseAdminModelFactory = baseAdminModelFactory;
            _categoryService = categoryService;
            _productService = productService;
            _elementService = elementService;
            _topicService = topicService;
            _sliderService = sliderService;
            _pictureService = pictureService;
        }

        #endregion

        #region Utilities

        private IList<SidebarShortModel> PrepareSidebars()
        {
            return _sidebarService.GetAll().OrderByDescending(tab => tab.CreatedOnUtc).Select(tab =>
                new SidebarShortModel
                {
                    Id = tab.Id,
                    Name = tab.Name
                }).ToList();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Prepare sidebar edit model
        /// </summary>
        /// <returns>Sidebar model</returns>
        public SidebarEditModel PrepareSidebarEditModel()
        {
            var model = new SidebarEditModel();

            model.Sidebars = PrepareSidebars();

            return model;
        }

        /// <summary>
        /// Prepare sidebar model
        /// </summary>
        /// <param name="model">Sidebar model</param>
        /// <param name="sidebar">Sidebar</param>
        /// <returns>Sidebar model</returns>
        public SidebarModel PrepareSidebarModel(SidebarModel model, Sidebar sidebar)
        {
            if (sidebar != null)
            {
                //fill in model values from the entity
                model = model ?? sidebar.ToModel<SidebarModel>();
            }

            return model;
        }

        /// <summary>
        /// Prepare paged sidebar item list model
        /// </summary>
        /// <param name="searchModel">Sidebar item search model</param>
        /// <returns>SidebarItem list model</returns>
        public SidebarItemListModel PrepareSidebarItemListModel(SidebarItemSearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            //get items
            var items = _sidebarItemService.GetColumnsHeadersBySidebarId(searchModel.SidebarId);

            //prepare grid model
            var model = new SidebarItemListModel
            {
                Data = items.Select(sidebarItem =>
                {
                    //fill in model values from the entity
                    var sidebarItemModel = sidebarItem.ToModel<SidebarItemModel>();

                    sidebarItemModel.ItemName = GetSidebarItemName(sidebarItem);
                    sidebarItemModel.ItemColor = string.IsNullOrWhiteSpace(sidebarItem.Color) ? null : sidebarItem.Color;
                    PrepareSidebarPicture(sidebarItemModel, sidebarItem);

                    //fill in additional values (not existing in the entity)
                    return sidebarItemModel;
                }),
                Total = items.TotalCount
            };

            return model;
        }

        private void PrepareSidebarPicture(SidebarItemModel sidebarItemModel, SidebarItem sidebarItem)
        {
            var picture = sidebarItem.Picture;

            if (picture == null)
            {
                switch (sidebarItem.RowConnectionType)
                {
                    case RowConnectionType.category:
                        picture = _pictureService.GetPluginDefaultByCategoryId(sidebarItem.RowConnectionId.Value, PictureKind.Sidebar);
                        break;
                    case RowConnectionType.product:
                        picture = _pictureService.GetPluginDefaultByProductId(sidebarItem.RowConnectionId.Value, PictureKind.Sidebar);
                        break;
                }
            }

            if (picture != null)
            {
                sidebarItemModel.ItemPicture.ItemPictureId = (sidebarItem.Picture == null) ? -1 : picture.Id;
                sidebarItemModel.ItemPicture.ItemPictureOriginalName = (sidebarItem.Picture == null) ? "default" : picture.OriginalName ?? string.Empty;
                sidebarItemModel.ItemPicture.ItemPictureUrl = _pictureService.GetPictureUrl(picture, 75);
            }
        }

        /// <summary>
        /// Prepare paged sidebar item row list model
        /// </summary>
        /// <param name="searchModel">Sidebar item row search model</param>
        /// <returns>SidebarItem row list model</returns>
        public SidebarItemListModel PrepareSidebarItemRowListModel(SidebarItemRowSearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            var column = _sidebarItemService.GetSidebarItemById(searchModel.SidebarItemId);
            if (column == null)
                throw new ArgumentNullException(nameof(column));

            //get items
            var items = _sidebarItemService.GetRowsBySidebarItem(column);

            //prepare grid model
            var model = new SidebarItemListModel
            {
                Data = items.Select(sidebarItem =>
                {
                    //fill in model values from the entity
                    var sidebarItemModel = sidebarItem.ToModel<SidebarItemModel>();

                    sidebarItemModel.ItemName = GetSidebarItemName(sidebarItem);
                    sidebarItemModel.ItemColor = string.IsNullOrWhiteSpace(sidebarItem.Color) ? null : sidebarItem.Color;
                    PrepareSidebarPicture(sidebarItemModel, sidebarItem);
                    //fill in additional values (not existing in the entity)
                    return sidebarItemModel;
                }),
                Total = items.TotalCount
            };

            return model;
        }

        private string GetSidebarItemName(SidebarItem sidebarItem)
        {
            switch (sidebarItem.RowConnectionType)
            {
                case RowConnectionType.category:
                    return _categoryService.GetCategoryById(sidebarItem.RowConnectionId.GetValueOrDefault())?.ShortName;
                case RowConnectionType.product:
                    return _productService.GetProductById(sidebarItem.RowConnectionId.GetValueOrDefault())?.Name;
                case RowConnectionType.element:
                    return _elementService.GetElementById(sidebarItem.RowConnectionId.GetValueOrDefault())?.Name;
                case RowConnectionType.topic:
                    return _topicService.GetTopicById(sidebarItem.RowConnectionId.GetValueOrDefault())?.SystemName;
                case RowConnectionType.slider:
                    return _sliderService.GetSliderById(sidebarItem.RowConnectionId.GetValueOrDefault())?.Name;
                default:
                    return string.Empty;
            }
        }

        /// <summary>
        /// Prepare sidebar item model
        /// </summary>
        /// <param name="model">Sidebar item model</param>
        /// <returns>Sidebar item model</returns>
        public SidebarItemModel PrepareSidebarItemModel(SidebarItemModel model)
        {
            model.ItemRowConnectionType = RowConnectionType.category;
            switch (model.ItemRowType)
            {
                case RowType.header:
                    model.ItemCssClass = "sd_header";
                    break;
                case RowType.textcolumn:
                    model.ItemCssClass = "sd_column_title";
                    break;
                case RowType.imagecolumn:
                    model.ItemCssClass = "sd_column_image";
                    break;
            }

            return model;
        }

        /// <summary>
        /// Prepare category search model to add sidebar category row.
        /// </summary>
        /// <param name="searchModel">Category search model to add sidebar category row</param>
        /// <returns>Category search model to add sidebar category row</returns>
        public SidebarAddRowCategorySearchModel PrepareAddRowCategorySearchModel(SidebarAddRowCategorySearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            //prepare page parameters
            searchModel.SetPopupGridPageSize();
            _baseAdminModelFactory.PrepareSidebarColumns(searchModel.AvailableColumns, searchModel.SidebarId, true, _localizationService.GetResource("Admin.Common.RowAsTitle"));

            return searchModel;
        }

        /// <summary>
        /// Prepare paged category list model to add sidebar category row.
        /// </summary>
        /// <param name="searchModel">Category search model to add sidebar category row.</param>
        /// <returns>Category list model to add to add sidebar category row.</returns>
        public SidebarAddRowCategoryListModel PrepareSidebarAddRowCategoryListModel(SidebarAddRowCategorySearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            //get categories
            var categories = _categoryService.GetAllCategories(showHidden: true,
                categoryName: searchModel.SearchCategoryName,
                pageIndex: searchModel.Page - 1, pageSize: searchModel.PageSize);

            //prepare grid model
            var model = new SidebarAddRowCategoryListModel
            {
                Data = categories.Select(category =>
                {
                    //fill in model values from the entity
                    var categoryModel = category.ToModel<CategoryModel>();

                    //fill in additional values (not existing in the entity)
                    categoryModel.Breadcrumb = _categoryService.GetFormattedBreadCrumb(category);

                    return categoryModel;
                }),
                Total = categories.TotalCount
            };

            return model;
        }

        /// <summary>
        /// Prepare product search model to add sidebar product row.
        /// </summary>
        /// <param name="searchModel">Product search model to add sidebar product row</param>
        /// <returns>Product search model to add sidebar product row</returns>
        public SidebarAddRowProductSearchModel PrepareAddRowProductSearchModel(SidebarAddRowProductSearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            //prepare page parameters
            searchModel.SetPopupGridPageSize();
            _baseAdminModelFactory.PrepareSidebarColumns(searchModel.AvailableColumns, searchModel.SidebarId, true, _localizationService.GetResource("Admin.Common.RowAsTitle"));

            //prepare available product types
            _baseAdminModelFactory.PrepareProductTypes(searchModel.AvailableProductTypes);

            return searchModel;
        }

        /// <summary>
        /// Prepare paged product list model to add sidebar product row.
        /// </summary>
        /// <param name="searchModel">Product search model to add sidebar product row.</param>
        /// <returns>Product list model to add to add sidebar product row.</returns>
        public SidebarAddRowProductListModel PrepareSidebarAddRowProductListModel(SidebarAddRowProductSearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            //get products
            var products = _productService.SearchProducts(showHidden: true,
                productType: searchModel.SearchProductTypeId > 0 ? (ProductType?)searchModel.SearchProductTypeId : null,
                keywords: searchModel.SearchProductName,
                pageIndex: searchModel.Page - 1, pageSize: searchModel.PageSize);

            //prepare grid model
            var model = new SidebarAddRowProductListModel
            {
                //fill in model values from the entity
                Data = products.Select(product => product.ToModel<ProductModel>()),
                Total = products.TotalCount
            };

            return model;
        }

        /// <summary>
        /// Prepare element search model to add sidebar element row.
        /// </summary>
        /// <param name="searchModel">Element search model to add sidebar element row</param>
        /// <returns>Element search model to add sidebar element row</returns>
        public SidebarAddRowElementSearchModel PrepareAddRowElementSearchModel(SidebarAddRowElementSearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            //prepare page parameters
            searchModel.SetPopupGridPageSize();
            _baseAdminModelFactory.PrepareSidebarColumns(searchModel.AvailableColumns, searchModel.SidebarId, true, _localizationService.GetResource("Admin.Common.RowAsTitle"));

            //prepare available product types
            _baseAdminModelFactory.PrepareElementTypes(searchModel.AvailableElementTypes);

            return searchModel;
        }

        /// <summary>
        /// Prepare paged product list model to add sidebar element row.
        /// </summary>
        /// <param name="searchModel">Element search model to add sidebar element row.</param>
        /// <returns>Element list model to add to add sidebar element row.</returns>
        public SidebarAddRowElementListModel PrepareSidebarAddRowElementListModel(SidebarAddRowElementSearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            //get elements
            var elements = _elementService.GetAllElements(
                elementType: searchModel.SearchElementTypeId > 0 ? (ElementType?)searchModel.SearchElementTypeId : null,
                elementName: searchModel.SearchElementName,
                pageIndex: searchModel.Page - 1,
                pageSize: searchModel.PageSize);

            //prepare grid model
            var model = new SidebarAddRowElementListModel
            {
                Data = elements.Select(element =>
                {
                    //fill in model values from the entity
                    var elementModel = element.ToModel<ElementModel>();
                    return elementModel;
                }),
                Total = elements.TotalCount
            };

            return model;
        }

        /// <summary>
        /// Prepare topic search model to add sidebar topic row.
        /// </summary>
        /// <param name="searchModel">Topic search model to add sidebar topic row</param>
        /// <returns>Topic search model to add sidebar topic row</returns>
        public SidebarAddRowTopicSearchModel PrepareAddRowTopicSearchModel(SidebarAddRowTopicSearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            //prepare page parameters
            searchModel.SetPopupGridPageSize();
            _baseAdminModelFactory.PrepareSidebarColumns(searchModel.AvailableColumns, searchModel.SidebarId, true, _localizationService.GetResource("Admin.Common.RowAsTitle"));
            
            return searchModel;
        }

        /// <summary>
        /// Prepare paged topic list model to add sidebar topic row.
        /// </summary>
        /// <param name="searchModel">Element search model to add sidebar topic row.</param>
        /// <returns>Topic list model to add to add sidebar topic row.</returns>
        public SidebarAddRowTopicListModel PrepareSidebarAddRowTopicListModel(SidebarAddRowTopicSearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            //get topics
            var topics = _topicService.GetAllTopics(searchModel.SearchKeywords, searchModel.Page - 1, searchModel.PageSize);

            //prepare grid model
            var model = new SidebarAddRowTopicListModel
            {
                Data = topics.Select(topic =>
                {
                    //fill in model values from the entity
                    var topicModel = topic.ToModel<TopicModel>();
                    return topicModel;
                }),
                Total = topics.TotalCount
            };

            return model;
        }

        /// <summary>
        /// Prepare slider search model to add sidebar category row.
        /// </summary>
        /// <param name="searchModel">Slider search model to add sidebar slider row</param>
        /// <returns>Slider search model to add sidebar slider row</returns>
        public SidebarAddRowSliderSearchModel PrepareAddRowSliderSearchModel(SidebarAddRowSliderSearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            //prepare page parameters
            searchModel.SetPopupGridPageSize();
            _baseAdminModelFactory.PrepareSidebarColumns(searchModel.AvailableColumns, searchModel.SidebarId, true, _localizationService.GetResource("Admin.Common.RowAsTitle"));

            return searchModel;
        }

        /// <summary>
        /// Prepare paged slider list model to add sidebar slider row.
        /// </summary>
        /// <param name="searchModel">Slider search model to add sidebar slider row.</param>
        /// <returns>Slider list model to add to add sidebar slider row.</returns>
        public SidebarAddRowSliderListModel PrepareSidebarAddRowSliderListModel(SidebarAddRowSliderSearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            //get sliders
            var sliders = _sliderService.GetAllSliders(sliderName: searchModel.SearchSliderName,
                pageIndex: searchModel.Page - 1, pageSize: searchModel.PageSize);

            //prepare grid model
            var model = new SidebarAddRowSliderListModel
            {
                Data = sliders.Select(slider =>
                {
                    //fill in model values from the entity
                    var sliderModel = slider.ToModel<SliderModel>();

                    return sliderModel;
                }),
                Total = sliders.TotalCount
            };

            return model;
        }

        /// <summary>
        /// Get picture list.
        /// </summary>
        /// <param name="connectionId">Connection identifier.</param>
        /// <param name="connectionType">Connection type</param>
        /// <returns>Get short picture list.</returns>
        public IList<PictureShortModel> GetAvailablePicture(int connectionId, RowConnectionType connectionType)
        {
            var pictures = _pictureService.GetAvailablePictureForPlugin(connectionId, connectionType, PictureKind.Sidebar);
            var model = new List<PictureShortModel>();

            _baseAdminModelFactory.AddDefaultPicture(model, connectionId, connectionType, PictureKind.Sidebar);

            foreach (var picture in pictures)
            {
                model.Add(new PictureShortModel
                {
                    ItemPictureId = picture.Id,
                    ItemPictureOriginalName = picture.OriginalName ?? string.Empty,
                    ItemPictureUrl = _pictureService.GetPictureUrl(picture, 75)
                });
            }

            return model;
        }

        #endregion
    }
}