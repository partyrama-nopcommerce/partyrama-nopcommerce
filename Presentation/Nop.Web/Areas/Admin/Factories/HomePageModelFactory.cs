﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Media;
using Nop.Services.Catalog;
using Nop.Services.Localization;
using Nop.Services.Media;
using Nop.Services.Topics;
using Nop.Web.Areas.Admin.Infrastructure.Mapper.Extensions;
using Nop.Web.Areas.Admin.Models.Catalog;
using Nop.Web.Areas.Admin.Models.Topics;

namespace Nop.Web.Areas.Admin.Factories
{
    /// <summary>
    /// Represents the home page model factory implementation
    /// </summary>
    public partial class HomePageModelFactory : IHomePageModelFactory
    {
        #region Fields

        private readonly IHomePageService _homePageService;
        private readonly IHomePageItemService _homePageItemService;
        private readonly ILocalizationService _localizationService;
        private readonly IBaseAdminModelFactory _baseAdminModelFactory;
        private readonly ICategoryService _categoryService;
        private readonly IProductService _productService;
        private readonly IElementService _elementService;
        private readonly ITopicService _topicService;
        private readonly ISliderService _sliderService;
        private readonly IPictureService _pictureService;

        #endregion

        #region Ctor

        public HomePageModelFactory(IHomePageService homePageService,
            IHomePageItemService homePageItemService,
            ILocalizationService localizationService,
            IBaseAdminModelFactory baseAdminModelFactory,
            ICategoryService categoryService,
            IProductService productService,
            IElementService elementService,
            ITopicService topicService,
            ISliderService sliderService,
            IPictureService pictureService)
        {
            _homePageService = homePageService;
            _homePageItemService = homePageItemService;
            _localizationService = localizationService;
            _baseAdminModelFactory = baseAdminModelFactory;
            _categoryService = categoryService;
            _productService = productService;
            _elementService = elementService;
            _topicService = topicService;
            _sliderService = sliderService;
            _pictureService = pictureService;
        }

        #endregion

        #region Utilities

        private IList<HomePageShortModel> PrepareHomePages()
        {
            return _homePageService.GetAll().OrderByDescending(tab => tab.CreatedOnUtc).Select(tab =>
                new HomePageShortModel
                {
                    Id = tab.Id,
                    Name = tab.Name
                }).ToList();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Prepare home page edit model
        /// </summary>
        /// <returns>Home page model</returns>
        public HomePageEditModel PrepareHomePageEditModel()
        {
            var model = new HomePageEditModel();

            model.HomePages = PrepareHomePages();

            return model;
        }

        /// <summary>
        /// Prepare home page model
        /// </summary>
        /// <param name="model">Home page model</param>
        /// <param name="homePage">Home page</param>
        /// <returns>Home page model</returns>
        public HomePageModel PrepareHomePageModel(HomePageModel model, HomePage homePage)
        {
            if (homePage != null)
            {
                //fill in model values from the entity
                model = model ?? homePage.ToModel<HomePageModel>();
            }

            return model;
        }

        /// <summary>
        /// Prepare paged home page item list model
        /// </summary>
        /// <param name="searchModel">Home page item search model</param>
        /// <returns>Home page item list model</returns>
        public HomePageItemListModel PrepareHomePageItemListModel(HomePageItemSearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            //get items
            var items = _homePageItemService.GetColumnsHeadersByHomePageId(searchModel.HomePageId);

            //prepare grid model
            var model = new HomePageItemListModel
            {
                Data = items.Select(homePageItem =>
                {
                    //fill in model values from the entity
                    var homePageItemModel = homePageItem.ToModel<HomePageItemModel>();

                    homePageItemModel.ItemName = GetHomePageItemName(homePageItem);
                    homePageItemModel.ItemColor = string.IsNullOrWhiteSpace(homePageItem.Color) ? null : homePageItem.Color;

                    PrepareHomePagePicture(homePageItemModel, homePageItem);
                    //fill in additional values (not existing in the entity)
                    return homePageItemModel;
                }),
                Total = items.TotalCount
            };

            return model;
        }

        private void PrepareHomePagePicture(HomePageItemModel homePageItemModel, HomePageItem homePageItem)
        {
            var picture = homePageItem.Picture;

            if (picture == null)
            {
                switch (homePageItem.RowConnectionType)
                {
                    case RowConnectionType.category:
                        picture = _pictureService.GetPluginDefaultByCategoryId(homePageItem.RowConnectionId.Value, PictureKind.HomePage);
                        break;
                    case RowConnectionType.product:
                        picture = _pictureService.GetPluginDefaultByProductId(homePageItem.RowConnectionId.Value, PictureKind.HomePage);
                        break;
                }
            }

            if (picture != null)
            {
                homePageItemModel.ItemPicture.ItemPictureId = (homePageItem.Picture == null) ? -1: picture.Id;
                homePageItemModel.ItemPicture.ItemPictureOriginalName = (homePageItem.Picture == null) ? "default": picture.OriginalName ?? string.Empty;
                homePageItemModel.ItemPicture.ItemPictureUrl = _pictureService.GetPictureUrl(picture, 75);
            }
        }

        /// <summary>
        /// Prepare paged home page item row list model
        /// </summary>
        /// <param name="searchModel">Home page item row search model</param>
        /// <returns>Home page item row list model</returns>
        public HomePageItemListModel PrepareHomePageItemRowListModel(HomePageItemRowSearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            var column = _homePageItemService.GetHomePageItemById(searchModel.HomePageItemId);
            if (column == null)
                throw new ArgumentNullException(nameof(column));

            //get items
            var items = _homePageItemService.GetRowsByHomePageItem(column);

            //prepare grid model
            var model = new HomePageItemListModel
            {
                Data = items.Select(homePageItem =>
                {
                    //fill in model values from the entity
                    var homePageItemModel = homePageItem.ToModel<HomePageItemModel>();

                    homePageItemModel.ItemName = GetHomePageItemName(homePageItem);
                    homePageItemModel.ItemColor = string.IsNullOrWhiteSpace(homePageItem.Color) ? null : homePageItem.Color;
                    PrepareHomePagePicture(homePageItemModel, homePageItem);
                    //fill in additional values (not existing in the entity)
                    return homePageItemModel;
                }),
                Total = items.TotalCount
            };

            return model;
        }

        private string GetHomePageItemName(HomePageItem homePageItem)
        {
            switch (homePageItem.RowConnectionType)
            {
                case RowConnectionType.category:
                    return _categoryService.GetCategoryById(homePageItem.RowConnectionId.GetValueOrDefault())?.ShortName;
                case RowConnectionType.product:
                    return _productService.GetProductById(homePageItem.RowConnectionId.GetValueOrDefault())?.Name;
                case RowConnectionType.element:
                    return _elementService.GetElementById(homePageItem.RowConnectionId.GetValueOrDefault())?.Name;
                case RowConnectionType.topic:
                    return _topicService.GetTopicById(homePageItem.RowConnectionId.GetValueOrDefault())?.SystemName;
                case RowConnectionType.slider:
                    return _sliderService.GetSliderById(homePageItem.RowConnectionId.GetValueOrDefault())?.Name;
                default:
                    return string.Empty;
            }
        }

        /// <summary>
        /// Prepare home page item model
        /// </summary>
        /// <param name="model">Home page item model</param>
        /// <returns>Home page item model</returns>
        public HomePageItemModel PrepareHomePageItemModel(HomePageItemModel model)
        {
            model.ItemRowConnectionType = RowConnectionType.category;
            switch (model.ItemRowType)
            {
                case RowType.header:
                    model.ItemCssClass = "hp_header";
                    break;
                case RowType.textcolumn:
                    model.ItemCssClass = "hp_column_title";
                    break;
                case RowType.imagecolumn:
                    model.ItemCssClass = "hp_column_image";
                    break;
            }

            return model;
        }

        /// <summary>
        /// Prepare category search model to add home page category row.
        /// </summary>
        /// <param name="searchModel">Category search model to add home page category row</param>
        /// <returns>Category search model to add home page category row</returns>
        public HomePageAddRowCategorySearchModel PrepareAddRowCategorySearchModel(HomePageAddRowCategorySearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            //prepare page parameters
            searchModel.SetPopupGridPageSize();
            _baseAdminModelFactory.PrepareHomePageColumns(searchModel.AvailableColumns, searchModel.HomePageId, true, _localizationService.GetResource("Admin.Common.RowAsTitle"));

            return searchModel;
        }

        /// <summary>
        /// Prepare paged category list model to add home page category row.
        /// </summary>
        /// <param name="searchModel">Category search model to add home page category row.</param>
        /// <returns>Category list model to add to add home page category row.</returns>
        public HomePageAddRowCategoryListModel PrepareHomePageAddRowCategoryListModel(HomePageAddRowCategorySearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            //get categories
            var categories = _categoryService.GetAllCategories(showHidden: true,
                categoryName: searchModel.SearchCategoryName,
                pageIndex: searchModel.Page - 1, pageSize: searchModel.PageSize);

            //prepare grid model
            var model = new HomePageAddRowCategoryListModel
            {
                Data = categories.Select(category =>
                {
                    //fill in model values from the entity
                    var categoryModel = category.ToModel<CategoryModel>();

                    //fill in additional values (not existing in the entity)
                    categoryModel.Breadcrumb = _categoryService.GetFormattedBreadCrumb(category);

                    return categoryModel;
                }),
                Total = categories.TotalCount
            };

            return model;
        }

        /// <summary>
        /// Prepare product search model to add home page product row.
        /// </summary>
        /// <param name="searchModel">Product search model to add home page product row</param>
        /// <returns>Product search model to add home page product row</returns>
        public HomePageAddRowProductSearchModel PrepareAddRowProductSearchModel(HomePageAddRowProductSearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            //prepare page parameters
            searchModel.SetPopupGridPageSize();
            _baseAdminModelFactory.PrepareHomePageColumns(searchModel.AvailableColumns, searchModel.HomePageId, true, _localizationService.GetResource("Admin.Common.RowAsTitle"));

            //prepare available product types
            _baseAdminModelFactory.PrepareProductTypes(searchModel.AvailableProductTypes);

            return searchModel;
        }

        /// <summary>
        /// Prepare paged product list model to add home page product row.
        /// </summary>
        /// <param name="searchModel">Product search model to add home page product row.</param>
        /// <returns>Product list model to add to add home page product row.</returns>
        public HomePageAddRowProductListModel PrepareHomePageAddRowProductListModel(HomePageAddRowProductSearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            //get products
            var products = _productService.SearchProducts(showHidden: true,
                productType: searchModel.SearchProductTypeId > 0 ? (ProductType?)searchModel.SearchProductTypeId : null,
                keywords: searchModel.SearchProductName,
                pageIndex: searchModel.Page - 1, pageSize: searchModel.PageSize);

            //prepare grid model
            var model = new HomePageAddRowProductListModel
            {
                //fill in model values from the entity
                Data = products.Select(product => product.ToModel<ProductModel>()),
                Total = products.TotalCount
            };

            return model;
        }

        /// <summary>
        /// Prepare element search model to add home page element row.
        /// </summary>
        /// <param name="searchModel">Element search model to add home page element row</param>
        /// <returns>Element search model to add home page element row</returns>
        public HomePageAddRowElementSearchModel PrepareAddRowElementSearchModel(HomePageAddRowElementSearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            //prepare page parameters
            searchModel.SetPopupGridPageSize();
            _baseAdminModelFactory.PrepareHomePageColumns(searchModel.AvailableColumns, searchModel.HomePageId, true, _localizationService.GetResource("Admin.Common.RowAsTitle"));

            //prepare available product types
            _baseAdminModelFactory.PrepareElementTypes(searchModel.AvailableElementTypes);

            return searchModel;
        }

        /// <summary>
        /// Prepare paged product list model to add home page element row.
        /// </summary>
        /// <param name="searchModel">Element search model to add home page element row.</param>
        /// <returns>Element list model to add to add home page element row.</returns>
        public HomePageAddRowElementListModel PrepareHomePageAddRowElementListModel(HomePageAddRowElementSearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            //get elements
            var elements = _elementService.GetAllElements(
                elementType: searchModel.SearchElementTypeId > 0 ? (ElementType?)searchModel.SearchElementTypeId : null,
                elementName: searchModel.SearchElementName,
                pageIndex: searchModel.Page - 1,
                pageSize: searchModel.PageSize);

            //prepare grid model
            var model = new HomePageAddRowElementListModel
            {
                Data = elements.Select(element =>
                {
                    //fill in model values from the entity
                    var elementModel = element.ToModel<ElementModel>();
                    return elementModel;
                }),
                Total = elements.TotalCount
            };

            return model;
        }

        /// <summary>
        /// Prepare topic search model to add home page topic row.
        /// </summary>
        /// <param name="searchModel">Topic search model to add home page topic row</param>
        /// <returns>Topic search model to add home page topic row</returns>
        public HomePageAddRowTopicSearchModel PrepareAddRowTopicSearchModel(HomePageAddRowTopicSearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            //prepare page parameters
            searchModel.SetPopupGridPageSize();
            _baseAdminModelFactory.PrepareHomePageColumns(searchModel.AvailableColumns, searchModel.HomePageId, true, _localizationService.GetResource("Admin.Common.RowAsTitle"));
            
            return searchModel;
        }

        /// <summary>
        /// Prepare paged topic list model to add home page topic row.
        /// </summary>
        /// <param name="searchModel">Element search model to add home page topic row.</param>
        /// <returns>Topic list model to add to add home page topic row.</returns>
        public HomePageAddRowTopicListModel PrepareHomePageAddRowTopicListModel(HomePageAddRowTopicSearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            //get topics
            var topics = _topicService.GetAllTopics(searchModel.SearchKeywords, searchModel.Page - 1, searchModel.PageSize);

            //prepare grid model
            var model = new HomePageAddRowTopicListModel
            {
                Data = topics.Select(topic =>
                {
                    //fill in model values from the entity
                    var topicModel = topic.ToModel<TopicModel>();
                    return topicModel;
                }),
                Total = topics.TotalCount
            };

            return model;
        }

        /// <summary>
        /// Prepare slider search model to add home page category row.
        /// </summary>
        /// <param name="searchModel">Slider search model to add home page slider row</param>
        /// <returns>Slider search model to add home page slider row</returns>
        public HomePageAddRowSliderSearchModel PrepareAddRowSliderSearchModel(HomePageAddRowSliderSearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            //prepare page parameters
            searchModel.SetPopupGridPageSize();
            _baseAdminModelFactory.PrepareHomePageColumns(searchModel.AvailableColumns, searchModel.HomePageId, true, _localizationService.GetResource("Admin.Common.RowAsTitle"));

            return searchModel;
        }

        /// <summary>
        /// Prepare paged slider list model to add home page slider row.
        /// </summary>
        /// <param name="searchModel">Slider search model to add home page slider row.</param>
        /// <returns>Slider list model to add to add home page slider row.</returns>
        public HomePageAddRowSliderListModel PrepareHomePageAddRowSliderListModel(HomePageAddRowSliderSearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            //get sliders
            var sliders = _sliderService.GetAllSliders(sliderName: searchModel.SearchSliderName,
                pageIndex: searchModel.Page - 1, pageSize: searchModel.PageSize);

            //prepare grid model
            var model = new HomePageAddRowSliderListModel
            {
                Data = sliders.Select(slider =>
                {
                    //fill in model values from the entity
                    var sliderModel = slider.ToModel<SliderModel>();

                    return sliderModel;
                }),
                Total = sliders.TotalCount
            };

            return model;
        }

        /// <summary>
        /// Get picture list.
        /// </summary>
        /// <param name="connectionId">Connection identifier.</param>
        /// <param name="connectionType">Connection type</param>
        /// <returns>Get short picture list.</returns>
        public IList<PictureShortModel> GetAvailablePicture(int connectionId, RowConnectionType connectionType)
        {
            var pictures = _pictureService.GetAvailablePictureForPlugin(connectionId, connectionType, PictureKind.HomePage);
            var model = new List<PictureShortModel>();

            _baseAdminModelFactory.AddDefaultPicture(model, connectionId, connectionType, PictureKind.HomePage);

            foreach (var picture in pictures)
            {
                model.Add(new PictureShortModel
                {
                    ItemPictureId = picture.Id,
                    ItemPictureOriginalName = picture.OriginalName ?? string.Empty,
                    ItemPictureUrl = _pictureService.GetPictureUrl(picture, 75)
                });
            }

            return model;
        }

        #endregion
    }
}