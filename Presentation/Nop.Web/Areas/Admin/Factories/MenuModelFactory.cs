﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core.Domain.Catalog;
using Nop.Services.Catalog;
using Nop.Services.Localization;
using Nop.Services.Topics;
using Nop.Web.Areas.Admin.Infrastructure.Mapper.Extensions;
using Nop.Web.Areas.Admin.Models.Catalog;
using Nop.Web.Areas.Admin.Models.Topics;

namespace Nop.Web.Areas.Admin.Factories
{
    /// <summary>
    /// Represents the menu model factory implementation
    /// </summary>
    public partial class MenuModelFactory : IMenuModelFactory
    {
        #region Fields

        private readonly IMenuService _menuService;
        private readonly IMenuItemService _menuItemService;
        private readonly ILocalizationService _localizationService;
        private readonly IBaseAdminModelFactory _baseAdminModelFactory;
        private readonly ICategoryService _categoryService;
        private readonly IProductService _productService;
        private readonly IElementService _elementService;
        private readonly ITopicService _topicService;

        #endregion

        #region Ctor

        public MenuModelFactory(IMenuService menuService,
            IMenuItemService menuItemService,
            ILocalizationService localizationService,
            IBaseAdminModelFactory baseAdminModelFactory,
            ICategoryService categoryService,
            IProductService productService,
            IElementService elementService,
            ITopicService topicService)
        {
            _menuService = menuService;
            _menuItemService = menuItemService;
            _localizationService = localizationService;
            _baseAdminModelFactory = baseAdminModelFactory;
            _categoryService = categoryService;
            _productService = productService;
            _elementService = elementService;
            _topicService = topicService;
        }

        #endregion

        #region Utilities

        private IList<MenuShortModel> PrepareMenus()
        {
            return _menuService.GetAll().OrderByDescending(tab => tab.CreatedOnUtc).Select(tab =>
                new MenuShortModel
                {
                    Id = tab.Id,
                    Name = tab.Name
                }).ToList();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Prepare menu edit model
        /// </summary>
        /// <returns>Mega menu model</returns>
        public MenuEditModel PrepareMenuEditModel()
        {
            var model = new MenuEditModel();

            model.Menus = PrepareMenus();

            return model;
        }

        /// <summary>
        /// Prepare menu model
        /// </summary>
        /// <param name="model">Menu model</param>
        /// <param name="menu">Menu</param>
        /// <returns>Menu model</returns>
        public MenuModel PrepareMenuModel(MenuModel model, Menu menu)
        {
            if (menu != null)
            {
                //fill in model values from the entity
                model = model ?? menu.ToModel<MenuModel>();
            }

            return model;
        }

        /// <summary>
        /// Prepare paged menu item list model
        /// </summary>
        /// <param name="searchModel">Menu item search model</param>
        /// <returns>MenuItem list model</returns>
        public MenuItemListModel PrepareMenuItemListModel(MenuItemSearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            //get items
            var items = _menuItemService.GetColumnsHeadersByMenuId(searchModel.MenuId);

            //prepare grid model
            var model = new MenuItemListModel
            {
                Data = items.Select(menuItem =>
                {
                    //fill in model values from the entity
                    var menuItemModel = menuItem.ToModel<MenuItemModel>();

                    menuItemModel.ItemMegaMenuTabId = menuItemModel.ItemMegaMenuTabId ?? 0;
                    menuItemModel.ItemName = GetMenuItemName(menuItem);
                    menuItemModel.ItemColor = string.IsNullOrWhiteSpace(menuItem.Color) ? null : menuItem.Color;
                    //fill in additional values (not existing in the entity)
                    return menuItemModel;
                }),
                Total = items.TotalCount
            };

            return model;
        }

        /// <summary>
        /// Prepare paged menu item row list model
        /// </summary>
        /// <param name="searchModel">Menu item row search model</param>
        /// <returns>MenuItem row list model</returns>
        public MenuItemListModel PrepareMenuItemRowListModel(MenuItemRowSearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            var column = _menuItemService.GetMenuItemById(searchModel.MenuItemId);
            if (column == null)
                throw new ArgumentNullException(nameof(column));

            //get items
            var items = _menuItemService.GetRowsByMenuItem(column);

            //prepare grid model
            var model = new MenuItemListModel
            {
                Data = items.Select(menuItem =>
                {
                    //fill in model values from the entity
                    var menuItemModel = menuItem.ToModel<MenuItemModel>();

                    menuItemModel.ItemName = GetMenuItemName(menuItem);
                    menuItemModel.ItemColor = string.IsNullOrWhiteSpace(menuItem.Color) ? null : menuItem.Color;
                    //fill in additional values (not existing in the entity)
                    return menuItemModel;
                }),
                Total = items.TotalCount
            };

            return model;
        }

        private string GetMenuItemName(MenuItem menuItem)
        {
            switch (menuItem.RowConnectionType)
            {
                case RowConnectionType.category:
                    return _categoryService.GetCategoryById(menuItem.RowConnectionId.GetValueOrDefault())?.ShortName;
                case RowConnectionType.product:
                    return _productService.GetProductById(menuItem.RowConnectionId.GetValueOrDefault())?.Name;
                case RowConnectionType.element:
                    return _elementService.GetElementById(menuItem.RowConnectionId.GetValueOrDefault())?.Name;
                case RowConnectionType.topic:
                    return _topicService.GetTopicById(menuItem.RowConnectionId.GetValueOrDefault())?.SystemName;
                default:
                    return string.Empty;
            }
        }

        /// <summary>
        /// Prepare menu item model
        /// </summary>
        /// <param name="model">Menu item model</param>
        /// <returns>Menu item model</returns>
        public MenuItemModel PrepareMenuItemModel(MenuItemModel model)
        {
            _baseAdminModelFactory.PrepareMegaMenuTabs(model.AvailableMegaMenuTab,
                defaultItemText: _localizationService.GetResource("Admin.Common.Select"));

            model.ItemRowConnectionType = RowConnectionType.category;
            switch (model.ItemRowType)
            {
                case RowType.header:
                    model.ItemCssClass = "m_header";
                    break;
                case RowType.textcolumn:
                    model.ItemCssClass = "m_column_title";
                    break;
                case RowType.imagecolumn:
                    model.ItemCssClass = "m_column_image";
                    break;
            }

            return model;
        }

        /// <summary>
        /// Prepare category search model to add menu category row.
        /// </summary>
        /// <param name="searchModel">Category search model to add menu category row</param>
        /// <returns>Category search model to add menu category row</returns>
        public MenuAddRowCategorySearchModel PrepareAddRowCategorySearchModel(MenuAddRowCategorySearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            //prepare page parameters
            searchModel.SetPopupGridPageSize();
            _baseAdminModelFactory.PrepareMenuColumns(searchModel.AvailableColumns, searchModel.MenuId, true, _localizationService.GetResource("Admin.Common.RowAsTitle"));

            return searchModel;
        }

        /// <summary>
        /// Prepare paged category list model to add menu category row.
        /// </summary>
        /// <param name="searchModel">Category search model to add menu category row.</param>
        /// <returns>Category list model to add to add menu category row.</returns>
        public MenuAddRowCategoryListModel PrepareMenuAddRowCategoryListModel(MenuAddRowCategorySearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            //get categories
            var categories = _categoryService.GetAllCategories(showHidden: true,
                categoryName: searchModel.SearchCategoryName,
                pageIndex: searchModel.Page - 1, pageSize: searchModel.PageSize);

            //prepare grid model
            var model = new MenuAddRowCategoryListModel
            {
                Data = categories.Select(category =>
                {
                    //fill in model values from the entity
                    var categoryModel = category.ToModel<CategoryModel>();

                    //fill in additional values (not existing in the entity)
                    categoryModel.Breadcrumb = _categoryService.GetFormattedBreadCrumb(category);

                    return categoryModel;
                }),
                Total = categories.TotalCount
            };

            return model;
        }

        /// <summary>
        /// Prepare product search model to add menu product row.
        /// </summary>
        /// <param name="searchModel">Product search model to add menu product row</param>
        /// <returns>Product search model to add menu product row</returns>
        public MenuAddRowProductSearchModel PrepareAddRowProductSearchModel(MenuAddRowProductSearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            //prepare page parameters
            searchModel.SetPopupGridPageSize();
            _baseAdminModelFactory.PrepareMenuColumns(searchModel.AvailableColumns, searchModel.MenuId, true, _localizationService.GetResource("Admin.Common.RowAsTitle"));

            //prepare available product types
            _baseAdminModelFactory.PrepareProductTypes(searchModel.AvailableProductTypes);

            return searchModel;
        }

        /// <summary>
        /// Prepare paged product list model to add menu product row.
        /// </summary>
        /// <param name="searchModel">Product search model to add menu product row.</param>
        /// <returns>Product list model to add to add menu product row.</returns>
        public MenuAddRowProductListModel PrepareMenuAddRowProductListModel(MenuAddRowProductSearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            //get products
            var products = _productService.SearchProducts(showHidden: true,
                productType: searchModel.SearchProductTypeId > 0 ? (ProductType?)searchModel.SearchProductTypeId : null,
                keywords: searchModel.SearchProductName,
                pageIndex: searchModel.Page - 1, pageSize: searchModel.PageSize);

            //prepare grid model
            var model = new MenuAddRowProductListModel
            {
                //fill in model values from the entity
                Data = products.Select(product => product.ToModel<ProductModel>()),
                Total = products.TotalCount
            };

            return model;
        }

        /// <summary>
        /// Prepare element search model to add menu element row.
        /// </summary>
        /// <param name="searchModel">Element search model to add menu element row</param>
        /// <returns>Element search model to add menu element row</returns>
        public MenuAddRowElementSearchModel PrepareAddRowElementSearchModel(MenuAddRowElementSearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            //prepare page parameters
            searchModel.SetPopupGridPageSize();
            _baseAdminModelFactory.PrepareMenuColumns(searchModel.AvailableColumns, searchModel.MenuId, true, _localizationService.GetResource("Admin.Common.RowAsTitle"));

            //prepare available product types
            _baseAdminModelFactory.PrepareElementTypes(searchModel.AvailableElementTypes);

            return searchModel;
        }

        /// <summary>
        /// Prepare paged product list model to add menu element row.
        /// </summary>
        /// <param name="searchModel">Element search model to add menu element row.</param>
        /// <returns>Element list model to add to add menu element row.</returns>
        public MenuAddRowElementListModel PrepareMenuAddRowElementListModel(MenuAddRowElementSearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            //get elements
            var elements = _elementService.GetAllElements(
                elementType: searchModel.SearchElementTypeId > 0 ? (ElementType?)searchModel.SearchElementTypeId : null,
                elementName: searchModel.SearchElementName,
                pageIndex: searchModel.Page - 1,
                pageSize: searchModel.PageSize);

            //prepare grid model
            var model = new MenuAddRowElementListModel
            {
                Data = elements.Select(element =>
                {
                    //fill in model values from the entity
                    var elementModel = element.ToModel<ElementModel>();
                    return elementModel;
                }),
                Total = elements.TotalCount
            };

            return model;
        }

        /// <summary>
        /// Prepare topic search model to add menu topic row.
        /// </summary>
        /// <param name="searchModel">Topic search model to add menu topic row</param>
        /// <returns>Topic search model to add menu topic row</returns>
        public MenuAddRowTopicSearchModel PrepareAddRowTopicSearchModel(MenuAddRowTopicSearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            //prepare page parameters
            searchModel.SetPopupGridPageSize();
            _baseAdminModelFactory.PrepareMenuColumns(searchModel.AvailableColumns, searchModel.MenuId, true, _localizationService.GetResource("Admin.Common.RowAsTitle"));
            
            return searchModel;
        }

        /// <summary>
        /// Prepare paged topic list model to add menu topic row.
        /// </summary>
        /// <param name="searchModel">Element search model to add menu topic row.</param>
        /// <returns>Topic list model to add to add menu topic row.</returns>
        public MenuAddRowTopicListModel PrepareMenuAddRowTopicListModel(MenuAddRowTopicSearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            //get topics
            var topics = _topicService.GetAllTopics(searchModel.SearchKeywords, searchModel.Page - 1, searchModel.PageSize);

            //prepare grid model
            var model = new MenuAddRowTopicListModel
            {
                Data = topics.Select(topic =>
                {
                    //fill in model values from the entity
                    var topicModel = topic.ToModel<TopicModel>();
                    return topicModel;
                }),
                Total = topics.TotalCount
            };

            return model;
        }

        #endregion
    }
}