﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Media;
using Nop.Services.Catalog;
using Nop.Services.Localization;
using Nop.Services.Media;
using Nop.Services.Topics;
using Nop.Web.Areas.Admin.Infrastructure.Mapper.Extensions;
using Nop.Web.Areas.Admin.Models.Catalog;
using Nop.Web.Areas.Admin.Models.Topics;

namespace Nop.Web.Areas.Admin.Factories
{
    /// <summary>
    /// Represents the slider model factory implementation
    /// </summary>
    public partial class SliderModelFactory : ISliderModelFactory
    {
        #region Fields

        private readonly ISliderService _sliderService;
        private readonly ISliderItemService _sliderItemService;
        private readonly ILocalizationService _localizationService;
        private readonly IBaseAdminModelFactory _baseAdminModelFactory;
        private readonly ICategoryService _categoryService;
        private readonly IProductService _productService;
        private readonly IElementService _elementService;
        private readonly ITopicService _topicService;
        private readonly IPictureService _pictureService;

        #endregion

        #region Ctor

        public SliderModelFactory(ISliderService sliderService,
            ISliderItemService sliderItemService,
            ILocalizationService localizationService,
            IBaseAdminModelFactory baseAdminModelFactory,
            ICategoryService categoryService,
            IProductService productService,
            IElementService elementService,
            ITopicService topicService,
            IPictureService pictureService)
        {
            _sliderService = sliderService;
            _sliderItemService = sliderItemService;
            _localizationService = localizationService;
            _baseAdminModelFactory = baseAdminModelFactory;
            _categoryService = categoryService;
            _productService = productService;
            _elementService = elementService;
            _topicService = topicService;
            _pictureService = pictureService;
        }

        #endregion

        #region Utilities

        private IList<SliderShortModel> PrepareSliders()
        {
            return _sliderService.GetAll().OrderByDescending(tab => tab.CreatedOnUtc).Select(tab =>
                new SliderShortModel
                {
                    Id = tab.Id,
                    Name = tab.Name
                }).ToList();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Prepare slider edit model
        /// </summary>
        /// <returns>Slider model</returns>
        public SliderEditModel PrepareSliderEditModel()
        {
            var model = new SliderEditModel();

            model.Sliders = PrepareSliders();

            return model;
        }

        /// <summary>
        /// Prepare slider model
        /// </summary>
        /// <param name="model">Slider model</param>
        /// <param name="slider">Slider</param>
        /// <returns>Slider model</returns>
        public SliderModel PrepareSliderModel(SliderModel model, Slider slider)
        {
            if (slider != null)
            {
                //fill in model values from the entity
                model = model ?? slider.ToModel<SliderModel>();
            }

            return model;
        }

        /// <summary>
        /// Prepare paged slider item list model
        /// </summary>
        /// <param name="searchModel">Slider item search model</param>
        /// <returns>SliderItem list model</returns>
        public SliderItemListModel PrepareSliderItemListModel(SliderItemSearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            //get items
            var items = _sliderItemService.GetColumnsHeadersBySliderId(searchModel.SliderId);

            //prepare grid model
            var model = new SliderItemListModel
            {
                Data = items.Select(sliderItem =>
                {
                    //fill in model values from the entity
                    var sliderItemModel = sliderItem.ToModel<SliderItemModel>();

                    sliderItemModel.ItemName = GetSliderItemName(sliderItem);
                    sliderItemModel.ItemColor = string.IsNullOrWhiteSpace(sliderItem.Color) ? null : sliderItem.Color;
                    PrepareSliderPicture(sliderItemModel, sliderItem);
                    //fill in additional values (not existing in the entity)
                    return sliderItemModel;
                }),
                Total = items.TotalCount
            };

            return model;
        }

        /// <summary>
        /// Prepare paged slider item row list model
        /// </summary>
        /// <param name="searchModel">Slider item row search model</param>
        /// <returns>SliderItem row list model</returns>
        public SliderItemListModel PrepareSliderItemRowListModel(SliderItemRowSearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            var column = _sliderItemService.GetSliderItemById(searchModel.SliderItemId);
            if (column == null)
                throw new ArgumentNullException(nameof(column));

            //get items
            var items = _sliderItemService.GetRowsBySliderItem(column);

            //prepare grid model
            var model = new SliderItemListModel
            {
                Data = items.Select(sliderItem =>
                {
                    //fill in model values from the entity
                    var sliderItemModel = sliderItem.ToModel<SliderItemModel>();

                    sliderItemModel.ItemName = GetSliderItemName(sliderItem);
                    sliderItemModel.ItemColor = string.IsNullOrWhiteSpace(sliderItem.Color) ? null : sliderItem.Color;
                    PrepareSliderPicture(sliderItemModel, sliderItem);
                    //fill in additional values (not existing in the entity)
                    return sliderItemModel;
                }),
                Total = items.TotalCount
            };

            return model;
        }

        private void PrepareSliderPicture(SliderItemModel sliderItemModel, SliderItem sliderItem)
        {
            var picture = sliderItem.Picture;

            if (picture == null)
            {
                switch (sliderItem.RowConnectionType)
                {
                    case RowConnectionType.category:
                        picture = _pictureService.GetPluginDefaultByCategoryId(sliderItem.RowConnectionId.Value, PictureKind.Slider);
                        break;
                    case RowConnectionType.product:
                        picture = _pictureService.GetPluginDefaultByProductId(sliderItem.RowConnectionId.Value, PictureKind.Slider);
                        break;
                }
            }

            if (picture != null)
            {
                sliderItemModel.ItemPicture.ItemPictureId = (sliderItem.Picture == null) ? -1 : picture.Id;
                sliderItemModel.ItemPicture.ItemPictureOriginalName = (sliderItem.Picture == null) ? "default" : picture.OriginalName ?? string.Empty;
                sliderItemModel.ItemPicture.ItemPictureUrl = _pictureService.GetPictureUrl(picture, 75);
            }
        }

        private string GetItemPictureUrl(SliderItem sliderItem)
        {
            var pictureId = sliderItem.PictureId;

            if (!pictureId.HasValue)
            {
                switch (sliderItem.RowConnectionType)
                {
                    case RowConnectionType.category:
                        pictureId = _pictureService.GetPluginDefaultByCategoryId(sliderItem.RowConnectionId.Value, PictureKind.Slider).Id;
                        break;
                    case RowConnectionType.product:
                        pictureId = _pictureService.GetPluginDefaultByProductId(sliderItem.RowConnectionId.Value, PictureKind.Slider).Id;
                        break;
                }
            }

            if (pictureId.GetValueOrDefault() > 0)
            {
                return _pictureService.GetPictureUrl(pictureId.GetValueOrDefault(), 75);
            }
            else
            {
                return string.Empty;
            }
        }

        private string GetSliderItemName(SliderItem sliderItem)
        {
            switch (sliderItem.RowConnectionType)
            {
                case RowConnectionType.category:
                    return _categoryService.GetCategoryById(sliderItem.RowConnectionId.GetValueOrDefault())?.ShortName;
                case RowConnectionType.product:
                    return _productService.GetProductById(sliderItem.RowConnectionId.GetValueOrDefault())?.Name;
                case RowConnectionType.element:
                    return _elementService.GetElementById(sliderItem.RowConnectionId.GetValueOrDefault())?.Name;
                case RowConnectionType.topic:
                    return _topicService.GetTopicById(sliderItem.RowConnectionId.GetValueOrDefault())?.SystemName;
                default:
                    return string.Empty;
            }
        }

        /// <summary>
        /// Prepare slider item model
        /// </summary>
        /// <param name="model">Slider item model</param>
        /// <returns>Slider item model</returns>
        public SliderItemModel PrepareSliderItemModel(SliderItemModel model)
        {
            model.ItemRowConnectionType = RowConnectionType.category;
            switch (model.ItemRowType)
            {
                case RowType.header:
                    model.ItemCssClass = "sl_header";
                    break;
                case RowType.textcolumn:
                    model.ItemCssClass = "sl_column_title";
                    break;
                case RowType.imagecolumn:
                    model.ItemCssClass = "sl_column_image";
                    break;
            }

            return model;
        }

        /// <summary>
        /// Prepare category search model to add slider category row.
        /// </summary>
        /// <param name="searchModel">Category search model to add slider category row</param>
        /// <returns>Category search model to add slider category row</returns>
        public SliderAddRowCategorySearchModel PrepareAddRowCategorySearchModel(SliderAddRowCategorySearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            //prepare page parameters
            searchModel.SetPopupGridPageSize();
            _baseAdminModelFactory.PrepareSliderColumns(searchModel.AvailableColumns, searchModel.SliderId, true, _localizationService.GetResource("Admin.Common.RowAsTitle"));

            return searchModel;
        }

        /// <summary>
        /// Prepare paged category list model to add slider category row.
        /// </summary>
        /// <param name="searchModel">Category search model to add slider category row.</param>
        /// <returns>Category list model to add to add slider category row.</returns>
        public SliderAddRowCategoryListModel PrepareSliderAddRowCategoryListModel(SliderAddRowCategorySearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            //get categories
            var categories = _categoryService.GetAllCategories(showHidden: true,
                categoryName: searchModel.SearchCategoryName,
                pageIndex: searchModel.Page - 1, pageSize: searchModel.PageSize);

            //prepare grid model
            var model = new SliderAddRowCategoryListModel
            {
                Data = categories.Select(category =>
                {
                    //fill in model values from the entity
                    var categoryModel = category.ToModel<CategoryModel>();

                    //fill in additional values (not existing in the entity)
                    categoryModel.Breadcrumb = _categoryService.GetFormattedBreadCrumb(category);

                    return categoryModel;
                }),
                Total = categories.TotalCount
            };

            return model;
        }

        /// <summary>
        /// Prepare product search model to add slider product row.
        /// </summary>
        /// <param name="searchModel">Product search model to add slider product row</param>
        /// <returns>Product search model to add slider product row</returns>
        public SliderAddRowProductSearchModel PrepareAddRowProductSearchModel(SliderAddRowProductSearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            //prepare page parameters
            searchModel.SetPopupGridPageSize();
            _baseAdminModelFactory.PrepareSliderColumns(searchModel.AvailableColumns, searchModel.SliderId, true, _localizationService.GetResource("Admin.Common.RowAsTitle"));

            //prepare available product types
            _baseAdminModelFactory.PrepareProductTypes(searchModel.AvailableProductTypes);

            return searchModel;
        }

        /// <summary>
        /// Prepare paged product list model to add slider product row.
        /// </summary>
        /// <param name="searchModel">Product search model to add slider product row.</param>
        /// <returns>Product list model to add to add slider product row.</returns>
        public SliderAddRowProductListModel PrepareSliderAddRowProductListModel(SliderAddRowProductSearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            //get products
            var products = _productService.SearchProducts(showHidden: true,
                productType: searchModel.SearchProductTypeId > 0 ? (ProductType?)searchModel.SearchProductTypeId : null,
                keywords: searchModel.SearchProductName,
                pageIndex: searchModel.Page - 1, pageSize: searchModel.PageSize);

            //prepare grid model
            var model = new SliderAddRowProductListModel
            {
                //fill in model values from the entity
                Data = products.Select(product => product.ToModel<ProductModel>()),
                Total = products.TotalCount
            };

            return model;
        }

        /// <summary>
        /// Prepare element search model to add slider element row.
        /// </summary>
        /// <param name="searchModel">Element search model to add slider element row</param>
        /// <returns>Element search model to add slider element row</returns>
        public SliderAddRowElementSearchModel PrepareAddRowElementSearchModel(SliderAddRowElementSearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            //prepare page parameters
            searchModel.SetPopupGridPageSize();
            _baseAdminModelFactory.PrepareSliderColumns(searchModel.AvailableColumns, searchModel.SliderId, true, _localizationService.GetResource("Admin.Common.RowAsTitle"));

            //prepare available product types
            _baseAdminModelFactory.PrepareElementTypes(searchModel.AvailableElementTypes);

            return searchModel;
        }

        /// <summary>
        /// Prepare paged product list model to add slider element row.
        /// </summary>
        /// <param name="searchModel">Element search model to add slider element row.</param>
        /// <returns>Element list model to add to add slider element row.</returns>
        public SliderAddRowElementListModel PrepareSliderAddRowElementListModel(SliderAddRowElementSearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            //get elements
            var elements = _elementService.GetAllElements(
                elementType: searchModel.SearchElementTypeId > 0 ? (ElementType?)searchModel.SearchElementTypeId : null,
                elementName: searchModel.SearchElementName,
                pageIndex: searchModel.Page - 1,
                pageSize: searchModel.PageSize);

            //prepare grid model
            var model = new SliderAddRowElementListModel
            {
                Data = elements.Select(element =>
                {
                    //fill in model values from the entity
                    var elementModel = element.ToModel<ElementModel>();
                    return elementModel;
                }),
                Total = elements.TotalCount
            };

            return model;
        }

        /// <summary>
        /// Prepare topic search model to add slider topic row.
        /// </summary>
        /// <param name="searchModel">Topic search model to add slider topic row</param>
        /// <returns>Topic search model to add slider topic row</returns>
        public SliderAddRowTopicSearchModel PrepareAddRowTopicSearchModel(SliderAddRowTopicSearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            //prepare page parameters
            searchModel.SetPopupGridPageSize();
            _baseAdminModelFactory.PrepareSliderColumns(searchModel.AvailableColumns, searchModel.SliderId, true, _localizationService.GetResource("Admin.Common.RowAsTitle"));
            
            return searchModel;
        }

        /// <summary>
        /// Prepare paged topic list model to add slider topic row.
        /// </summary>
        /// <param name="searchModel">Element search model to add slider topic row.</param>
        /// <returns>Topic list model to add to add slider topic row.</returns>
        public SliderAddRowTopicListModel PrepareSliderAddRowTopicListModel(SliderAddRowTopicSearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            //get topics
            var topics = _topicService.GetAllTopics(searchModel.SearchKeywords, searchModel.Page - 1, searchModel.PageSize);

            //prepare grid model
            var model = new SliderAddRowTopicListModel
            {
                Data = topics.Select(topic =>
                {
                    //fill in model values from the entity
                    var topicModel = topic.ToModel<TopicModel>();
                    return topicModel;
                }),
                Total = topics.TotalCount
            };

            return model;
        }

        /// <summary>
        /// Get picture list.
        /// </summary>
        /// <param name="connectionId">Connection identifier.</param>
        /// <param name="connectionType">Connection type</param>
        /// <returns>Get short picture list.</returns>
        public IList<PictureShortModel> GetAvailablePicture(int connectionId, RowConnectionType connectionType)
        {
            var pictures = _pictureService.GetAvailablePictureForPlugin(connectionId, connectionType, PictureKind.Slider);
            var model = new List<PictureShortModel>();

            _baseAdminModelFactory.AddDefaultPicture(model, connectionId, connectionType, PictureKind.Slider);

            foreach (var picture in pictures)
            {
                model.Add(new PictureShortModel
                {
                    ItemPictureId = picture.Id,
                    ItemPictureOriginalName = picture.OriginalName ?? string.Empty,
                    ItemPictureUrl = _pictureService.GetPictureUrl(picture, 75)
                });
            }

            return model;
        }

        #endregion
    }
}