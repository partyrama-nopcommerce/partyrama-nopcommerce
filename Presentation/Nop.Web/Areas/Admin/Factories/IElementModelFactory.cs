﻿using Nop.Core.Domain.Catalog;
using Nop.Web.Areas.Admin.Models.Catalog;

namespace Nop.Web.Areas.Admin.Factories
{
    /// <summary>
    /// Represents the element model factory
    /// </summary>
    public partial interface IElementModelFactory
    {
        /// <summary>
        /// Prepare element search model
        /// </summary>
        /// <param name="searchModel">Element search model</param>
        /// <returns>Element search model</returns>
        ElementSearchModel PrepareElementSearchModel(ElementSearchModel searchModel);

        /// <summary>
        /// Prepare paged element list model
        /// </summary>
        /// <param name="searchModel">Element search model</param>
        /// <returns>Element list model</returns>
        ElementListModel PrepareElementListModel(ElementSearchModel searchModel);

        /// <summary>
        /// Prepare element model
        /// </summary>
        /// <param name="model">Element model</param>
        /// <param name="element">Element</param>
        /// <param name="excludeProperties">Whether to exclude populating of some properties of model</param>
        /// <returns>Element model</returns>
        ElementModel PrepareElementModel(ElementModel model, Element element, bool excludeProperties = false);
    }
}