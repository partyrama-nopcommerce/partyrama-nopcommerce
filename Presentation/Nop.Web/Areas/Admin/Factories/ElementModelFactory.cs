﻿using System;
using System.Linq;
using Nop.Core.Domain.Catalog;
using Nop.Services.Catalog;
using Nop.Services.Localization;
using Nop.Services.Media;
using Nop.Web.Areas.Admin.Infrastructure.Mapper.Extensions;
using Nop.Web.Areas.Admin.Models.Catalog;
using Nop.Web.Framework.Factories;

namespace Nop.Web.Areas.Admin.Factories
{
    /// <summary>
    /// Represents the element model factory implementation
    /// </summary>
    public partial class ElementModelFactory : IElementModelFactory
    {
        #region Fields

        private readonly IBaseAdminModelFactory _baseAdminModelFactory;
        private readonly IElementService _elementService;
        private readonly IPictureService _pictureService;
        private readonly ILocalizationService _localizationService;
        private readonly ILocalizedModelFactory _localizedModelFactory;

        #endregion

        #region Ctor

        public ElementModelFactory(
            IBaseAdminModelFactory baseAdminModelFactory,
            IElementService elementService,
            IPictureService pictureService,
            ILocalizationService localizationService,
            ILocalizedModelFactory localizedModelFactory)
        {
            _baseAdminModelFactory = baseAdminModelFactory;
            _elementService = elementService;
            _pictureService = pictureService;
            _localizationService = localizationService;
            _localizedModelFactory = localizedModelFactory;
        }

        #endregion

        #region Utilities

        #endregion

        #region Methods

        /// <summary>
        /// Prepare element search model
        /// </summary>
        /// <param name="searchModel">Element search model</param>
        /// <returns>Element search model</returns>
        public ElementSearchModel PrepareElementSearchModel(ElementSearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            //prepare available product types
            _baseAdminModelFactory.PrepareElementTypes(searchModel.AvailableElementTypes);

            //prepare page parameters
            searchModel.SetGridPageSize();

            return searchModel;
        }

        /// <summary>
        /// Prepare paged element list model
        /// </summary>
        /// <param name="searchModel">Element search model</param>
        /// <returns>Element list model</returns>
        public ElementListModel PrepareElementListModel(ElementSearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            //get elements
            var elements = _elementService.GetAllElements(
                elementType: searchModel.SearchElementTypeId > 0 ? (ElementType?)searchModel.SearchElementTypeId : null,
                elementName: searchModel.SearchElementName,
                pageIndex: searchModel.Page - 1,
                pageSize: searchModel.PageSize);

            //prepare grid model
            var model = new ElementListModel
            {
                Data = elements.Select(element =>
                {
                    //fill in model values from the entity
                    var elementModel = element.ToModel<ElementModel>();
                    elementModel.PictureThumbnailUrl = _pictureService.GetPictureUrl(element.PictureId, 75);

                    return elementModel;
                }),
                Total = elements.TotalCount
            };

            return model;
        }

        /// <summary>
        /// Prepare element model
        /// </summary>
        /// <param name="model">Element model</param>
        /// <param name="element">Element</param>
        /// <param name="excludeProperties">Whether to exclude populating of some properties of model</param>
        /// <returns>Element model</returns>
        public ElementModel PrepareElementModel(ElementModel model, Element element, bool excludeProperties = false)
        {
            Action<ElementLocalizedModel, int> localizedModelConfiguration = null;

            if (element != null)
            {
                //fill in model values from the entity
                model = model ?? element.ToModel<ElementModel>();

                //define localized model configuration action
                localizedModelConfiguration = (locale, languageId) =>
                {
                    locale.Name = _localizationService.GetLocalized(element, entity => entity.Name, languageId, false, false);
                    locale.Description = _localizationService.GetLocalized(element, entity => entity.Description, languageId, false, false);
                    locale.ShortName = _localizationService.GetLocalized(element, entity => entity.ShortName, languageId, false, false);
                };
            }

            //prepare localized models
            if (!excludeProperties)
                model.Locales = _localizedModelFactory.PrepareLocalizedModels(localizedModelConfiguration);

            return model;
        }

        #endregion
    }
}