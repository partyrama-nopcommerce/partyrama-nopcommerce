﻿using Nop.Core.Domain.Catalog;
using Nop.Web.Areas.Admin.Models.Catalog;

namespace Nop.Web.Areas.Admin.Factories
{
    /// <summary>
    /// Represents the menu model factory
    /// </summary>
    public partial interface IMenuModelFactory
    {
        /// <summary>
        /// Prepare menu edit model
        /// </summary>
        /// <returns>Mega menu model</returns>
        MenuEditModel PrepareMenuEditModel();

        /// <summary>
        /// Prepare menu model
        /// </summary>
        /// <param name="model">Menu model</param>
        /// <param name="menu">Menu</param>
        /// <returns>Menu model</returns>
        MenuModel PrepareMenuModel(MenuModel model, Menu menu);

        /// <summary>
        /// Prepare paged menu item list model
        /// </summary>
        /// <param name="searchModel">Menu item search model</param>
        /// <returns>MenuItem list model</returns>
        MenuItemListModel PrepareMenuItemListModel(MenuItemSearchModel searchModel);

        /// <summary>
        /// Prepare paged menu item row list model
        /// </summary>
        /// <param name="searchModel">Menu item row search model</param>
        /// <returns>MenuItem row list model</returns>
        MenuItemListModel PrepareMenuItemRowListModel(MenuItemRowSearchModel searchModel);
        
        /// <summary>
        /// Prepare menu item model
        /// </summary>
        /// <param name="model">Menu item model</param>
        /// <returns>Menu item model</returns>
        MenuItemModel PrepareMenuItemModel(MenuItemModel model);

        /// <summary>
        /// Prepare category search model to add menu category row.
        /// </summary>
        /// <param name="searchModel">Category search model to add menu category row</param>
        /// <returns>Category search model to add menu category row</returns>
        MenuAddRowCategorySearchModel PrepareAddRowCategorySearchModel(MenuAddRowCategorySearchModel searchModel);

        /// <summary>
        /// Prepare paged category list model to add menu category row.
        /// </summary>
        /// <param name="searchModel">Category search model to add menu category row.</param>
        /// <returns>Category list model to add to add menu category row.</returns>
        MenuAddRowCategoryListModel PrepareMenuAddRowCategoryListModel(MenuAddRowCategorySearchModel searchModel);

        /// <summary>
        /// Prepare product search model to add menu product row.
        /// </summary>
        /// <param name="searchModel">Product search model to add menu product row</param>
        /// <returns>Product search model to add menu product row</returns>
        MenuAddRowProductSearchModel PrepareAddRowProductSearchModel(MenuAddRowProductSearchModel searchModel);

        /// <summary>
        /// Prepare paged product list model to add menu product row.
        /// </summary>
        /// <param name="searchModel">Product search model to add menu product row.</param>
        /// <returns>Product list model to add to add menu product row.</returns>
        MenuAddRowProductListModel PrepareMenuAddRowProductListModel(MenuAddRowProductSearchModel searchModel);

        /// <summary>
        /// Prepare element search model to add menu element row.
        /// </summary>
        /// <param name="searchModel">Element search model to add menu element row</param>
        /// <returns>Element search model to add menu element row</returns>
        MenuAddRowElementSearchModel PrepareAddRowElementSearchModel(MenuAddRowElementSearchModel searchModel);

        /// <summary>
        /// Prepare paged product list model to add menu element row.
        /// </summary>
        /// <param name="searchModel">Element search model to add menu element row.</param>
        /// <returns>Element list model to add to add menu element row.</returns>
        MenuAddRowElementListModel PrepareMenuAddRowElementListModel(MenuAddRowElementSearchModel searchModel);

        /// <summary>
        /// Prepare topic search model to add menu topic row.
        /// </summary>
        /// <param name="searchModel">Topic search model to add menu topic row</param>
        /// <returns>Topic search model to add menu topic row</returns>
        MenuAddRowTopicSearchModel PrepareAddRowTopicSearchModel(MenuAddRowTopicSearchModel searchModel);

        /// <summary>
        /// Prepare paged topic list model to add menu topic row.
        /// </summary>
        /// <param name="searchModel">Element search model to add menu topic row.</param>
        /// <returns>Topic list model to add to add menu topic row.</returns>
        MenuAddRowTopicListModel PrepareMenuAddRowTopicListModel(MenuAddRowTopicSearchModel searchModel);
    }
}