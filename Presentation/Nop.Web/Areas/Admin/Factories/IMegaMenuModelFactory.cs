﻿using Nop.Core.Domain.Catalog;
using Nop.Web.Areas.Admin.Models.Catalog;
using System.Collections.Generic;

namespace Nop.Web.Areas.Admin.Factories
{
    /// <summary>
    /// Represents the mega menu model factory
    /// </summary>
    public partial interface IMegaMenuModelFactory
    {
        /// <summary>
        /// Prepare mega menu model
        /// </summary>
        /// <returns>Mega menu model</returns>
        MegaMenuModel PrepareMegaMenuModel();

        /// <summary>
        /// Prepare mega menu tab model
        /// </summary>
        /// <param name="model">Mega menu tab model</param>
        /// <param name="megaMenuTab">Mega menu tab</param>
        /// <returns>Mega menu tab model</returns>
        MegaMenuTabModel PrepareMegaMenuTabModel(MegaMenuTabModel model, MegaMenuTab megaMenuTab);

        /// <summary>
        /// Prepare paged mega menu tab item list model
        /// </summary>
        /// <param name="searchModel">Mega menu tab item search model</param>
        /// <returns>MegaMenuTabItem list model</returns>
        MegaMenuTabItemListModel PrepareMegaMenuTabItemListModel(MegaMenuTabItemSearchModel searchModel);

        /// <summary>
        /// Prepare paged mega menu tab item row list model
        /// </summary>
        /// <param name="searchModel">Mega menu tab item row search model</param>
        /// <returns>MegaMenuTabItem row list model</returns>
        MegaMenuTabItemListModel PrepareMegaMenuTabItemRowListModel(MegaMenuTabItemRowSearchModel searchModel);
        
        /// <summary>
        /// Prepare mega menu tab item model
        /// </summary>
        /// <param name="model">Mega menu tab item model</param>
        /// <returns>Mega menu tab item model</returns>
        MegaMenuTabItemModel PrepareMegaMenuTabItemModel(MegaMenuTabItemModel model);

        /// <summary>
        /// Prepare category search model to add mega menu category row.
        /// </summary>
        /// <param name="searchModel">Category search model to add mega menu category row</param>
        /// <returns>Category search model to add mega menu category row</returns>
        MegaMenuAddRowCategorySearchModel PrepareAddRowCategorySearchModel(MegaMenuAddRowCategorySearchModel searchModel);

        /// <summary>
        /// Prepare paged category list model to add mega menu category row.
        /// </summary>
        /// <param name="searchModel">Category search model to add mega menu category row.</param>
        /// <returns>Category list model to add to add mega menu category row.</returns>
        MegaMenuAddRowCategoryListModel PrepareMegaMenuAddRowCategoryListModel(MegaMenuAddRowCategorySearchModel searchModel);

        /// <summary>
        /// Prepare product search model to add mega menu product row.
        /// </summary>
        /// <param name="searchModel">Product search model to add mega menu product row</param>
        /// <returns>Product search model to add mega menu product row</returns>
        MegaMenuAddRowProductSearchModel PrepareAddRowProductSearchModel(MegaMenuAddRowProductSearchModel searchModel);

        /// <summary>
        /// Prepare paged product list model to add mega menu product row.
        /// </summary>
        /// <param name="searchModel">Product search model to add mega menu product row.</param>
        /// <returns>Product list model to add to add mega menu product row.</returns>
        MegaMenuAddRowProductListModel PrepareMegaMenuAddRowProductListModel(MegaMenuAddRowProductSearchModel searchModel);

        /// <summary>
        /// Prepare element search model to add mega menu element row.
        /// </summary>
        /// <param name="searchModel">Element search model to add mega menu element row</param>
        /// <returns>Element search model to add mega menu element row</returns>
        MegaMenuAddRowElementSearchModel PrepareAddRowElementSearchModel(MegaMenuAddRowElementSearchModel searchModel);

        /// <summary>
        /// Prepare paged element list model to add mega menu element row.
        /// </summary>
        /// <param name="searchModel">Element search model to add mega menu element row.</param>
        /// <returns>Element list model to add to add mega menu element row.</returns>
        MegaMenuAddRowElementListModel PrepareMegaMenuAddRowElementListModel(MegaMenuAddRowElementSearchModel searchModel);

        /// <summary>
        /// Prepare topic search model to add mega menu topic row.
        /// </summary>
        /// <param name="searchModel">Topic search model to add mega menu topic row</param>
        /// <returns>Topic search model to add mega menu topic row</returns>
        MegaMenuAddRowTopicSearchModel PrepareAddRowTopicSearchModel(MegaMenuAddRowTopicSearchModel searchModel);

        /// <summary>
        /// Prepare paged topic list model to add mega menu topic row.
        /// </summary>
        /// <param name="searchModel">Element search model to add mega menu topic row.</param>
        /// <returns>Topic list model to add to add mega menu topic row.</returns>
        MegaMenuAddRowTopicListModel PrepareMegaMenuAddRowTopicListModel(MegaMenuAddRowTopicSearchModel searchModel);

        /// <summary>
        /// Prepare slider search model to add mega menu category row.
        /// </summary>
        /// <param name="searchModel">Slider search model to add mega menu slider row</param>
        /// <returns>Slider search model to add mega menu slider row</returns>
        MegaMenuAddRowSliderSearchModel PrepareAddRowSliderSearchModel(MegaMenuAddRowSliderSearchModel searchModel);

        /// <summary>
        /// Prepare paged slider list model to add mega menu slider row.
        /// </summary>
        /// <param name="searchModel">Slider search model to add mega menu slider row.</param>
        /// <returns>Slider list model to add to add mega menu slider row.</returns>
        MegaMenuAddRowSliderListModel PrepareMegaMenuAddRowSliderListModel(MegaMenuAddRowSliderSearchModel searchModel);

        /// <summary>
        /// Get picture list.
        /// </summary>
        /// <param name="connectionId">Connection identifier.</param>
        /// <param name="connectionType">Connection type</param>
        /// <returns>Get short picture list.</returns>
        IList<PictureShortModel> GetAvailablePicture(int connectionId, RowConnectionType connectionType);
    }
}