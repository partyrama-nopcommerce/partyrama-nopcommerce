﻿using Nop.Core.Domain.Catalog;
using Nop.Web.Areas.Admin.Models.Catalog;
using System.Collections.Generic;

namespace Nop.Web.Areas.Admin.Factories
{
    /// <summary>
    /// Represents the connection model factory
    /// </summary>
    public partial interface IConnectionModelFactory
    {
        /// <summary>
        /// Prepare paged connection list model
        /// </summary>
        /// <param name="searchModel">Connection search model</param>
        /// <param name="category">Category</param>
        /// <param name="product">Product</param>
        /// <returns>Connection list model</returns>
        ConnectionListModel PrepareConnectionListModel(ConnectionsSearchModel searchModel, Category category = null, Product product = null);

        /// <summary>
        /// Checks if the connection is with category or product
        /// </summary>
        /// <param name="type">Connection from type</param>
        /// <returns>If true: connection to category, otherwise: connection to product</returns>
        bool IsConnectionToCategory(ConnectionFromType fromType);

        /// <summary>
        /// Prepare product search model to add to the category or to the product.
        /// </summary>
        /// <param name="searchModel">Product search model to add to the category or to the product</param>
        /// <returns>Product search model to add to the category or to the product</returns>
        ConnectionAddProductSearchModel PrepareAddProductSearchModel(ConnectionAddProductSearchModel searchModel);

        /// <summary>
        /// Prepare paged product list model to add to the category or to the product.
        /// </summary>
        /// <param name="searchModel">Product search model to add to the category or to the product.</param>
        /// <returns>Product list model to add to the category or to the product.</returns>
        ConnectionAddProductListModel PrepareConnectionAddProductListModel(ConnectionAddProductSearchModel searchModel);

        /// <summary>
        /// Prepare category search model to add to the category or to the product.
        /// </summary>
        /// <param name="searchModel">Category search model to add to the category or to the product</param>
        /// <returns>Category search model to add to the category or to the product</returns>
        ConnectionAddCategorySearchModel PrepareAddCategorySearchModel(ConnectionAddCategorySearchModel searchModel);

        /// <summary>
        /// Prepare paged category list model to add to the category or to the product.
        /// </summary>
        /// <param name="searchModel">Category search model to add to the category or to the product.</param>
        /// <returns>Category list model to add to the category or to the product.</returns>
        ConnectionAddCategoryListModel PrepareConnectionAddCategoryListModel(ConnectionAddCategorySearchModel searchModel);

        /// <summary>
        /// Prepare element search model to add to the category or to the product.
        /// </summary>
        /// <param name="searchModel">Element search model to add to the category or to the product</param>
        /// <returns>Element search model to add to the category or to the product</returns>
        ConnectionAddElementSearchModel PrepareAddElementSearchModel(ConnectionAddElementSearchModel searchModel);

        /// <summary>
        /// Prepare paged element list model to add to the category or to the product.
        /// </summary>
        /// <param name="searchModel">Element search model to add to the category or to the product.</param>
        /// <returns>Element list model to add to the category or to the product.</returns>
        ConnectionAddElementListModel PrepareConnectionAddElementListModel(ConnectionAddElementSearchModel searchModel);

        /// <summary>
        /// Prepare slider search model to add to the category or to the product.
        /// </summary>
        /// <param name="searchModel">Slider search model to add to the category or to the product</param>
        /// <returns>Slider search model to add to the category or to the product</returns>
        ConnectionAddSliderSearchModel PrepareAddSliderSearchModel(ConnectionAddSliderSearchModel searchModel);

        /// <summary>
        /// Prepare paged slider list model to add to the category or to the product.
        /// </summary>
        /// <param name="searchModel">Slider search model to add to the category or to the product.</param>
        /// <returns>Slider list model to add to the category or to the product.</returns>
        ConnectionAddSliderListModel PrepareConnectionAddSliderListModel(ConnectionAddSliderSearchModel searchModel);

        /// <summary>
        /// Get picture list.
        /// </summary>
        /// <param name="toId">Connection to identifier.</param>
        /// <param name="toType">Connection to type</param>
        /// <returns>Get short picture list.</returns>
        IList<PictureShortModel> GetAvailablePicture(int toId, ConnectionToType toType);
    }
}