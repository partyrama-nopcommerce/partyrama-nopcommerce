﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Media;
using Nop.Services.Catalog;
using Nop.Services.Media;
using Nop.Web.Areas.Admin.Infrastructure.Mapper.Extensions;
using Nop.Web.Areas.Admin.Models.Catalog;

namespace Nop.Web.Areas.Admin.Factories
{
    /// <summary>
    /// Represents the connection model factory implementation
    /// </summary>
    public partial class ConnectionModelFactory : IConnectionModelFactory
    {
        #region Fields

        private readonly IConnectionsService _connectionsService;
        private readonly IProductService _productService;
        private readonly ICategoryService _categoryService;
        private readonly IPictureService _pictureService;
        private readonly IBaseAdminModelFactory _baseAdminModelFactory;
        private readonly IElementService _elementService;
        private readonly ISliderService _sliderService;

        #endregion

        #region Ctor

        public ConnectionModelFactory(IConnectionsService connectionsService,
            IProductService productService,
            ICategoryService categoryService,
            IPictureService pictureService,
            IBaseAdminModelFactory baseAdminModelFactory,
            IElementService elementService,
            ISliderService sliderService)
        {
            _categoryService = categoryService;
            _connectionsService = connectionsService;
            _productService = productService;
            _pictureService = pictureService;
            _baseAdminModelFactory = baseAdminModelFactory;
            _elementService = elementService;
            _sliderService = sliderService;
        }

        #endregion

        #region Utilities

        #endregion

        #region Methods

        /// <summary>
        /// Prepare paged connection list model
        /// </summary>
        /// <param name="searchModel">Connection search model</param>
        /// <param name="category">Category</param>
        /// <param name="product">Product</param>
        /// <returns>Connection list model</returns>
        public ConnectionListModel PrepareConnectionListModel(ConnectionsSearchModel searchModel, Category category = null, Product product = null)
        {
            var dateStart = DateTime.Now;
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            if (category == null && product == null)
                throw new ArgumentNullException($"{nameof(category)} and {nameof(product)}");

            IPagedList<Connections> connections;
            if (category != null)
            {
                connections = _connectionsService.GetConnectionsByCategoryId(category.Id,
                    pageIndex: searchModel.Page - 1,
                    pageSize: searchModel.PageSize);
            }
            else
            {
                connections = _connectionsService.GetConnectionsByProductId(product.Id,
                    pageIndex: searchModel.Page - 1,
                    pageSize: searchModel.PageSize);
            }

            var catConnections = _categoryService.GetCategoriesByIds(connections.Where(cc => cc.ConnectionToType == ConnectionToType.cat).Select(cc => cc.To).ToArray());
            var proConnections = _productService.GetProductsByIds(connections.Where(cc => cc.ConnectionToType == ConnectionToType.prod).Select(cc => cc.To).ToArray());
            var elConnections = _elementService.GetElementsByIds(connections.Where(cc => cc.ConnectionToType == ConnectionToType.desc || cc.ConnectionToType == ConnectionToType.sep).Select(cc => cc.To).ToArray());
            var sliderConnections = _sliderService.GetSlidersByIds(connections.Where(cc => cc.ConnectionToType == ConnectionToType.slider).Select(cc => cc.To).ToArray());
            var proDefaultPictures = _pictureService.GetPluginDefaultByProductsIds(connections.Where(cc => cc.ConnectionToType == ConnectionToType.prod && cc.Picture == null).Select(cc => cc.To).ToArray(), PictureKind.Basic);
            var catDefaultPictures = _pictureService.GetPluginDefaultByCategoriesIds(connections.Where(cc => cc.ConnectionToType == ConnectionToType.cat && cc.Picture == null).Select(cc => cc.To).ToArray(), PictureKind.Basic);

            //prepare grid model
            var model = new ConnectionListModel
            {
                //fill in model values from the entity
                Data = connections.Select(conn =>
                {
                    var connectionModel = new ConnectionModel
                    {
                        Id = conn.Id,
                        CssClass = conn.CssClass ?? string.Empty,
                        DisplayOrder = conn.DisplayOrder,
                        FromId = conn.From,
                        ToId = conn.To,
                        ToType = conn.ConnectionToType,
                        FromType = conn.ConnectionFromType,
                        ShowDescription = conn.ShowDescription
                    };

                    var name = string.Empty;
                    switch (conn.ConnectionToType)
                    {
                        case ConnectionToType.cat:
                            name = catConnections.First(cc => cc.Id == conn.To).Name;
                            break;
                        case ConnectionToType.prod:
                            name = proConnections.First(cc => cc.Id == conn.To).Name;
                            break;
                        case ConnectionToType.sep:
                        case ConnectionToType.desc:
                            name = elConnections.First(cc => cc.Id == conn.To).Name;
                            break;
                        case ConnectionToType.slider:
                            name = sliderConnections.First(cc => cc.Id == conn.To).Name;
                            break;
                    }

                    connectionModel.Name = name;
                    PrepareConnectionPicture(connectionModel, conn, proDefaultPictures, catDefaultPictures);

                    return connectionModel;
                }),
                Total = connections.TotalCount
            };
            return model;
        }

        private void PrepareConnectionPicture(ConnectionModel connectionModel, Connections conn, Dictionary<int, Picture> proPictures, Dictionary<int, Picture> catPictures)
        {
            var picture = conn.Picture;

            if (picture == null)
            {
                switch (conn.ConnectionToType)
                {
                    case ConnectionToType.cat:
                        picture = catPictures.GetValueOrDefault(conn.To);
                        break;
                    case ConnectionToType.prod:
                        picture = proPictures.GetValueOrDefault(conn.To);
                        break;
                }
            }

            if (picture != null)
            {
                connectionModel.Picture.ItemPictureId = (conn.Picture == null) ? -1 : picture.Id;
                connectionModel.Picture.ItemPictureOriginalName = (conn.Picture == null) ? "default" : picture.OriginalName ?? string.Empty;
                connectionModel.Picture.ItemPictureUrl = _pictureService.GetPictureUrl(picture, 75);
            }
        }

        /// <summary>
        /// Checks if the connection is with category or product
        /// </summary>
        /// <param name="type">Connection from type</param>
        /// <returns>If true: connection to category, otherwise: connection to product</returns>
        public bool IsConnectionToCategory(ConnectionFromType fromType)
        {
            return fromType == ConnectionFromType.cat;
        }

        /// <summary>
        /// Prepare product search model to add to the category or to the product
        /// </summary>
        /// <param name="searchModel">Product search model to add to the category or to the product</param>
        /// <returns>Product search model to add to the category or to the product</returns>
        public ConnectionAddProductSearchModel PrepareAddProductSearchModel(ConnectionAddProductSearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            //prepare available product types
            _baseAdminModelFactory.PrepareProductTypes(searchModel.AvailableProductTypes);

            //prepare page parameters
            searchModel.SetPopupGridPageSize();

            return searchModel;
        }

        /// <summary>
        /// Prepare paged product list model to add to the category or to the product.
        /// </summary>
        /// <param name="searchModel">Product search model to add to the category or to the product.</param>
        /// <returns>Product list model to add to the category or to the product.</returns>
        public ConnectionAddProductListModel PrepareConnectionAddProductListModel(ConnectionAddProductSearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            //get products
            var products = _productService.SearchProducts(showHidden: true,
                productType: searchModel.SearchProductTypeId > 0 ? (ProductType?)searchModel.SearchProductTypeId : null,
                keywords: searchModel.SearchProductName,
                pageIndex: searchModel.Page - 1, pageSize: searchModel.PageSize);

            //prepare grid model
            var model = new ConnectionAddProductListModel
            {
                //fill in model values from the entity
                Data = products.Select(product => product.ToModel<ProductModel>()),
                Total = products.TotalCount
            };

            return model;
        }

        /// <summary>
        /// Prepare category search model to add to the category or to the product.
        /// </summary>
        /// <param name="searchModel">Category search model to add to the category or to the product</param>
        /// <returns>Category search model to add to the category or to the product</returns>
        public ConnectionAddCategorySearchModel PrepareAddCategorySearchModel(ConnectionAddCategorySearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            //prepare page parameters
            searchModel.SetPopupGridPageSize();

            return searchModel;
        }

        /// <summary>
        /// Prepare paged category list model to add to the category or to the product.
        /// </summary>
        /// <param name="searchModel">Category search model to add to the category or to the product.</param>
        /// <returns>Category list model to add to the category or to the product.</returns>
        public ConnectionAddCategoryListModel PrepareConnectionAddCategoryListModel(ConnectionAddCategorySearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            //get categories
            var categories = _categoryService.GetAllCategories(showHidden: true,
                categoryName: searchModel.SearchCategoryName,
                pageIndex: searchModel.Page - 1, pageSize: searchModel.PageSize);

            //prepare grid model
            var model = new ConnectionAddCategoryListModel
            {
                Data = categories.Select(category =>
                {
                    //fill in model values from the entity
                    var categoryModel = category.ToModel<CategoryModel>();

                    //fill in additional values (not existing in the entity)
                    categoryModel.Breadcrumb = _categoryService.GetFormattedBreadCrumb(category);

                    return categoryModel;
                }),
                Total = categories.TotalCount
            };

            return model;
        }

        /// <summary>
        /// Prepare element search model to add to the category or to the product.
        /// </summary>
        /// <param name="searchModel">Element search model to add to the category or to the product</param>
        /// <returns>Element search model to add to the category or to the product</returns>
        public ConnectionAddElementSearchModel PrepareAddElementSearchModel(ConnectionAddElementSearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            //prepare page parameters
            searchModel.SetPopupGridPageSize();

            return searchModel;
        }

        /// <summary>
        /// Prepare paged element list model to add to the category or to the product.
        /// </summary>
        /// <param name="searchModel">Element search model to add to the category or to the product.</param>
        /// <returns>Element list model to add to the category or to the product.</returns>
        public ConnectionAddElementListModel PrepareConnectionAddElementListModel(ConnectionAddElementSearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            var elementType = ElementType.Separator;
            if (searchModel.ToType == ConnectionToType.desc)
            {
                elementType = ElementType.Description;
            }

            //get elements
            var elements = _elementService.GetAllElements(
                elementType: elementType, 
                elementName: searchModel.SearchElementName,
                pageIndex: searchModel.Page - 1, 
                pageSize: searchModel.PageSize);

            //prepare grid model
            var model = new ConnectionAddElementListModel
            {
                Data = elements.Select(element =>
                {
                    //fill in model values from the entity
                    var elementModel = element.ToModel<ElementModel>();
                    return elementModel;
                }),
                Total = elements.TotalCount
            };

            return model;
        }

        /// <summary>
        /// Prepare slider search model to add to the category or to the product.
        /// </summary>
        /// <param name="searchModel">Slider search model to add to the category or to the product</param>
        /// <returns>Slider search model to add to the category or to the product</returns>
        public ConnectionAddSliderSearchModel PrepareAddSliderSearchModel(ConnectionAddSliderSearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            //prepare page parameters
            searchModel.SetPopupGridPageSize();

            return searchModel;
        }

        /// <summary>
        /// Prepare paged slider list model to add to the category or to the product.
        /// </summary>
        /// <param name="searchModel">Slider search model to add to the category or to the product.</param>
        /// <returns>Slider list model to add to the category or to the product.</returns>
        public ConnectionAddSliderListModel PrepareConnectionAddSliderListModel(ConnectionAddSliderSearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            //get sliders
            var sliders = _sliderService.GetAllSliders(showHidden: true,
                sliderName: searchModel.SearchSliderName,
                pageIndex: searchModel.Page - 1, pageSize: searchModel.PageSize);

            //prepare grid model
            var model = new ConnectionAddSliderListModel
            {
                Data = sliders.Select(slider =>
                {
                    //fill in model values from the entity
                    var sliderModel = slider.ToModel<SliderModel>();

                    return sliderModel;
                }),
                Total = sliders.TotalCount
            };

            return model;
        }

        /// <summary>
        /// Get picture list.
        /// </summary>
        /// <param name="toId">Connection to identifier.</param>
        /// <param name="toType">Connection to type</param>
        /// <returns>Get short picture list.</returns>
        public IList<PictureShortModel> GetAvailablePicture(int toId, ConnectionToType toType)
        {
            var pictures = _pictureService.GetAvailablePictureForConnection(toId, toType);
            var model = new List<PictureShortModel>();
            _baseAdminModelFactory.AddDefaultPicture(model, toId, toType);

            foreach (var picture in pictures)
            {
                model.Add(new PictureShortModel
                {
                    ItemPictureId = picture.Id,
                    ItemPictureOriginalName = picture.OriginalName ?? string.Empty,
                    ItemPictureUrl = _pictureService.GetPictureUrl(picture, 75)
                });
            }

            return model;
        }

        #endregion
    }
}