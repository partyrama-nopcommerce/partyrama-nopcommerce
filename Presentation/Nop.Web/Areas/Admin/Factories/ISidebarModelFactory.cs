﻿using Nop.Core.Domain.Catalog;
using Nop.Web.Areas.Admin.Models.Catalog;
using System.Collections.Generic;

namespace Nop.Web.Areas.Admin.Factories
{
    /// <summary>
    /// Represents the sidebar model factory
    /// </summary>
    public partial interface ISidebarModelFactory
    {
        /// <summary>
        /// Prepare sidebar edit model
        /// </summary>
        /// <returns>Sidebar model</returns>
        SidebarEditModel PrepareSidebarEditModel();

        /// <summary>
        /// Prepare sidebar model
        /// </summary>
        /// <param name="model">Sidebar model</param>
        /// <param name="sidebar">Sidebar</param>
        /// <returns>Sidebar model</returns>
        SidebarModel PrepareSidebarModel(SidebarModel model, Sidebar sidebar);

        /// <summary>
        /// Prepare paged sidebar item list model
        /// </summary>
        /// <param name="searchModel">Sidebar item search model</param>
        /// <returns>SidebarItem list model</returns>
        SidebarItemListModel PrepareSidebarItemListModel(SidebarItemSearchModel searchModel);

        /// <summary>
        /// Prepare paged sidebar item row list model
        /// </summary>
        /// <param name="searchModel">Sidebar item row search model</param>
        /// <returns>SidebarItem row list model</returns>
        SidebarItemListModel PrepareSidebarItemRowListModel(SidebarItemRowSearchModel searchModel);

        /// <summary>
        /// Prepare sidebar item model
        /// </summary>
        /// <param name="model">Sidebar item model</param>
        /// <returns>Sidebar item model</returns>
        SidebarItemModel PrepareSidebarItemModel(SidebarItemModel model);

        /// <summary>
        /// Prepare category search model to add sidebar category row.
        /// </summary>
        /// <param name="searchModel">Category search model to add sidebar category row</param>
        /// <returns>Category search model to add sidebar category row</returns>
        SidebarAddRowCategorySearchModel PrepareAddRowCategorySearchModel(SidebarAddRowCategorySearchModel searchModel);

        /// <summary>
        /// Prepare paged category list model to add sidebar category row.
        /// </summary>
        /// <param name="searchModel">Category search model to add sidebar category row.</param>
        /// <returns>Category list model to add to add sidebar category row.</returns>
        SidebarAddRowCategoryListModel PrepareSidebarAddRowCategoryListModel(SidebarAddRowCategorySearchModel searchModel);

        /// <summary>
        /// Prepare product search model to add sidebar product row.
        /// </summary>
        /// <param name="searchModel">Product search model to add sidebar product row</param>
        /// <returns>Product search model to add sidebar product row</returns>
        SidebarAddRowProductSearchModel PrepareAddRowProductSearchModel(SidebarAddRowProductSearchModel searchModel);

        /// <summary>
        /// Prepare paged product list model to add sidebar product row.
        /// </summary>
        /// <param name="searchModel">Product search model to add sidebar product row.</param>
        /// <returns>Product list model to add to add sidebar product row.</returns>
        SidebarAddRowProductListModel PrepareSidebarAddRowProductListModel(SidebarAddRowProductSearchModel searchModel);

        /// <summary>
        /// Prepare element search model to add sidebar element row.
        /// </summary>
        /// <param name="searchModel">Element search model to add sidebar element row</param>
        /// <returns>Element search model to add sidebar element row</returns>
        SidebarAddRowElementSearchModel PrepareAddRowElementSearchModel(SidebarAddRowElementSearchModel searchModel);

        /// <summary>
        /// Prepare paged product list model to add sidebar element row.
        /// </summary>
        /// <param name="searchModel">Element search model to add sidebar element row.</param>
        /// <returns>Element list model to add to add sidebar element row.</returns>
        SidebarAddRowElementListModel PrepareSidebarAddRowElementListModel(SidebarAddRowElementSearchModel searchModel);

        /// <summary>
        /// Prepare topic search model to add sidebar topic row.
        /// </summary>
        /// <param name="searchModel">Topic search model to add sidebar topic row</param>
        /// <returns>Topic search model to add sidebar topic row</returns>
        SidebarAddRowTopicSearchModel PrepareAddRowTopicSearchModel(SidebarAddRowTopicSearchModel searchModel);

        /// <summary>
        /// Prepare paged topic list model to add sidebar topic row.
        /// </summary>
        /// <param name="searchModel">Element search model to add sidebar topic row.</param>
        /// <returns>Topic list model to add to add sidebar topic row.</returns>
        SidebarAddRowTopicListModel PrepareSidebarAddRowTopicListModel(SidebarAddRowTopicSearchModel searchModel);

        /// <summary>
        /// Prepare slider search model to add sidebar category row.
        /// </summary>
        /// <param name="searchModel">Slider search model to add sidebar slider row</param>
        /// <returns>Slider search model to add sidebar slider row</returns>
        SidebarAddRowSliderSearchModel PrepareAddRowSliderSearchModel(SidebarAddRowSliderSearchModel searchModel);

        /// <summary>
        /// Prepare paged slider list model to add sidebar slider row.
        /// </summary>
        /// <param name="searchModel">Slider search model to add sidebar slider row.</param>
        /// <returns>Slider list model to add to add sidebar slider row.</returns>
        SidebarAddRowSliderListModel PrepareSidebarAddRowSliderListModel(SidebarAddRowSliderSearchModel searchModel);

        /// <summary>
        /// Get picture list.
        /// </summary>
        /// <param name="connectionId">Connection identifier.</param>
        /// <param name="connectionType">Connection type</param>
        /// <returns>Get short picture list.</returns>
        IList<PictureShortModel> GetAvailablePicture(int connectionId, RowConnectionType connectionType);
    }
}