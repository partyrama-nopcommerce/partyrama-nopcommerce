﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Areas.Admin.Infrastructure.Cache;
using Nop.Core.Caching;
using Nop.Services.Catalog;
using Nop.Services.Vendors;
using Nop.Services.Topics;

namespace Nop.Web.Areas.Admin.Helpers
{
    /// <summary>
    /// Select list helper
    /// </summary>
    public static class SelectListHelper
    {
        /// <summary>
        /// Get category list
        /// </summary>
        /// <param name="categoryService">Category service</param>
        /// <param name="cacheManager">Cache manager</param>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>Category list</returns>
        public static List<SelectListItem> GetCategoryList(ICategoryService categoryService, ICacheManager cacheManager, bool showHidden = false)
        {
            if (categoryService == null)
                throw new ArgumentNullException(nameof(categoryService));

            if (cacheManager == null)
                throw new ArgumentNullException(nameof(cacheManager));

            var cacheKey = string.Format(ModelCacheEventConsumer.CATEGORIES_LIST_KEY, showHidden);
            var listItems = cacheManager.Get(cacheKey, () =>
            {
                var categories = categoryService.GetAllCategories(showHidden: showHidden);
                return categories.Select(c => new SelectListItem
                {
                    Text = categoryService.GetFormattedBreadCrumb(c, categories),
                    Value = c.Id.ToString()
                });
            });

            var result = new List<SelectListItem>();
            //clone the list to ensure that "selected" property is not set
            foreach (var item in listItems)
            {
                result.Add(new SelectListItem
                {
                    Text = item.Text,
                    Value = item.Value
                });
            }

            return result;
        }

        /// <summary>
        /// Get manufacturer list
        /// </summary>
        /// <param name="manufacturerService">Manufacturer service</param>
        /// <param name="cacheManager">Cache manager</param>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>Manufacturer list</returns>
        public static List<SelectListItem> GetManufacturerList(IManufacturerService manufacturerService, ICacheManager cacheManager, bool showHidden = false)
        {
            if (manufacturerService == null)
                throw new ArgumentNullException(nameof(manufacturerService));

            if (cacheManager == null)
                throw new ArgumentNullException(nameof(cacheManager));

            var cacheKey = string.Format(ModelCacheEventConsumer.MANUFACTURERS_LIST_KEY, showHidden);
            var listItems = cacheManager.Get(cacheKey, () =>
            {
                var manufacturers = manufacturerService.GetAllManufacturers(showHidden: showHidden);
                return manufacturers.Select(m => new SelectListItem
                {
                    Text = m.Name,
                    Value = m.Id.ToString()
                });
            });

            var result = new List<SelectListItem>();
            //clone the list to ensure that "selected" property is not set
            foreach (var item in listItems)
            {
                result.Add(new SelectListItem
                {
                    Text = item.Text,
                    Value = item.Value
                });
            }

            return result;
        }

        /// <summary>
        /// Get vendor list
        /// </summary>
        /// <param name="vendorService">Vendor service</param>
        /// <param name="cacheManager">Cache manager</param>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>Vendor list</returns>
        public static List<SelectListItem> GetVendorList(IVendorService vendorService, ICacheManager cacheManager, bool showHidden = false)
        {
            if (vendorService == null)
                throw new ArgumentNullException(nameof(vendorService));

            if (cacheManager == null)
                throw new ArgumentNullException(nameof(cacheManager));

            var cacheKey = string.Format(ModelCacheEventConsumer.VENDORS_LIST_KEY, showHidden);
            var listItems = cacheManager.Get(cacheKey, () =>
            {
                var vendors = vendorService.GetAllVendors(showHidden: showHidden);
                return vendors.Select(v => new SelectListItem
                {
                    Text = v.Name,
                    Value = v.Id.ToString()
                });
            });

            var result = new List<SelectListItem>();
            //clone the list to ensure that "selected" property is not set
            foreach (var item in listItems)
            {
                result.Add(new SelectListItem
                {
                    Text = item.Text,
                    Value = item.Value
                });
            }

            return result;
        }

        /// <summary>
        /// Get mega menu column list
        /// </summary>
        /// <param name="megaMenuItemService">Mega menu item service</param>
        /// <param name="cacheManager">Cache manager</param>
        /// <param name="megaMenuTabId">Mega menu tab identifier</param>
        /// <returns>Category list</returns>
        public static List<SelectListItem> GetMegaMenuColumnsList(IMegaMenuTabItemService megaMenuItemService, ICategoryService categoryService, ICacheManager cacheManager, int megaMenuTabId)
        {
            if (megaMenuItemService == null)
                throw new ArgumentNullException(nameof(megaMenuItemService));

            if (cacheManager == null)
                throw new ArgumentNullException(nameof(cacheManager));

            var cacheKey = string.Format(ModelCacheEventConsumer.MEGAMENU_COLUMN_LIST_KEY, megaMenuTabId);
            var listItems = cacheManager.Get(cacheKey, () =>
            {
                var columns = megaMenuItemService.GetColumnsByMegaMenuTabId(megaMenuTabId);
                var categoryNames = new Dictionary<int, string>();
                foreach (var column in columns)
                {
                    categoryNames.Add(column.Id, categoryService.GetCategoryById(column.RowConnectionId.GetValueOrDefault())?.Name);
                }

                return columns.Select(c => new SelectListItem
                {
                    Text = categoryNames[c.Id],
                    Value = c.Id.ToString()
                });
            });

            var result = new List<SelectListItem>();
            //clone the list to ensure that "selected" property is not set
            foreach (var item in listItems)
            {
                result.Add(new SelectListItem
                {
                    Text = item.Text,
                    Value = item.Value
                });
            }

            return result;
        }

        /// <summary>
        /// Get menu column list
        /// </summary>
        /// <param name="menuItemService">Menu item service</param>
        /// <param name="cacheManager">Cache manager</param>
        /// <param name="menuId">Menu identifier</param>
        /// <returns>Menu item list</returns>
        public static List<SelectListItem> GetMenuColumnsList(IMenuItemService menuItemService, ICategoryService categoryService, ICacheManager cacheManager, int menuId)
        {
            if (menuItemService == null)
                throw new ArgumentNullException(nameof(menuItemService));

            if (cacheManager == null)
                throw new ArgumentNullException(nameof(cacheManager));

            var cacheKey = string.Format(ModelCacheEventConsumer.MENU_COLUMN_LIST_KEY, menuId);
            var listItems = cacheManager.Get(cacheKey, () =>
            {
                var columns = menuItemService.GetColumnsByMenuId(menuId);
                var categoryNames = new Dictionary<int, string>();
                foreach (var column in columns)
                {
                    categoryNames.Add(column.Id, categoryService.GetCategoryById(column.RowConnectionId.GetValueOrDefault())?.Name);
                }

                return columns.Select(c => new SelectListItem
                {
                    Text = categoryNames[c.Id],
                    Value = c.Id.ToString()
                });
            });

            var result = new List<SelectListItem>();
            //clone the list to ensure that "selected" property is not set
            foreach (var item in listItems)
            {
                result.Add(new SelectListItem
                {
                    Text = item.Text,
                    Value = item.Value
                });
            }

            return result;
        }

        /// <summary>
        /// Get sidebar column list
        /// </summary>
        /// <param name="sidebarItemService">Sidebar item service</param>
        /// <param name="cacheManager">Cache manager</param>
        /// <param name="sidebarId">Sidebar identifier</param>
        /// <returns>Sidebar item list</returns>
        public static List<SelectListItem> GetSidebarColumnsList(ISidebarItemService sidebarItemService, ICategoryService categoryService, ICacheManager cacheManager, int sidebarId)
        {
            if (sidebarItemService == null)
                throw new ArgumentNullException(nameof(sidebarItemService));

            if (cacheManager == null)
                throw new ArgumentNullException(nameof(cacheManager));

            var cacheKey = string.Format(ModelCacheEventConsumer.SIDEBAR_COLUMN_LIST_KEY, sidebarId);
            var listItems = cacheManager.Get(cacheKey, () =>
            {
                var columns = sidebarItemService.GetColumnsBySidebarId(sidebarId);
                var categoryNames = new Dictionary<int, string>();
                foreach (var column in columns)
                {
                    categoryNames.Add(column.Id, categoryService.GetCategoryById(column.RowConnectionId.GetValueOrDefault())?.Name);
                }

                return columns.Select(c => new SelectListItem
                {
                    Text = categoryNames[c.Id],
                    Value = c.Id.ToString()
                });
            });

            var result = new List<SelectListItem>();
            //clone the list to ensure that "selected" property is not set
            foreach (var item in listItems)
            {
                result.Add(new SelectListItem
                {
                    Text = item.Text,
                    Value = item.Value
                });
            }

            return result;
        }

        /// <summary>
        /// Get slider column list
        /// </summary>
        /// <param name="sliderItemService">Slider item service</param>
        /// <param name="cacheManager">Cache manager</param>
        /// <param name="sliderId">Slider identifier</param>
        /// <returns>Slider item list</returns>
        public static List<SelectListItem> GetSliderColumnsList(ISliderItemService sliderItemService, ICategoryService categoryService, ICacheManager cacheManager, int sliderId)
        {
            if (sliderItemService == null)
                throw new ArgumentNullException(nameof(sliderItemService));

            if (cacheManager == null)
                throw new ArgumentNullException(nameof(cacheManager));

            var cacheKey = string.Format(ModelCacheEventConsumer.SLIDER_COLUMN_LIST_KEY, sliderId);
            var listItems = cacheManager.Get(cacheKey, () =>
            {
                var columns = sliderItemService.GetColumnsBySliderId(sliderId);
                var categoryNames = new Dictionary<int, string>();
                foreach (var column in columns)
                {
                    categoryNames.Add(column.Id, categoryService.GetCategoryById(column.RowConnectionId.GetValueOrDefault())?.Name);
                }

                return columns.Select(c => new SelectListItem
                {
                    Text = categoryNames[c.Id],
                    Value = c.Id.ToString()
                });
            });

            var result = new List<SelectListItem>();
            //clone the list to ensure that "selected" property is not set
            foreach (var item in listItems)
            {
                result.Add(new SelectListItem
                {
                    Text = item.Text,
                    Value = item.Value
                });
            }

            return result;
        }

        /// <summary>
        /// Get home page column list
        /// </summary>
        /// <param name="homePageItemService">Home page item service</param>
        /// <param name="cacheManager">Cache manager</param>
        /// <param name="homePageId">Home page identifier</param>
        /// <returns>Home page item list</returns>
        public static List<SelectListItem> GetHomePageColumnsList(IHomePageItemService homePageItemService, ICategoryService categoryService, ICacheManager cacheManager, int homePageId)
        {
            if (homePageItemService == null)
                throw new ArgumentNullException(nameof(homePageItemService));

            if (cacheManager == null)
                throw new ArgumentNullException(nameof(cacheManager));

            var cacheKey = string.Format(ModelCacheEventConsumer.HOMEPAGE_COLUMN_LIST_KEY, homePageId);
            var listItems = cacheManager.Get(cacheKey, () =>
            {
                var columns = homePageItemService.GetColumnsByHomePageId(homePageId);
                var categoryNames = new Dictionary<int, string>();
                foreach (var column in columns)
                {
                    categoryNames.Add(column.Id, categoryService.GetCategoryById(column.RowConnectionId.GetValueOrDefault())?.Name);
                }

                return columns.Select(c => new SelectListItem
                {
                    Text = categoryNames[c.Id],
                    Value = c.Id.ToString()
                });
            });

            var result = new List<SelectListItem>();
            //clone the list to ensure that "selected" property is not set
            foreach (var item in listItems)
            {
                result.Add(new SelectListItem
                {
                    Text = item.Text,
                    Value = item.Value
                });
            }

            return result;
        }

        /// <summary>
        /// Get menu column list
        /// </summary>
        /// <param name="megaMenuTabService">Mega menu tab service</param>
        /// <param name="cacheManager">Cache manager</param>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>Mega menu tab list</returns>
        public static List<SelectListItem> GetMegaMenuTabList(IMegaMenuTabService megaMenuTabService, IStaticCacheManager cacheManager, bool showHidden = false)
        {
            if (megaMenuTabService == null)
                throw new ArgumentNullException(nameof(megaMenuTabService));

            if (cacheManager == null)
                throw new ArgumentNullException(nameof(cacheManager));

            var cacheKey = string.Format(ModelCacheEventConsumer.MEGAMENU_TAB_LIST_KEY, showHidden);
            var listItems = cacheManager.Get(cacheKey, () =>
            {
                var tabs = megaMenuTabService.GetAll(showHidden);

                return tabs.Select(tab => new SelectListItem
                {
                    Text = tab.Name,
                    Value = tab.Id.ToString()
                });
            });

            var result = new List<SelectListItem>();
            //clone the list to ensure that "selected" property is not set
            foreach (var item in listItems)
            {
                result.Add(new SelectListItem
                {
                    Text = item.Text,
                    Value = item.Value
                });
            }

            return result;
        }

        /// <summary>
        /// Get product  list
        /// </summary>
        /// <param name="productService">Product service</param>
        /// <param name="cacheManager">Cache manager</param>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>Element list</returns>
        public static List<SelectListItem> GetProductList(IProductService productService, ICacheManager cacheManager, bool showHidden = false)
        {
            if (productService == null)
                throw new ArgumentNullException(nameof(productService));

            if (cacheManager == null)
                throw new ArgumentNullException(nameof(cacheManager));

            var cacheKey = string.Format(ModelCacheEventConsumer.PRODUCTS_LIST_KEY, showHidden);
            var listItems = cacheManager.Get(cacheKey, () =>
            {
                var products = productService.GetAll(showHidden: showHidden);
                return products.Select(p => new SelectListItem
                {
                    Text = p.Name,
                    Value = p.Id.ToString()
                });
            });

            var result = new List<SelectListItem>();
            //clone the list to ensure that "selected" property is not set
            foreach (var item in listItems)
            {
                result.Add(new SelectListItem
                {
                    Text = item.Text,
                    Value = item.Value
                });
            }

            return result;
        }

        /// <summary>
        /// Get element  list
        /// </summary>
        /// <param name="elementService">Element service</param>
        /// <param name="cacheManager">Cache manager</param>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>Element list</returns>
        public static List<SelectListItem> GetElementList(IElementService elementService, ICacheManager cacheManager, bool showHidden = false)
        {
            if (elementService == null)
                throw new ArgumentNullException(nameof(elementService));

            if (cacheManager == null)
                throw new ArgumentNullException(nameof(cacheManager));

            var cacheKey = string.Format(ModelCacheEventConsumer.ELEMENTS_LIST_KEY, showHidden);
            var listItems = cacheManager.Get(cacheKey, () =>
            {
                var elements = elementService.GetAllElementsList(showHidden);
                return elements.Select(p => new SelectListItem
                {
                    Text = p.Name,
                    Value = p.Id.ToString()
                });
            });

            var result = new List<SelectListItem>();
            //clone the list to ensure that "selected" property is not set
            foreach (var item in listItems)
            {
                result.Add(new SelectListItem
                {
                    Text = item.Text,
                    Value = item.Value
                });
            }

            return result;
        }

        /// <summary>
        /// Get topic  list
        /// </summary>
        /// <param name="topicService">Topic service</param>
        /// <param name="cacheManager">Cache manager</param>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>Element list</returns>
        public static List<SelectListItem> GetTopicList(ITopicService topicService, IStaticCacheManager cacheManager, bool showHidden = false)
        {
            if (topicService == null)
                throw new ArgumentNullException(nameof(topicService));

            if (cacheManager == null)
                throw new ArgumentNullException(nameof(cacheManager));

            var cacheKey = string.Format(ModelCacheEventConsumer.TOPICS_LIST_KEY, showHidden);
            var listItems = cacheManager.Get(cacheKey, () =>
            {
                var topics = topicService.GetAllTopics();
                return topics.Select(p => new SelectListItem
                {
                    Text = p.SystemName,
                    Value = p.Id.ToString()
                });
            });

            var result = new List<SelectListItem>();
            //clone the list to ensure that "selected" property is not set
            foreach (var item in listItems)
            {
                result.Add(new SelectListItem
                {
                    Text = item.Text,
                    Value = item.Value
                });
            }

            return result;
        }

        /// <summary>
        /// Get sidebar  list
        /// </summary>
        /// <param name="sidebarService">Sidebar service</param>
        /// <param name="cacheManager">Cache manager</param>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>Sidebar list</returns>
        public static List<SelectListItem> GetSidebarList(ISidebarService sidebarService, IStaticCacheManager cacheManager, bool showHidden = false)
        {
            if (sidebarService == null)
                throw new ArgumentNullException(nameof(sidebarService));

            if (cacheManager == null)
                throw new ArgumentNullException(nameof(cacheManager));

            var cacheKey = string.Format(ModelCacheEventConsumer.SIDEBARS_LIST_KEY, showHidden);
            var listItems = cacheManager.Get(cacheKey, () =>
            {
                var sidebars = sidebarService.GetAll(showHidden);
                return sidebars.Select(s => new SelectListItem
                {
                    Text = s.Name,
                    Value = s.Id.ToString()
                });
            });

            var result = new List<SelectListItem>();
            //clone the list to ensure that "selected" property is not set
            foreach (var item in listItems)
            {
                result.Add(new SelectListItem
                {
                    Text = item.Text,
                    Value = item.Value
                });
            }

            return result;
        }
    }
}