﻿using FluentValidation;
using Nop.Web.Areas.Admin.Models.Catalog;
using Nop.Core.Domain.Catalog;
using Nop.Data;
using Nop.Services.Localization;
using Nop.Web.Framework.Validators;

namespace Nop.Web.Areas.Admin.Validators.Catalog
{
    public partial class SidebarItemValidator : BaseNopValidator<SidebarItemModel>
    {
        public SidebarItemValidator(ILocalizationService localizationService, IDbContext dbContext)
        {
            RuleFor(x => x.ItemCssClass).NotEmpty();
            RuleFor(x => x.ItemRowConnectionId).Must((x, context) =>
            {
                return x.ItemRowType == RowType.data || x.ItemRowConnectionId.GetValueOrDefault() > 0;
            }).WithMessage(localizationService.GetResource("Admin.Catalog.SidebarItem.Fields.ItemRowConnectionId.CategoryNotNull"));

            SetDatabaseValidationRules<SidebarItem>(dbContext);
        }
    }
}