﻿using FluentValidation;
using Nop.Web.Areas.Admin.Models.Catalog;
using Nop.Core.Domain.Catalog;
using Nop.Data;
using Nop.Services.Localization;
using Nop.Web.Framework.Validators;

namespace Nop.Web.Areas.Admin.Validators.Catalog
{
    public partial class ElementValidator : BaseNopValidator<ElementModel>
    {
        public ElementValidator(ILocalizationService localizationService, IDbContext dbContext)
        {
            RuleFor(x => x.Name).NotEmpty().Length(0, 400);
            RuleFor(x => x.CssClass).Length(0, 255);
            RuleFor(x => x.ShortName).Length(0, 255);

            SetDatabaseValidationRules<Element>(dbContext);
        }
    }
}