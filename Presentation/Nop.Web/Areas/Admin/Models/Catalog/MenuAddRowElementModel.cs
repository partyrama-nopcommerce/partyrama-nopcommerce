﻿using System.Collections.Generic;
using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    /// <summary>
    /// Represents an element model to add menu element row
    /// </summary> 
    public partial class MenuAddRowElementModel : BaseNopModel
    {
        #region Ctor

        public MenuAddRowElementModel()
        {
            SelectedElementIds = new List<int>();
        }
        #endregion

        #region Properties

        public int MenuId { get; set; }

        public int MenuItemId { get; set; }

        public IList<int> SelectedElementIds { get; set; }

        #endregion
    }
}