﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    /// <summary>
    /// Represents a product search model to add sidebar product row
    /// </summary>
    public partial class SidebarAddRowProductSearchModel : BaseSearchModel
    {
        #region Ctor

        public SidebarAddRowProductSearchModel()
        {
            AvailableProductTypes = new List<SelectListItem>();
            AvailableColumns = new List<SelectListItem>();
        }

        #endregion

        #region Properties

        [NopResourceDisplayName("Admin.Catalog.Products.List.SearchProductName")]
        public string SearchProductName { get; set; }

        [NopResourceDisplayName("Admin.Catalog.Products.List.SearchProductType")]
        public int SearchProductTypeId { get; set; }

        public IList<SelectListItem> AvailableProductTypes { get; set; }

        public int SidebarId { get; set; }

        [NopResourceDisplayName("Admin.Catalog.SidebarId.Column")]
        public int SidebarItemId { get; set; }

        public IList<SelectListItem> AvailableColumns { get; set; }

        #endregion
    }
}