﻿using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    /// <summary>
    /// Represents a menu item list model
    /// </summary>
    public partial class MenuItemListModel : BasePagedListModel<MenuItemModel>
    {
    }
}