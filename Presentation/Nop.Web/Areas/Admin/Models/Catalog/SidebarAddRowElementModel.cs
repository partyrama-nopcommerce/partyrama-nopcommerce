﻿using System.Collections.Generic;
using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    /// <summary>
    /// Represents an element model to add sidebar element row
    /// </summary> 
    public partial class SidebarAddRowElementModel : BaseNopModel
    {
        #region Ctor

        public SidebarAddRowElementModel()
        {
            SelectedElementIds = new List<int>();
        }
        #endregion

        #region Properties

        public int SidebarId { get; set; }

        public int SidebarItemId { get; set; }

        public IList<int> SelectedElementIds { get; set; }

        #endregion
    }
}