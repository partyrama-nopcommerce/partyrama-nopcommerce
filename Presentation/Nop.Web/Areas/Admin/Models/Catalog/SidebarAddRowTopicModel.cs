﻿using System.Collections.Generic;
using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    /// <summary>
    /// Represents a topic model to add sidebar topic row
    /// </summary> 
    public partial class SidebarAddRowTopicModel : BaseNopModel
    {
        #region Ctor

        public SidebarAddRowTopicModel()
        {
            SelectedTopicIds = new List<int>();
        }
        #endregion

        #region Properties

        public int SidebarId { get; set; }

        public int SidebarItemId { get; set; }

        public IList<int> SelectedTopicIds { get; set; }

        #endregion
    }
}