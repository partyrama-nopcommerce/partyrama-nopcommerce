﻿using Nop.Core.Domain.Catalog;
using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    /// <summary>
    /// Represents a slider search model to add to the category or the product
    /// </summary>
    public partial class ConnectionAddSliderSearchModel : BaseSearchModel
    {
        #region Properties

        [NopResourceDisplayName("Admin.Catalog.Categories.List.SearchSliderName")]
        public string SearchSliderName { get; set; }

        public ConnectionFromType FromType { get; set; }

        #endregion
    }
}