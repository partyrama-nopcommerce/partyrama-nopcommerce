﻿using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    /// <summary>
    /// Represents a slider list model to add home page topic row
    /// </summary>
    public partial class HomePageAddRowSliderListModel : BasePagedListModel<SliderModel>
    {
    }
}