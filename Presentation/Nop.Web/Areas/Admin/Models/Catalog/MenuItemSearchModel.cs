﻿using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    /// <summary>
    /// Represents a menu item search model
    /// </summary>
    public partial class MenuItemSearchModel : BaseSearchModel
    {
        #region Properties

        public int MenuId { get; set; }

        #endregion
    }
}