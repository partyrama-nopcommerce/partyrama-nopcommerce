﻿namespace Nop.Web.Areas.Admin.Models.Catalog
{
    /// <summary>
    /// Represents a reorder mega menu item model
    /// </summary>
    public partial class ReOrderMegaMenuItemModel
    {
        #region Properties

        public int OldDisplayOrder { get; set; }

        public int NewDisplayOrder { get; set; }

        public int DragItemId { get; set; }

        #endregion
    }
}