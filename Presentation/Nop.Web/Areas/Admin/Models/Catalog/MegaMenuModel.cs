﻿using Nop.Web.Framework.Models;
using System.Collections.Generic;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    public partial class MegaMenuModel : BaseNopEntityModel
    {
        #region Ctor

        public MegaMenuModel()
        {
            Tabs = new List<MegaMenuShortTabModel>();
        }

        #endregion

        #region Properties

        public MegaMenuTabModel NewTab { get; set; }
        public IList<MegaMenuShortTabModel> Tabs { get; set; }

        #endregion 
    }
}
