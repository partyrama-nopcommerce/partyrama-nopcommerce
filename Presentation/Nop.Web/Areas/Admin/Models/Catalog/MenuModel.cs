﻿using FluentValidation.Attributes;
using Nop.Core.Domain.Catalog;
using Nop.Web.Areas.Admin.Validators.Catalog;
using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;
using System;
using System.ComponentModel.DataAnnotations;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    /// <summary>
    /// Represents a menu model
    /// </summary>
    [Validator(typeof(MenuValidator))]
    public partial class MenuModel : BaseNopEntityModel
    {
        #region Ctor

        public MenuModel()
        {

        }

        #endregion

        #region Properties

        [NopResourceDisplayName("Admin.Catalog.MenuModel.Fields.Name")]
        public string Name { get; set; }

        [NopResourceDisplayName("Admin.Catalog.MenuModel.Fields.SetCountDown")]
        public bool SetCountDown { get; set; }

        [NopResourceDisplayName("Admin.Catalog.MenuModel.Fields.StartDateUtc")]
        [UIHint("DateTimeNullable")]
        public DateTime? StartDateUtc { get; set; }

        [NopResourceDisplayName("Admin.Catalog.MenuModel.Fields.EndDateUtc")]
        [UIHint("DateTimeNullable")]
        public DateTime? EndDateUtc { get; set; }

        [NopResourceDisplayName("Admin.Catalog.MenuModel.Fields.Visible")]
        public bool Visible { get; set; }

        public RowConnectionType TabConnectionType { get; set; }

        public string TabConnectionName { get; set; }

        public int? TabConnectionId { get; set; }

        #endregion
    }
}
