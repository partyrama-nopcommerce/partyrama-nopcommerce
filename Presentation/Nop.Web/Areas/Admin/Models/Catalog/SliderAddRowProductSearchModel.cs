﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    /// <summary>
    /// Represents a product search model to add slider product row
    /// </summary>
    public partial class SliderAddRowProductSearchModel : BaseSearchModel
    {
        #region Ctor

        public SliderAddRowProductSearchModel()
        {
            AvailableProductTypes = new List<SelectListItem>();
            AvailableColumns = new List<SelectListItem>();
        }

        #endregion

        #region Properties

        [NopResourceDisplayName("Admin.Catalog.Products.List.SearchProductName")]
        public string SearchProductName { get; set; }

        [NopResourceDisplayName("Admin.Catalog.Products.List.SearchProductType")]
        public int SearchProductTypeId { get; set; }

        public IList<SelectListItem> AvailableProductTypes { get; set; }

        public int SliderId { get; set; }

        [NopResourceDisplayName("Admin.Catalog.SliderId.Column")]
        public int SliderItemId { get; set; }

        public IList<SelectListItem> AvailableColumns { get; set; }

        #endregion
    }
}