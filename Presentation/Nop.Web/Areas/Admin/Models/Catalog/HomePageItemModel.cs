﻿using FluentValidation.Attributes;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Core.Domain.Catalog;
using Nop.Web.Areas.Admin.Validators.Catalog;
using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    /// <summary>
    /// Represents a home page item model
    /// </summary>
    [Validator(typeof(HomePageItemValidator))]
    public partial class HomePageItemModel : BaseNopEntityModel
    {
        #region Ctor

        public HomePageItemModel()
        {
            ItemPicture = new PictureShortModel();
        }

        #endregion

        [NopResourceDisplayName("Admin.Catalog.HomePageItem.Fields.CssClass")]
        public string ItemCssClass { get; set; }

        [NopResourceDisplayName("Admin.Catalog.HomePageItem.Fields.DisplayOrder")]
        public int ItemDisplayOrder { get; set; }

        [NopResourceDisplayName("Admin.Catalog.HomePageItem.Fields.HomePage")]
        public int ItemHomePageId { get; set; }

        [NopResourceDisplayName("Admin.Catalog.HomePageItem.Fields.Column")]
        public int ItemColumnId { get; set; }

        public RowType ItemRowType { get; set; }

        [NopResourceDisplayName("Admin.Catalog.HomePageItem.Fields.Color")]
        public string ItemColor { get; set; }

        [NopResourceDisplayName("Admin.Catalog.HomePageItem.Fields.Visible")]
        public bool ItemVisible { get; set; }

        [NopResourceDisplayName("Admin.Catalog.HomePageItem.Fields.StartDateUtc")]
        [UIHint("DateTimeNullable")]
        public DateTime? ItemStartDateUtc { get; set; }

        [NopResourceDisplayName("Admin.Catalog.HomePageItem.Fields.EndDateUtc")]
        [UIHint("DateTimeNullable")]
        public DateTime? ItemEndDateUtc { get; set; }

        [UIHint("Int32Nullable")]
        public int? ItemRowConnectionId { get; set; }

        [NopResourceDisplayName("Admin.Catalog.HomePageItem.Fields.ShowName")]
        public bool ItemShowName { get; set; }

        [NopResourceDisplayName("Admin.Catalog.HomePageItem.Fields.ShowDescription")]
        public bool ItemShowDescription { get; set; }

        [NopResourceDisplayName("Admin.Catalog.HomePageItem.Fields.ShowPicture")]
        public bool ItemShowPicture { get; set; }

        public RowConnectionType? ItemRowConnectionType { get; set; }

        public string ItemName { get; set; }

        public PictureShortModel ItemPicture { get; set; }

        public string ItemRowConnectionTypeName => ItemRowConnectionType.HasValue ? ItemRowConnectionType.Value.ToString(): string.Empty;

        public bool ItemAvailable => ItemVisible
                    && (!ItemStartDateUtc.HasValue || ItemStartDateUtc.Value <= DateTime.UtcNow)
                    && (!ItemEndDateUtc.HasValue || ItemEndDateUtc.Value > DateTime.UtcNow);
    }
}
