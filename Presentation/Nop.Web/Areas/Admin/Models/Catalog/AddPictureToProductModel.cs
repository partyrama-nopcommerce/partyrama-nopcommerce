﻿using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Media;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    /// <summary>
    /// Represents a picture model to add to the product
    /// </summary>
    public partial class AddPictureToProductModel
    {
        #region Properties

        public int PictureId { get; set; }
        public int DisplayOrder { get; set; }
        public string OverrideAltAttribute { get; set; }
        public string OverrideTitleAttribute { get; set; }
        public int ProductId { get; set; }
        public PictureKind PictureKind { get; set; }

        #endregion
    }
}