﻿using System.Collections.Generic;
using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    /// <summary>
    /// Represents a topic model to add menu topic row
    /// </summary> 
    public partial class MenuAddRowTopicModel : BaseNopModel
    {
        #region Ctor

        public MenuAddRowTopicModel()
        {
            SelectedTopicIds = new List<int>();
        }
        #endregion

        #region Properties

        public int MenuId { get; set; }

        public int MenuItemId { get; set; }

        public IList<int> SelectedTopicIds { get; set; }

        #endregion
    }
}