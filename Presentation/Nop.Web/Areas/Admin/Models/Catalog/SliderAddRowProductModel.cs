﻿using System.Collections.Generic;
using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    /// <summary>
    /// Represents a product model to add slider product row
    /// </summary> 
    public partial class SliderAddRowProductModel : BaseNopModel
    {
        #region Ctor

        public SliderAddRowProductModel()
        {
            SelectedProductIds = new List<int>();
        }
        #endregion

        #region Properties

        public int SliderId { get; set; }

        public int SliderItemId { get; set; }

        public IList<int> SelectedProductIds { get; set; }

        #endregion
    }
}