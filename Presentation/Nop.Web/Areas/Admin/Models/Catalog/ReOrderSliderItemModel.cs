﻿namespace Nop.Web.Areas.Admin.Models.Catalog
{
    /// <summary>
    /// Represents a reorder slider item model
    /// </summary>
    public partial class ReOrderSliderItemModel
    {
        #region Properties

        public int OldDisplayOrder { get; set; }

        public int NewDisplayOrder { get; set; }

        public int DragItemId { get; set; }

        #endregion
    }
}