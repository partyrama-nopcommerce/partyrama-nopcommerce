﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    /// <summary>
    /// Represents a topic search model to add mega menu topic row
    /// </summary>
    public partial class MegaMenuAddRowTopicSearchModel : BaseSearchModel
    {
        #region Ctor

        public MegaMenuAddRowTopicSearchModel()
        {
            AvailableColumns = new List<SelectListItem>();
        }

        #endregion

        #region Properties

        public int MegaMenuTabId { get; set; }

        [NopResourceDisplayName("Admin.Catalog.MegaMenuTabId.Column")]
        public int MegaMenuTabItemId { get; set; }

        [NopResourceDisplayName("Admin.ContentManagement.Topics.List.SearchKeywords")]
        public string SearchKeywords { get; set; }

        public IList<SelectListItem> AvailableColumns { get; set; }

        #endregion
    }
}