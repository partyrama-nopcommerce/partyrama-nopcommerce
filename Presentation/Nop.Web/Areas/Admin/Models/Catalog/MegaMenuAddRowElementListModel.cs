﻿using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    /// <summary>
    /// Represents an element list model to add mega menu element row
    /// </summary>
    public partial class MegaMenuAddRowElementListModel : BasePagedListModel<ElementModel>
    {
    }
}