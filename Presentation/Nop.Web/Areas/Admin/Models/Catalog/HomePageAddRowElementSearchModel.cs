﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    /// <summary>
    /// Represents an element search model to add home page element row
    /// </summary>
    public partial class HomePageAddRowElementSearchModel : BaseSearchModel
    {
        #region Ctor

        public HomePageAddRowElementSearchModel()
        {
            AvailableElementTypes = new List<SelectListItem>();
            AvailableColumns = new List<SelectListItem>();
        }

        #endregion

        #region Properties

        [NopResourceDisplayName("Admin.Catalog.Elements.List.SearchElementName")]
        public string SearchElementName { get; set; }

        [NopResourceDisplayName("Admin.Catalog.Elements.List.SearchElementType")]
        public int SearchElementTypeId { get; set; }

        public IList<SelectListItem> AvailableElementTypes { get; set; }

        public int HomePageId { get; set; }

        [NopResourceDisplayName("Admin.Catalog.HomePageId.Column")]
        public int HomePageItemId { get; set; }

        public IList<SelectListItem> AvailableColumns { get; set; }

        #endregion
    }
}