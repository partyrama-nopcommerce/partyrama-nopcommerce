﻿using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    public partial class MenuShortModel : BaseNopEntityModel
    {
        #region Ctor

        public MenuShortModel()
        {

        }

        #endregion

        #region Properties

        [NopResourceDisplayName("Admin.Catalog.Menu.Fields.Name")]
        public string Name { get; set; }

        #endregion 
    }
}
