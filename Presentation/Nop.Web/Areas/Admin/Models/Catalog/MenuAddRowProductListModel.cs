﻿using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    /// <summary>
    /// Represents a product list model to add menu product row
    /// </summary>
    public partial class MenuAddRowProductListModel : BasePagedListModel<ProductModel>
    {
    }
}