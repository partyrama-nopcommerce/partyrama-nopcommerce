﻿using System.Collections.Generic;
using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    /// <summary>
    /// Represents a product model to add menu product row
    /// </summary> 
    public partial class MenuAddRowProductModel : BaseNopModel
    {
        #region Ctor

        public MenuAddRowProductModel()
        {
            SelectedProductIds = new List<int>();
        }
        #endregion

        #region Properties

        public int MenuId { get; set; }

        public int MenuItemId { get; set; }

        public IList<int> SelectedProductIds { get; set; }

        #endregion
    }
}