﻿using Nop.Web.Framework.Models;
using System.Collections.Generic;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    public partial class SliderEditModel : BaseNopEntityModel
    {
        #region Ctor

        public SliderEditModel()
        {
            Sliders = new List<SliderShortModel>();
            NewSlider = new SliderModel { VisibleArrows = true };
        }

        #endregion

        #region Properties

        public SliderModel NewSlider { get; set; }
        public IList<SliderShortModel> Sliders { get; set; }

        #endregion 
    }
}
