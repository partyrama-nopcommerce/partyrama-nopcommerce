﻿using System.Collections.Generic;
using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    /// <summary>
    /// Represents an element model to add home page element row
    /// </summary> 
    public partial class HomePageAddRowElementModel : BaseNopModel
    {
        #region Ctor

        public HomePageAddRowElementModel()
        {
            SelectedElementIds = new List<int>();
        }
        #endregion

        #region Properties

        public int HomePageId { get; set; }

        public int HomePageItemId { get; set; }

        public IList<int> SelectedElementIds { get; set; }

        #endregion
    }
}