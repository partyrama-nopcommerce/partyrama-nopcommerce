﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Core.Domain.Catalog;
using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    /// <summary>
    /// Represents a mega menu search model to add mega menu topic row
    /// </summary>
    public partial class MegaMenuAddRowSliderSearchModel : BaseSearchModel
    {
        #region Ctor

        public MegaMenuAddRowSliderSearchModel()
        {
            AvailableColumns = new List<SelectListItem>();
        }

        #endregion

        #region Properties

        public int MegaMenuTabId { get; set; }

        [NopResourceDisplayName("Admin.Catalog.MegaMenuTabId.Column")]
        public int MegaMenuTabItemId { get; set; }

        public IList<SelectListItem> AvailableColumns { get; set; }

        [NopResourceDisplayName("Admin.Catalog.Categories.List.SearchSliderName")]
        public string SearchSliderName { get; set; }

        #endregion
    }
}