﻿using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    /// <summary>
    /// Represents a product list model to add mega menu product row
    /// </summary>
    public partial class MegaMenuAddRowProductListModel : BasePagedListModel<ProductModel>
    {
    }
}