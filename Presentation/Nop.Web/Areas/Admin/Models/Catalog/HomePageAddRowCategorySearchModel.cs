﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;
using System.Collections.Generic;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    /// <summary>
    /// Represents a category search model to add to the home page
    /// </summary>
    public partial class HomePageAddRowCategorySearchModel : BaseSearchModel
    {
        #region Ctor

        public HomePageAddRowCategorySearchModel()
        {
            AvailableColumns = new List<SelectListItem>();
        }

        #endregion

        #region Properties

        [NopResourceDisplayName("Admin.Catalog.Categories.List.SearchCategoryName")]
        public string SearchCategoryName { get; set; }

        public int HomePageId { get; set; }

        [NopResourceDisplayName("Admin.Catalog.HomePageId.Column")]
        public int HomePageItemId { get; set; }

        public IList<SelectListItem> AvailableColumns { get; set; }

        #endregion
    }
}