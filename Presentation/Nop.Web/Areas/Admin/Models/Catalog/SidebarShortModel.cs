﻿using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    public partial class SidebarShortModel : BaseNopEntityModel
    {
        #region Ctor

        public SidebarShortModel()
        {

        }

        #endregion

        #region Properties

        [NopResourceDisplayName("Admin.Catalog.Sidebar.Fields.Name")]
        public string Name { get; set; }

        #endregion 
    }
}
