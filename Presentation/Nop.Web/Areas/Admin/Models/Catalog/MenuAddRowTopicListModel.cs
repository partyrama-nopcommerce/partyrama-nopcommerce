﻿using Nop.Web.Areas.Admin.Models.Topics;
using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    /// <summary>
    /// Represents a topic list model to add menu topic row
    /// </summary>
    public partial class MenuAddRowTopicListModel : BasePagedListModel<TopicModel>
    {
    }
}