﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Core.Domain.Catalog;
using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    /// <summary>
    /// Represents an topic search model to add menu topic row
    /// </summary>
    public partial class MenuAddRowTopicSearchModel : BaseSearchModel
    {
        #region Ctor

        public MenuAddRowTopicSearchModel()
        {
            AvailableColumns = new List<SelectListItem>();
        }

        #endregion

        #region Properties

        public int MenuId { get; set; }

        [NopResourceDisplayName("Admin.Catalog.MegaMenuTabId.Column")]
        public int MenuItemId { get; set; }

        public IList<SelectListItem> AvailableColumns { get; set; }

        [NopResourceDisplayName("Admin.ContentManagement.Topics.List.SearchKeywords")]
        public string SearchKeywords { get; set; }

        #endregion
    }
}