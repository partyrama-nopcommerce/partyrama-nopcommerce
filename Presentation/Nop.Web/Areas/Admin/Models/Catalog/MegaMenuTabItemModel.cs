﻿using FluentValidation.Attributes;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Core.Domain.Catalog;
using Nop.Web.Areas.Admin.Validators.Catalog;
using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    /// <summary>
    /// Represents a mega menu tab item model
    /// </summary>
    [Validator(typeof(MegaMenuTabItemValidator))]
    public partial class MegaMenuTabItemModel : BaseNopEntityModel
    {
        #region Ctor

        public MegaMenuTabItemModel()
        {
            ItemPicture = new PictureShortModel();
        }

        #endregion

        [NopResourceDisplayName("Admin.Catalog.MegaMenuTabItem.Fields.CssClass")]
        public string ItemCssClass { get; set; }

        [NopResourceDisplayName("Admin.Catalog.MegaMenuTabItem.Fields.DisplayOrder")]
        public int ItemDisplayOrder { get; set; }

        [NopResourceDisplayName("Admin.Catalog.MegaMenuTabItem.Fields.MegaMenuTab")]
        public int ItemMegaMenuTabId { get; set; }

        [NopResourceDisplayName("Admin.Catalog.MegaMenuTabItem.Fields.Column")]
        public int ItemColumnId { get; set; }

        public RowType ItemRowType { get; set; }

        [NopResourceDisplayName("Admin.Catalog.MegaMenuTabItem.Fields.Color")]
        public string ItemColor { get; set; }

        [NopResourceDisplayName("Admin.Catalog.MegaMenuTabItem.Fields.Visible")]
        public bool ItemVisible { get; set; }

        [NopResourceDisplayName("Admin.Catalog.MegaMenuTabItem.Fields.StartDateUtc")]
        [UIHint("DateTimeNullable")]
        public DateTime? ItemStartDateUtc { get; set; }

        [NopResourceDisplayName("Admin.Catalog.MegaMenuTabItem.Fields.EndDateUtc")]
        [UIHint("DateTimeNullable")]
        public DateTime? ItemEndDateUtc { get; set; }

        [UIHint("Int32Nullable")]
        public int? ItemRowConnectionId { get; set; }

        public RowConnectionType? ItemRowConnectionType { get; set; }

        public bool ItemAvailable => ItemVisible
                    && (!ItemStartDateUtc.HasValue || ItemStartDateUtc.Value <= DateTime.UtcNow)
                    && (!ItemEndDateUtc.HasValue || ItemEndDateUtc.Value > DateTime.UtcNow);

        public string ItemName { get; set; }

        public PictureShortModel ItemPicture { get; set; }

        public RowType? ItemParentRowType { get; set; }

        public string ItemRowConnectionTypeName => ItemRowConnectionType.HasValue ? ItemRowConnectionType.Value.ToString(): string.Empty;
    }
}
