﻿using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    /// <summary>
    /// Represents a sidebar item list model
    /// </summary>
    public partial class SidebarItemListModel : BasePagedListModel<SidebarItemModel>
    {
    }
}