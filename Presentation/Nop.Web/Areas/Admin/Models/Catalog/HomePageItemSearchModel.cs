﻿using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    /// <summary>
    /// Represents a home page item search model
    /// </summary>
    public partial class HomePageItemSearchModel : BaseSearchModel
    {
        #region Properties

        public int HomePageId { get; set; }

        #endregion
    }
}