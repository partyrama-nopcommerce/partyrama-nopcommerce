﻿using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    /// <summary>
    /// Represents a category list model to add sidebar category row.
    /// </summary>
    public partial class SidebarAddRowCategoryListModel : BasePagedListModel<CategoryModel>
    {
    }
}