﻿using System.Collections.Generic;
using Nop.Core.Domain.Catalog;
using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    /// <summary>
    /// Represents a category model to add to the category or the product
    /// </summary> 
    public partial class ConnectionAddCategoryModel : BaseNopModel
    {
        #region Ctor

        public ConnectionAddCategoryModel()
        {
            SelectedCategoryIds = new List<int>();
        }
        #endregion

        #region Properties

        public int FromId { get; set; }

        public ConnectionFromType FromType { get; set; }

        public IList<int> SelectedCategoryIds { get; set; }

        #endregion
    }
}