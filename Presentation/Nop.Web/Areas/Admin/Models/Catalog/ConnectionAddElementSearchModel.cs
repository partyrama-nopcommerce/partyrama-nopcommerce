﻿using Nop.Core.Domain.Catalog;
using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    /// <summary>
    /// Represents an element search model to add to the category or the product
    /// </summary>
    public partial class ConnectionAddElementSearchModel : BaseSearchModel
    {
        #region Properties

        [NopResourceDisplayName("Admin.Catalog.Categories.List.SearchElementName")]
        public string SearchElementName { get; set; }

        public ConnectionFromType FromType { get; set; }

        public ConnectionToType ToType { get; set; }

        #endregion
    }
}