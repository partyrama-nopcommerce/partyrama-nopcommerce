﻿using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    /// <summary>
    /// Represents a mege menu tab item row search model
    /// </summary>
    public partial class MegaMenuTabItemRowSearchModel : BaseSearchModel
    {
        #region Properties

        public int MegaMenuTabItemId { get; set; }

        #endregion
    }
}