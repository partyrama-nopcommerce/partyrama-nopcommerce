﻿using System.Collections.Generic;
using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    /// <summary>
    /// Represents an element model to add mega menu element row
    /// </summary> 
    public partial class MegaMenuAddRowElementModel : BaseNopModel
    {
        #region Ctor

        public MegaMenuAddRowElementModel()
        {
            SelectedElementIds = new List<int>();
        }
        #endregion

        #region Properties

        public int MegaMenuTabId { get; set; }

        public int MegaMenuTabItemId { get; set; }

        public IList<int> SelectedElementIds { get; set; }

        #endregion
    }
}