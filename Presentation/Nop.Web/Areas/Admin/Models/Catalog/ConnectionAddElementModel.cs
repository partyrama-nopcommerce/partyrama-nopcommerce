﻿using System.Collections.Generic;
using Nop.Core.Domain.Catalog;
using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    /// <summary>
    /// Represents an element model to add to the category or the product
    /// </summary> 
    public partial class ConnectionAddElementModel : BaseNopModel
    {
        #region Ctor

        public ConnectionAddElementModel()
        {
            SelectedElementIds = new List<int>();
        }
        #endregion

        #region Properties

        public int FromId { get; set; }

        public ConnectionFromType FromType { get; set; }

        public ConnectionToType ToType { get; set; }

        public IList<int> SelectedElementIds { get; set; }

        #endregion
    }
}