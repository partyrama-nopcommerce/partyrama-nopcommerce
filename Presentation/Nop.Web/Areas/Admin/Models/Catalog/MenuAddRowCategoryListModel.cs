﻿using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    /// <summary>
    /// Represents a category list model to add menu category row.
    /// </summary>
    public partial class MenuAddRowCategoryListModel : BasePagedListModel<CategoryModel>
    {
    }
}