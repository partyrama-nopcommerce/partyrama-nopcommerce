﻿using System.Collections.Generic;
using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    /// <summary>
    /// Represents a topic model to add mega menu topic row
    /// </summary> 
    public partial class MegaMenuAddRowTopicModel : BaseNopModel
    {
        #region Ctor

        public MegaMenuAddRowTopicModel()
        {
            SelectedTopicIds = new List<int>();
        }
        #endregion

        #region Properties

        public int MegaMenuTabId { get; set; }

        public int MegaMenuTabItemId { get; set; }

        public IList<int> SelectedTopicIds { get; set; }

        #endregion
    }
}