﻿using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    /// <summary>
    /// Represents a category picture search model
    /// </summary>
    public partial class CategoryPictureSearchModel : BaseSearchModel
    {
        #region Properties

        public int CategoryId { get; set; }
        
        #endregion
    }
}