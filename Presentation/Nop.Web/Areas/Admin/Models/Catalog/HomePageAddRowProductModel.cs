﻿using System.Collections.Generic;
using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    /// <summary>
    /// Represents a product model to add home page product row
    /// </summary> 
    public partial class HomePageAddRowProductModel : BaseNopModel
    {
        #region Ctor

        public HomePageAddRowProductModel()
        {
            SelectedProductIds = new List<int>();
        }
        #endregion

        #region Properties

        public int HomePageId { get; set; }

        public int HomePageItemId { get; set; }

        public IList<int> SelectedProductIds { get; set; }

        #endregion
    }
}