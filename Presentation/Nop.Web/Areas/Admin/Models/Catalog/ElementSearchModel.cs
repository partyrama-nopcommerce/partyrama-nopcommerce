﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Framework.Mvc.ModelBinding;
using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    /// <summary>
    /// Represents an element search model
    /// </summary>
    public partial class ElementSearchModel : BaseSearchModel
    {
        #region Ctor

        public ElementSearchModel()
        {
            AvailableElementTypes = new List<SelectListItem>();
        }

        #endregion

        #region Properties

        [NopResourceDisplayName("Admin.Catalog.Elements.List.SearchElementName")]
        public string SearchElementName { get; set; }

        [NopResourceDisplayName("Admin.Catalog.Products.List.SearchElementType")]
        public int SearchElementTypeId { get; set; }

        public IList<SelectListItem> AvailableElementTypes { get; set; }

        #endregion
    }
}