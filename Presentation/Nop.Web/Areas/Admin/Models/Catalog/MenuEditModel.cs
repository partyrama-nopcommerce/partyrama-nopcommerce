﻿using Nop.Web.Framework.Models;
using System.Collections.Generic;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    public partial class MenuEditModel : BaseNopEntityModel
    {
        #region Ctor

        public MenuEditModel()
        {
            Menus = new List<MenuShortModel>();
        }

        #endregion

        #region Properties

        public MenuModel NewMenu { get; set; }
        public IList<MenuShortModel> Menus { get; set; }

        #endregion 
    }
}
