﻿using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    /// <summary>
    /// Represents an element list model
    /// </summary>
    public partial class ElementListModel : BasePagedListModel<ElementModel>
    {
    }
}