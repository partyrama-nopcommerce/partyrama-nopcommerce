﻿using System.Collections.Generic;
using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    /// <summary>
    /// Represents an element model to add slider element row
    /// </summary> 
    public partial class SliderAddRowElementModel : BaseNopModel
    {
        #region Ctor

        public SliderAddRowElementModel()
        {
            SelectedElementIds = new List<int>();
        }
        #endregion

        #region Properties

        public int SliderId { get; set; }

        public int SliderItemId { get; set; }

        public IList<int> SelectedElementIds { get; set; }

        #endregion
    }
}