﻿using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    /// <summary>
    /// Represents a mege menu tab item search model
    /// </summary>
    public partial class MegaMenuTabItemSearchModel : BaseSearchModel
    {
        #region Properties

        public int MegaMenuTabId { get; set; }

        #endregion
    }
}