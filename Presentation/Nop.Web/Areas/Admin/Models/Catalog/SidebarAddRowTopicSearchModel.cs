﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Core.Domain.Catalog;
using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    /// <summary>
    /// Represents an topic search model to add sidebar topic row
    /// </summary>
    public partial class SidebarAddRowTopicSearchModel : BaseSearchModel
    {
        #region Ctor

        public SidebarAddRowTopicSearchModel()
        {
            AvailableColumns = new List<SelectListItem>();
        }

        #endregion

        #region Properties

        public int SidebarId { get; set; }

        [NopResourceDisplayName("Admin.Catalog.SidebarId.Column")]
        public int SidebarItemId { get; set; }

        public IList<SelectListItem> AvailableColumns { get; set; }

        [NopResourceDisplayName("Admin.ContentManagement.Topics.List.SearchKeywords")]
        public string SearchKeywords { get; set; }

        #endregion
    }
}