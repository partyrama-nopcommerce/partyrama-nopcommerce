﻿using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    /// <summary>
    /// Represents a slider item list model
    /// </summary>
    public partial class SliderItemListModel : BasePagedListModel<SliderItemModel>
    {
    }
}