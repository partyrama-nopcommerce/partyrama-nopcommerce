﻿using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    /// <summary>
    /// Represents a product list model to add sidebar product row
    /// </summary>
    public partial class SidebarAddRowProductListModel : BasePagedListModel<ProductModel>
    {
    }
}