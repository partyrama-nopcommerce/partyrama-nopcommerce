﻿using System.Collections.Generic;
using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    /// <summary>
    /// Represents a slider model to add sidebar topic row
    /// </summary> 
    public partial class SidebarAddRowSliderModel : BaseNopModel
    {
        #region Ctor

        public SidebarAddRowSliderModel()
        {
            SelectedSliderIds = new List<int>();
        }
        #endregion

        #region Properties

        public int SidebarId { get; set; }

        public int SidebarItemId { get; set; }

        public IList<int> SelectedSliderIds { get; set; }

        #endregion
    }
}