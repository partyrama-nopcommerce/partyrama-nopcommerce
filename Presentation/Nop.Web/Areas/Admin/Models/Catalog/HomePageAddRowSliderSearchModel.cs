﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Core.Domain.Catalog;
using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    /// <summary>
    /// Represents a slider search model to add home page topic row
    /// </summary>
    public partial class HomePageAddRowSliderSearchModel : BaseSearchModel
    {
        #region Ctor

        public HomePageAddRowSliderSearchModel()
        {
            AvailableColumns = new List<SelectListItem>();
        }

        #endregion

        #region Properties

        public int HomePageId { get; set; }

        [NopResourceDisplayName("Admin.Catalog.HomePageId.Column")]
        public int HomePageItemId { get; set; }

        public IList<SelectListItem> AvailableColumns { get; set; }

        [NopResourceDisplayName("Admin.Catalog.Categories.List.SearchSliderName")]
        public string SearchSliderName { get; set; }

        #endregion
    }
}