﻿using System.Collections.Generic;
using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    /// <summary>
    /// Represents a topic model to add home page topic row
    /// </summary> 
    public partial class HomePageAddRowTopicModel : BaseNopModel
    {
        #region Ctor

        public HomePageAddRowTopicModel()
        {
            SelectedTopicIds = new List<int>();
        }
        #endregion

        #region Properties

        public int HomePageId { get; set; }

        public int HomePageItemId { get; set; }

        public IList<int> SelectedTopicIds { get; set; }

        #endregion
    }
}