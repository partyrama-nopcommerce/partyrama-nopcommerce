﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Core.Domain.Catalog;
using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    /// <summary>
    /// Represents an topic search model to add slider topic row
    /// </summary>
    public partial class SliderAddRowTopicSearchModel : BaseSearchModel
    {
        #region Ctor

        public SliderAddRowTopicSearchModel()
        {
            AvailableColumns = new List<SelectListItem>();
        }

        #endregion

        #region Properties

        public int SliderId { get; set; }

        [NopResourceDisplayName("Admin.Catalog.SliderId.Column")]
        public int SliderItemId { get; set; }

        public IList<SelectListItem> AvailableColumns { get; set; }

        [NopResourceDisplayName("Admin.ContentManagement.Topics.List.SearchKeywords")]
        public string SearchKeywords { get; set; }

        #endregion
    }
}