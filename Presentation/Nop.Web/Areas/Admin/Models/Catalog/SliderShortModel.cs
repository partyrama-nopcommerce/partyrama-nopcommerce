﻿using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    public partial class SliderShortModel : BaseNopEntityModel
    {
        #region Ctor

        public SliderShortModel()
        {

        }

        #endregion

        #region Properties

        [NopResourceDisplayName("Admin.Catalog.Slider.Fields.Name")]
        public string Name { get; set; }

        #endregion 
    }
}
