﻿using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    /// <summary>
    /// Represents a home page item list model
    /// </summary>
    public partial class HomePageItemListModel : BasePagedListModel<HomePageItemModel>
    {
    }
}