﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    /// <summary>
    /// Represents an element search model to add sidebar element row
    /// </summary>
    public partial class SidebarAddRowElementSearchModel : BaseSearchModel
    {
        #region Ctor

        public SidebarAddRowElementSearchModel()
        {
            AvailableElementTypes = new List<SelectListItem>();
            AvailableColumns = new List<SelectListItem>();
        }

        #endregion

        #region Properties

        [NopResourceDisplayName("Admin.Catalog.Elements.List.SearchElementName")]
        public string SearchElementName { get; set; }

        [NopResourceDisplayName("Admin.Catalog.Elements.List.SearchElementType")]
        public int SearchElementTypeId { get; set; }

        public IList<SelectListItem> AvailableElementTypes { get; set; }

        public int SidebarId { get; set; }

        [NopResourceDisplayName("Admin.Catalog.SidebarId.Column")]
        public int SidebarItemId { get; set; }

        public IList<SelectListItem> AvailableColumns { get; set; }

        #endregion
    }
}