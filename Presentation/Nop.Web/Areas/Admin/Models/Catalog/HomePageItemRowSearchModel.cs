﻿using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    /// <summary>
    /// Represents a home page item row search model
    /// </summary>
    public partial class HomePageItemRowSearchModel : BaseSearchModel
    {
        #region Properties

        public int HomePageItemId { get; set; }

        #endregion
    }
}