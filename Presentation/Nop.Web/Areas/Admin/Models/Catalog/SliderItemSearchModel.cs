﻿using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    /// <summary>
    /// Represents a slider item search model
    /// </summary>
    public partial class SliderItemSearchModel : BaseSearchModel
    {
        #region Properties

        public int SliderId { get; set; }

        #endregion
    }
}