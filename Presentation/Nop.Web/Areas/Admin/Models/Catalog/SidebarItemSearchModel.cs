﻿using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    /// <summary>
    /// Represents a sidebar item search model
    /// </summary>
    public partial class SidebarItemSearchModel : BaseSearchModel
    {
        #region Properties

        public int SidebarId { get; set; }

        #endregion
    }
}