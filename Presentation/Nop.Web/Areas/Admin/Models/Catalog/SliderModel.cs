﻿using FluentValidation.Attributes;
using Nop.Core.Domain.Catalog;
using Nop.Web.Areas.Admin.Validators.Catalog;
using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;
using System;
using System.ComponentModel.DataAnnotations;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    /// <summary>
    /// Represents a slider model
    /// </summary>
    [Validator(typeof(SliderValidator))]
    public partial class SliderModel : BaseNopEntityModel
    {
        #region Ctor

        public SliderModel()
        {

        }

        #endregion

        #region Properties

        [NopResourceDisplayName("Admin.Catalog.Slider.Fields.Name")]
        public string Name { get; set; }

        [NopResourceDisplayName("Admin.Catalog.Slider.Fields.SetCountDown")]
        public bool SetCountDown { get; set; }

        [NopResourceDisplayName("Admin.Catalog.Slider.Fields.StartDateUtc")]
        [UIHint("DateTimeNullable")]
        public DateTime? StartDateUtc { get; set; }

        [NopResourceDisplayName("Admin.Catalog.Slider.Fields.EndDateUtc")]
        [UIHint("DateTimeNullable")]
        public DateTime? EndDateUtc { get; set; }

        [NopResourceDisplayName("Admin.Catalog.Slider.Fields.Visible")]
        public bool Visible { get; set; }

        public RowConnectionType TabConnectionType { get; set; }

        public string TabConnectionName { get; set; }

        public int? TabConnectionId { get; set; }

        [NopResourceDisplayName("Admin.Catalog.Slider.Fields.Type")]
        public int SliderTypeId { get; set; }

        [NopResourceDisplayName("Admin.Catalog.Slider.Fields.Speed")]
        [UIHint("Int32Nullable")]
        public int? Speed { get; set; }

        [NopResourceDisplayName("Admin.Catalog.Slider.Fields.VisibleArrows")]
        public bool VisibleArrows { get; set; }

        [NopResourceDisplayName("Admin.Catalog.Slider.Fields.VisibleDots")]
        public bool VisibleDots { get; set; }

        [NopResourceDisplayName("Admin.Catalog.Slider.Fields.CssClass")]
        public string CssClass { get; set; }

        [NopResourceDisplayName("Admin.Catalog.Slider.Fields.BorderStyle")]
        public int BorderStyleId { get; set; }

        [NopResourceDisplayName("Admin.Catalog.Slider.Fields.BorderHeight")]
        [UIHint("Int32Nullable")]
        public int? BorderHeight { get; set; }

        [NopResourceDisplayName("Admin.Catalog.Slider.Fields.BorderColor")]
        public string BorderColor { get; set; }

        [NopResourceDisplayName("Admin.Catalog.Slider.Fields.BorderPlacement")]
        public int BorderPlacementId { get; set; }

        [NopResourceDisplayName("Admin.Catalog.Slider.Fields.Number")]
        [UIHint("Int32Nullable")]
        public int? Number { get; set; }

        [NopResourceDisplayName("Admin.Catalog.Slider.Fields.ShowBuyButton")]
        public bool ShowBuyButton { get; set; }

        #endregion
    }
}
