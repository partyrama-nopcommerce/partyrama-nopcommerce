﻿using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    public partial class MegaMenuShortTabModel : BaseNopEntityModel
    {
        #region Ctor

        public MegaMenuShortTabModel()
        {

        }

        #endregion

        #region Properties

        [NopResourceDisplayName("Admin.Catalog.MegaMenuTab.Fields.Name")]
        public string Name { get; set; }

        #endregion 
    }
}
