﻿using Nop.Web.Areas.Admin.Models.Topics;
using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    /// <summary>
    /// Represents a topic list model to add mega menu topic row
    /// </summary>
    public partial class MegaMenuAddRowTopicListModel : BasePagedListModel<TopicModel>
    {
    }
}