﻿using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    /// <summary>
    /// Represents an element list model to add to the category or to the product
    /// </summary>
    public partial class ConnectionAddElementListModel : BasePagedListModel<ElementModel>
    {
    }
}