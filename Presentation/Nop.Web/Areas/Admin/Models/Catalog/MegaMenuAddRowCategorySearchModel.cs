﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Core.Domain.Catalog;
using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;
using System.Collections.Generic;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    /// <summary>
    /// Represents a category search model to add to the category or the product
    /// </summary>
    public partial class MegaMenuAddRowCategorySearchModel: BaseSearchModel
    {
        #region Ctor

        public MegaMenuAddRowCategorySearchModel()
        {
            AvailableColumns = new List<SelectListItem>();
        }

        #endregion

        #region Properties

        [NopResourceDisplayName("Admin.Catalog.Categories.List.SearchCategoryName")]
        public string SearchCategoryName { get; set; }

        public int MegaMenuTabId { get; set; }

        [NopResourceDisplayName("Admin.Catalog.MegaMenuTabId.Column")]
        public int MegaMenuTabItemId { get; set; }

        public IList<SelectListItem> AvailableColumns { get; set; }

        #endregion
    }
}