﻿using System.Collections.Generic;
using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    /// <summary>
    /// Represents a category model to add menu category row
    /// </summary> 
    public partial class MenuAddRowCategoryModel : BaseNopModel
    {
        #region Ctor

        public MenuAddRowCategoryModel()
        {
            SelectedCategoryIds = new List<int>();
        }
        #endregion

        #region Properties

        public int MenuId { get; set; }

        public int MenuItemId { get; set; }

        public IList<int> SelectedCategoryIds { get; set; }

        #endregion
    }
}