﻿using System.Collections.Generic;
using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    /// <summary>
    /// Represents a product model to add mega menu product row
    /// </summary> 
    public partial class MegaMenuAddRowProductModel : BaseNopModel
    {
        #region Ctor

        public MegaMenuAddRowProductModel()
        {
            SelectedProductIds = new List<int>();
        }
        #endregion

        #region Properties

        public int MegaMenuTabId { get; set; }

        public int MegaMenuTabItemId { get; set; }

        public IList<int> SelectedProductIds { get; set; }

        #endregion
    }
}