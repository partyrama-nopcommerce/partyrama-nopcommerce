﻿using System.Collections.Generic;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    /// <summary>
    /// Represents a store language model
    /// </summary>
    public class StoreLanguageModel
    {
        #region Properties

        public IList<StoreUrls> StoreUrls { get; set; } 

        public int LanguageId { get; set; }

        #endregion Properties
    }

    public class StoreUrls
    {
        #region Properties

        public string Url { get; set; }

        public string Name { get; set; }

        #endregion Properties
    }
}
