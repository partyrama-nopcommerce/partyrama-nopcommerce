﻿using System.Collections.Generic;
using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    /// <summary>
    /// Represents a slider model to add home page slider row
    /// </summary> 
    public partial class HomePageAddRowSliderModel : BaseNopModel
    {
        #region Ctor

        public HomePageAddRowSliderModel()
        {
            SelectedSliderIds = new List<int>();
        }
        #endregion

        #region Properties

        public int HomePageId { get; set; }

        public int HomePageItemId { get; set; }

        public IList<int> SelectedSliderIds { get; set; }

        #endregion
    }
}