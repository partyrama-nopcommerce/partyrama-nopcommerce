﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;
using System.Collections.Generic;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    /// <summary>
    /// Represents a category search model to add to the category or the product
    /// </summary>
    public partial class MenuAddRowCategorySearchModel: BaseSearchModel
    {
        #region Ctor

        public MenuAddRowCategorySearchModel()
        {
            AvailableColumns = new List<SelectListItem>();
        }

        #endregion

        #region Properties

        [NopResourceDisplayName("Admin.Catalog.Categories.List.SearchCategoryName")]
        public string SearchCategoryName { get; set; }

        public int MenuId { get; set; }

        [NopResourceDisplayName("Admin.Catalog.MenuId.Column")]
        public int MenuItemId { get; set; }

        public IList<SelectListItem> AvailableColumns { get; set; }

        #endregion
    }
}