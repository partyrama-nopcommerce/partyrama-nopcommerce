﻿using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    /// <summary>
    /// Represents a category list model to add mega menu category row.
    /// </summary>
    public partial class MegaMenuAddRowCategoryListModel : BasePagedListModel<CategoryModel>
    {
    }
}