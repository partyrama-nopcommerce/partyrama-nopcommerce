﻿using Nop.Core.Domain.Catalog;
using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    /// <summary>
    /// Represents a connections search model
    /// </summary>
    public partial class ConnectionsSearchModel : BaseSearchModel
    {
        #region Properties

        public int FromId { get; set; }
        public ConnectionFromType FromType { get; set; }
        public string FormId => FromType == ConnectionFromType.cat ? "category-form" : "product-form";

        #endregion
    }
}