﻿using System.Collections.Generic;
using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    /// <summary>
    /// Represents a category model to add mega menu category row
    /// </summary> 
    public partial class MegaMenuAddRowCategoryModel : BaseNopModel
    {
        #region Ctor

        public MegaMenuAddRowCategoryModel()
        {
            SelectedCategoryIds = new List<int>();
        }
        #endregion

        #region Properties

        public int MegaMenuTabId { get; set; }

        public int MegaMenuTabItemId { get; set; }

        public IList<int> SelectedCategoryIds { get; set; }

        #endregion
    }
}