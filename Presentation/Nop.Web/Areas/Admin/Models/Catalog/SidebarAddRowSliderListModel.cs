﻿using Nop.Web.Areas.Admin.Models.Topics;
using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    /// <summary>
    /// Represents a slider list model to add sidebar topic row
    /// </summary>
    public partial class SidebarAddRowSliderListModel : BasePagedListModel<SliderModel>
    {
    }
}