﻿using Nop.Web.Framework.Models;
using System.Collections.Generic;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    public partial class SidebarEditModel : BaseNopEntityModel
    {
        #region Ctor

        public SidebarEditModel()
        {
            Sidebars = new List<SidebarShortModel>();
        }

        #endregion

        #region Properties

        public SidebarModel NewSidebar { get; set; }
        public IList<SidebarShortModel> Sidebars { get; set; }

        #endregion 
    }
}
