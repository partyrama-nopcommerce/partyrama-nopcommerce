﻿using System.ComponentModel.DataAnnotations;
using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    /// <summary>
    /// Represents a category picture model
    /// </summary>
    public partial class CategoryPictureModel : BaseNopEntityModel
    {
        #region Properties

        public int CategoryId { get; set; }

        [UIHint("Picture")]
        [NopResourceDisplayName("Admin.Catalog.Categories.Pictures.Fields.Picture")]
        public int PictureId { get; set; }

        [NopResourceDisplayName("Admin.Catalog.Categories.Pictures.Fields.Picture")]
        public string PictureUrl { get; set; }

        [NopResourceDisplayName("Admin.Catalog.Categories.Pictures.Fields.DisplayOrder")]
        public int DisplayOrder { get; set; }

        [NopResourceDisplayName("Admin.Catalog.Categories.Pictures.Fields.OverrideAltAttribute")]
        public string OverrideAltAttribute { get; set; }

        [NopResourceDisplayName("Admin.Catalog.Categories.Pictures.Fields.OverrideTitleAttribute")]
        public string OverrideTitleAttribute { get; set; }

        [NopResourceDisplayName("Admin.Catalog.Categories.Pictures.Fields.PictureType")]
        public int PictureKindId { get; set; }

        [NopResourceDisplayName("Admin.Catalog.Categories.Pictures.Fields.PictureType")]
        public string PictureKindName { get; set; }

        [NopResourceDisplayName("Admin.Catalog.Categories.Pictures.Fields.OriginalName")]
        public string OriginalName { get; set; }

        #endregion
    }
}