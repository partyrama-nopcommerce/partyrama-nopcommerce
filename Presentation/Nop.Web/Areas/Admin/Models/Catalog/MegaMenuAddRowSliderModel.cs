﻿using System.Collections.Generic;
using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    /// <summary>
    /// Represents a mega menu model to add mega menu topic row
    /// </summary> 
    public partial class MegaMenuAddRowSliderModel : BaseNopModel
    {
        #region Ctor

        public MegaMenuAddRowSliderModel()
        {
            SelectedSliderIds = new List<int>();
        }
        #endregion

        #region Properties

        public int MegaMenuTabId { get; set; }

        public int MegaMenuTabItemId { get; set; }

        public IList<int> SelectedSliderIds { get; set; }

        #endregion
    }
}