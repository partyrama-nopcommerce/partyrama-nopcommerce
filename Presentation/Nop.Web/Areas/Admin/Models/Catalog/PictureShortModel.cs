﻿using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    public partial class PictureShortModel
    {
        #region Ctor

        public PictureShortModel()
        {
        }

        #endregion

        #region Properties

        public int ItemPictureId { get; set; }

        public string ItemPictureUrl { get; set; }

        public string ItemPictureOriginalName { get; set; }

        #endregion 
    }
}
