﻿namespace Nop.Web.Areas.Admin.Models.Catalog
{
    /// <summary>
    /// Represents a reorder sidebar item model
    /// </summary>
    public partial class ReOrderSidebarItemModel
    {
        #region Properties

        public int OldDisplayOrder { get; set; }

        public int NewDisplayOrder { get; set; }

        public int DragItemId { get; set; }

        #endregion
    }
}