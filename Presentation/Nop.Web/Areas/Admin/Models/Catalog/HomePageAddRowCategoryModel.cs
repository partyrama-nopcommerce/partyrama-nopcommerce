﻿using System.Collections.Generic;
using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    /// <summary>
    /// Represents a category model to add home page category row
    /// </summary> 
    public partial class HomePageAddRowCategoryModel : BaseNopModel
    {
        #region Ctor

        public HomePageAddRowCategoryModel()
        {
            SelectedCategoryIds = new List<int>();
        }
        #endregion

        #region Properties

        public int HomePageItemId { get; set; }

        public int HomePageId { get; set; }

        public IList<int> SelectedCategoryIds { get; set; }

        #endregion
    }
}