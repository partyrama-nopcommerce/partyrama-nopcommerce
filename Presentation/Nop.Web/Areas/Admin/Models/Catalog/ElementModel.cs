﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using FluentValidation.Attributes;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Areas.Admin.Validators.Catalog;
using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    /// <summary>
    /// Represents an element model
    /// </summary>
    [Validator(typeof(ElementValidator))]
    public partial class ElementModel : BaseNopEntityModel, ILocalizedModel<ElementLocalizedModel>
    {
        #region Ctor

        public ElementModel()
        {
            Locales = new List<ElementLocalizedModel>();
        }

        #endregion

        #region Properties

        [NopResourceDisplayName("Admin.Catalog.Elements.Fields.ElementType")]
        public int ElementTypeId { get; set; }

        [NopResourceDisplayName("Admin.Catalog.Elements.Fields.Name")]
        public string Name { get; set; }

        [NopResourceDisplayName("Admin.Catalog.Elements.Fields.ShortName")]
        public string ShortName { get; set; }

        [NopResourceDisplayName("Admin.Catalog.Elements.Fields.Description")]
        public string Description { get; set; }

        [NopResourceDisplayName("Admin.Catalog.Elements.Fields.Visible")]
        public bool Visible { get; set; }

        public IList<ElementLocalizedModel> Locales { get; set; }

        [NopResourceDisplayName("Admin.Catalog.Elements.Fields.CssClass")]
        public string CssClass { get; set; }

        [UIHint("Picture")]
        [NopResourceDisplayName("Admin.Catalog.Elements.Fields.Picture")]
        public int PictureId { get; set; }

        [NopResourceDisplayName("Admin.Catalog.Elements.Fields.Color")]
        public string Color { get; set; }

        [NopResourceDisplayName("Admin.Catalog.Elements.Fields.StartDateUtc")]
        [UIHint("DateTimeNullable")]
        public DateTime? StartDateUtc { get; set; }

        [NopResourceDisplayName("Admin.Catalog.Elements.Fields.EndDateUtc")]
        [UIHint("DateTimeNullable")]
        public DateTime? EndDateUtc { get; set; }

        [NopResourceDisplayName("Admin.Catalog.Elements.Fields.CreatedOn")]
        public DateTime? CreatedOn { get; set; }

        [NopResourceDisplayName("Admin.Catalog.Elements.Fields.UpdatedOn")]
        public DateTime? UpdatedOn { get; set; }

        public string PictureThumbnailUrl { get; set; }
        #endregion
    }

    public partial class ElementLocalizedModel : ILocalizedLocaleModel
    {
        public int LanguageId { get; set; }

        [NopResourceDisplayName("Admin.Catalog.Elements.Fields.Name")]
        public string Name { get; set; }

        [NopResourceDisplayName("Admin.Catalog.Elements.Fields.ShortName")]
        public string ShortName { get; set; }

        [NopResourceDisplayName("Admin.Catalog.Elements.Fields.Description")]
        public string Description {get;set;}
    }
}