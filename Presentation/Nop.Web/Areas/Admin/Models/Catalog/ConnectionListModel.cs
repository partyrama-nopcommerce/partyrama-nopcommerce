﻿using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    /// <summary>
    /// Represents a connection list model
    /// </summary>
    public partial class ConnectionListModel : BasePagedListModel<ConnectionModel>
    {
    }
}