﻿using FluentValidation.Attributes;
using Nop.Core.Domain.Catalog;
using Nop.Web.Areas.Admin.Validators.Catalog;
using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;
using System;
using System.ComponentModel.DataAnnotations;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    /// <summary>
    /// Represents a sidebar model
    /// </summary>
    [Validator(typeof(SidebarValidator))]
    public partial class SidebarModel : BaseNopEntityModel
    {
        #region Ctor

        public SidebarModel()
        {

        }

        #endregion

        #region Properties

        [NopResourceDisplayName("Admin.Catalog.Sidebar.Fields.Name")]
        public string Name { get; set; }

        [NopResourceDisplayName("Admin.Catalog.Sidebar.Fields.SetCountDown")]
        public bool SetCountDown { get; set; }

        [NopResourceDisplayName("Admin.Catalog.Sidebar.Fields.StartDateUtc")]
        [UIHint("DateTimeNullable")]
        public DateTime? StartDateUtc { get; set; }

        [NopResourceDisplayName("Admin.Catalog.Sidebar.Fields.EndDateUtc")]
        [UIHint("DateTimeNullable")]
        public DateTime? EndDateUtc { get; set; }

        [NopResourceDisplayName("Admin.Catalog.Sidebar.Fields.Visible")]
        public bool Visible { get; set; }

        public RowConnectionType TabConnectionType { get; set; }

        public string TabConnectionName { get; set; }

        public int? TabConnectionId { get; set; }

        #endregion
    }
}
