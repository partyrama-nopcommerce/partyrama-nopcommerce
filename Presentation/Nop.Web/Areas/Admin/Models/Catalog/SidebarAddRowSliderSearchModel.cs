﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Core.Domain.Catalog;
using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    /// <summary>
    /// Represents a slider search model to add sidebar topic row
    /// </summary>
    public partial class SidebarAddRowSliderSearchModel : BaseSearchModel
    {
        #region Ctor

        public SidebarAddRowSliderSearchModel()
        {
            AvailableColumns = new List<SelectListItem>();
        }

        #endregion

        #region Properties

        public int SidebarId { get; set; }

        [NopResourceDisplayName("Admin.Catalog.SidebarId.Column")]
        public int SidebarItemId { get; set; }

        public IList<SelectListItem> AvailableColumns { get; set; }

        [NopResourceDisplayName("Admin.Catalog.Categories.List.SearchSliderName")]
        public string SearchSliderName { get; set; }

        #endregion
    }
}