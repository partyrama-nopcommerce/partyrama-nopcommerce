﻿using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    /// <summary>
    /// Represents a sidebar item row search model
    /// </summary>
    public partial class SidebarItemRowSearchModel : BaseSearchModel
    {
        #region Properties

        public int SidebarItemId { get; set; }

        #endregion
    }
}