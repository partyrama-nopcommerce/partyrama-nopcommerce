﻿using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    /// <summary>
    /// Represents a category list model to add slider category row.
    /// </summary>
    public partial class SliderAddRowCategoryListModel : BasePagedListModel<CategoryModel>
    {
    }
}