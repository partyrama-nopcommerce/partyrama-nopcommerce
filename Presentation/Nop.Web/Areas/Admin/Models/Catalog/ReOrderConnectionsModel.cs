﻿using Nop.Core.Domain.Catalog;
using Nop.Web.Models.Catalog;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    /// <summary>
    /// Represents a reorder connections model
    /// </summary>
    public partial class ReOrderConnectionsModel
    {
        #region Properties

        public int FromId { get; set; }

        public int OldDisplayOrder { get; set; }

        public int NewDisplayOrder { get; set; }

        public ConnectionFromType FromType { get; set; }

        public int DropItemId { get; set; }

        public int DragItemId { get; set; }

        #endregion
    }
}