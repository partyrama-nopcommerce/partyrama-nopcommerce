﻿using System.Collections.Generic;
using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    /// <summary>
    /// Represents a category model to add sidebar category row
    /// </summary> 
    public partial class SidebarAddRowCategoryModel : BaseNopModel
    {
        #region Ctor

        public SidebarAddRowCategoryModel()
        {
            SelectedCategoryIds = new List<int>();
        }
        #endregion

        #region Properties

        public int SidebarId { get; set; }

        public int SidebarItemId { get; set; }

        public IList<int> SelectedCategoryIds { get; set; }

        #endregion
    }
}