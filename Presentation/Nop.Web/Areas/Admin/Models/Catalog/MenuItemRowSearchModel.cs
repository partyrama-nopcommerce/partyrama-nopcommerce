﻿using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    /// <summary>
    /// Represents a menu item row search model
    /// </summary>
    public partial class MenuItemRowSearchModel : BaseSearchModel
    {
        #region Properties

        public int MenuItemId { get; set; }

        #endregion
    }
}