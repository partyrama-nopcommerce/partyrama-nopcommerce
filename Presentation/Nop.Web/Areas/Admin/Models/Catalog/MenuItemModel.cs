﻿using FluentValidation.Attributes;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Core.Domain.Catalog;
using Nop.Web.Areas.Admin.Validators.Catalog;
using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    /// <summary>
    /// Represents a menu item model
    /// </summary>
    [Validator(typeof(MenuItemValidator))]
    public partial class MenuItemModel : BaseNopEntityModel
    {
        #region Ctor

        public MenuItemModel()
        {
            AvailableMegaMenuTab = new List<SelectListItem>();
        }

        #endregion

        [NopResourceDisplayName("Admin.Catalog.MenuItem.Fields.CssClass")]
        public string ItemCssClass { get; set; }

        [NopResourceDisplayName("Admin.Catalog.MenuItem.Fields.DisplayOrder")]
        public int ItemDisplayOrder { get; set; }

        [NopResourceDisplayName("Admin.Catalog.MenuItem.Fields.Menu")]
        public int ItemMenuId { get; set; }

        [NopResourceDisplayName("Admin.Catalog.MenuItem.Fields.Column")]
        public int ItemColumnId { get; set; }

        public RowType ItemRowType { get; set; }

        [NopResourceDisplayName("Admin.Catalog.MenuItem.Fields.Color")]
        public string ItemColor { get; set; }

        [NopResourceDisplayName("Admin.Catalog.MenuItem.Fields.Visible")]
        public bool ItemVisible { get; set; }

        [NopResourceDisplayName("Admin.Catalog.MenuItem.Fields.StartDateUtc")]
        [UIHint("DateTimeNullable")]
        public DateTime? ItemStartDateUtc { get; set; }

        [NopResourceDisplayName("Admin.Catalog.MenuItem.Fields.EndDateUtc")]
        [UIHint("DateTimeNullable")]
        public DateTime? ItemEndDateUtc { get; set; }

        [UIHint("Int32Nullable")]
        public int? ItemRowConnectionId { get; set; }

        [NopResourceDisplayName("Admin.Catalog.MenuItem.Fields.MegaMenuTab")]
        [UIHint("Int32Nullable")]
        public int? ItemMegaMenuTabId { get; set; }

        public RowConnectionType? ItemRowConnectionType { get; set; }

        public string ItemMegaMenuTabName { get; set; }

        public string ItemName { get; set; }

        public string ItemRowConnectionTypeName => ItemRowConnectionType.HasValue ? ItemRowConnectionType.Value.ToString(): string.Empty;

        public IList<SelectListItem> AvailableMegaMenuTab { get; set; }

        public bool ItemAvailable => ItemVisible
                    && (!ItemStartDateUtc.HasValue || ItemStartDateUtc.Value <= DateTime.UtcNow)
                    && (!ItemEndDateUtc.HasValue || ItemEndDateUtc.Value > DateTime.UtcNow);
    }
}
