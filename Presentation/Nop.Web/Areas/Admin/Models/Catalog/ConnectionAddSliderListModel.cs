﻿using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    /// <summary>
    /// Represents a slider list model to add to the category or to the product
    /// </summary>
    public partial class ConnectionAddSliderListModel : BasePagedListModel<SliderModel>
    {
    }
}