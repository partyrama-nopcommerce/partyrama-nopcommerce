﻿using Nop.Web.Framework.Models;
using System.Collections.Generic;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    public partial class HomePageEditModel : BaseNopEntityModel
    {
        #region Ctor

        public HomePageEditModel()
        {
            HomePages = new List<HomePageShortModel>();
        }

        #endregion

        #region Properties

        public HomePageModel NewHomePage { get; set; }
        public IList<HomePageShortModel> HomePages { get; set; }

        #endregion 
    }
}
