﻿using Nop.Core.Domain.Catalog;
using Nop.Web.Framework.Models;
namespace Nop.Web.Areas.Admin.Models.Catalog
{
    public partial class ConnectionModel : BaseNopEntityModel
    {
        public ConnectionModel()
        {
            Picture = new PictureShortModel();
        }

        public ConnectionToType ToType { get; set; }

        public ConnectionFromType FromType { get; set; }

        public string CssClass { get; set; }

        public int DisplayOrder { get; set; }

        public string Name { get; set; }

        public int FromId { get; set; }

        public int ToId { get; set; }

        public bool ShowDescription { get; set; }

        public string Type => $"{ToType}_to_{FromType}";

        public PictureShortModel Picture { get; set; }
    }
}
