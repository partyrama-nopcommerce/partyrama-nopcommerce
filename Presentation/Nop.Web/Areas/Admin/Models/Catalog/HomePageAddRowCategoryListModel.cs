﻿using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    /// <summary>
    /// Represents a category list model to add home page category row.
    /// </summary>
    public partial class HomePageAddRowCategoryListModel : BasePagedListModel<CategoryModel>
    {
    }
}