﻿using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    /// <summary>
    /// Represents a slider item row search model
    /// </summary>
    public partial class SliderItemRowSearchModel : BaseSearchModel
    {
        #region Properties

        public int SliderItemId { get; set; }

        #endregion
    }
}