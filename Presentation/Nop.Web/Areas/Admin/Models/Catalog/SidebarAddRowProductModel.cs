﻿using System.Collections.Generic;
using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    /// <summary>
    /// Represents a product model to add sidebar product row
    /// </summary> 
    public partial class SidebarAddRowProductModel : BaseNopModel
    {
        #region Ctor

        public SidebarAddRowProductModel()
        {
            SelectedProductIds = new List<int>();
        }
        #endregion

        #region Properties

        public int SidebarId { get; set; }

        public int SidebarItemId { get; set; }

        public IList<int> SelectedProductIds { get; set; }

        #endregion
    }
}