﻿using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    /// <summary>
    /// Represents an element list model to add menu slider row
    /// </summary>
    public partial class SliderAddRowElementListModel : BasePagedListModel<ElementModel>
    {
    }
}