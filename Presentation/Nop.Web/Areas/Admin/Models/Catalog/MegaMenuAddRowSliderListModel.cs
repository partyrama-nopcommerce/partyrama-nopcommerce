﻿using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    /// <summary>
    /// Represents a slider list model to add mega menu topic row
    /// </summary>
    public partial class MegaMenuAddRowSliderListModel : BasePagedListModel<SliderModel>
    {
    }
}