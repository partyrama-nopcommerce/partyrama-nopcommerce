﻿using FluentValidation.Attributes;
using Nop.Core.Domain.Catalog;
using Nop.Web.Areas.Admin.Validators.Catalog;
using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;
using System;
using System.ComponentModel.DataAnnotations;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    /// <summary>
    /// Represents a home page model
    /// </summary>
    [Validator(typeof(HomePageValidator))]
    public partial class HomePageModel : BaseNopEntityModel
    {
        #region Ctor

        public HomePageModel()
        {

        }

        #endregion

        #region Properties

        [NopResourceDisplayName("Admin.Catalog.HomePage.Fields.Name")]
        public string Name { get; set; }

        [NopResourceDisplayName("Admin.Catalog.HomePage.Fields.SetCountDown")]
        public bool SetCountDown { get; set; }

        [NopResourceDisplayName("Admin.Catalog.HomePage.Fields.StartDateUtc")]
        [UIHint("DateTimeNullable")]
        public DateTime? StartDateUtc { get; set; }

        [NopResourceDisplayName("Admin.Catalog.HomePage.Fields.EndDateUtc")]
        [UIHint("DateTimeNullable")]
        public DateTime? EndDateUtc { get; set; }

        [NopResourceDisplayName("Admin.Catalog.HomePage.Fields.Visible")]
        public bool Visible { get; set; }

        public RowConnectionType TabConnectionType { get; set; }

        public string TabConnectionName { get; set; }

        public int? TabConnectionId { get; set; }

        #endregion
    }
}
