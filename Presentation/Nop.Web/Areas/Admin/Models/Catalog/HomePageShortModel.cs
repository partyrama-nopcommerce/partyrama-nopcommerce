﻿using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    public partial class HomePageShortModel : BaseNopEntityModel
    {
        #region Ctor

        public HomePageShortModel()
        {

        }

        #endregion

        #region Properties

        [NopResourceDisplayName("Admin.Catalog.HomePage.Fields.Name")]
        public string Name { get; set; }

        #endregion 
    }
}
