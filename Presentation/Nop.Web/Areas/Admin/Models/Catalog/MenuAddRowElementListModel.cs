﻿using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    /// <summary>
    /// Represents an element list model to add menu element row
    /// </summary>
    public partial class MenuAddRowElementListModel : BasePagedListModel<ElementModel>
    {
    }
}