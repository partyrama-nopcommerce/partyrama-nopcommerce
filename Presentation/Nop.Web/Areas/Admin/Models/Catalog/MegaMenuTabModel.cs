﻿using FluentValidation.Attributes;
using Nop.Core.Domain.Catalog;
using Nop.Web.Areas.Admin.Validators.Catalog;
using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;
using System;
using System.ComponentModel.DataAnnotations;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    /// <summary>
    /// Represents a mega menu tab model
    /// </summary>
    [Validator(typeof(MegaMenuTabValidator))]
    public partial class MegaMenuTabModel : BaseNopEntityModel
    {
        #region Ctor

        public MegaMenuTabModel()
        {

        }

        #endregion

        #region Properties

        [NopResourceDisplayName("Admin.Catalog.MegaMenuTab.Fields.Name")]
        public string Name { get; set; }

        [NopResourceDisplayName("Admin.Catalog.MegaMenuTab.Fields.SetCountDown")]
        public bool SetCountDown { get; set; }

        [NopResourceDisplayName("Admin.Catalog.MegaMenuTab.Fields.StartDateUtc")]
        [UIHint("DateTimeNullable")]
        public DateTime? StartDateUtc { get; set; }

        [NopResourceDisplayName("Admin.Catalog.MegaMenuTab.Fields.EndDateUtc")]
        [UIHint("DateTimeNullable")]
        public DateTime? EndDateUtc { get; set; }

        [NopResourceDisplayName("Admin.Catalog.MegaMenuTab.Fields.Visible")]
        public bool Visible { get; set; }

        public RowConnectionType TabConnectionType { get; set; }

        public string TabConnectionName { get; set; }

        public int? TabConnectionId { get; set; }

        #endregion
    }
}
