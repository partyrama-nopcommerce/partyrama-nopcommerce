﻿namespace Nop.Web.Areas.Admin.Models.Catalog
{
    /// <summary>
    /// Represents a reorder menu item model
    /// </summary>
    public partial class ReOrderMenuItemModel
    {
        #region Properties

        public int OldDisplayOrder { get; set; }

        public int NewDisplayOrder { get; set; }

        public int DragItemId { get; set; }

        #endregion
    }
}