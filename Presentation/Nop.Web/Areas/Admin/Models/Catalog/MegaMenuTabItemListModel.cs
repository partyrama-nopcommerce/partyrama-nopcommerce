﻿using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    /// <summary>
    /// Represents a mega menu tab item list model
    /// </summary>
    public partial class MegaMenuTabItemListModel : BasePagedListModel<MegaMenuTabItemModel>
    {
    }
}