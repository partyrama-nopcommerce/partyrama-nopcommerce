﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;
using System.Collections.Generic;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    /// <summary>
    /// Represents a category search model to add to the sidebar
    /// </summary>
    public partial class SidebarAddRowCategorySearchModel: BaseSearchModel
    {
        #region Ctor

        public SidebarAddRowCategorySearchModel()
        {
            AvailableColumns = new List<SelectListItem>();
        }

        #endregion

        #region Properties

        [NopResourceDisplayName("Admin.Catalog.Categories.List.SearchCategoryName")]
        public string SearchCategoryName { get; set; }

        public int SidebarId { get; set; }

        [NopResourceDisplayName("Admin.Catalog.SidebarId.Column")]
        public int SidebarItemId { get; set; }

        public IList<SelectListItem> AvailableColumns { get; set; }

        #endregion
    }
}