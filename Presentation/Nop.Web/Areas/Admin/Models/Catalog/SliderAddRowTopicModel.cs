﻿using System.Collections.Generic;
using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    /// <summary>
    /// Represents a topic model to add slider topic row
    /// </summary> 
    public partial class SliderAddRowTopicModel : BaseNopModel
    {
        #region Ctor

        public SliderAddRowTopicModel()
        {
            SelectedTopicIds = new List<int>();
        }
        #endregion

        #region Properties

        public int SliderId { get; set; }

        public int SliderItemId { get; set; }

        public IList<int> SelectedTopicIds { get; set; }

        #endregion
    }
}