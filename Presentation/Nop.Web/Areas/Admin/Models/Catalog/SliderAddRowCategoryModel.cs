﻿using System.Collections.Generic;
using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    /// <summary>
    /// Represents a category model to add slider category row
    /// </summary> 
    public partial class SliderAddRowCategoryModel : BaseNopModel
    {
        #region Ctor

        public SliderAddRowCategoryModel()
        {
            SelectedCategoryIds = new List<int>();
        }
        #endregion

        #region Properties

        public int SliderId { get; set; }

        public int SliderItemId { get; set; }

        public IList<int> SelectedCategoryIds { get; set; }

        #endregion
    }
}