﻿using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.Catalog
{
    /// <summary>
    /// Represents a category picture list model
    /// </summary>
    public partial class CategoryPictureListModel : BasePagedListModel<CategoryPictureModel>
    {
    }
}