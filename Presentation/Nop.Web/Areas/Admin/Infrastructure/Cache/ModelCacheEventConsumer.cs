﻿using Nop.Core.Caching;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Configuration;
using Nop.Core.Domain.Topics;
using Nop.Core.Domain.Vendors;
using Nop.Core.Events;
using Nop.Services.Events;

namespace Nop.Web.Areas.Admin.Infrastructure.Cache
{
    /// <summary>
    /// Model cache event consumer (used for caching of presentation layer models)
    /// </summary>
    public partial class ModelCacheEventConsumer: 
        //settings
        IConsumer<EntityUpdatedEvent<Setting>>,
        //specification attributes
        IConsumer<EntityInsertedEvent<SpecificationAttribute>>,
        IConsumer<EntityUpdatedEvent<SpecificationAttribute>>,
        IConsumer<EntityDeletedEvent<SpecificationAttribute>>,
        //categories
        IConsumer<EntityInsertedEvent<Category>>,
        IConsumer<EntityUpdatedEvent<Category>>,
        IConsumer<EntityDeletedEvent<Category>>,
        //manufacturers
        IConsumer<EntityInsertedEvent<Manufacturer>>,
        IConsumer<EntityUpdatedEvent<Manufacturer>>,
        IConsumer<EntityDeletedEvent<Manufacturer>>,
        //vendors
        IConsumer<EntityInsertedEvent<Vendor>>,
        IConsumer<EntityUpdatedEvent<Vendor>>,
        IConsumer<EntityDeletedEvent<Vendor>>,
        //mega menu tab item
        IConsumer<EntityInsertedEvent<MegaMenuTabItem>>,
        IConsumer<EntityUpdatedEvent<MegaMenuTabItem>>,
        IConsumer<EntityDeletedEvent<MegaMenuTabItem>>,
        //menu item
        IConsumer<EntityInsertedEvent<MenuItem>>,
        IConsumer<EntityUpdatedEvent<MenuItem>>,
        IConsumer<EntityDeletedEvent<MenuItem>>,
        //mega menu tab
        IConsumer<EntityInsertedEvent<MegaMenuTab>>,
        IConsumer<EntityUpdatedEvent<MegaMenuTab>>,
        IConsumer<EntityDeletedEvent<MegaMenuTab>>,
        //products
        IConsumer<EntityInsertedEvent<Product>>,
        IConsumer<EntityUpdatedEvent<Product>>,
        IConsumer<EntityDeletedEvent<Product>>,
        //elements
        IConsumer<EntityInsertedEvent<Element>>,
        IConsumer<EntityUpdatedEvent<Element>>,
        IConsumer<EntityDeletedEvent<Element>>,
        //topics
        IConsumer<EntityInsertedEvent<Topic>>,
        IConsumer<EntityUpdatedEvent<Topic>>,
        IConsumer<EntityDeletedEvent<Topic>>,
        //sidebar item
        IConsumer<EntityInsertedEvent<SidebarItem>>,
        IConsumer<EntityUpdatedEvent<SidebarItem>>,
        IConsumer<EntityDeletedEvent<SidebarItem>>,
        //slider item
        IConsumer<EntityInsertedEvent<SliderItem>>,
        IConsumer<EntityUpdatedEvent<SliderItem>>,
        IConsumer<EntityDeletedEvent<SliderItem>>,
        //home page item
        IConsumer<EntityInsertedEvent<HomePageItem>>,
        IConsumer<EntityUpdatedEvent<HomePageItem>>,
        IConsumer<EntityDeletedEvent<HomePageItem>>,
        //sidebar
        IConsumer<EntityInsertedEvent<Sidebar>>,
        IConsumer<EntityUpdatedEvent<Sidebar>>,
        IConsumer<EntityDeletedEvent<Sidebar>>
    {
        /// <summary>
        /// Key for nopCommerce.com news cache
        /// </summary>
        public const string OFFICIAL_NEWS_MODEL_KEY = "Nop.pres.admin.official.news";
        public const string OFFICIAL_NEWS_PATTERN_KEY = "Nop.pres.admin.official.news";
        
        /// <summary>
        /// Key for specification attributes caching (product details page)
        /// </summary>
        public const string SPEC_ATTRIBUTES_MODEL_KEY = "Nop.pres.admin.product.specs";
        public const string SPEC_ATTRIBUTES_PATTERN_KEY = "Nop.pres.admin.product.specs";

        /// <summary>
        /// Key for categories caching
        /// </summary>
        /// <remarks>
        /// {0} : show hidden records?
        /// </remarks>
        public const string CATEGORIES_LIST_KEY = "Nop.pres.admin.categories.list-{0}";
        public const string CATEGORIES_LIST_PATTERN_KEY = "Nop.pres.admin.categories.list";

        /// <summary>
        /// Key for products caching
        /// </summary>
        /// <remarks>
        /// {0} : show hidden records?
        /// </remarks>
        public const string PRODUCTS_LIST_KEY = "Nop.pres.admin.products.list-{0}";
        public const string PRODUCTS_LIST_PATTERN_KEY = "Nop.pres.admin.products.list";

        /// <summary>
        /// Key for elements caching
        /// </summary>
        /// <remarks>
        /// {0} : show hidden records?
        /// </remarks>
        public const string ELEMENTS_LIST_KEY = "Nop.pres.admin.elements.list-{0}";
        public const string ELEMENTS_LIST_PATTERN_KEY = "Nop.pres.admin.elements.list";

        /// <summary>
        /// Key for topics caching
        /// </summary>
        /// <remarks>
        /// {0} : show hidden records?
        /// </remarks>
        public const string TOPICS_LIST_KEY = "Nop.pres.admin.topics.list-{0}";
        public const string TOPICS_LIST_PATTERN_KEY = "Nop.pres.admin.topics.list";

        /// <summary>
        /// Key for sidebars caching
        /// </summary>
        /// <remarks>
        /// {0} : show hidden records?
        /// </remarks>
        public const string SIDEBARS_LIST_KEY = "Nop.pres.admin.sidebars.list-{0}";
        public const string SIDEBARS_LIST_PATTERN_KEY = "Nop.pres.admin.sidebars.list";

        /// <summary>
        /// Key for manufacturers caching
        /// </summary>
        /// <remarks>
        /// {0} : show hidden records?
        /// </remarks>
        public const string MANUFACTURERS_LIST_KEY = "Nop.pres.admin.manufacturers.list-{0}";
        public const string MANUFACTURERS_LIST_PATTERN_KEY = "Nop.pres.admin.manufacturers.list";

        /// <summary>
        /// Key for vendors caching
        /// </summary>
        /// <remarks>
        /// {0} : show hidden records?
        /// </remarks>
        public const string VENDORS_LIST_KEY = "Nop.pres.admin.vendors.list-{0}";
        public const string VENDORS_LIST_PATTERN_KEY = "Nop.pres.admin.vendors.list";

        /// <summary>
        /// Key for mega menu tab columns caching
        /// </summary>
        /// <remarks>
        /// {0} : Mega menu tab identifier
        /// </remarks>
        public const string MEGAMENU_COLUMN_LIST_KEY = "Nop.pres.admin.megamenucolumn.list-{0}";
        public const string MEGAMENU_COLUMN_LIST_PATTERN_KEY = "Nop.pres.admin.megamenucolumn.list";

        /// <summary>
        /// Key for menu columns caching
        /// </summary>
        /// <remarks>
        /// {0} : Menu identifier
        /// </remarks>
        public const string MENU_COLUMN_LIST_KEY = "Nop.pres.admin.menucolumn.list-{0}";
        public const string MENU_COLUMN_LIST_PATTERN_KEY = "Nop.pres.admin.menucolumn.list";

        /// <summary>
        /// Key for sidebar columns caching
        /// </summary>
        /// <remarks>
        /// {0} : Sidebar identifier
        /// </remarks>
        public const string SIDEBAR_COLUMN_LIST_KEY = "Nop.pres.admin.sidebarcolumn.list-{0}";
        public const string SIDEBAR_COLUMN_LIST_PATTERN_KEY = "Nop.pres.admin.sidebarcolumn.list";

        /// <summary>
        /// Key for slider columns caching
        /// </summary>
        /// <remarks>
        /// {0} : Slider identifier
        /// </remarks>
        public const string SLIDER_COLUMN_LIST_KEY = "Nop.pres.admin.slidercolumn.list-{0}";
        public const string SLIDER_COLUMN_LIST_PATTERN_KEY = "Nop.pres.admin.slidercolumn.list";

        /// <summary>
        /// Key for home page columns caching
        /// </summary>
        /// <remarks>
        /// {0} : Home page identifier
        /// </remarks>
        public const string HOMEPAGE_COLUMN_LIST_KEY = "Nop.pres.admin.homepagecolumn.list-{0}";
        public const string HOMEPAGE_COLUMN_LIST_PATTERN_KEY = "Nop.pres.admin.homepagecolumn.list";

        /// <summary>
        /// Key for mega menu tabs caching
        /// </summary>
        /// <remarks>
        /// {0} : show hidden records?
        /// </remarks>
        public const string MEGAMENU_TAB_LIST_KEY = "Nop.pres.admin.megamenutab.list-{0}";
        public const string MEGAMENU_TAB_LIST_PATTERN_KEY = "Nop.pres.admin.megamenutab.list";

        private readonly ICacheManager _cacheManager;
        
        public ModelCacheEventConsumer(IStaticCacheManager cacheManager)
        {
            this._cacheManager = cacheManager;
        }

        public void HandleEvent(EntityUpdatedEvent<Setting> eventMessage)
        {
            //clear models which depend on settings
            _cacheManager.RemoveByPattern(OFFICIAL_NEWS_PATTERN_KEY); //depends on AdminAreaSettings.HideAdvertisementsOnAdminArea
        }
        
        //specification attributes
        public void HandleEvent(EntityInsertedEvent<SpecificationAttribute> eventMessage)
        {
            _cacheManager.RemoveByPattern(SPEC_ATTRIBUTES_PATTERN_KEY);
        }
        public void HandleEvent(EntityUpdatedEvent<SpecificationAttribute> eventMessage)
        {
            _cacheManager.RemoveByPattern(SPEC_ATTRIBUTES_PATTERN_KEY);
        }
        public void HandleEvent(EntityDeletedEvent<SpecificationAttribute> eventMessage)
        {
            _cacheManager.RemoveByPattern(SPEC_ATTRIBUTES_PATTERN_KEY);
        }

        //categories
        public void HandleEvent(EntityInsertedEvent<Category> eventMessage)
        {
            _cacheManager.RemoveByPattern(CATEGORIES_LIST_PATTERN_KEY);
            _cacheManager.RemoveByPattern(MEGAMENU_COLUMN_LIST_PATTERN_KEY);
            _cacheManager.RemoveByPattern(MENU_COLUMN_LIST_PATTERN_KEY);
            _cacheManager.RemoveByPattern(SIDEBAR_COLUMN_LIST_PATTERN_KEY);
            _cacheManager.RemoveByPattern(SLIDER_COLUMN_LIST_PATTERN_KEY);
            _cacheManager.RemoveByPattern(HOMEPAGE_COLUMN_LIST_PATTERN_KEY);
        }
        public void HandleEvent(EntityUpdatedEvent<Category> eventMessage)
        {
            _cacheManager.RemoveByPattern(CATEGORIES_LIST_PATTERN_KEY);
            _cacheManager.RemoveByPattern(MEGAMENU_COLUMN_LIST_PATTERN_KEY);
            _cacheManager.RemoveByPattern(MENU_COLUMN_LIST_PATTERN_KEY);
            _cacheManager.RemoveByPattern(SIDEBAR_COLUMN_LIST_PATTERN_KEY);
            _cacheManager.RemoveByPattern(SLIDER_COLUMN_LIST_PATTERN_KEY);
            _cacheManager.RemoveByPattern(HOMEPAGE_COLUMN_LIST_PATTERN_KEY);
        }
        public void HandleEvent(EntityDeletedEvent<Category> eventMessage)
        {
            _cacheManager.RemoveByPattern(CATEGORIES_LIST_PATTERN_KEY);
            _cacheManager.RemoveByPattern(MEGAMENU_COLUMN_LIST_PATTERN_KEY);
            _cacheManager.RemoveByPattern(MENU_COLUMN_LIST_PATTERN_KEY);
            _cacheManager.RemoveByPattern(SIDEBAR_COLUMN_LIST_PATTERN_KEY);
            _cacheManager.RemoveByPattern(SLIDER_COLUMN_LIST_PATTERN_KEY);
            _cacheManager.RemoveByPattern(HOMEPAGE_COLUMN_LIST_PATTERN_KEY);
        }

        //manufacturers
        public void HandleEvent(EntityInsertedEvent<Manufacturer> eventMessage)
        {
            _cacheManager.RemoveByPattern(MANUFACTURERS_LIST_PATTERN_KEY);
        }
        public void HandleEvent(EntityUpdatedEvent<Manufacturer> eventMessage)
        {
            _cacheManager.RemoveByPattern(MANUFACTURERS_LIST_PATTERN_KEY);
        }
        public void HandleEvent(EntityDeletedEvent<Manufacturer> eventMessage)
        {
            _cacheManager.RemoveByPattern(MANUFACTURERS_LIST_PATTERN_KEY);
        }

        //vendors
        public void HandleEvent(EntityInsertedEvent<Vendor> eventMessage)
        {
            _cacheManager.RemoveByPattern(VENDORS_LIST_PATTERN_KEY);
        }
        public void HandleEvent(EntityUpdatedEvent<Vendor> eventMessage)
        {
            _cacheManager.RemoveByPattern(VENDORS_LIST_PATTERN_KEY);
        }
        public void HandleEvent(EntityDeletedEvent<Vendor> eventMessage)
        {
            _cacheManager.RemoveByPattern(VENDORS_LIST_PATTERN_KEY);
        }

        //mega menu tab item
        public void HandleEvent(EntityInsertedEvent<MegaMenuTabItem> eventMessage)
        {
            _cacheManager.RemoveByPattern(MEGAMENU_COLUMN_LIST_PATTERN_KEY);
        }
        public void HandleEvent(EntityUpdatedEvent<MegaMenuTabItem> eventMessage)
        {
            _cacheManager.RemoveByPattern(MEGAMENU_COLUMN_LIST_PATTERN_KEY);
        }
        public void HandleEvent(EntityDeletedEvent<MegaMenuTabItem> eventMessage)
        {
            _cacheManager.RemoveByPattern(MEGAMENU_COLUMN_LIST_PATTERN_KEY);
        }

        //menu item
        public void HandleEvent(EntityInsertedEvent<MenuItem> eventMessage)
        {
            _cacheManager.RemoveByPattern(MENU_COLUMN_LIST_PATTERN_KEY);
        }
        public void HandleEvent(EntityUpdatedEvent<MenuItem> eventMessage)
        {
            _cacheManager.RemoveByPattern(MENU_COLUMN_LIST_PATTERN_KEY);
        }
        public void HandleEvent(EntityDeletedEvent<MenuItem> eventMessage)
        {
            _cacheManager.RemoveByPattern(MENU_COLUMN_LIST_PATTERN_KEY);
        }

        //sidebar item
        public void HandleEvent(EntityInsertedEvent<SidebarItem> eventMessage)
        {
            _cacheManager.RemoveByPattern(SIDEBAR_COLUMN_LIST_PATTERN_KEY);
        }
        public void HandleEvent(EntityUpdatedEvent<SidebarItem> eventMessage)
        {
            _cacheManager.RemoveByPattern(SIDEBAR_COLUMN_LIST_PATTERN_KEY);
        }
        public void HandleEvent(EntityDeletedEvent<SidebarItem> eventMessage)
        {
            _cacheManager.RemoveByPattern(SIDEBAR_COLUMN_LIST_PATTERN_KEY);
        }

        //slider item
        public void HandleEvent(EntityInsertedEvent<SliderItem> eventMessage)
        {
            _cacheManager.RemoveByPattern(SLIDER_COLUMN_LIST_PATTERN_KEY);
        }
        public void HandleEvent(EntityUpdatedEvent<SliderItem> eventMessage)
        {
            _cacheManager.RemoveByPattern(SLIDER_COLUMN_LIST_PATTERN_KEY);
        }
        public void HandleEvent(EntityDeletedEvent<SliderItem> eventMessage)
        {
            _cacheManager.RemoveByPattern(SLIDER_COLUMN_LIST_PATTERN_KEY);
        }

        //home papge item
        public void HandleEvent(EntityInsertedEvent<HomePageItem> eventMessage)
        {
            _cacheManager.RemoveByPattern(HOMEPAGE_COLUMN_LIST_PATTERN_KEY);
        }
        public void HandleEvent(EntityUpdatedEvent<HomePageItem> eventMessage)
        {
            _cacheManager.RemoveByPattern(HOMEPAGE_COLUMN_LIST_PATTERN_KEY);
        }
        public void HandleEvent(EntityDeletedEvent<HomePageItem> eventMessage)
        {
            _cacheManager.RemoveByPattern(HOMEPAGE_COLUMN_LIST_PATTERN_KEY);
        }

        //mega menu tab
        public void HandleEvent(EntityInsertedEvent<MegaMenuTab> eventMessage)
        {
            _cacheManager.RemoveByPattern(MEGAMENU_TAB_LIST_PATTERN_KEY);
        }
        public void HandleEvent(EntityUpdatedEvent<MegaMenuTab> eventMessage)
        {
            _cacheManager.RemoveByPattern(MEGAMENU_TAB_LIST_PATTERN_KEY);
        }
        public void HandleEvent(EntityDeletedEvent<MegaMenuTab> eventMessage)
        {
            _cacheManager.RemoveByPattern(MEGAMENU_TAB_LIST_PATTERN_KEY);
        }

        //products
        public void HandleEvent(EntityInsertedEvent<Product> eventMessage)
        {
            _cacheManager.RemoveByPattern(PRODUCTS_LIST_PATTERN_KEY);
            _cacheManager.RemoveByPattern(MEGAMENU_COLUMN_LIST_PATTERN_KEY);
            _cacheManager.RemoveByPattern(MENU_COLUMN_LIST_PATTERN_KEY);
            _cacheManager.RemoveByPattern(SIDEBAR_COLUMN_LIST_PATTERN_KEY);
            _cacheManager.RemoveByPattern(SLIDER_COLUMN_LIST_PATTERN_KEY);
            _cacheManager.RemoveByPattern(HOMEPAGE_COLUMN_LIST_PATTERN_KEY);
        }
        public void HandleEvent(EntityUpdatedEvent<Product> eventMessage)
        {
            _cacheManager.RemoveByPattern(PRODUCTS_LIST_PATTERN_KEY);
            _cacheManager.RemoveByPattern(MEGAMENU_COLUMN_LIST_PATTERN_KEY);
            _cacheManager.RemoveByPattern(MENU_COLUMN_LIST_PATTERN_KEY);
            _cacheManager.RemoveByPattern(SIDEBAR_COLUMN_LIST_PATTERN_KEY);
            _cacheManager.RemoveByPattern(SLIDER_COLUMN_LIST_PATTERN_KEY);
            _cacheManager.RemoveByPattern(HOMEPAGE_COLUMN_LIST_PATTERN_KEY);
        }
        public void HandleEvent(EntityDeletedEvent<Product> eventMessage)
        {
            _cacheManager.RemoveByPattern(PRODUCTS_LIST_PATTERN_KEY);
            _cacheManager.RemoveByPattern(MEGAMENU_COLUMN_LIST_PATTERN_KEY);
            _cacheManager.RemoveByPattern(MENU_COLUMN_LIST_PATTERN_KEY);
            _cacheManager.RemoveByPattern(SIDEBAR_COLUMN_LIST_PATTERN_KEY);
            _cacheManager.RemoveByPattern(SLIDER_COLUMN_LIST_PATTERN_KEY);
            _cacheManager.RemoveByPattern(HOMEPAGE_COLUMN_LIST_PATTERN_KEY);
        }

        //elements
        public void HandleEvent(EntityInsertedEvent<Element> eventMessage)
        {
            _cacheManager.RemoveByPattern(ELEMENTS_LIST_PATTERN_KEY);
            _cacheManager.RemoveByPattern(MEGAMENU_COLUMN_LIST_PATTERN_KEY);
            _cacheManager.RemoveByPattern(MENU_COLUMN_LIST_PATTERN_KEY);
            _cacheManager.RemoveByPattern(SIDEBAR_COLUMN_LIST_PATTERN_KEY);
            _cacheManager.RemoveByPattern(SLIDER_COLUMN_LIST_PATTERN_KEY);
            _cacheManager.RemoveByPattern(HOMEPAGE_COLUMN_LIST_PATTERN_KEY);
        }
        public void HandleEvent(EntityUpdatedEvent<Element> eventMessage)
        {
            _cacheManager.RemoveByPattern(ELEMENTS_LIST_PATTERN_KEY);
            _cacheManager.RemoveByPattern(MEGAMENU_COLUMN_LIST_PATTERN_KEY);
            _cacheManager.RemoveByPattern(SIDEBAR_COLUMN_LIST_PATTERN_KEY);
            _cacheManager.RemoveByPattern(MENU_COLUMN_LIST_PATTERN_KEY);
            _cacheManager.RemoveByPattern(SLIDER_COLUMN_LIST_PATTERN_KEY);
            _cacheManager.RemoveByPattern(HOMEPAGE_COLUMN_LIST_PATTERN_KEY);
        }
        public void HandleEvent(EntityDeletedEvent<Element> eventMessage)
        {
            _cacheManager.RemoveByPattern(ELEMENTS_LIST_PATTERN_KEY);
            _cacheManager.RemoveByPattern(MEGAMENU_COLUMN_LIST_PATTERN_KEY);
            _cacheManager.RemoveByPattern(MENU_COLUMN_LIST_PATTERN_KEY);
            _cacheManager.RemoveByPattern(SIDEBAR_COLUMN_LIST_PATTERN_KEY);
            _cacheManager.RemoveByPattern(SLIDER_COLUMN_LIST_PATTERN_KEY);
            _cacheManager.RemoveByPattern(HOMEPAGE_COLUMN_LIST_PATTERN_KEY);
        }

        //topics
        public void HandleEvent(EntityInsertedEvent<Topic> eventMessage)
        {
            _cacheManager.RemoveByPattern(TOPICS_LIST_PATTERN_KEY);
            _cacheManager.RemoveByPattern(MEGAMENU_COLUMN_LIST_PATTERN_KEY);
            _cacheManager.RemoveByPattern(MENU_COLUMN_LIST_PATTERN_KEY);
            _cacheManager.RemoveByPattern(SIDEBAR_COLUMN_LIST_PATTERN_KEY);
            _cacheManager.RemoveByPattern(SLIDER_COLUMN_LIST_PATTERN_KEY);
            _cacheManager.RemoveByPattern(HOMEPAGE_COLUMN_LIST_PATTERN_KEY);
        }
        public void HandleEvent(EntityUpdatedEvent<Topic> eventMessage)
        {
            _cacheManager.RemoveByPattern(TOPICS_LIST_PATTERN_KEY);
            _cacheManager.RemoveByPattern(MEGAMENU_COLUMN_LIST_PATTERN_KEY);
            _cacheManager.RemoveByPattern(MENU_COLUMN_LIST_PATTERN_KEY);
            _cacheManager.RemoveByPattern(SIDEBAR_COLUMN_LIST_PATTERN_KEY);
            _cacheManager.RemoveByPattern(SLIDER_COLUMN_LIST_PATTERN_KEY);
            _cacheManager.RemoveByPattern(HOMEPAGE_COLUMN_LIST_PATTERN_KEY);
        }
        public void HandleEvent(EntityDeletedEvent<Topic> eventMessage)
        {
            _cacheManager.RemoveByPattern(TOPICS_LIST_PATTERN_KEY);
            _cacheManager.RemoveByPattern(MEGAMENU_COLUMN_LIST_PATTERN_KEY);
            _cacheManager.RemoveByPattern(MENU_COLUMN_LIST_PATTERN_KEY);
            _cacheManager.RemoveByPattern(SLIDER_COLUMN_LIST_PATTERN_KEY);
            _cacheManager.RemoveByPattern(HOMEPAGE_COLUMN_LIST_PATTERN_KEY);
        }

        //sidebar list
        public void HandleEvent(EntityInsertedEvent<Sidebar> eventMessage)
        {
            _cacheManager.RemoveByPattern(SIDEBARS_LIST_PATTERN_KEY);
        }
        public void HandleEvent(EntityUpdatedEvent<Sidebar> eventMessage)
        {
            _cacheManager.RemoveByPattern(SIDEBARS_LIST_PATTERN_KEY);
        }
        public void HandleEvent(EntityDeletedEvent<Sidebar> eventMessage)
        {
            _cacheManager.RemoveByPattern(SIDEBARS_LIST_PATTERN_KEY);
        }
    }
}