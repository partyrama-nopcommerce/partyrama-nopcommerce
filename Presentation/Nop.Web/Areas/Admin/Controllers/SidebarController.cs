﻿using Microsoft.AspNetCore.Mvc;
using Nop.Core.Caching;
using Nop.Core.Domain.Catalog;
using Nop.Services.Catalog;
using Nop.Services.Localization;
using Nop.Services.Security;
using Nop.Services.Topics;
using Nop.Web.Areas.Admin.Factories;
using Nop.Web.Areas.Admin.Helpers;
using Nop.Web.Areas.Admin.Infrastructure.Mapper.Extensions;
using Nop.Web.Areas.Admin.Models.Catalog;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Mvc;
using System;
using System.Linq;

namespace Nop.Web.Areas.Admin.Controllers
{
    public partial class SidebarController : BaseAdminController
    {
        #region Fields

        private readonly ISidebarModelFactory _sidebarModelFactory;
        private readonly IPermissionService _permissionService;
        private readonly ISidebarService _sidebarService;
        private readonly ILocalizationService _localizationService;
        private readonly ISidebarItemService _sidebarItemService;
        private readonly ICategoryService _categoryService;
        private readonly IProductService _productService;
        private readonly IElementService _elementService;
        private readonly IStaticCacheManager _cacheManager;
        private readonly ITopicService _topicService;
        private readonly ISliderService _sliderService;

        #endregion

        #region Ctor

        public SidebarController(ISidebarModelFactory sidebarModelFactory,
            IPermissionService permissionService,
            ISidebarService sidebarService,
            ILocalizationService localizationService,
            ISidebarItemService sidebarItemService,
            ICategoryService categoryService,
            IProductService productService,
            IElementService elementService,
            IStaticCacheManager cacheManager,
            ITopicService topicService,
            ISliderService sliderService)
        {
            _sidebarModelFactory = sidebarModelFactory;
            _permissionService = permissionService;
            _sidebarService = sidebarService;
            _localizationService = localizationService;
            _sidebarItemService = sidebarItemService;
            _categoryService = categoryService;
            _productService = productService;
            _elementService = elementService;
            _cacheManager = cacheManager;
            _topicService = topicService;
            _sliderService = sliderService;
        }

        #endregion

        #region Utilities

        #endregion

        #region List

        [HttpPost]
        public virtual IActionResult SidebarItemList(SidebarItemSearchModel searchModel)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSidebar))
                return AccessDeniedKendoGridJson();

            //prepare model
            var model = _sidebarModelFactory.PrepareSidebarItemListModel(searchModel);

            return Json(model);
        }

        [HttpPost]
        public virtual IActionResult SidebarItemRowList(SidebarItemRowSearchModel searchModel)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSidebar))
                return AccessDeniedKendoGridJson();

            //prepare model
            var model = _sidebarModelFactory.PrepareSidebarItemRowListModel(searchModel);

            return Json(model);
        }

        [HttpPost]
        public virtual JsonResult GetAvailableSidebars()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSidebar))
                return Json("Access denied");

            var availableSidebarItems = SelectListHelper.GetSidebarList(_sidebarService, _cacheManager, true);
            return Json(availableSidebarItems);
        }

        [HttpPost]
        public virtual JsonResult GetAvailableCollapseItem(int parentId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSidebar))
                return Json("Access denied");

            var availableCollapseItems = SelectListHelper.GetSidebarColumnsList(_sidebarItemService, _categoryService, _cacheManager, parentId);
            return Json(availableCollapseItems);
        }

        [HttpPost]
        public virtual IActionResult GetAvailablePicture(int connectionId, RowConnectionType connectionType)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSidebar))
                return AccessDeniedKendoGridJson();

            var model = _sidebarModelFactory.GetAvailablePicture(connectionId, connectionType);

            return Json(model);
        }

        #endregion

        #region Create / Edit / Delete

        public virtual IActionResult Edit()
        {
            //prepare model
            var model = _sidebarModelFactory.PrepareSidebarEditModel();

            return View(model);
        }

        #region Sidebars

        [HttpPost]
        public virtual IActionResult CreateSidebar(SidebarModel newSidebar)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSidebar))
                return AccessDeniedView();

            if (ModelState.IsValid)
            {
                var sidebar = newSidebar.ToEntity<Sidebar>();
                sidebar.CreatedOnUtc = DateTime.UtcNow;
                sidebar.UpdatedOnUtc = DateTime.UtcNow;
                _sidebarService.InsertSidebar(sidebar);

                return Json(new
                {
                    id = sidebar.Id,
                    name = sidebar.Name,
                    url = Url.Action("EditSidebar", "Sidebar", new { id = sidebar.Id }),
                    success = true
                });
            }
            else
            {
                var errorMessage = string.Empty;
                foreach (var modelState in ModelState.Values)
                {
                    foreach (var error in modelState.Errors)
                    {
                        errorMessage += error.ErrorMessage + Environment.NewLine;
                    }  
                }
                    

                return Json(new
                {
                    success = false,
                    message = errorMessage
                });
            }
        }

        [HttpPost]
        public virtual IActionResult DeleteSidebar(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSidebar))
                return AccessDeniedView();

            //try to get a sidebar with the specified id
            var sidebar = _sidebarService.GetSidebarById(id);
            if (sidebar == null)
            {
                return Json(new
                {
                    success = false,
                    message = _localizationService.GetResource("Sidebar.DeleteSidebar.Error")
                });
            }

            _sidebarService.DeleteSidebar(sidebar);
            return Json(new
            {
                success = true
            });
        }

        public virtual IActionResult EditSidebar(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSidebar))
                return AccessDeniedView();

            //try to get a sidebar with the specified id
            var sidebar = _sidebarService.GetSidebarById(id);

            if (sidebar == null)
                return RedirectToAction("Edit");

            //prepare model
            var model = _sidebarModelFactory.PrepareSidebarModel(null, sidebar);
            return PartialView("_SidebarEdit", model);
        }

        [HttpPost]
        public virtual IActionResult EditSidebar(SidebarModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSidebar))
                return AccessDeniedView();

            //try to get a sidebar with the specified id
            var sidebar = _sidebarService.GetSidebarById(model.Id);

            if (sidebar == null)
                return RedirectToAction("Edit");

            if (ModelState.IsValid)
            {
                sidebar = model.ToEntity(sidebar);
                sidebar.UpdatedOnUtc = DateTime.UtcNow;
                _sidebarService.UpdateSidebar(sidebar);

                return Json(new
                {
                    name = sidebar.Name,
                    success = true
                });
            }
            else
            {
                var errorMessage = string.Empty;
                foreach (var modelState in ModelState.Values)
                {
                    foreach (var error in modelState.Errors)
                    {
                        errorMessage += error.ErrorMessage + Environment.NewLine;
                    }
                }

                return Json(new
                {
                    success = false,
                    message = errorMessage
                });
            }
        }

        #endregion Sidebars

        #region Items 

        public virtual IActionResult AddColumn(int id, RowType rowType)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSidebar))
                return AccessDeniedView();

            //try to get a sidebar with the specified id
            var sidebar = _sidebarService.GetSidebarById(id);

            if (sidebar == null)
                return RedirectToAction("Edit");

            //prepare model

            var model = new SidebarItemModel
            {
                ItemSidebarId = sidebar.Id,
                ItemRowType = rowType
            };

            _sidebarModelFactory.PrepareSidebarItemModel(model);

            return PartialView("_AddSidebarItem", model);
        }

        [HttpPost]
        public virtual IActionResult AddColumn(SidebarItemModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSidebar))
                return AccessDeniedView();

            if (ModelState.IsValid)
            {
                var sidebarItem = model.ToEntity<SidebarItem>();

                sidebarItem.CreatedOnUtc = DateTime.UtcNow;
                sidebarItem.UpdatedOnUtc = DateTime.UtcNow;
                _sidebarItemService.InsertSidebarItem(sidebarItem);

                return Json(new
                {
                    success = true
                });
            }
            else
            {
                var errorMessage = string.Empty;
                foreach (var modelState in ModelState.Values)
                {
                    foreach (var error in modelState.Errors)
                    {
                        errorMessage += error.ErrorMessage + Environment.NewLine;
                    }
                }


                return Json(new
                {
                    success = false,
                    message = errorMessage
                });
            }
        }


        public virtual IActionResult DeleteSidebarItem(int id)
        {
            //try to get a sidebar item with the specified id
            var sidebarItem = _sidebarItemService.GetSidebarItemById(id) ??
                throw new ArgumentException("No sidebar item found with the specified id");

            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSidebar))
                return AccessDeniedView();

            _sidebarItemService.DeleteSidebarItem(sidebarItem);

            return new NullJsonResult();
        }

        public virtual IActionResult UpdateSidebarItem(SidebarItemModel model)
        {
            if (ModelState.IsValid)
            {
                if (!_permissionService.Authorize(StandardPermissionProvider.ManageSidebar))
                    return AccessDeniedView();

                //try to get a sidebar item with the specified id
                var row = _sidebarItemService.GetSidebarItemById(model.Id) ??
                    throw new ArgumentException("No sidebar item found with the specified id");

                row = model.ToEntity(row);

                row.UpdatedOnUtc = DateTime.UtcNow;
                _sidebarItemService.UpdateSidebarItem(row);
            }

            return new NullJsonResult();
        }

        [HttpPost]
        public virtual IActionResult ReOrderColumn(ReOrderSidebarItemModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSidebar))
                return AccessDeniedView();

            _sidebarItemService.ReOrderSidebarItem(model.NewDisplayOrder, model.OldDisplayOrder, model.DragItemId, true);

            return new NullJsonResult();
        }

        [HttpPost]
        public virtual IActionResult ReOrderRow(ReOrderSidebarItemModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSidebar))
                return AccessDeniedView();

            _sidebarItemService.ReOrderSidebarItem(model.NewDisplayOrder, model.OldDisplayOrder, model.DragItemId, false);

            return new NullJsonResult();
        }


        #region Add Category Row

        public virtual IActionResult SidebarAddRowCategoryPopup(int sidebarId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSidebar))
                return AccessDeniedView();

            //prepare model
            var model = _sidebarModelFactory.PrepareAddRowCategorySearchModel(new SidebarAddRowCategorySearchModel
            {
                SidebarId = sidebarId
            });

            return View(model);
        }

        [HttpPost]
        public virtual IActionResult RowCategoryAddPopupList(SidebarAddRowCategorySearchModel searchModel)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSidebar))
                return AccessDeniedKendoGridJson();

            //prepare model
            var model = _sidebarModelFactory.PrepareSidebarAddRowCategoryListModel(searchModel);

            return Json(model);
        }

        [HttpPost]
        [FormValueRequired("save")]
        public virtual IActionResult SidebarAddRowCategoryPopup(SidebarAddRowCategoryModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSidebar))
                return AccessDeniedView();

            //get selected categories
            var selectedCategories = _categoryService.GetCategoriesByIds(model.SelectedCategoryIds.ToArray());
            if (selectedCategories.Any())
            {
                _sidebarItemService.InsertCategoryRows(selectedCategories, model.SidebarItemId, model.SidebarId);
            }

            ViewBag.RefreshPage = true;

            return View(new SidebarAddRowCategorySearchModel());
        }

        #endregion Add Category Row

        #region Add Product Row

        public virtual IActionResult SidebarAddRowProductPopup(int sidebarId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSidebar))
                return AccessDeniedView();

            //prepare model
            var model = _sidebarModelFactory.PrepareAddRowProductSearchModel(new SidebarAddRowProductSearchModel
            {
                SidebarId = sidebarId
            });

            return View(model);
        }

        [HttpPost]
        public virtual IActionResult RowProductAddPopupList(SidebarAddRowProductSearchModel searchModel)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSidebar))
                return AccessDeniedKendoGridJson();

            //prepare model
            var model = _sidebarModelFactory.PrepareSidebarAddRowProductListModel(searchModel);

            return Json(model);
        }

        [HttpPost]
        [FormValueRequired("save")]
        public virtual IActionResult SidebarAddRowProductPopup(SidebarAddRowProductModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSidebar))
                return AccessDeniedView();

            //get selected products
            var selectedProducts = _productService.GetProductsByIds(model.SelectedProductIds.ToArray());
            if (selectedProducts.Any())
            {
                _sidebarItemService.InsertProductRows(selectedProducts.ToList(), model.SidebarItemId, model.SidebarId);
            }

            ViewBag.RefreshPage = true;

            return View(new SidebarAddRowProductSearchModel());
        }

        #endregion Add Product Row

        #region Add Element Row

        public virtual IActionResult SidebarAddRowElementPopup(int sidebarId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSidebar))
                return AccessDeniedView();

            //prepare model
            var model = _sidebarModelFactory.PrepareAddRowElementSearchModel(new SidebarAddRowElementSearchModel
            {
                SidebarId = sidebarId
            });

            return View(model);
        }

        [HttpPost]
        public virtual IActionResult RowElementAddPopupList(SidebarAddRowElementSearchModel searchModel)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSidebar))
                return AccessDeniedKendoGridJson();

            //prepare model
            var model = _sidebarModelFactory.PrepareSidebarAddRowElementListModel(searchModel);

            return Json(model);
        }

        [HttpPost]
        [FormValueRequired("save")]
        public virtual IActionResult SidebarAddRowElementPopup(SidebarAddRowElementModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSidebar))
                return AccessDeniedView();

            //get selected elements
            var selectedElements = _elementService.GetElementsByIds(model.SelectedElementIds.ToArray());
            if (selectedElements.Any())
            {
                _sidebarItemService.InsertElementRows(selectedElements, model.SidebarItemId, model.SidebarId);
            }

            ViewBag.RefreshPage = true;

            return View(new SidebarAddRowElementSearchModel());
        }

        #endregion Add Element Row

        #region Add Topic Row

        public virtual IActionResult SidebarAddRowTopicPopup(int sidebarId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSidebar))
                return AccessDeniedView();

            //prepare model
            var model = _sidebarModelFactory.PrepareAddRowTopicSearchModel(new SidebarAddRowTopicSearchModel
            {
                SidebarId = sidebarId
            });

            return View(model);
        }

        [HttpPost]
        public virtual IActionResult RowTopicAddPopupList(SidebarAddRowTopicSearchModel searchModel)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSidebar))
                return AccessDeniedKendoGridJson();

            //prepare model
            var model = _sidebarModelFactory.PrepareSidebarAddRowTopicListModel(searchModel);

            return Json(model);
        }

        [HttpPost]
        [FormValueRequired("save")]
        public virtual IActionResult SidebarAddRowTopicPopup(SidebarAddRowTopicModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSidebar))
                return AccessDeniedView();

            //get selected topics
            var selectedTopics = _topicService.GetTopicsByIds(model.SelectedTopicIds.ToArray());
            if (selectedTopics.Any())
            {
                _sidebarItemService.InsertTopicsRows(selectedTopics, model.SidebarItemId, model.SidebarId);
            }

            ViewBag.RefreshPage = true;

            return View(new SidebarAddRowTopicSearchModel());
        }

        #endregion Add Topic Row

        #region Add Slider Row

        public virtual IActionResult SidebarAddRowSliderPopup(int sidebarId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSidebar))
                return AccessDeniedView();

            //prepare model
            var model = _sidebarModelFactory.PrepareAddRowSliderSearchModel(new SidebarAddRowSliderSearchModel
            {
                SidebarId = sidebarId
            });

            return View(model);
        }

        [HttpPost]
        public virtual IActionResult RowSliderAddPopupList(SidebarAddRowSliderSearchModel searchModel)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSidebar))
                return AccessDeniedKendoGridJson();

            //prepare model
            var model = _sidebarModelFactory.PrepareSidebarAddRowSliderListModel(searchModel);

            return Json(model);
        }

        [HttpPost]
        [FormValueRequired("save")]
        public virtual IActionResult SidebarAddRowSliderPopup(SidebarAddRowSliderModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSidebar))
                return AccessDeniedView();

            //get selected sliders
            var selectedSliders = _sliderService.GetSlidersByIds(model.SelectedSliderIds.ToArray());
            if (selectedSliders.Any())
            {
                _sidebarItemService.InsertSliderRows(selectedSliders, model.SidebarItemId, model.SidebarId);
            }

            ViewBag.RefreshPage = true;

            return View(new SidebarAddRowSliderSearchModel());
        }

        #endregion Add Slider Row

        #endregion Items

        #endregion

    }
}