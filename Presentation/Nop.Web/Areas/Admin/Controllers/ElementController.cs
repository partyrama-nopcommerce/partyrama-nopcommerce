﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Nop.Core.Caching;
using Nop.Core.Domain.Catalog;
using Nop.Services.Catalog;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Media;
using Nop.Services.Security;
using Nop.Web.Areas.Admin.Factories;
using Nop.Web.Areas.Admin.Helpers;
using Nop.Web.Areas.Admin.Infrastructure.Mapper.Extensions;
using Nop.Web.Areas.Admin.Models.Catalog;
using Nop.Web.Framework.Mvc;
using Nop.Web.Framework.Mvc.Filters;

namespace Nop.Web.Areas.Admin.Controllers
{
    public partial class ElementController : BaseAdminController
    {
        #region Fields

        private readonly IPermissionService _permissionService;
        private readonly IElementModelFactory _elementModelFactory;
        private readonly IElementService _elementService;
        private readonly ILocalizedEntityService _localizedEntityService;
        private readonly ICustomerActivityService _customerActivityService;
        private readonly ILocalizationService _localizationService;
        private readonly IPictureService _pictureService;
        private readonly IStaticCacheManager _cacheManager;

        #endregion

        #region Ctor

        public ElementController(IPermissionService permissionService,
            IElementModelFactory elementModelFactory,
            IElementService elementService,
            ILocalizedEntityService localizedEntityService,
            ICustomerActivityService customerActivityService,
            ILocalizationService localizationService,
            IPictureService pictureService,
            IStaticCacheManager cacheManager)
        {
            _permissionService = permissionService;
            _elementModelFactory = elementModelFactory;
            _elementService = elementService;
            _localizedEntityService = localizedEntityService;
            _customerActivityService = customerActivityService;
            _localizationService = localizationService;
            _pictureService = pictureService;
            _cacheManager = cacheManager;
        }

        #endregion

        #region Utilities

        private void UpdateLocales(Element element, ElementModel model)
        {
            foreach (var localized in model.Locales)
            {
                _localizedEntityService.SaveLocalizedValue(element,
                    x => x.Name,
                    localized.Name,
                    localized.LanguageId);

                _localizedEntityService.SaveLocalizedValue(element,
                    x => x.Description,
                    localized.Description,
                    localized.LanguageId);

                _localizedEntityService.SaveLocalizedValue(element,
                    x => x.ShortName,
                    localized.ShortName,
                    localized.LanguageId);
            }
        }

        protected virtual void UpdatePictureSeoNames(Element element)
        {
            var picture = _pictureService.GetPictureById(element.PictureId);
            if (picture != null)
                _pictureService.SetSeoFilename(picture.Id, _pictureService.GetPictureSeName(element.Name));
        }

        #endregion

        #region List

        public virtual IActionResult Index()
        {
            return RedirectToAction("List");
        }

        public virtual IActionResult List()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageElements))
                return AccessDeniedView();

            //prepare model
            var model = _elementModelFactory.PrepareElementSearchModel(new ElementSearchModel());

            return View(model);
        }

        [HttpPost]
        public virtual IActionResult List(ElementSearchModel searchModel)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageElements))
                return AccessDeniedKendoGridJson();

            //prepare model
            var model = _elementModelFactory.PrepareElementListModel(searchModel);

            return Json(model);
        }

        [HttpPost]
        public virtual JsonResult GetAvailableElements()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageElements))
                return Json("Access denied");

            var availableElementItems = SelectListHelper.GetElementList(_elementService, _cacheManager, true);
            return Json(availableElementItems);
        }

        #endregion

        #region Create / Edit / Delete

        public virtual IActionResult Create()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageElements))
                return AccessDeniedView();

            //prepare model
            var model = _elementModelFactory.PrepareElementModel(new ElementModel(), null);

            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public virtual IActionResult Create(ElementModel model, bool continueEditing)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageElements))
                return AccessDeniedView();

            if (ModelState.IsValid)
            {
                var element = model.ToEntity<Element>();
                element.CreatedOnUtc = DateTime.UtcNow;
                element.UpdatedOnUtc = DateTime.UtcNow;
                _elementService.InsertElement(element);

                //locales
                UpdateLocales(element, model);

                //update picture seo file name
                UpdatePictureSeoNames(element);

                //activity log
                _customerActivityService.InsertActivity("AddNewElement",
                    string.Format(_localizationService.GetResource("ActivityLog.AddNewElement"), element.Name), element);

                SuccessNotification(_localizationService.GetResource("Admin.Catalog.Elements.Added"));

                if (!continueEditing)
                    return RedirectToAction("List");

                //selected tab
                SaveSelectedTabName();

                return RedirectToAction("Edit", new { id = element.Id });
            }

            //prepare model
            model = _elementModelFactory.PrepareElementModel(model, null, true);

            //if we got this far, something failed, redisplay form
            return View(model);
        }

        public virtual IActionResult Edit(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageElements))
                return AccessDeniedView();

            //try to get an element with the specified id
            var element = _elementService.GetElementById(id);
            if (element == null)
                return RedirectToAction("List");

            //prepare model
            var model = _elementModelFactory.PrepareElementModel(null, element);

            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public virtual IActionResult Edit(ElementModel model, bool continueEditing)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageElements))
                return AccessDeniedView();

            //try to get a model with the specified id
            var element = _elementService.GetElementById(model.Id);
            if (element == null)
                return RedirectToAction("List");

            if (ModelState.IsValid)
            {
                var prevPictureId = element.PictureId;

                element = model.ToEntity(element);
                element.UpdatedOnUtc = DateTime.UtcNow;
                _elementService.UpdateElement(element);

                //locales
                UpdateLocales(element, model);

                //delete an old picture (if deleted or updated)
                if (prevPictureId > 0 && prevPictureId != element.PictureId)
                {
                    var prevPicture = _pictureService.GetPictureById(prevPictureId);
                    if (prevPicture != null)
                        _pictureService.DeletePicture(prevPicture);
                }

                //update picture seo file name
                UpdatePictureSeoNames(element);

                //activity log
                _customerActivityService.InsertActivity("EditElement",
                    string.Format(_localizationService.GetResource("ActivityLog.EditElement"), element.Name), element);

                SuccessNotification(_localizationService.GetResource("Admin.Catalog.Elements.Updated"));

                if (!continueEditing)
                    return RedirectToAction("List");

                //selected tab
                SaveSelectedTabName();

                return RedirectToAction("Edit", new { id = element.Id });
            }

            //prepare model
            model = _elementModelFactory.PrepareElementModel(model, element, true);

            //if we got this far, something failed, redisplay form
            return View(model);
        }

        [HttpPost]
        public virtual IActionResult Delete(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageElements))
                return AccessDeniedView();

            //try to get an element with the specified id
            var element = _elementService.GetElementById(id);
            if (element == null)
                return RedirectToAction("List");

            _elementService.DeleteElement(element);

            //activity log
            _customerActivityService.InsertActivity("DeleteElement",
                string.Format(_localizationService.GetResource("ActivityLog.DeleteElement"), element.Name), element);

            SuccessNotification(_localizationService.GetResource("Admin.Catalog.Elements.Deleted"));

            return RedirectToAction("List");
        }

        public virtual IActionResult DeleteFromList(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageElements))
                return AccessDeniedView();

            //try to get an element with the specified id
            var element = _elementService.GetElementById(id) ??
             throw new ArgumentException("No element found with the specified id");

            _elementService.DeleteElement(element);

            return new NullJsonResult();
        }

        #endregion
    }
}