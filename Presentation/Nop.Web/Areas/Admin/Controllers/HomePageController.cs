﻿using Microsoft.AspNetCore.Mvc;
using Nop.Core.Caching;
using Nop.Core.Domain.Catalog;
using Nop.Services.Catalog;
using Nop.Services.Localization;
using Nop.Services.Security;
using Nop.Services.Topics;
using Nop.Web.Areas.Admin.Factories;
using Nop.Web.Areas.Admin.Infrastructure.Mapper.Extensions;
using Nop.Web.Areas.Admin.Models.Catalog;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Mvc;
using System;
using System.Linq;

namespace Nop.Web.Areas.Admin.Controllers
{
    public partial class HomePageController : BaseAdminController
    {
        #region Fields

        private readonly IHomePageModelFactory _homePageModelFactory;
        private readonly IPermissionService _permissionService;
        private readonly IHomePageService _homePageService;
        private readonly ILocalizationService _localizationService;
        private readonly IHomePageItemService _homePageItemService;
        private readonly ICategoryService _categoryService;
        private readonly IProductService _productService;
        private readonly IElementService _elementService;
        private readonly IStaticCacheManager _cacheManager;
        private readonly ITopicService _topicService;
        private readonly ISliderService _sliderService;

        #endregion

        #region Ctor

        public HomePageController(IHomePageModelFactory homePageModelFactory,
            IPermissionService permissionService,
            IHomePageService homePageService,
            ILocalizationService localizationService,
            IHomePageItemService homePageItemService,
            ICategoryService categoryService,
            IProductService productService,
            IElementService elementService,
            IStaticCacheManager cacheManager,
            ITopicService topicService,
            ISliderService sliderService)
        {
            _homePageModelFactory = homePageModelFactory;
            _permissionService = permissionService;
            _homePageService = homePageService;
            _localizationService = localizationService;
            _homePageItemService = homePageItemService;
            _categoryService = categoryService;
            _productService = productService;
            _elementService = elementService;
            _cacheManager = cacheManager;
            _topicService = topicService;
            _sliderService = sliderService;
        }

        #endregion

        #region Utilities

        #endregion

        #region List

        [HttpPost]
        public virtual IActionResult HomePageItemList(HomePageItemSearchModel searchModel)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageHomePage))
                return AccessDeniedKendoGridJson();

            //prepare model
            var model = _homePageModelFactory.PrepareHomePageItemListModel(searchModel);

            return Json(model);
        }

        [HttpPost]
        public virtual IActionResult HomePageItemRowList(HomePageItemRowSearchModel searchModel)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageHomePage))
                return AccessDeniedKendoGridJson();

            //prepare model
            var model = _homePageModelFactory.PrepareHomePageItemRowListModel(searchModel);

            return Json(model);
        }

        [HttpPost]
        public virtual IActionResult GetAvailablePicture(int connectionId, RowConnectionType connectionType)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageHomePage))
                return AccessDeniedKendoGridJson();

            var model = _homePageModelFactory.GetAvailablePicture(connectionId, connectionType);

            return Json(model);
        }

        #endregion

        #region Create / Edit / Delete

        public virtual IActionResult Edit()
        {
            //prepare model
            var model = _homePageModelFactory.PrepareHomePageEditModel();

            return View(model);
        }

        #region Home pages

        [HttpPost]
        public virtual IActionResult CreateHomePage(HomePageModel newHomePage)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageHomePage))
                return AccessDeniedView();

            if (ModelState.IsValid)
            {
                var homePage = newHomePage.ToEntity<HomePage>();
                homePage.CreatedOnUtc = DateTime.UtcNow;
                homePage.UpdatedOnUtc = DateTime.UtcNow;
                _homePageService.InsertHomePage(homePage);

                return Json(new
                {
                    id = homePage.Id,
                    name = homePage.Name,
                    url = Url.Action("EditHomePage", "HomePage", new { id = homePage.Id }),
                    success = true
                });
            }
            else
            {
                var errorMessage = string.Empty;
                foreach (var modelState in ModelState.Values)
                {
                    foreach (var error in modelState.Errors)
                    {
                        errorMessage += error.ErrorMessage + Environment.NewLine;
                    }  
                }
                    

                return Json(new
                {
                    success = false,
                    message = errorMessage
                });
            }
        }

        [HttpPost]
        public virtual IActionResult DeleteHomePage(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageHomePage))
                return AccessDeniedView();

            //try to get a home page with the specified id
            var homePage = _homePageService.GetHomePageById(id);
            if (homePage == null)
            {
                return Json(new
                {
                    success = false,
                    message = _localizationService.GetResource("HomePage.DeleteHomePage.Error")
                });
            }

            _homePageService.DeleteHomePage(homePage);
            return Json(new
            {
                success = true
            });
        }

        public virtual IActionResult EditHomePage(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageHomePage))
                return AccessDeniedView();

            //try to get a home page with the specified id
            var homePage = _homePageService.GetHomePageById(id);

            if (homePage == null)
                return RedirectToAction("Edit");

            //prepare model
            var model = _homePageModelFactory.PrepareHomePageModel(null, homePage);
            return PartialView("_HomePageEdit", model);
        }

        [HttpPost]
        public virtual IActionResult EditHomePage(HomePageModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageHomePage))
                return AccessDeniedView();

            //try to get a home page with the specified id
            var homePage = _homePageService.GetHomePageById(model.Id);

            if (homePage == null)
                return RedirectToAction("Edit");

            if (ModelState.IsValid)
            {
                homePage = model.ToEntity(homePage);
                homePage.UpdatedOnUtc = DateTime.UtcNow;
                _homePageService.UpdateHomePage(homePage);

                return Json(new
                {
                    name = homePage.Name,
                    success = true
                });
            }
            else
            {
                var errorMessage = string.Empty;
                foreach (var modelState in ModelState.Values)
                {
                    foreach (var error in modelState.Errors)
                    {
                        errorMessage += error.ErrorMessage + Environment.NewLine;
                    }
                }

                return Json(new
                {
                    success = false,
                    message = errorMessage
                });
            }
        }

        #endregion Home pages

        #region Items 

        public virtual IActionResult AddColumn(int id, RowType rowType)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageHomePage))
                return AccessDeniedView();

            //try to get a home page with the specified id
            var homePage = _homePageService.GetHomePageById(id);

            if (homePage == null)
                return RedirectToAction("Edit");

            //prepare model

            var model = new HomePageItemModel
            {
                ItemHomePageId = homePage.Id,
                ItemRowType = rowType
            };

            _homePageModelFactory.PrepareHomePageItemModel(model);

            return PartialView("_AddHomePageItem", model);
        }

        [HttpPost]
        public virtual IActionResult AddColumn(HomePageItemModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageHomePage))
                return AccessDeniedView();

            if (ModelState.IsValid)
            {
                var homePageItem = model.ToEntity<HomePageItem>();
                homePageItem.ShowDescription = true;
                homePageItem.ShowName = true;
                homePageItem.ShowPicture = true;
                homePageItem.CreatedOnUtc = DateTime.UtcNow;
                homePageItem.UpdatedOnUtc = DateTime.UtcNow;
                _homePageItemService.InsertHomePageItem(homePageItem);

                return Json(new
                {
                    success = true
                });
            }
            else
            {
                var errorMessage = string.Empty;
                foreach (var modelState in ModelState.Values)
                {
                    foreach (var error in modelState.Errors)
                    {
                        errorMessage += error.ErrorMessage + Environment.NewLine;
                    }
                }


                return Json(new
                {
                    success = false,
                    message = errorMessage
                });
            }
        }


        public virtual IActionResult DeleteHomePageItem(int id)
        {
            //try to get a home page item with the specified id
            var homePageItem = _homePageItemService.GetHomePageItemById(id) ??
                throw new ArgumentException("No home page item found with the specified id");

            if (!_permissionService.Authorize(StandardPermissionProvider.ManageHomePage))
                return AccessDeniedView();

            _homePageItemService.DeleteHomePageItem(homePageItem);

            return new NullJsonResult();
        }

        public virtual IActionResult UpdateHomePageItem(HomePageItemModel model)
        {
            if (ModelState.IsValid)
            {
                if (!_permissionService.Authorize(StandardPermissionProvider.ManageHomePage))
                    return AccessDeniedView();

                //try to get a home page item with the specified id
                var row = _homePageItemService.GetHomePageItemById(model.Id) ??
                    throw new ArgumentException("No home page item found with the specified id");

                row = model.ToEntity(row);
                row.UpdatedOnUtc = DateTime.UtcNow;
                _homePageItemService.UpdateHomePageItem(row);
            }

            return new NullJsonResult();
        }

        [HttpPost]
        public virtual IActionResult ReOrderColumn(ReOrderHomePageItemModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageHomePage))
                return AccessDeniedView();

            _homePageItemService.ReOrderHomePageItem(model.NewDisplayOrder, model.OldDisplayOrder, model.DragItemId, true);

            return new NullJsonResult();
        }

        [HttpPost]
        public virtual IActionResult ReOrderRow(ReOrderHomePageItemModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageHomePage))
                return AccessDeniedView();

            _homePageItemService.ReOrderHomePageItem(model.NewDisplayOrder, model.OldDisplayOrder, model.DragItemId, false);

            return new NullJsonResult();
        }


        #region Add Category Row

        public virtual IActionResult HomePageAddRowCategoryPopup(int homePageId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageHomePage))
                return AccessDeniedView();

            //prepare model
            var model = _homePageModelFactory.PrepareAddRowCategorySearchModel(new HomePageAddRowCategorySearchModel
            {
                HomePageId = homePageId
            });

            return View(model);
        }

        [HttpPost]
        public virtual IActionResult RowCategoryAddPopupList(HomePageAddRowCategorySearchModel searchModel)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageHomePage))
                return AccessDeniedKendoGridJson();

            //prepare model
            var model = _homePageModelFactory.PrepareHomePageAddRowCategoryListModel(searchModel);

            return Json(model);
        }

        [HttpPost]
        [FormValueRequired("save")]
        public virtual IActionResult HomePageAddRowCategoryPopup(HomePageAddRowCategoryModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageHomePage))
                return AccessDeniedView();

            //get selected categories
            var selectedCategories = _categoryService.GetCategoriesByIds(model.SelectedCategoryIds.ToArray());
            if (selectedCategories.Any())
            {
                _homePageItemService.InsertCategoryRows(selectedCategories, model.HomePageItemId, model.HomePageId);
            }

            ViewBag.RefreshPage = true;

            return View(new HomePageAddRowCategorySearchModel());
        }

        #endregion Add Category Row

        #region Add Product Row

        public virtual IActionResult HomePageAddRowProductPopup(int homePageId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageHomePage))
                return AccessDeniedView();

            //prepare model
            var model = _homePageModelFactory.PrepareAddRowProductSearchModel(new HomePageAddRowProductSearchModel
            {
                HomePageId = homePageId
            });

            return View(model);
        }

        [HttpPost]
        public virtual IActionResult RowProductAddPopupList(HomePageAddRowProductSearchModel searchModel)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageHomePage))
                return AccessDeniedKendoGridJson();

            //prepare model
            var model = _homePageModelFactory.PrepareHomePageAddRowProductListModel(searchModel);

            return Json(model);
        }

        [HttpPost]
        [FormValueRequired("save")]
        public virtual IActionResult HomePageAddRowProductPopup(HomePageAddRowProductModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageHomePage))
                return AccessDeniedView();

            //get selected products
            var selectedProducts = _productService.GetProductsByIds(model.SelectedProductIds.ToArray());
            if (selectedProducts.Any())
            {
                _homePageItemService.InsertProductRows(selectedProducts.ToList(), model.HomePageItemId, model.HomePageId);
            }

            ViewBag.RefreshPage = true;

            return View(new HomePageAddRowProductSearchModel());
        }

        #endregion Add Product Row

        #region Add Element Row

        public virtual IActionResult HomePageAddRowElementPopup(int homePageId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageHomePage))
                return AccessDeniedView();

            //prepare model
            var model = _homePageModelFactory.PrepareAddRowElementSearchModel(new HomePageAddRowElementSearchModel
            {
                HomePageId = homePageId
            });

            return View(model);
        }

        [HttpPost]
        public virtual IActionResult RowElementAddPopupList(HomePageAddRowElementSearchModel searchModel)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageHomePage))
                return AccessDeniedKendoGridJson();

            //prepare model
            var model = _homePageModelFactory.PrepareHomePageAddRowElementListModel(searchModel);

            return Json(model);
        }

        [HttpPost]
        [FormValueRequired("save")]
        public virtual IActionResult HomePageAddRowElementPopup(HomePageAddRowElementModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageHomePage))
                return AccessDeniedView();

            //get selected elements
            var selectedElements = _elementService.GetElementsByIds(model.SelectedElementIds.ToArray());
            if (selectedElements.Any())
            {
                _homePageItemService.InsertElementRows(selectedElements, model.HomePageItemId, model.HomePageId);
            }

            ViewBag.RefreshPage = true;

            return View(new HomePageAddRowElementSearchModel());
        }

        #endregion Add Element Row

        #region Add Topic Row

        public virtual IActionResult HomePageAddRowTopicPopup(int homePageId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageHomePage))
                return AccessDeniedView();

            //prepare model
            var model = _homePageModelFactory.PrepareAddRowTopicSearchModel(new HomePageAddRowTopicSearchModel
            {
                HomePageId = homePageId
            });

            return View(model);
        }

        [HttpPost]
        public virtual IActionResult RowTopicAddPopupList(HomePageAddRowTopicSearchModel searchModel)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageHomePage))
                return AccessDeniedKendoGridJson();

            //prepare model
            var model = _homePageModelFactory.PrepareHomePageAddRowTopicListModel(searchModel);

            return Json(model);
        }

        [HttpPost]
        [FormValueRequired("save")]
        public virtual IActionResult HomePageAddRowTopicPopup(HomePageAddRowTopicModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageHomePage))
                return AccessDeniedView();

            //get selected topics
            var selectedTopics = _topicService.GetTopicsByIds(model.SelectedTopicIds.ToArray());
            if (selectedTopics.Any())
            {
                _homePageItemService.InsertTopicsRows(selectedTopics, model.HomePageItemId, model.HomePageId);
            }

            ViewBag.RefreshPage = true;

            return View(new HomePageAddRowTopicSearchModel());
        }

        #endregion Add Topic Row

        #region Add Slider Row

        public virtual IActionResult HomePageAddRowSliderPopup(int homePageId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageHomePage))
                return AccessDeniedView();

            //prepare model
            var model = _homePageModelFactory.PrepareAddRowSliderSearchModel(new HomePageAddRowSliderSearchModel
            {
                HomePageId = homePageId
            });

            return View(model);
        }

        [HttpPost]
        public virtual IActionResult RowSliderAddPopupList(HomePageAddRowSliderSearchModel searchModel)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageHomePage))
                return AccessDeniedKendoGridJson();

            //prepare model
            var model = _homePageModelFactory.PrepareHomePageAddRowSliderListModel(searchModel);

            return Json(model);
        }

        [HttpPost]
        [FormValueRequired("save")]
        public virtual IActionResult HomePageAddRowSliderPopup(HomePageAddRowSliderModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageHomePage))
                return AccessDeniedView();

            //get selected sliders
            var selectedSliders = _sliderService.GetSlidersByIds(model.SelectedSliderIds.ToArray());
            if (selectedSliders.Any())
            {
                _homePageItemService.InsertSliderRows(selectedSliders, model.HomePageItemId, model.HomePageId);
            }

            ViewBag.RefreshPage = true;

            return View(new HomePageAddRowSliderSearchModel());
        }

        #endregion Add Slider Row

        #endregion Items

        #endregion

    }
}