﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Core.Caching;
using Nop.Core.Domain.Catalog;
using Nop.Services.Catalog;
using Nop.Services.Localization;
using Nop.Services.Security;
using Nop.Services.Topics;
using Nop.Web.Areas.Admin.Factories;
using Nop.Web.Areas.Admin.Helpers;
using Nop.Web.Areas.Admin.Infrastructure.Mapper.Extensions;
using Nop.Web.Areas.Admin.Models.Catalog;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Mvc;
using System;
using System.Linq;

namespace Nop.Web.Areas.Admin.Controllers
{
    public partial class MegaMenuController : BaseAdminController
    {
        #region Fields

        private readonly IMegaMenuModelFactory _megaMenuModelFactory;
        private readonly IPermissionService _permissionService;
        private readonly IMegaMenuTabService _megaMenuTabService;
        private readonly ILocalizationService _localizationService;
        private readonly IMegaMenuTabItemService _megaMenuTabItemService;
        private readonly ICategoryService _categoryService;
        private readonly IProductService _productService;
        private readonly IElementService _elementService;
        private readonly IStaticCacheManager _cacheManager;
        private readonly ITopicService _topicService;
        private readonly ISliderService _sliderService;

        #endregion

        #region Ctor

        public MegaMenuController(IMegaMenuModelFactory megaMenuModelFactory,
            IPermissionService permissionService,
            IMegaMenuTabService megaMenuTabService,
            ILocalizationService localizationService,
            IMegaMenuTabItemService megaMenuTabItemService,
            ICategoryService categoryService,
            IProductService productService,
            IElementService elementService,
            IStaticCacheManager cacheManager,
            ITopicService topicService,
            ISliderService sliderService)
        {
            _megaMenuModelFactory = megaMenuModelFactory;
            _permissionService = permissionService;
            _megaMenuTabService = megaMenuTabService;
            _localizationService = localizationService;
            _megaMenuTabItemService = megaMenuTabItemService;
            _categoryService = categoryService;
            _productService = productService;
            _elementService = elementService;
            _cacheManager = cacheManager;
            _topicService = topicService;
            _sliderService = sliderService;
        }

        #endregion

        #region Utilities

        #endregion

        #region List

        [HttpPost]
        public virtual IActionResult MegaMenuTabItemList(MegaMenuTabItemSearchModel searchModel)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageMenu))
                return AccessDeniedKendoGridJson();

            //prepare model
            var model = _megaMenuModelFactory.PrepareMegaMenuTabItemListModel(searchModel);

            return Json(model);
        }

        [HttpPost]
        public virtual IActionResult MegaMenuTabItemRowList(MegaMenuTabItemRowSearchModel searchModel)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageMenu))
                return AccessDeniedKendoGridJson();

            //prepare model
            var model = _megaMenuModelFactory.PrepareMegaMenuTabItemRowListModel(searchModel);

            return Json(model);
        }

        [HttpPost]
        public virtual JsonResult GetAvailableMegaMenus()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageMenu))
                return Json("Access denied");

            var availableItems = SelectListHelper.GetMegaMenuTabList(_megaMenuTabService, _cacheManager, true);
            //insert this default item at first
            availableItems.Insert(0, new SelectListItem { Text = _localizationService.GetResource("Admin.Common.Select"), Value = "0" });

            return Json(availableItems);
        }

        [HttpPost]
        public virtual IActionResult GetAvailablePicture(int connectionId, RowConnectionType connectionType)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageMenu))
                return AccessDeniedKendoGridJson();

            var model = _megaMenuModelFactory.GetAvailablePicture(connectionId, connectionType);

            return Json(model);
        }

        #endregion

        #region Create / Edit / Delete

        public virtual IActionResult Edit()
        {
            //prepare model
            var model = _megaMenuModelFactory.PrepareMegaMenuModel();

            return View(model);
        }

        #region Tabs

        [HttpPost]
        public virtual IActionResult CreateTab(MegaMenuTabModel newTab)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageMenu))
                return AccessDeniedView();

            if (ModelState.IsValid)
            {
                var megaMenuTab = newTab.ToEntity<MegaMenuTab>();
                megaMenuTab.CreatedOnUtc = DateTime.UtcNow;
                megaMenuTab.UpdatedOnUtc = DateTime.UtcNow;
                _megaMenuTabService.InsertMegaMenuTab(megaMenuTab);

                return Json(new
                {
                    id = megaMenuTab.Id,
                    name = megaMenuTab.Name,
                    url = Url.Action("EditTab", "MegaMenu", new { id = megaMenuTab.Id }),
                    success = true
                });
            }
            else
            {
                var errorMessage = string.Empty;
                foreach (var modelState in ModelState.Values)
                {
                    foreach (var error in modelState.Errors)
                    {
                        errorMessage += error.ErrorMessage + Environment.NewLine;
                    }  
                }
                    

                return Json(new
                {
                    success = false,
                    message = errorMessage
                });
            }
        }

        [HttpPost]
        public virtual IActionResult DeleteTab(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageMenu))
                return AccessDeniedView();

            //try to get a mega menu with the specified id
            var megaMenuTab = _megaMenuTabService.GetMegaMenuTabById(id);
            if (megaMenuTab == null)
            {
                return Json(new
                {
                    success = false,
                    message = _localizationService.GetResource("MegaMenu.DeleteMegaMenuTab.Error")
                });
            }

            _megaMenuTabService.DeleteMegaMenuTab(megaMenuTab);
            return Json(new
            {
                success = true
            });
        }

        public virtual IActionResult EditTab(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageMenu))
                return AccessDeniedView();

            //try to get a mega menu tab with the specified id
            var megaMenuTab = _megaMenuTabService.GetMegaMenuTabById(id);

            if (megaMenuTab == null)
                return RedirectToAction("Edit");

            //prepare model
            var model = _megaMenuModelFactory.PrepareMegaMenuTabModel(null, megaMenuTab);
            return PartialView("_MegaMenuTabEdit", model);
        }

        [HttpPost]
        public virtual IActionResult EditTab(MegaMenuTabModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageMenu))
                return AccessDeniedView();

            //try to get a mega menu tab with the specified id
            var megaMenuTab = _megaMenuTabService.GetMegaMenuTabById(model.Id);

            if (megaMenuTab == null)
                return RedirectToAction("Edit");

            if (ModelState.IsValid)
            {
                megaMenuTab = model.ToEntity(megaMenuTab);
                megaMenuTab.UpdatedOnUtc = DateTime.UtcNow;
                _megaMenuTabService.UpdateMegaMenuTab(megaMenuTab);

                return Json(new
                {
                    name = megaMenuTab.Name,
                    success = true
                });
            }
            else
            {
                var errorMessage = string.Empty;
                foreach (var modelState in ModelState.Values)
                {
                    foreach (var error in modelState.Errors)
                    {
                        errorMessage += error.ErrorMessage + Environment.NewLine;
                    }
                }

                return Json(new
                {
                    success = false,
                    message = errorMessage
                });
            }
        }

        #endregion Tabs

        #region Items 

        public virtual IActionResult AddColumn(int id, RowType rowType)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageMenu))
                return AccessDeniedView();

            //try to get a mega menu tab with the specified id
            var megaMenuTab = _megaMenuTabService.GetMegaMenuTabById(id);

            if (megaMenuTab == null)
                return RedirectToAction("Edit");

            //prepare model

            var model = new MegaMenuTabItemModel
            {
                ItemMegaMenuTabId = megaMenuTab.Id,
                ItemRowType = rowType
            };

            _megaMenuModelFactory.PrepareMegaMenuTabItemModel(model);

            return PartialView("_AddMegaMenuTabItem", model);
        }

        [HttpPost]
        public virtual IActionResult AddColumn(MegaMenuTabItemModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageMenu))
                return AccessDeniedView();

            if (ModelState.IsValid)
            {
                var megaMenuTabItem = model.ToEntity<MegaMenuTabItem>();
                megaMenuTabItem.CreatedOnUtc = DateTime.UtcNow;
                megaMenuTabItem.UpdatedOnUtc = DateTime.UtcNow;
                _megaMenuTabItemService.InsertMegaMenuTabItem(megaMenuTabItem);

                return Json(new
                {
                    success = true
                });
            }
            else
            {
                var errorMessage = string.Empty;
                foreach (var modelState in ModelState.Values)
                {
                    foreach (var error in modelState.Errors)
                    {
                        errorMessage += error.ErrorMessage + Environment.NewLine;
                    }
                }


                return Json(new
                {
                    success = false,
                    message = errorMessage
                });
            }
        }


        public virtual IActionResult DeleteMegaMenuTabItem(int id)
        {
            //try to get a mega menu tab item with the specified id
            var megeMenuTabItem = _megaMenuTabItemService.GetMegaMenuTabItemById(id) ??
                throw new ArgumentException("No mega menu tab item found with the specified id");

            if (!_permissionService.Authorize(StandardPermissionProvider.ManageMenu))
                return AccessDeniedView();

            _megaMenuTabItemService.DeleteMegaMenuTabItem(megeMenuTabItem);

            return new NullJsonResult();
        }

        public virtual IActionResult UpdateMegaMenuTabItem(MegaMenuTabItemModel model)
        {
            if (ModelState.IsValid)
            {
                if (!_permissionService.Authorize(StandardPermissionProvider.ManageMenu))
                    return AccessDeniedView();

                //try to get a megamenu tab item with the specified id
                var row = _megaMenuTabItemService.GetMegaMenuTabItemById(model.Id) ??
                    throw new ArgumentException("No megamenu tab item found with the specified id");

                row = model.ToEntity(row);
                row.UpdatedOnUtc = DateTime.UtcNow;
                _megaMenuTabItemService.UpdateMegaMenuTabItem(row);
            }

            return new NullJsonResult();
        }

        [HttpPost]
        public virtual IActionResult ReOrderColumn(ReOrderMegaMenuItemModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageMenu))
                return AccessDeniedView();

            _megaMenuTabItemService.ReOrderMegaMenuTabItem(model.NewDisplayOrder, model.OldDisplayOrder, model.DragItemId, true);

            return new NullJsonResult();
        }

        [HttpPost]
        public virtual IActionResult ReOrderRow(ReOrderMegaMenuItemModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageMenu))
                return AccessDeniedView();

            _megaMenuTabItemService.ReOrderMegaMenuTabItem(model.NewDisplayOrder, model.OldDisplayOrder, model.DragItemId, false);

            return new NullJsonResult();
        }


        #region Add Category Row

        public virtual IActionResult MegaMenuAddRowCategoryPopup(int megaMenuTabId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageMenu))
                return AccessDeniedView();

            //prepare model
            var model = _megaMenuModelFactory.PrepareAddRowCategorySearchModel(new MegaMenuAddRowCategorySearchModel
            {
                MegaMenuTabId = megaMenuTabId
            });

            return View(model);
        }

        [HttpPost]
        public virtual IActionResult RowCategoryAddPopupList(MegaMenuAddRowCategorySearchModel searchModel)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageMenu))
                return AccessDeniedKendoGridJson();

            //prepare model
            var model = _megaMenuModelFactory.PrepareMegaMenuAddRowCategoryListModel(searchModel);

            return Json(model);
        }

        [HttpPost]
        [FormValueRequired("save")]
        public virtual IActionResult MegaMenuAddRowCategoryPopup(MegaMenuAddRowCategoryModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageMenu))
                return AccessDeniedView();

            //get selected categories
            var selectedCategories = _categoryService.GetCategoriesByIds(model.SelectedCategoryIds.ToArray());
            if (selectedCategories.Any())
            {
                _megaMenuTabItemService.InsertCategoryRows(selectedCategories, model.MegaMenuTabItemId, model.MegaMenuTabId);
            }

            ViewBag.RefreshPage = true;

            return View(new MegaMenuAddRowCategorySearchModel());
        }

        #endregion Add Category Row

        #region Add Product Row

        public virtual IActionResult MegaMenuAddRowProductPopup(int megaMenuTabId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageMenu))
                return AccessDeniedView();

            //prepare model
            var model = _megaMenuModelFactory.PrepareAddRowProductSearchModel(new MegaMenuAddRowProductSearchModel
            {
                MegaMenuTabId = megaMenuTabId
            });

            return View(model);
        }

        [HttpPost]
        public virtual IActionResult RowProductAddPopupList(MegaMenuAddRowProductSearchModel searchModel)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageMenu))
                return AccessDeniedKendoGridJson();

            //prepare model
            var model = _megaMenuModelFactory.PrepareMegaMenuAddRowProductListModel(searchModel);

            return Json(model);
        }

        [HttpPost]
        [FormValueRequired("save")]
        public virtual IActionResult MegaMenuAddRowProductPopup(MegaMenuAddRowProductModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageMenu))
                return AccessDeniedView();

            //get selected products
            var selectedProducts = _productService.GetProductsByIds(model.SelectedProductIds.ToArray());
            if (selectedProducts.Any())
            {
                _megaMenuTabItemService.InsertProductRows(selectedProducts.ToList(), model.MegaMenuTabItemId, model.MegaMenuTabId);
            }

            ViewBag.RefreshPage = true;

            return View(new MegaMenuAddRowProductSearchModel());
        }

        #endregion Add Product Row

        #region Add Element Row

        public virtual IActionResult MegaMenuAddRowElementPopup(int megaMenuTabId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageMenu))
                return AccessDeniedView();

            //prepare model
            var model = _megaMenuModelFactory.PrepareAddRowElementSearchModel(new MegaMenuAddRowElementSearchModel
            {
                MegaMenuTabId = megaMenuTabId
            });

            return View(model);
        }

        [HttpPost]
        public virtual IActionResult RowElementAddPopupList(MegaMenuAddRowElementSearchModel searchModel)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageMenu))
                return AccessDeniedKendoGridJson();

            //prepare model
            var model = _megaMenuModelFactory.PrepareMegaMenuAddRowElementListModel(searchModel);

            return Json(model);
        }

        [HttpPost]
        [FormValueRequired("save")]
        public virtual IActionResult MegaMenuAddRowElementPopup(MegaMenuAddRowElementModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageMenu))
                return AccessDeniedView();

            //get selected elements
            var selectedElements = _elementService.GetElementsByIds(model.SelectedElementIds.ToArray());
            if (selectedElements.Any())
            {
                _megaMenuTabItemService.InsertElementRows(selectedElements, model.MegaMenuTabItemId, model.MegaMenuTabId);
            }

            ViewBag.RefreshPage = true;

            return View(new MegaMenuAddRowElementSearchModel());
        }

        #endregion Add Element Row

        #region Add Topic Row

        public virtual IActionResult MegaMenuAddRowTopicPopup(int megaMenuTabId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageMenu))
                return AccessDeniedView();

            //prepare model
            var model = _megaMenuModelFactory.PrepareAddRowTopicSearchModel(new MegaMenuAddRowTopicSearchModel
            {
                MegaMenuTabId = megaMenuTabId
            });

            return View(model);
        }

        [HttpPost]
        public virtual IActionResult RowTopicAddPopupList(MegaMenuAddRowTopicSearchModel searchModel)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageMenu))
                return AccessDeniedKendoGridJson();

            //prepare model
            var model = _megaMenuModelFactory.PrepareMegaMenuAddRowTopicListModel(searchModel);

            return Json(model);
        }

        [HttpPost]
        [FormValueRequired("save")]
        public virtual IActionResult MegaMenuAddRowTopicPopup(MegaMenuAddRowTopicModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageMenu))
                return AccessDeniedView();

            //get selected topics
            var selectedTopics = _topicService.GetTopicsByIds(model.SelectedTopicIds.ToArray());
            if (selectedTopics.Any())
            {
                _megaMenuTabItemService.InsertTopicsRows(selectedTopics, model.MegaMenuTabItemId, model.MegaMenuTabId);
            }

            ViewBag.RefreshPage = true;

            return View(new MegaMenuAddRowTopicSearchModel());
        }

        #endregion Add Topic Row

        #region Add Slider Row

        public virtual IActionResult MegaMenuAddRowSliderPopup(int megaMenuTabId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageMenu))
                return AccessDeniedView();

            //prepare model
            var model = _megaMenuModelFactory.PrepareAddRowSliderSearchModel(new MegaMenuAddRowSliderSearchModel
            {
                MegaMenuTabId = megaMenuTabId
            });

            return View(model);
        }

        [HttpPost]
        public virtual IActionResult RowSliderAddPopupList(MegaMenuAddRowSliderSearchModel searchModel)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageMenu))
                return AccessDeniedKendoGridJson();

            //prepare model
            var model = _megaMenuModelFactory.PrepareMegaMenuAddRowSliderListModel(searchModel);

            return Json(model);
        }

        [HttpPost]
        [FormValueRequired("save")]
        public virtual IActionResult MegaMenuAddRowSliderPopup(MegaMenuAddRowSliderModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageMenu))
                return AccessDeniedView();

            //get selected sliders
            var selectedSliders = _sliderService.GetSlidersByIds(model.SelectedSliderIds.ToArray());
            if (selectedSliders.Any())
            {
                _megaMenuTabItemService.InsertSliderRows(selectedSliders, model.MegaMenuTabItemId, model.MegaMenuTabId);
            }

            ViewBag.RefreshPage = true;

            return View(new MegaMenuAddRowSliderSearchModel());
        }

        #endregion Add Slider Row

        #endregion Items

        #endregion

    }
}