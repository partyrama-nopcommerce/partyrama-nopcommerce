﻿using Microsoft.AspNetCore.Mvc;
using Nop.Core.Caching;
using Nop.Core.Domain.Catalog;
using Nop.Services.Catalog;
using Nop.Services.Localization;
using Nop.Services.Security;
using Nop.Services.Topics;
using Nop.Web.Areas.Admin.Factories;
using Nop.Web.Areas.Admin.Infrastructure.Mapper.Extensions;
using Nop.Web.Areas.Admin.Models.Catalog;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Mvc;
using System;
using System.Linq;

namespace Nop.Web.Areas.Admin.Controllers
{
    public partial class SliderController : BaseAdminController
    {
        #region Fields

        private readonly ISliderModelFactory _sliderModelFactory;
        private readonly IPermissionService _permissionService;
        private readonly ISliderService _sliderService;
        private readonly ILocalizationService _localizationService;
        private readonly ISliderItemService _sliderItemService;
        private readonly ICategoryService _categoryService;
        private readonly IProductService _productService;
        private readonly IElementService _elementService;
        private readonly IStaticCacheManager _cacheManager;
        private readonly ITopicService _topicService;

        #endregion

        #region Ctor

        public SliderController(ISliderModelFactory sliderModelFactory,
            IPermissionService permissionService,
            ISliderService sliderService,
            ILocalizationService localizationService,
            ISliderItemService sliderItemService,
            ICategoryService categoryService,
            IProductService productService,
            IElementService elementService,
            IStaticCacheManager cacheManager,
            ITopicService topicService)
        {
            _sliderModelFactory = sliderModelFactory;
            _permissionService = permissionService;
            _sliderService = sliderService;
            _localizationService = localizationService;
            _sliderItemService = sliderItemService;
            _categoryService = categoryService;
            _productService = productService;
            _elementService = elementService;
            _cacheManager = cacheManager;
            _topicService = topicService;
        }

        #endregion

        #region Utilities

        #endregion

        #region List

        [HttpPost]
        public virtual IActionResult SliderItemList(SliderItemSearchModel searchModel)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSlider))
                return AccessDeniedKendoGridJson();

            //prepare model
            var model = _sliderModelFactory.PrepareSliderItemListModel(searchModel);

            return Json(model);
        }

        [HttpPost]
        public virtual IActionResult SliderItemRowList(SliderItemRowSearchModel searchModel)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSlider))
                return AccessDeniedKendoGridJson();

            //prepare model
            var model = _sliderModelFactory.PrepareSliderItemRowListModel(searchModel);

            return Json(model);
        }

        [HttpPost]
        public virtual IActionResult GetAvailablePicture(int connectionId, RowConnectionType connectionType)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSlider))
                return AccessDeniedKendoGridJson();

            var model = _sliderModelFactory.GetAvailablePicture(connectionId, connectionType);

            return Json(model);
        }

        #endregion

        #region Create / Edit / Delete

        public virtual IActionResult Edit()
        {
            //prepare model
            var model = _sliderModelFactory.PrepareSliderEditModel();

            return View(model);
        }

        #region Sliders

        [HttpPost]
        public virtual IActionResult CreateSlider(SliderModel newSlider)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSlider))
                return AccessDeniedView();

            if (ModelState.IsValid)
            {
                var slider = newSlider.ToEntity<Slider>();
                slider.CreatedOnUtc = DateTime.UtcNow;
                slider.UpdatedOnUtc = DateTime.UtcNow;
                if (slider.SliderType == SliderType.New 
                    || slider.SliderType == SliderType.Sale)
                {
                    slider.Number = 10;
                }

                _sliderService.InsertSlider(slider);

                return Json(new
                {
                    id = slider.Id,
                    name = slider.Name,
                    url = Url.Action("EditSlider", "Slider", new { id = slider.Id }),
                    success = true
                });
            }
            else
            {
                var errorMessage = string.Empty;
                foreach (var modelState in ModelState.Values)
                {
                    foreach (var error in modelState.Errors)
                    {
                        errorMessage += error.ErrorMessage + Environment.NewLine;
                    }  
                }
                    

                return Json(new
                {
                    success = false,
                    message = errorMessage
                });
            }
        }

        [HttpPost]
        public virtual IActionResult DeleteSlider(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSlider))
                return AccessDeniedView();

            //try to get a slider with the specified id
            var slider = _sliderService.GetSliderById(id);
            if (slider == null)
            {
                return Json(new
                {
                    success = false,
                    message = _localizationService.GetResource("Slider.DeleteSlider.Error")
                });
            }

            _sliderService.DeleteSlider(slider);
            return Json(new
            {
                success = true
            });
        }

        public virtual IActionResult EditSlider(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSlider))
                return AccessDeniedView();

            //try to get a slider with the specified id
            var slider = _sliderService.GetSliderById(id);

            if (slider == null)
                return RedirectToAction("Edit");

            //prepare model
            var model = _sliderModelFactory.PrepareSliderModel(null, slider);
            return PartialView("_SliderEdit", model);
        }

        [HttpPost]
        public virtual IActionResult EditSlider(SliderModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSlider))
                return AccessDeniedView();

            //try to get a slider with the specified id
            var slider = _sliderService.GetSliderById(model.Id);

            if (slider == null)
                return RedirectToAction("Edit");

            if (ModelState.IsValid)
            {
                slider = model.ToEntity(slider);
                slider.UpdatedOnUtc = DateTime.UtcNow;
                _sliderService.UpdateSlider(slider);

                return Json(new
                {
                    name = slider.Name,
                    success = true
                });
            }
            else
            {
                var errorMessage = string.Empty;
                foreach (var modelState in ModelState.Values)
                {
                    foreach (var error in modelState.Errors)
                    {
                        errorMessage += error.ErrorMessage + Environment.NewLine;
                    }
                }

                return Json(new
                {
                    success = false,
                    message = errorMessage
                });
            }
        }

        #endregion Sliders

        #region Items 

        public virtual IActionResult AddColumn(int id, RowType rowType)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSlider))
                return AccessDeniedView();

            //try to get a slider with the specified id
            var slider = _sliderService.GetSliderById(id);

            if (slider == null)
                return RedirectToAction("Edit");

            //prepare model

            var model = new SliderItemModel
            {
                ItemSliderId = slider.Id,
                ItemRowType = rowType
            };

            _sliderModelFactory.PrepareSliderItemModel(model);

            return PartialView("_AddSliderItem", model);
        }

        [HttpPost]
        public virtual IActionResult AddColumn(SliderItemModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSlider))
                return AccessDeniedView();

            if (ModelState.IsValid)
            {
                var sliderItem = model.ToEntity<SliderItem>();

                sliderItem.CreatedOnUtc = DateTime.UtcNow;
                sliderItem.UpdatedOnUtc = DateTime.UtcNow;
                _sliderItemService.InsertSliderItem(sliderItem);

                return Json(new
                {
                    success = true
                });
            }
            else
            {
                var errorMessage = string.Empty;
                foreach (var modelState in ModelState.Values)
                {
                    foreach (var error in modelState.Errors)
                    {
                        errorMessage += error.ErrorMessage + Environment.NewLine;
                    }
                }


                return Json(new
                {
                    success = false,
                    message = errorMessage
                });
            }
        }


        public virtual IActionResult DeleteSliderItem(int id)
        {
            //try to get a slider item with the specified id
            var sliderItem = _sliderItemService.GetSliderItemById(id) ??
                throw new ArgumentException("No slider item found with the specified id");

            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSlider))
                return AccessDeniedView();

            _sliderItemService.DeleteSliderItem(sliderItem);

            return new NullJsonResult();
        }

        public virtual IActionResult UpdateSliderItem(SliderItemModel model)
        {
            if (ModelState.IsValid)
            {
                if (!_permissionService.Authorize(StandardPermissionProvider.ManageSlider))
                    return AccessDeniedView();

                //try to get a slider item with the specified id
                var row = _sliderItemService.GetSliderItemById(model.Id) ??
                    throw new ArgumentException("No slider item found with the specified id");

                row = model.ToEntity(row);

                row.UpdatedOnUtc = DateTime.UtcNow;
                _sliderItemService.UpdateSliderItem(row);
            }

            return new NullJsonResult();
        }

        [HttpPost]
        public virtual IActionResult ReOrderColumn(ReOrderSliderItemModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSlider))
                return AccessDeniedView();

            _sliderItemService.ReOrderSliderItem(model.NewDisplayOrder, model.OldDisplayOrder, model.DragItemId, true);

            return new NullJsonResult();
        }

        [HttpPost]
        public virtual IActionResult ReOrderRow(ReOrderSliderItemModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSlider))
                return AccessDeniedView();

            _sliderItemService.ReOrderSliderItem(model.NewDisplayOrder, model.OldDisplayOrder, model.DragItemId, false);

            return new NullJsonResult();
        }


        #region Add Category Row

        public virtual IActionResult SliderAddRowCategoryPopup(int sliderId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSlider))
                return AccessDeniedView();

            //prepare model
            var model = _sliderModelFactory.PrepareAddRowCategorySearchModel(new SliderAddRowCategorySearchModel
            {
                SliderId = sliderId
            });

            return View(model);
        }

        [HttpPost]
        public virtual IActionResult RowCategoryAddPopupList(SliderAddRowCategorySearchModel searchModel)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSlider))
                return AccessDeniedKendoGridJson();

            //prepare model
            var model = _sliderModelFactory.PrepareSliderAddRowCategoryListModel(searchModel);

            return Json(model);
        }

        [HttpPost]
        [FormValueRequired("save")]
        public virtual IActionResult SliderAddRowCategoryPopup(SliderAddRowCategoryModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSlider))
                return AccessDeniedView();

            //get selected categories
            var selectedCategories = _categoryService.GetCategoriesByIds(model.SelectedCategoryIds.ToArray());
            if (selectedCategories.Any())
            {
                _sliderItemService.InsertCategoryRows(selectedCategories, model.SliderItemId, model.SliderId);
            }

            ViewBag.RefreshPage = true;

            return View(new SliderAddRowCategorySearchModel());
        }

        #endregion Add Category Row

        #region Add Product Row

        public virtual IActionResult SliderAddRowProductPopup(int sliderId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSlider))
                return AccessDeniedView();

            //prepare model
            var model = _sliderModelFactory.PrepareAddRowProductSearchModel(new SliderAddRowProductSearchModel
            {
                SliderId = sliderId
            });

            return View(model);
        }

        [HttpPost]
        public virtual IActionResult RowProductAddPopupList(SliderAddRowProductSearchModel searchModel)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSlider))
                return AccessDeniedKendoGridJson();

            //prepare model
            var model = _sliderModelFactory.PrepareSliderAddRowProductListModel(searchModel);

            return Json(model);
        }

        [HttpPost]
        [FormValueRequired("save")]
        public virtual IActionResult SliderAddRowProductPopup(SliderAddRowProductModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSlider))
                return AccessDeniedView();

            //get selected products
            var selectedProducts = _productService.GetProductsByIds(model.SelectedProductIds.ToArray());
            if (selectedProducts.Any())
            {
                _sliderItemService.InsertProductRows(selectedProducts.ToList(), model.SliderItemId, model.SliderId);
            }

            ViewBag.RefreshPage = true;

            return View(new SliderAddRowProductSearchModel());
        }

        #endregion Add Product Row

        #region Add Element Row

        public virtual IActionResult SliderAddRowElementPopup(int sliderId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSlider))
                return AccessDeniedView();

            //prepare model
            var model = _sliderModelFactory.PrepareAddRowElementSearchModel(new SliderAddRowElementSearchModel
            {
                SliderId = sliderId
            });

            return View(model);
        }

        [HttpPost]
        public virtual IActionResult RowElementAddPopupList(SliderAddRowElementSearchModel searchModel)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSlider))
                return AccessDeniedKendoGridJson();

            //prepare model
            var model = _sliderModelFactory.PrepareSliderAddRowElementListModel(searchModel);

            return Json(model);
        }

        [HttpPost]
        [FormValueRequired("save")]
        public virtual IActionResult SliderAddRowElementPopup(SliderAddRowElementModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSlider))
                return AccessDeniedView();

            //get selected elements
            var selectedElements = _elementService.GetElementsByIds(model.SelectedElementIds.ToArray());
            if (selectedElements.Any())
            {
                _sliderItemService.InsertElementRows(selectedElements, model.SliderItemId, model.SliderId);
            }

            ViewBag.RefreshPage = true;

            return View(new SliderAddRowElementSearchModel());
        }

        #endregion Add Element Row

        #region Add Topic Row

        public virtual IActionResult SliderAddRowTopicPopup(int sliderId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSlider))
                return AccessDeniedView();

            //prepare model
            var model = _sliderModelFactory.PrepareAddRowTopicSearchModel(new SliderAddRowTopicSearchModel
            {
                SliderId = sliderId
            });

            return View(model);
        }

        [HttpPost]
        public virtual IActionResult RowTopicAddPopupList(SliderAddRowTopicSearchModel searchModel)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSlider))
                return AccessDeniedKendoGridJson();

            //prepare model
            var model = _sliderModelFactory.PrepareSliderAddRowTopicListModel(searchModel);

            return Json(model);
        }

        [HttpPost]
        [FormValueRequired("save")]
        public virtual IActionResult SliderAddRowTopicPopup(SliderAddRowTopicModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSlider))
                return AccessDeniedView();

            //get selected topics
            var selectedTopics = _topicService.GetTopicsByIds(model.SelectedTopicIds.ToArray());
            if (selectedTopics.Any())
            {
                _sliderItemService.InsertTopicsRows(selectedTopics, model.SliderItemId, model.SliderId);
            }

            ViewBag.RefreshPage = true;

            return View(new SliderAddRowTopicSearchModel());
        }

        #endregion Add Topic Row

        #endregion Items

        #endregion

    }
}