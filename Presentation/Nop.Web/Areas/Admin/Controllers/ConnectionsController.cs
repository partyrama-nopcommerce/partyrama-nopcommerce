﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Nop.Core.Domain.Catalog;
using Nop.Services.Catalog;
using Nop.Services.Security;
using Nop.Web.Areas.Admin.Factories;
using Nop.Web.Areas.Admin.Models.Catalog;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Mvc;

namespace Nop.Web.Areas.Admin.Controllers
{
    public partial class ConnectionsController : BaseAdminController
    {
        #region Fields

        private readonly IPermissionService _permissionService;
        private readonly ICategoryService _categoryService;
        private readonly IProductService _productService;
        private readonly IConnectionModelFactory _connectionModelFactory;
        private readonly IConnectionsService _connectionsService;
        private readonly IElementService _elementService;
        private readonly ISliderService _sliderService;

        #endregion

        #region Ctor

        public ConnectionsController(
            IPermissionService permissionService,
            ICategoryService categoryService,
            IProductService productService,
            IConnectionModelFactory connectionModelFactory,
            IConnectionsService connectionsService,
            IElementService elementService,
            ISliderService sliderService)
        {
            _permissionService = permissionService;
            _productService = productService;
            _categoryService = categoryService;
            _connectionModelFactory = connectionModelFactory;
            _connectionsService = connectionsService;
            _elementService = elementService;
            _sliderService = sliderService;
        }

        #endregion

        #region Utilities

        

        #endregion

        #region List

        [HttpPost]
        public virtual IActionResult List(ConnectionsSearchModel searchModel)
        {
            if (_connectionModelFactory.IsConnectionToCategory(searchModel.FromType) && !_permissionService.Authorize(StandardPermissionProvider.ManageCategories)
                || (!_connectionModelFactory.IsConnectionToCategory(searchModel.FromType) && !_permissionService.Authorize(StandardPermissionProvider.ManageProducts)))
                return AccessDeniedKendoGridJson();

            ConnectionListModel model = new ConnectionListModel();

            if (searchModel.FromType == ConnectionFromType.prod)
            {
                //try to get a category with the specified id
                var product = _productService.GetProductById(searchModel.FromId)
                    ?? throw new ArgumentException("No product found with the specified id");

                model = _connectionModelFactory.PrepareConnectionListModel(searchModel, null, product);
            }
            else
            {
                //try to get a category with the specified id
                var category = _categoryService.GetCategoryById(searchModel.FromId)
                    ?? throw new ArgumentException("No category found with the specified id");

                model = _connectionModelFactory.PrepareConnectionListModel(searchModel, category);
            }
            
            return Json(model);
        }

        [HttpPost]
        public virtual IActionResult GetAvailablePicture(int connectionId, ConnectionToType toType, ConnectionFromType fromType)
        {
            if (_connectionModelFactory.IsConnectionToCategory(fromType) && !_permissionService.Authorize(StandardPermissionProvider.ManageCategories)
                || (!_connectionModelFactory.IsConnectionToCategory(fromType) && !_permissionService.Authorize(StandardPermissionProvider.ManageProducts)))
                return AccessDeniedKendoGridJson();

            var model = _connectionModelFactory.GetAvailablePicture(connectionId, toType);

            return Json(model);
        }

        #endregion

        #region Edit 

        public virtual IActionResult Update(ConnectionModel model)
        {
            if (_connectionModelFactory.IsConnectionToCategory(model.FromType) && !_permissionService.Authorize(StandardPermissionProvider.ManageCategories)
                || (!_connectionModelFactory.IsConnectionToCategory(model.FromType) && !_permissionService.Authorize(StandardPermissionProvider.ManageProducts)))
                return AccessDeniedView();

            //try to get a connection with the specified id
            var connection = _connectionsService.GetConnectionById(model.Id) ?? 
                throw new ArgumentException("No connection found with the specified id");

            connection.CssClass = model.CssClass;
            connection.PictureId = model.Picture?.ItemPictureId > 0 ? model.Picture.ItemPictureId : (int?)null;
            connection.ShowDescription = model.ShowDescription;
            connection.UpdatedOnUtc = DateTime.UtcNow;
            _connectionsService.UpdateConnection(connection);

            return new NullJsonResult();
        }

        public virtual IActionResult Delete(int id)
        {
            //try to get a connection category with the specified id
            var connection = _connectionsService.GetConnectionById(id) ??
                throw new ArgumentException("No connection found with the specified id");

            if (_connectionModelFactory.IsConnectionToCategory(connection.ConnectionFromType) && !_permissionService.Authorize(StandardPermissionProvider.ManageCategories)
                || (!_connectionModelFactory.IsConnectionToCategory(connection.ConnectionFromType) && !_permissionService.Authorize(StandardPermissionProvider.ManageProducts)))
                return AccessDeniedView();

            _connectionsService.DeleteConnection(connection);

            return new NullJsonResult();
        }

        #region Add Product

        public virtual IActionResult ConnectionProductAddPopup(int fromId, ConnectionFromType fromType)
        {
            if (_connectionModelFactory.IsConnectionToCategory(fromType) && !_permissionService.Authorize(StandardPermissionProvider.ManageCategories)
                || (!_connectionModelFactory.IsConnectionToCategory(fromType) && !_permissionService.Authorize(StandardPermissionProvider.ManageProducts)))
                return AccessDeniedView();

            //prepare model
            var model = _connectionModelFactory.PrepareAddProductSearchModel(new ConnectionAddProductSearchModel { FromType = fromType });

            return View(model);
        }

        [HttpPost]
        public virtual IActionResult ConnectionProductAddPopupList(ConnectionAddProductSearchModel searchModel)
        {
            if (_connectionModelFactory.IsConnectionToCategory(searchModel.FromType) && !_permissionService.Authorize(StandardPermissionProvider.ManageCategories)
                || (!_connectionModelFactory.IsConnectionToCategory(searchModel.FromType) && !_permissionService.Authorize(StandardPermissionProvider.ManageProducts)))
                return AccessDeniedKendoGridJson();

            //prepare model
            var model = _connectionModelFactory.PrepareConnectionAddProductListModel(searchModel);

            return Json(model);
        }

        [HttpPost]
        [FormValueRequired("save")]
        public virtual IActionResult ConnectionProductAddPopup(ConnectionAddProductModel model)
        {
            if (_connectionModelFactory.IsConnectionToCategory(model.FromType) && !_permissionService.Authorize(StandardPermissionProvider.ManageCategories)
                || (!_connectionModelFactory.IsConnectionToCategory(model.FromType) && !_permissionService.Authorize(StandardPermissionProvider.ManageProducts)))
                return AccessDeniedView();

            //get selected products
            var selectedProducts = _productService.GetProductsByIds(model.SelectedProductIds.ToArray());
            if (selectedProducts.Any())
            {
                var connectionsToInsert = new List<Connections>();
                var existingConnections = _connectionsService.GetProductConnectionsByObjectId(model.FromId, model.FromType);
                var utcDate = DateTime.UtcNow;
                var connectionType = model.FromType == ConnectionFromType.cat ? ConnectionType.prod_to_cat : ConnectionType.prod_to_prod;
                foreach (var product in selectedProducts.Where(p => !existingConnections.Contains(p.Id)))
                {
                    connectionsToInsert.Add(new Connections
                    {
                        ConnectionFromType = model.FromType,
                        ConnectionToType = ConnectionToType.prod,
                        From = model.FromId,
                        To = product.Id,
                        CreatedOnUtc = utcDate,
                        UpdatedOnUtc = utcDate,
                        ConnectionType = connectionType
                });
                }

                if (connectionsToInsert.Any())
                {
                    _connectionsService.InsertConnections(connectionsToInsert);
                }
            }

            ViewBag.RefreshPage = true;

            return View(new ConnectionAddProductSearchModel());
        }

        #endregion Add Product

        #region Add Category

        public virtual IActionResult ConnectionCategoryAddPopup(int fromId, ConnectionFromType fromType)
        {
            if (_connectionModelFactory.IsConnectionToCategory(fromType) && !_permissionService.Authorize(StandardPermissionProvider.ManageCategories)
                || (!_connectionModelFactory.IsConnectionToCategory(fromType) && !_permissionService.Authorize(StandardPermissionProvider.ManageProducts)))
                return AccessDeniedView();

            //prepare model
            var model = _connectionModelFactory.PrepareAddCategorySearchModel(new ConnectionAddCategorySearchModel { FromType = fromType });

            return View(model);
        }

        [HttpPost]
        public virtual IActionResult ConnectionCategoryAddPopupList(ConnectionAddCategorySearchModel searchModel)
        {
            if (_connectionModelFactory.IsConnectionToCategory(searchModel.FromType) && !_permissionService.Authorize(StandardPermissionProvider.ManageCategories)
                || (!_connectionModelFactory.IsConnectionToCategory(searchModel.FromType) && !_permissionService.Authorize(StandardPermissionProvider.ManageProducts)))
                return AccessDeniedKendoGridJson();

            //prepare model
            var model = _connectionModelFactory.PrepareConnectionAddCategoryListModel(searchModel);

            return Json(model);
        }

        [HttpPost]
        [FormValueRequired("save")]
        public virtual IActionResult ConnectionCategoryAddPopup(ConnectionAddCategoryModel model)
        {
            if (_connectionModelFactory.IsConnectionToCategory(model.FromType) && !_permissionService.Authorize(StandardPermissionProvider.ManageCategories)
                || (!_connectionModelFactory.IsConnectionToCategory(model.FromType) && !_permissionService.Authorize(StandardPermissionProvider.ManageProducts)))
                return AccessDeniedView();

            //get selected categories
            var selectedCategories = _categoryService.GetCategoriesByIds(model.SelectedCategoryIds.ToArray());
            if (selectedCategories.Any())
            {
                var connectionsToInsert = new List<Connections>();
                var existingConnections = _connectionsService.GetCategoryConnectionsByObjectId(model.FromId, model.FromType);
                var utcDate = DateTime.UtcNow;
                var connectionType = model.FromType == ConnectionFromType.cat ? ConnectionType.cat_to_cat : ConnectionType.cat_to_prod;
                foreach (var category in selectedCategories.Where(p => !existingConnections.Contains(p.Id)))
                {
                    connectionsToInsert.Add(new Connections
                    {
                        ConnectionFromType = model.FromType,
                        ConnectionToType = ConnectionToType.cat,
                        From = model.FromId,
                        To = category.Id,
                        CreatedOnUtc = utcDate,
                        UpdatedOnUtc = utcDate,
                        ConnectionType = connectionType,
                        ShowDescription = true
                    });
                }

                if (connectionsToInsert.Any())
                {
                    _connectionsService.InsertConnections(connectionsToInsert);
                }
            }

            ViewBag.RefreshPage = true;

            return View(new ConnectionAddCategorySearchModel());
        }

        #endregion Add Category

        #region Add Element 

        public virtual IActionResult ConnectionElementAddPopup(int fromId, ConnectionFromType fromType, ConnectionToType toType)
        {
            if (_connectionModelFactory.IsConnectionToCategory(fromType) && !_permissionService.Authorize(StandardPermissionProvider.ManageCategories)
                || (!_connectionModelFactory.IsConnectionToCategory(fromType) && !_permissionService.Authorize(StandardPermissionProvider.ManageProducts)))
                return AccessDeniedView();

            //prepare model
            var model = _connectionModelFactory.PrepareAddElementSearchModel(new ConnectionAddElementSearchModel
            {
                FromType = fromType,
                ToType = toType
            });

            return View(model);
        }

        [HttpPost]
        public virtual IActionResult ConnectionElementAddPopupList(ConnectionAddElementSearchModel searchModel)
        {
            if (_connectionModelFactory.IsConnectionToCategory(searchModel.FromType) && !_permissionService.Authorize(StandardPermissionProvider.ManageCategories)
                || (!_connectionModelFactory.IsConnectionToCategory(searchModel.FromType) && !_permissionService.Authorize(StandardPermissionProvider.ManageProducts)))
                return AccessDeniedKendoGridJson();

            //prepare model
            var model = _connectionModelFactory.PrepareConnectionAddElementListModel(searchModel);

            return Json(model);
        }

        [HttpPost]
        [FormValueRequired("save")]
        public virtual IActionResult ConnectionElementAddPopup(ConnectionAddElementModel model)
        {
            if (_connectionModelFactory.IsConnectionToCategory(model.FromType) && !_permissionService.Authorize(StandardPermissionProvider.ManageCategories)
                || (!_connectionModelFactory.IsConnectionToCategory(model.FromType) && !_permissionService.Authorize(StandardPermissionProvider.ManageProducts)))
                return AccessDeniedView();

            //get selected elements
            var selectedElements = _elementService.GetElementsByIds(model.SelectedElementIds.ToArray());
            if (selectedElements.Any())
            {
                var connectionsToInsert = new List<Connections>();
                var existingConnections = _connectionsService.GetElementConnectionsByObjectId(model.FromId, model.FromType, model.ToType);
                var utcDate = DateTime.UtcNow;
                ConnectionType connectionType;
                switch (model.ToType)
                {
                    case ConnectionToType.sep:
                        connectionType = model.FromType == ConnectionFromType.cat ? ConnectionType.sep_to_cat : ConnectionType.sep_to_prod;
                        break;
                    case ConnectionToType.desc:
                        connectionType = model.FromType == ConnectionFromType.cat ? ConnectionType.desc_to_cat : ConnectionType.desc_to_prod;
                        break;
                    default:
                        throw new NotSupportedException();
                }

                foreach (var element in selectedElements.Where(el => !existingConnections.Contains(el.Id)))
                {
                    connectionsToInsert.Add(new Connections
                    {
                        ConnectionFromType = model.FromType,
                        ConnectionToType = model.ToType,
                        From = model.FromId,
                        To = element.Id,
                        CreatedOnUtc = utcDate,
                        UpdatedOnUtc = utcDate,
                        ConnectionType = connectionType
                    });
                }

                if (connectionsToInsert.Any())
                {
                    _connectionsService.InsertConnections(connectionsToInsert);
                }
            }

            ViewBag.RefreshPage = true;

            return View(new ConnectionAddElementSearchModel());
        }

        #endregion Add Element

        #region Add Slider 

        public virtual IActionResult ConnectionSliderAddPopup(int fromId, ConnectionFromType fromType)
        {
            if (_connectionModelFactory.IsConnectionToCategory(fromType) && !_permissionService.Authorize(StandardPermissionProvider.ManageCategories)
                || (!_connectionModelFactory.IsConnectionToCategory(fromType) && !_permissionService.Authorize(StandardPermissionProvider.ManageProducts)))
                return AccessDeniedView();

            //prepare model
            var model = _connectionModelFactory.PrepareAddSliderSearchModel(new ConnectionAddSliderSearchModel
            {
                FromType = fromType
            });

            return View(model);
        }

        [HttpPost]
        public virtual IActionResult ConnectionSliderAddPopupList(ConnectionAddSliderSearchModel searchModel)
        {
            if (_connectionModelFactory.IsConnectionToCategory(searchModel.FromType) && !_permissionService.Authorize(StandardPermissionProvider.ManageCategories)
                || (!_connectionModelFactory.IsConnectionToCategory(searchModel.FromType) && !_permissionService.Authorize(StandardPermissionProvider.ManageProducts)))
                return AccessDeniedKendoGridJson();

            //prepare model
            var model = _connectionModelFactory.PrepareConnectionAddSliderListModel(searchModel);

            return Json(model);
        }

        [HttpPost]
        [FormValueRequired("save")]
        public virtual IActionResult ConnectionSliderAddPopup(ConnectionAddSliderModel model)
        {
            if (_connectionModelFactory.IsConnectionToCategory(model.FromType) && !_permissionService.Authorize(StandardPermissionProvider.ManageCategories)
                || (!_connectionModelFactory.IsConnectionToCategory(model.FromType) && !_permissionService.Authorize(StandardPermissionProvider.ManageProducts)))
                return AccessDeniedView();

            //get selected sliders
            var selectedSliders = _sliderService.GetSlidersByIds(model.SelectedSliderIds.ToArray());
            if (selectedSliders.Any())
            {
                var connectionsToInsert = new List<Connections>();
                var existingConnections = _connectionsService.GetSliderConnectionsByObjectId(model.FromId, model.FromType);
                var utcDate = DateTime.UtcNow;
                var connectionType = model.FromType == ConnectionFromType.cat ? ConnectionType.slider_to_cat : ConnectionType.slider_to_prod;

                foreach (var element in selectedSliders.Where(el => !existingConnections.Contains(el.Id)))
                {
                    connectionsToInsert.Add(new Connections
                    {
                        ConnectionFromType = model.FromType,
                        ConnectionToType = ConnectionToType.slider,
                        From = model.FromId,
                        To = element.Id,
                        CreatedOnUtc = utcDate,
                        UpdatedOnUtc = utcDate,
                        ConnectionType = connectionType
                    });
                }

                if (connectionsToInsert.Any())
                {
                    _connectionsService.InsertConnections(connectionsToInsert);
                }
            }

            ViewBag.RefreshPage = true;

            return View(new ConnectionAddSliderSearchModel());
        }

        #endregion Add Slider

        [HttpPost]
        public virtual IActionResult ReOrder(ReOrderConnectionsModel model)
        {
            if (_connectionModelFactory.IsConnectionToCategory(model.FromType) && !_permissionService.Authorize(StandardPermissionProvider.ManageCategories)
                || (!_connectionModelFactory.IsConnectionToCategory(model.FromType) && !_permissionService.Authorize(StandardPermissionProvider.ManageProducts)))
                return AccessDeniedView();

            _connectionsService.ReOrderConnections(model.NewDisplayOrder - model.OldDisplayOrder, model.FromId, model.DropItemId, model.DragItemId);

            return new NullJsonResult();
        }

        #endregion Edit
    }
}