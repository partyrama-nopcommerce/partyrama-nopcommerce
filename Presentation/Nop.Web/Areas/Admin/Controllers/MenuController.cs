﻿using Microsoft.AspNetCore.Mvc;
using Nop.Core.Caching;
using Nop.Core.Domain.Catalog;
using Nop.Services.Catalog;
using Nop.Services.Localization;
using Nop.Services.Security;
using Nop.Services.Topics;
using Nop.Web.Areas.Admin.Factories;
using Nop.Web.Areas.Admin.Helpers;
using Nop.Web.Areas.Admin.Infrastructure.Mapper.Extensions;
using Nop.Web.Areas.Admin.Models.Catalog;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Mvc;
using System;
using System.Linq;

namespace Nop.Web.Areas.Admin.Controllers
{
    public partial class MenuController : BaseAdminController
    {
        #region Fields

        private readonly IMenuModelFactory _menuModelFactory;
        private readonly IPermissionService _permissionService;
        private readonly IMenuService _menuService;
        private readonly ILocalizationService _localizationService;
        private readonly IMenuItemService _menuItemService;
        private readonly ICategoryService _categoryService;
        private readonly IProductService _productService;
        private readonly IElementService _elementService;
        private readonly IStaticCacheManager _cacheManager;
        private readonly ITopicService _topicService;

        #endregion

        #region Ctor

        public MenuController(IMenuModelFactory menuModelFactory,
            IPermissionService permissionService,
            IMenuService menuService,
            ILocalizationService localizationService,
            IMenuItemService menuItemService,
            ICategoryService categoryService,
            IProductService productService,
            IElementService elementService,
            IStaticCacheManager cacheManager,
            ITopicService topicService)
        {
            _menuModelFactory = menuModelFactory;
            _permissionService = permissionService;
            _menuService = menuService;
            _localizationService = localizationService;
            _menuItemService = menuItemService;
            _categoryService = categoryService;
            _productService = productService;
            _elementService = elementService;
            _cacheManager = cacheManager;
            _topicService = topicService;
        }

        #endregion

        #region Utilities

        #endregion

        #region List

        [HttpPost]
        public virtual IActionResult MenuItemList(MenuItemSearchModel searchModel)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageMenu))
                return AccessDeniedKendoGridJson();

            //prepare model
            var model = _menuModelFactory.PrepareMenuItemListModel(searchModel);

            return Json(model);
        }

        [HttpPost]
        public virtual IActionResult MenuItemRowList(MenuItemRowSearchModel searchModel)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageMenu))
                return AccessDeniedKendoGridJson();

            //prepare model
            var model = _menuModelFactory.PrepareMenuItemRowListModel(searchModel);

            return Json(model);
        }

        #endregion

        #region Create / Edit / Delete

        public virtual IActionResult Edit()
        {
            //prepare model
            var model = _menuModelFactory.PrepareMenuEditModel();

            return View(model);
        }

        #region Menus

        [HttpPost]
        public virtual IActionResult CreateMenu(MenuModel newMenu)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageMenu))
                return AccessDeniedView();

            if (ModelState.IsValid)
            {
                var menu = newMenu.ToEntity<Menu>();
                menu.CreatedOnUtc = DateTime.UtcNow;
                menu.UpdatedOnUtc = DateTime.UtcNow;
                _menuService.InsertMenu(menu);

                return Json(new
                {
                    id = menu.Id,
                    name = menu.Name,
                    url = Url.Action("EditMenu", "Menu", new { id = menu.Id }),
                    success = true
                });
            }
            else
            {
                var errorMessage = string.Empty;
                foreach (var modelState in ModelState.Values)
                {
                    foreach (var error in modelState.Errors)
                    {
                        errorMessage += error.ErrorMessage + Environment.NewLine;
                    }  
                }
                    

                return Json(new
                {
                    success = false,
                    message = errorMessage
                });
            }
        }

        [HttpPost]
        public virtual IActionResult DeleteMenu(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageMenu))
                return AccessDeniedView();

            //try to get a menu with the specified id
            var menu = _menuService.GetMenuById(id);
            if (menu == null)
            {
                return Json(new
                {
                    success = false,
                    message = _localizationService.GetResource("MegaMenu.DeleteMenu.Error")
                });
            }

            _menuService.DeleteMenu(menu);
            return Json(new
            {
                success = true
            });
        }

        public virtual IActionResult EditMenu(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageMenu))
                return AccessDeniedView();

            //try to get a menu with the specified id
            var menu = _menuService.GetMenuById(id);

            if (menu == null)
                return RedirectToAction("Edit");

            //prepare model
            var model = _menuModelFactory.PrepareMenuModel(null, menu);
            return PartialView("_MenuEdit", model);
        }

        [HttpPost]
        public virtual IActionResult EditMenu(MenuModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageMenu))
                return AccessDeniedView();

            //try to get a menu with the specified id
            var menu = _menuService.GetMenuById(model.Id);

            if (menu == null)
                return RedirectToAction("Edit");

            if (ModelState.IsValid)
            {
                menu = model.ToEntity(menu);
                menu.UpdatedOnUtc = DateTime.UtcNow;
                _menuService.UpdateMenu(menu);

                return Json(new
                {
                    name = menu.Name,
                    success = true
                });
            }
            else
            {
                var errorMessage = string.Empty;
                foreach (var modelState in ModelState.Values)
                {
                    foreach (var error in modelState.Errors)
                    {
                        errorMessage += error.ErrorMessage + Environment.NewLine;
                    }
                }

                return Json(new
                {
                    success = false,
                    message = errorMessage
                });
            }
        }

        #endregion Menus

        #region Items 

        public virtual IActionResult AddColumn(int id, RowType rowType)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageMenu))
                return AccessDeniedView();

            //try to get a menu with the specified id
            var menu = _menuService.GetMenuById(id);

            if (menu == null)
                return RedirectToAction("Edit");

            //prepare model

            var model = new MenuItemModel
            {
                ItemMenuId = menu.Id,
                ItemRowType = rowType
            };

            _menuModelFactory.PrepareMenuItemModel(model);

            return PartialView("_AddMenuItem", model);
        }

        [HttpPost]
        public virtual IActionResult AddColumn(MenuItemModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageMenu))
                return AccessDeniedView();

            if (ModelState.IsValid)
            {
                var menuItem = model.ToEntity<MenuItem>();
                if (menuItem.MegaMenuTabId == 0)
                    menuItem.MegaMenuTabId = null;

                menuItem.CreatedOnUtc = DateTime.UtcNow;
                menuItem.UpdatedOnUtc = DateTime.UtcNow;
                _menuItemService.InsertMenuItem(menuItem);

                return Json(new
                {
                    success = true
                });
            }
            else
            {
                var errorMessage = string.Empty;
                foreach (var modelState in ModelState.Values)
                {
                    foreach (var error in modelState.Errors)
                    {
                        errorMessage += error.ErrorMessage + Environment.NewLine;
                    }
                }


                return Json(new
                {
                    success = false,
                    message = errorMessage
                });
            }
        }


        public virtual IActionResult DeleteMenuItem(int id)
        {
            //try to get a menu item with the specified id
            var menuItem = _menuItemService.GetMenuItemById(id) ??
                throw new ArgumentException("No menu item found with the specified id");

            if (!_permissionService.Authorize(StandardPermissionProvider.ManageMenu))
                return AccessDeniedView();

            _menuItemService.DeleteMenuItem(menuItem);

            return new NullJsonResult();
        }

        public virtual IActionResult UpdateMenuItem(MenuItemModel model)
        {
            if (ModelState.IsValid)
            {
                if (!_permissionService.Authorize(StandardPermissionProvider.ManageMenu))
                    return AccessDeniedView();

                //try to get a menu item with the specified id
                var row = _menuItemService.GetMenuItemById(model.Id) ??
                    throw new ArgumentException("No menu item found with the specified id");

                row = model.ToEntity(row);
                if (row.MegaMenuTabId == 0)
                {
                    row.MegaMenuTabId = null;
                }

                row.UpdatedOnUtc = DateTime.UtcNow;
                _menuItemService.UpdateMenuItem(row);
            }

            return new NullJsonResult();
        }

        [HttpPost]
        public virtual IActionResult ReOrderColumn(ReOrderMenuItemModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageMenu))
                return AccessDeniedView();

            _menuItemService.ReOrderMenuItem(model.NewDisplayOrder, model.OldDisplayOrder, model.DragItemId, true);

            return new NullJsonResult();
        }

        [HttpPost]
        public virtual IActionResult ReOrderRow(ReOrderMenuItemModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageMenu))
                return AccessDeniedView();

            _menuItemService.ReOrderMenuItem(model.NewDisplayOrder, model.OldDisplayOrder, model.DragItemId, false);

            return new NullJsonResult();
        }


        #region Add Category Row

        public virtual IActionResult MenuAddRowCategoryPopup(int menuId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageMenu))
                return AccessDeniedView();

            //prepare model
            var model = _menuModelFactory.PrepareAddRowCategorySearchModel(new MenuAddRowCategorySearchModel
            {
                MenuId = menuId
            });

            return View(model);
        }

        [HttpPost]
        public virtual IActionResult RowCategoryAddPopupList(MenuAddRowCategorySearchModel searchModel)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageMenu))
                return AccessDeniedKendoGridJson();

            //prepare model
            var model = _menuModelFactory.PrepareMenuAddRowCategoryListModel(searchModel);

            return Json(model);
        }

        [HttpPost]
        [FormValueRequired("save")]
        public virtual IActionResult MenuAddRowCategoryPopup(MenuAddRowCategoryModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageMenu))
                return AccessDeniedView();

            //get selected categories
            var selectedCategories = _categoryService.GetCategoriesByIds(model.SelectedCategoryIds.ToArray());
            if (selectedCategories.Any())
            {
                _menuItemService.InsertCategoryRows(selectedCategories, model.MenuItemId, model.MenuId);
            }

            ViewBag.RefreshPage = true;

            return View(new MenuAddRowCategorySearchModel());
        }

        #endregion Add Category Row

        #region Add Product Row

        public virtual IActionResult MenuAddRowProductPopup(int menuId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageMenu))
                return AccessDeniedView();

            //prepare model
            var model = _menuModelFactory.PrepareAddRowProductSearchModel(new MenuAddRowProductSearchModel
            {
                MenuId = menuId
            });

            return View(model);
        }

        [HttpPost]
        public virtual IActionResult RowProductAddPopupList(MenuAddRowProductSearchModel searchModel)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageMenu))
                return AccessDeniedKendoGridJson();

            //prepare model
            var model = _menuModelFactory.PrepareMenuAddRowProductListModel(searchModel);

            return Json(model);
        }

        [HttpPost]
        [FormValueRequired("save")]
        public virtual IActionResult MenuAddRowProductPopup(MenuAddRowProductModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageMenu))
                return AccessDeniedView();

            //get selected products
            var selectedProducts = _productService.GetProductsByIds(model.SelectedProductIds.ToArray());
            if (selectedProducts.Any())
            {
                _menuItemService.InsertProductRows(selectedProducts.ToList(), model.MenuItemId, model.MenuId);
            }

            ViewBag.RefreshPage = true;

            return View(new MenuAddRowProductSearchModel());
        }

        #endregion Add Product Row

        #region Add Element Row

        public virtual IActionResult MenuAddRowElementPopup(int menuId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageMenu))
                return AccessDeniedView();

            //prepare model
            var model = _menuModelFactory.PrepareAddRowElementSearchModel(new MenuAddRowElementSearchModel
            {
                MenuId = menuId
            });

            return View(model);
        }

        [HttpPost]
        public virtual IActionResult RowElementAddPopupList(MenuAddRowElementSearchModel searchModel)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageMenu))
                return AccessDeniedKendoGridJson();

            //prepare model
            var model = _menuModelFactory.PrepareMenuAddRowElementListModel(searchModel);

            return Json(model);
        }

        [HttpPost]
        [FormValueRequired("save")]
        public virtual IActionResult MenuAddRowElementPopup(MenuAddRowElementModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageMenu))
                return AccessDeniedView();

            //get selected elements
            var selectedElements = _elementService.GetElementsByIds(model.SelectedElementIds.ToArray());
            if (selectedElements.Any())
            {
                _menuItemService.InsertElementRows(selectedElements, model.MenuItemId, model.MenuId);
            }

            ViewBag.RefreshPage = true;

            return View(new MenuAddRowElementSearchModel());
        }

        #endregion Add Element Row

        #region Add Topic Row

        public virtual IActionResult MenuAddRowTopicPopup(int menuId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageMenu))
                return AccessDeniedView();

            //prepare model
            var model = _menuModelFactory.PrepareAddRowTopicSearchModel(new MenuAddRowTopicSearchModel
            {
                MenuId = menuId
            });

            return View(model);
        }

        [HttpPost]
        public virtual IActionResult RowTopicAddPopupList(MenuAddRowTopicSearchModel searchModel)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageMenu))
                return AccessDeniedKendoGridJson();

            //prepare model
            var model = _menuModelFactory.PrepareMenuAddRowTopicListModel(searchModel);

            return Json(model);
        }

        [HttpPost]
        [FormValueRequired("save")]
        public virtual IActionResult MenuAddRowTopicPopup(MenuAddRowTopicModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageMenu))
                return AccessDeniedView();

            //get selected topics
            var selectedTopics = _topicService.GetTopicsByIds(model.SelectedTopicIds.ToArray());
            if (selectedTopics.Any())
            {
                _menuItemService.InsertTopicsRows(selectedTopics, model.MenuItemId, model.MenuId);
            }

            ViewBag.RefreshPage = true;

            return View(new MenuAddRowTopicSearchModel());
        }

        #endregion Add Topic Row

        #endregion Items

        #endregion

    }
}