Nop Commerce - Worldpay UK Payment Gateway Plugin by https://www.techwareone.com/
=================================================================================
If you have any problems with this plugin, please contact us for technical support.

Purchase of this plugin entitles you to all future versions of the same plugin.

You will need a Worldpay UK merchant account to use this plugin. Apply for one here: https://business.worldpay.com/partner/tech-ware-one-ltd


Release Notes
=============

v4.0.2
	* Added extra null checks to name+address fields when creating Worldpay payment request

v4.0.1
	* Updated to use TLS v1.2
	* Now adds a note to order with details of payment transaction

v4.0.0
	* Initial release
