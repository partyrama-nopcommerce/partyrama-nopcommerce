
GO

INSERT INTO [PermissionRecord]
           ([Name]
           ,[SystemName]
           ,[Category])
     VALUES
           ('Admin area. Manage Elements',
           'ManageElements',
           'Catalog')
GO

