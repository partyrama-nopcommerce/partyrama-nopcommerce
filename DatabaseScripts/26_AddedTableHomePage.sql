/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE HomePage
	(
	Id int NOT NULL IDENTITY (1, 1),
	Name varchar(50) NOT NULL,
	SetCountDown bit NOT NULL,
	StartDateUtc datetime2(7) NULL,
	EndDateUtc datetime2(7) NULL,
	Visible bit NOT NULL,
	TabConnectionTypeId int NULL,
	TabConnectionId int NULL,
	CreatedOnUtc datetime2(7) NOT NULL,
	UpdatedOnUtc datetime2(7) NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE HomePage ADD CONSTRAINT
	DF_HomePage_SetCountDown DEFAULT 0 FOR SetCountDown
GO
ALTER TABLE HomePage ADD CONSTRAINT
	DF_HomePage_Visible DEFAULT 0 FOR Visible
GO
ALTER TABLE HomePage ADD CONSTRAINT
	PK_HomePage PRIMARY KEY CLUSTERED 
	(
	Id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE HomePage SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
