BEGIN TRANSACTION
GO  
EXEC sp_rename 'Element.Hide', 'Visible', 'COLUMN';  
EXEC sp_rename 'MegaMenuTab.Hide', 'Visible', 'COLUMN';  
EXEC sp_rename 'MegaMenuTabItem.Hide', 'Visible', 'COLUMN';  
EXEC sp_rename 'Menu.Hide', 'Visible', 'COLUMN';  
EXEC sp_rename 'MenuItem.Hide', 'Visible', 'COLUMN';  
GO  
UPDATE Element SET Visible = ~Visible;
UPDATE MegaMenuTab SET Visible = ~Visible;
UPDATE MegaMenuTabItem SET Visible = ~Visible;
UPDATE Menu SET Visible = ~Visible;
UPDATE MenuItem SET Visible = ~Visible;
GO
COMMIT
