
GO

INSERT INTO [PermissionRecord]
           ([Name]
           ,[SystemName]
           ,[Category])
     VALUES
           ('Admin area. Manage Home Page',
           'ManageHomePage',
           'Catalog')
GO

INSERT INTO [PermissionRecord]
           ([Name]
           ,[SystemName]
           ,[Category])
     VALUES
           ('Admin area. Manage Slider',
           'ManageSlider',
           'Catalog')
GO

INSERT INTO [PermissionRecord]
           ([Name]
           ,[SystemName]
           ,[Category])
     VALUES
           ('Admin area. Manage Sidebar',
           'ManageSidebar',
           'Catalog')
GO

