BEGIN TRANSACTION
GO

WITH RD AS
(SELECT ROW_NUMBER() OVER (PARTITION BY Id, [Type], [From], [To] ORDER BY (SELECT 1)) Row,
 Id FROM Connections)

DELETE FROM RD
WHERE RD.Row > 1

GO

ALTER TABLE Connections ADD PRIMARY KEY(Id);

GO
 
ALTER TABLE Connections ADD
	FromType varchar(50) NULL,
	ToType varchar(50) NULL
GO

UPDATE Connections SET ToType = SUBSTRING([Type], 0, CHARINDEX('_', [Type]));
UPDATE Connections SET FromType = SUBSTRING([Type], CHARINDEX('_', [Type]) + 4, LEN(Type));

GO
WITH UpdateData  AS
(
	SELECT 
		ROW_NUMBER() OVER(PARTITION BY [From], [FromType] ORDER BY DisplayOrder ASC, Id ASC) AS RN,
		*
	FROM Connections
)

UPDATE Connections SET DisplayOrder = UpdateData.RN
FROM Connections
INNER JOIN UpdateData ON Connections.Id = UpdateData.Id
GO

ALTER TABLE Connections  
ADD CONSTRAINT Unique_DisplayOrder UNIQUE ([DisplayOrder], [From], [FromType]); 

GO
COMMIT