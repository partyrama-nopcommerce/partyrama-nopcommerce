/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
GO  
EXEC sp_rename 'Connections.conn_id', 'Id', 'COLUMN';  
EXEC sp_rename 'Connections.conn_type', 'Type', 'COLUMN';  
EXEC sp_rename 'Connections.conn_order', 'DisplayOrder', 'COLUMN';  
EXEC sp_rename 'Connections.conn_class', 'CssClass', 'COLUMN';  
EXEC sp_rename 'Connections.conn_name', 'Name', 'COLUMN';  
EXEC sp_rename 'Connections.conn_from', 'From', 'COLUMN';  
EXEC sp_rename 'Connections.conn_to', 'To', 'COLUMN';  
EXEC sp_rename 'Connections.last_update_date', 'UpdatedOnUtc', 'COLUMN';  
EXEC sp_rename 'Connections.conn_date', 'CreatedOnUtc', 'COLUMN';  
GO  
ALTER TABLE Connections
ALTER COLUMN DisplayOrder int;
GO
COMMIT
