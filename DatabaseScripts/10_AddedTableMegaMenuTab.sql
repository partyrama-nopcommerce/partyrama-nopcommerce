/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE MegaMenuTab
	(
	Id int NOT NULL IDENTITY (1, 1),
	mm_settings_id int NULL,
	mm_tab_id int NULL,
	Name varchar(50) NOT NULL,
	mm_tab_type varchar(50) NULL,
	SetCountDown bit NOT NULL,
	StartDateUtc datetime2(7) NULL,
	EndDateUtc datetime2(7) NULL,
	Hide bit NOT NULL,
	TabConnectionTypeId int NULL,
	TabConnectionId int NULL,
	CreatedOnUtc datetime2(7) NOT NULL,
	UpdatedOnUtc datetime2(7) NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE MegaMenuTab ADD CONSTRAINT
	DF_MegaMenuTab_SetCountDown DEFAULT 0 FOR SetCountDown
GO
ALTER TABLE MegaMenuTab ADD CONSTRAINT
	DF_Table_1_Hide DEFAULT 0 FOR Hide
GO
ALTER TABLE MegaMenuTab ADD CONSTRAINT
	PK_MegaMenuTab PRIMARY KEY CLUSTERED 
	(
	Id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE MegaMenuTab SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
