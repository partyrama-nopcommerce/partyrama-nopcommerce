GO
CREATE TABLE [Category_Picture_Mapping](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CategoryId] [int] NOT NULL,
	[PictureId] [int] NOT NULL,
	[DisplayOrder] [int] NOT NULL,
 CONSTRAINT [PK_Category_Picture_Mapping] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[Category_Picture_Mapping]  WITH CHECK ADD  CONSTRAINT [FK_Category_Picture_Mapping_Picture_PictureId] FOREIGN KEY([PictureId])
REFERENCES [dbo].[Picture] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[Category_Picture_Mapping] CHECK CONSTRAINT [FK_Category_Picture_Mapping_Picture_PictureId]
GO

ALTER TABLE [dbo].[Category_Picture_Mapping]  WITH CHECK ADD  CONSTRAINT [FK_Category_Picture_Mapping_Category_CategoryId] FOREIGN KEY([CategoryId])
REFERENCES [dbo].[Category] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[Category_Picture_Mapping] CHECK CONSTRAINT [FK_Category_Picture_Mapping_Category_CategoryId]
GO


