ALTER TABLE [MegaMenuTabItem] DROP COLUMN [mm_eu_id]
GO
ALTER TABLE [MegaMenuTabItem] DROP COLUMN [mm_cat_id]
GO
ALTER TABLE [MegaMenuTabItem] DROP COLUMN [mm_image]
GO
ALTER TABLE [MegaMenuTabItem] DROP COLUMN [mm_url]
GO
ALTER TABLE [MegaMenuTabItem] DROP COLUMN [mm_name]
GO
ALTER TABLE [MegaMenuTabItem] DROP COLUMN [mm_tab_name]
GO
ALTER TABLE [MegaMenuTabItem] DROP COLUMN [mm_id]
GO
ALTER TABLE [MegaMenuTab] DROP COLUMN [mm_tab_type]
GO
ALTER TABLE [MegaMenuTab] DROP COLUMN [mm_tab_id]
GO
ALTER TABLE [MegaMenuTab] DROP COLUMN [mm_settings_id]
GO
