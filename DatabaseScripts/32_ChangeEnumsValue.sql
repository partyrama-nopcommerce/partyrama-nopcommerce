UPDATE MegaMenuTabItem SET RowConnectionTypeId = RowConnectionTypeId + 1 WHERE RowConnectionTypeId IS NOT NULL;
UPDATE MegaMenuTabItem SET RowTypeId = RowTypeId + 1;

UPDATE MenuItem SET RowConnectionTypeId = RowConnectionTypeId + 1 WHERE RowConnectionTypeId IS NOT NULL;
UPDATE MenuItem SET RowTypeId = RowTypeId + 1;

UPDATE SidebarItem SET RowConnectionTypeId = RowConnectionTypeId + 1 WHERE RowConnectionTypeId IS NOT NULL;
UPDATE SidebarItem SET RowTypeId = RowTypeId + 1;

UPDATE SliderItem SET RowConnectionTypeId = RowConnectionTypeId + 1 WHERE RowConnectionTypeId IS NOT NULL;
UPDATE SliderItem SET RowTypeId = RowTypeId + 1;

UPDATE HomePageItem SET RowConnectionTypeId = RowConnectionTypeId + 1 WHERE RowConnectionTypeId IS NOT NULL;
UPDATE HomePageItem SET RowTypeId = RowTypeId + 1;