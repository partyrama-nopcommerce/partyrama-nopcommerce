BEGIN TRANSACTION
GO
EXEC sp_rename 'Element.Image', 'PictureId', 'COLUMN';  
GO  
ALTER TABLE [Element]
ALTER COLUMN PictureId int NOT NULL;
GO
ALTER TABLE [Element] ADD
	ShortName varchar(255) NULL
GO
GO

INSERT INTO [ActivityLogType]
           ([SystemKeyword]
		  ,[Name]
		  ,[Enabled])
     VALUES
           ('DeleteElement',
           'Delete element',
           1);

INSERT INTO [ActivityLogType]
           ([SystemKeyword]
		  ,[Name]
		  ,[Enabled])
     VALUES
           ('EditElement',
           'Edit element',
           1)

INSERT INTO [ActivityLogType]
           ([SystemKeyword]
		  ,[Name]
		  ,[Enabled])
     VALUES
           ('AddNewElement',
           'Add a new element',
           1)

GO
COMMIT


