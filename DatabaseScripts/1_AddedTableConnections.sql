/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Connections
	(
	conn_id int NOT NULL IDENTITY (1, 1),
	conn_type varchar(50) NULL,
	conn_from int NULL,
	conn_to int NULL,
	conn_order varchar(50) NULL,
	conn_class varchar(50) NULL,
	conn_name varchar(50) NULL,
	conn_date datetime NULL,
	last_update_date datetime NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Connections SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
