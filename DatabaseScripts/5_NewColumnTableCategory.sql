BEGIN TRANSACTION
GO
 
ALTER TABLE Category ADD
	ShortName varchar(255) NULL,
	SidebarID int NULL,
	collapseID int NULL,
	CatClass varchar(255) NULL
GO  

EXEC sp_rename 'Category.SidebarID', 'SidebarId', 'COLUMN';  
EXEC sp_rename 'Category.collapseID', 'CollapseId', 'COLUMN';   
EXEC sp_rename 'Category.CatClass', 'CssClass', 'COLUMN';

GO
COMMIT
