BEGIN TRANSACTION
GO
 
ALTER TABLE Product ADD
	ProductStatusId int NULL,
	SalePrice money NULL,
	SpecialPrice money NULL,
	ProdClass varchar(255) NULL,
	SidebarID int NULL,
	CollapseID int NULL,
	Hex varchar(255) NULL,
	AppSKU varchar(255) NULL,
	VFR int NULL,
	AppImage varchar(255) NULL
GO  

UPDATE Product SET ProductStatusId = 0;
ALTER TABLE Product
ALTER COLUMN ProductStatusId int NOT NULL;

GO 

EXEC sp_rename 'Product.SidebarID', 'SidebarId', 'COLUMN';  
EXEC sp_rename 'Product.CollapseID', 'CollapseId', 'COLUMN';   
EXEC sp_rename 'Product.ProdClass', 'CssClass', 'COLUMN';
EXEC sp_rename 'Product.AppSKU', 'AppSku', 'COLUMN';

GO
COMMIT
