using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Nop.Core.Domain.Catalog;

namespace Nop.Data.Mapping.Catalog
{
    /// <summary>
    /// Represents an element mapping configuration
    /// </summary>
    public partial class ElementMap : NopEntityTypeConfiguration<Element>
    {
        #region Methods

        /// <summary>
        /// Configures the entity
        /// </summary>
        /// <param name="builder">The builder to be used to configure the entity</param>
        public override void Configure(EntityTypeBuilder<Element> builder)
        {
            builder.ToTable(nameof(Element));
            builder.HasKey(element => element.Id);

            builder.Property(element => element.Name).HasMaxLength(400).IsRequired();
            builder.Property(element => element.CssClass).HasMaxLength(255);
            builder.Property(Element => Element.ShortName).HasMaxLength(255);

            builder.Ignore(element => element.ElementType);

            base.Configure(builder);
        }

        #endregion
    }
}