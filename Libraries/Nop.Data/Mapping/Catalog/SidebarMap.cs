using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Nop.Core.Domain.Catalog;

namespace Nop.Data.Mapping.Catalog
{
    /// <summary>
    /// Represents a sidebar mapping configuration
    /// </summary>
    public partial class SidebarMap : NopEntityTypeConfiguration<Sidebar>
    {
        #region Methods

        /// <summary>
        /// Configures the entity
        /// </summary>
        /// <param name="builder">The builder to be used to configure the entity</param>
        public override void Configure(EntityTypeBuilder<Sidebar> builder)
        {
            builder.ToTable(nameof(Sidebar));
            builder.HasKey(c => c.Id);

            builder.Property(c => c.Name).HasMaxLength(50).IsRequired();

            builder.Ignore(c => c.TabConnectionType);

            base.Configure(builder);
        }

        #endregion
    }
}