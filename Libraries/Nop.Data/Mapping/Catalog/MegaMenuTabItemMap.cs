using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Nop.Core.Domain.Catalog;

namespace Nop.Data.Mapping.Catalog
{
    /// <summary>
    /// Represents a mega menu tab item mapping configuration
    /// </summary>
    public partial class MegaMenuTabItemMap : NopEntityTypeConfiguration<MegaMenuTabItem>
    {
        #region Methods

        /// <summary>
        /// Configures the entity
        /// </summary>
        /// <param name="builder">The builder to be used to configure the entity</param>
        public override void Configure(EntityTypeBuilder<MegaMenuTabItem> builder)
        {
            builder.ToTable(nameof(MegaMenuTabItem));
            builder.HasKey(c => c.Id);

            builder.Property(c => c.CssClass).HasMaxLength(255);
            builder.Property(c => c.Color).HasMaxLength(7);

            builder.HasOne(tabItem => tabItem.MegaMenuTab)
                .WithMany(tab => tab.MegaMenuTabItems)
                .HasForeignKey(tabItem => tabItem.MegaMenuTabId)
                .IsRequired();

            builder.Ignore(c => c.RowConnectionType);
            builder.Ignore(c => c.RowType);

            base.Configure(builder);
        }

        #endregion
    }
}