using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Nop.Core.Domain.Catalog;

namespace Nop.Data.Mapping.Catalog
{
    /// <summary>
    /// Represents a menu mapping configuration
    /// </summary>
    public partial class MenuMap : NopEntityTypeConfiguration<Menu>
    {
        #region Methods

        /// <summary>
        /// Configures the entity
        /// </summary>
        /// <param name="builder">The builder to be used to configure the entity</param>
        public override void Configure(EntityTypeBuilder<Menu> builder)
        {
            builder.ToTable(nameof(Menu));
            builder.HasKey(c => c.Id);

            builder.Property(c => c.Name).HasMaxLength(50).IsRequired();

            builder.Ignore(c => c.TabConnectionType);

            base.Configure(builder);
        }

        #endregion
    }
}