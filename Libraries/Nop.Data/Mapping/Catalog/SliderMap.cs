using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Nop.Core.Domain.Catalog;

namespace Nop.Data.Mapping.Catalog
{
    /// <summary>
    /// Represents a slider mapping configuration
    /// </summary>
    public partial class SliderMap : NopEntityTypeConfiguration<Slider>
    {
        #region Methods

        /// <summary>
        /// Configures the entity
        /// </summary>
        /// <param name="builder">The builder to be used to configure the entity</param>
        public override void Configure(EntityTypeBuilder<Slider> builder)
        {
            builder.ToTable(nameof(Slider));
            builder.HasKey(c => c.Id);

            builder.Property(c => c.Name).HasMaxLength(50).IsRequired();
            builder.Property(c => c.CssClass).HasMaxLength(255);
            builder.Property(c => c.BorderColor).HasMaxLength(7);

            builder.Ignore(c => c.TabConnectionType);
            builder.Ignore(c => c.SliderType);
            builder.Ignore(c => c.BorderStyle);
            builder.Ignore(c => c.BorderPlacement);

            base.Configure(builder);
        }

        #endregion
    }
}