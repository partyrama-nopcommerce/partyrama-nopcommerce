using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Nop.Core.Domain.Catalog;

namespace Nop.Data.Mapping.Catalog
{
    /// <summary>
    /// Represents a megamenu tab mapping configuration
    /// </summary>
    public partial class MegaMenuTabMap : NopEntityTypeConfiguration<MegaMenuTab>
    {
        #region Methods

        /// <summary>
        /// Configures the entity
        /// </summary>
        /// <param name="builder">The builder to be used to configure the entity</param>
        public override void Configure(EntityTypeBuilder<MegaMenuTab> builder)
        {
            builder.ToTable(nameof(MegaMenuTab));
            builder.HasKey(c => c.Id);

            builder.Property(c => c.Name).HasMaxLength(50).IsRequired();

            builder.Ignore(c => c.TabConnectionType);

            base.Configure(builder);
        }

        #endregion
    }
}