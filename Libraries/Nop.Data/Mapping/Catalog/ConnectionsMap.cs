using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Nop.Core.Domain.Catalog;

namespace Nop.Data.Mapping.Catalog
{
    /// <summary>
    /// Represents a connection mapping configuration
    /// </summary>
    public partial class ConnectionsMap : NopEntityTypeConfiguration<Connections>
    {
        #region Methods

        /// <summary>
        /// Configures the entity
        /// </summary>
        /// <param name="builder">The builder to be used to configure the entity</param>
        public override void Configure(EntityTypeBuilder<Connections> builder)
        {
            builder.ToTable(nameof(Connections));
            builder.HasKey(c => c.Id);

            builder.Property(c => c.FromType).HasMaxLength(50).IsRequired();
            builder.Property(c => c.ToType).HasMaxLength(50).IsRequired();
            builder.Property(c => c.CssClass).HasMaxLength(50).IsRequired();

            builder.Ignore(c => c.ConnectionType);
            builder.Ignore(c => c.ConnectionFromType);
            builder.Ignore(c => c.ConnectionToType);

            base.Configure(builder);
        }

        #endregion
    }
}