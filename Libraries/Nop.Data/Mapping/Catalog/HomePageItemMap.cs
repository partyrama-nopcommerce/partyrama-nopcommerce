using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Nop.Core.Domain.Catalog;

namespace Nop.Data.Mapping.Catalog
{
    /// <summary>
    /// Represents a home page item mapping configuration
    /// </summary>
    public partial class HomePageItemMap : NopEntityTypeConfiguration<HomePageItem>
    {
        #region Methods

        /// <summary>
        /// Configures the entity
        /// </summary>
        /// <param name="builder">The builder to be used to configure the entity</param>
        public override void Configure(EntityTypeBuilder<HomePageItem> builder)
        {
            builder.ToTable(nameof(HomePageItem));
            builder.HasKey(c => c.Id);

            builder.Property(c => c.CssClass).HasMaxLength(255);
            builder.Property(c => c.Color).HasMaxLength(7);

            builder.HasOne(item => item.HomePage)
                .WithMany(homePage => homePage.HomePageItems)
                .HasForeignKey(item => item.HomePageId)
                .IsRequired();

            builder.Ignore(c => c.RowConnectionType);
            builder.Ignore(c => c.RowType);

            base.Configure(builder);
        }

        #endregion
    }
}