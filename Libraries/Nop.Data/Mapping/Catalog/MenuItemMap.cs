using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Nop.Core.Domain.Catalog;

namespace Nop.Data.Mapping.Catalog
{
    /// <summary>
    /// Represents a menu item mapping configuration
    /// </summary>
    public partial class MenuItemMap : NopEntityTypeConfiguration<MenuItem>
    {
        #region Methods

        /// <summary>
        /// Configures the entity
        /// </summary>
        /// <param name="builder">The builder to be used to configure the entity</param>
        public override void Configure(EntityTypeBuilder<MenuItem> builder)
        {
            builder.ToTable(nameof(MenuItem));
            builder.HasKey(c => c.Id);

            builder.Property(c => c.CssClass).HasMaxLength(255);
            builder.Property(c => c.Color).HasMaxLength(7);

            builder.HasOne(tabItem => tabItem.Menu)
                .WithMany(tab => tab.MenuItems)
                .HasForeignKey(tabItem => tabItem.MenuId)
                .IsRequired();

            builder.Ignore(c => c.RowConnectionType);
            builder.Ignore(c => c.RowType);

            base.Configure(builder);
        }

        #endregion
    }
}