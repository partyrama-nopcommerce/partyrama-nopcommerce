using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Nop.Core.Domain.Catalog;

namespace Nop.Data.Mapping.Catalog
{
    /// <summary>
    /// Represents a sidebar item mapping configuration
    /// </summary>
    public partial class SidebarItemMap : NopEntityTypeConfiguration<SidebarItem>
    {
        #region Methods

        /// <summary>
        /// Configures the entity
        /// </summary>
        /// <param name="builder">The builder to be used to configure the entity</param>
        public override void Configure(EntityTypeBuilder<SidebarItem> builder)
        {
            builder.ToTable(nameof(SidebarItem));
            builder.HasKey(c => c.Id);

            builder.Property(c => c.CssClass).HasMaxLength(255);
            builder.Property(c => c.Color).HasMaxLength(7);

            builder.HasOne(item => item.Sidebar)
                .WithMany(sidebar => sidebar.SidebarItems)
                .HasForeignKey(item => item.SidebarId)
                .IsRequired();

            builder.Ignore(c => c.RowConnectionType);
            builder.Ignore(c => c.RowType);

            base.Configure(builder);
        }

        #endregion
    }
}