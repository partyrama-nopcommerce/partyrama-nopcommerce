using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Nop.Core.Domain.Catalog;

namespace Nop.Data.Mapping.Catalog
{
    /// <summary>
    /// Represents a category picture mapping configuration
    /// </summary>
    public partial class CategoryPictureMap : NopEntityTypeConfiguration<CategoryPicture>
    {
        #region Methods

        /// <summary>
        /// Configures the entity
        /// </summary>
        /// <param name="builder">The builder to be used to configure the entity</param>
        public override void Configure(EntityTypeBuilder<CategoryPicture> builder)
        {
            builder.ToTable(NopMappingDefaults.CategoryPictureTable);
            builder.HasKey(categoryPicture => categoryPicture.Id);

            builder.HasOne(categoryPicture => categoryPicture.Picture)
                .WithMany()
                .HasForeignKey(categoryPicture => categoryPicture.PictureId)
                .IsRequired();

            builder.HasOne(categoryPicture => categoryPicture.Category)
                .WithMany(category => category.CategoryPictures)
                .HasForeignKey(categoryPicture => categoryPicture.CategoryId)
                .IsRequired();

            base.Configure(builder);
        }

        #endregion
    }
}