using System.Collections.Generic;
using Nop.Core;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Topics;

namespace Nop.Services.Catalog
{
    /// <summary>
    /// Home page item service interface
    /// </summary>
    public partial interface IHomePageItemService
    {
        /// <summary>
        /// Gets home page items collection
        /// </summary>
        /// <param name="homePageId">Home page identifier</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <returns>Home page item collection</returns>
        IPagedList<HomePageItem> GetColumnsHeadersByHomePageId(int homePageId,
            int pageIndex = 0,
            int pageSize = int.MaxValue);

        /// <summary>
        /// Gets home page items collection
        /// </summary>
        /// <param name="homePageItem">Home page item</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <returns>Home page item collection</returns>
        IPagedList<HomePageItem> GetRowsByHomePageItem(HomePageItem homePageItem,
            int pageIndex = 0,
            int pageSize = int.MaxValue);

        /// <summary>
        /// Inserts home page item
        /// </summary>
        /// <param name="homePageItem">Home page item</param>
        void InsertHomePageItem(HomePageItem homePageItem);

        /// <summary>
        /// Gets a home page item
        /// </summary>
        /// <param name="homePageItemId">Home page item identifier</param>
        /// <returns>Home page item</returns>
        HomePageItem GetHomePageItemById(int homePageItemId);

        /// <summary>
        /// Delete a home page item
        /// </summary>
        /// <param name="homePageItem">Home page item</param>
        void DeleteHomePageItem(HomePageItem homePageItem);

        /// <summary>
        /// Delete a home page item
        /// </summary>
        /// <param name="connectionType">Row connection type</param>
        /// <param name="rowConnectionIds">Row connection identifiers</param>
        void DeleteConnectionHomePageItem(RowConnectionType connectionType, IEnumerable<int> rowConnectionIds);

        /// <summary>
        /// Gets home page items collection
        /// </summary>
        /// <param name="homePageId">Home page identifier</param>
        /// <returns>Columns</returns>
        IList<HomePageItem> GetColumnsByHomePageId(int homePageId);

        /// <summary>
        /// Inserts home page item rows
        /// </summary>
        /// <param name="selectedCategories">Selected categories</param>
        /// <param name="homePageItemId">Home page item column identifier</param>
        /// <param name="homePageId">Home page identifier</param>
        void InsertCategoryRows(List<Category> selectedCategories, int homePageItemId, int homePageId);

        /// <summary>
        /// Inserts home page item rows
        /// </summary>
        /// <param name="selectedProducts">Selected products</param>
        /// <param name="homePageItemId">Home page item column identifier</param>
        /// <param name="homePageId">Home page identifier</param>
        void InsertProductRows(List<Product> selectedProducts, int homePageItemId, int homePageId);

        /// <summary>
        /// Inserts home page item rows
        /// </summary>
        /// <param name="selectedElements">Selected elements</param>
        /// <param name="homePageItemId">Home page item column identifier</param>
        /// <param name="homePageId">Home page identifier</param>
        void InsertElementRows(List<Element> selectedElements, int homePageItemId, int homePageId);

        /// <summary>
        /// Inserts home page item rows
        /// </summary>
        /// <param name="selectedTopics">Selected topics</param>
        /// <param name="homePageItemId">Home page item column identifier</param>
        /// <param name="homePageId">Home page identifier</param>
        void InsertTopicsRows(List<Topic> selectedTopics, int homePageItemId, int homePageId);

        /// <summary>
        /// Updates the home page item
        /// </summary>
        /// <param name="homePageItem">Home page item</param>
        void UpdateHomePageItem(HomePageItem homePageItem);

        /// <summary>
        /// Reorder home page item
        /// </summary>
        /// <param name="newDisplayOrder">New display order</param>
        /// <param name="oldDisplayOrder">Old display order</param>
        /// <param name="dragItemId">Item that we are moving</param>
        /// <param name="isColumn">A value indicating whether drag item is column</param>
        void ReOrderHomePageItem(int newDisplayOrder, int oldDisplayOrder, int dragItemId, bool isColumn);

        /// <summary>
        /// Gets home page items collection
        /// </summary>
        /// <param name="homePageId">Home page identifier</param>
        /// <param name="visible">Show only visible</param>
        /// <returns>Home page items</returns>
        IList<HomePageItem> GetItemsByHomePageId(int homePageId, bool visible = true);

        /// <summary>
        /// Inserts home page item rows
        /// </summary>
        /// <param name="selectedSliders">Selected sliders</param>
        /// <param name="homePageItemId">Home page item column identifier</param>
        /// <param name="homePageId">Home page identifier</param>
        void InsertSliderRows(List<Slider> selectedSliders, int homePageItemId, int homePageId);
    }
}