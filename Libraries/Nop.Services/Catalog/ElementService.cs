using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Data;
using Nop.Core.Domain.Catalog;
using Nop.Services.Events;

namespace Nop.Services.Catalog
{
    /// <summary>
    /// Element service
    /// </summary>
    public partial class ElementService : IElementService
    {
        #region Fields

        private readonly IRepository<Element> _elementRepository;
        private readonly ICacheManager _cacheManager;
        private readonly IStaticCacheManager _staticCacheManager;
        private readonly IEventPublisher _eventPublisher;
        private readonly IRepository<Connections> _connectionsRepository;
        private readonly IMegaMenuTabItemService _megaMenuTabItemService;
        private readonly IMenuItemService _menuItemService;

        #endregion

        #region Ctor

        public ElementService(IRepository<Element> elementRepository,
            ICacheManager cacheManager,
            IStaticCacheManager staticCacheManager,
            IEventPublisher eventPublisher,
            IRepository<Connections> connectionsRepository,
            IMegaMenuTabItemService megaMenuTabItemService,
            IMenuItemService menuItemService)
        {
            _elementRepository = elementRepository;
            _cacheManager = cacheManager;
            _staticCacheManager = staticCacheManager;
            _eventPublisher = eventPublisher;
            _connectionsRepository = connectionsRepository;
            _megaMenuTabItemService = megaMenuTabItemService;
            _menuItemService = menuItemService;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Gets an element
        /// </summary>
        /// <param name="elementId">Element identifier</param>
        /// <returns>Element</returns>
        public Element GetElementById(int elementId)
        {
            if (elementId == 0)
                return null;

            var key = string.Format(NopCatalogDefaults.ElementsByIdCacheKey, elementId);
            return _cacheManager.Get(key, () => _elementRepository.GetById(elementId));
        }

        /// <summary>
        /// Gets elements by identifier
        /// </summary>
        /// <param name="elementIds">Element identifiers</param>
        /// <returns>Elements</returns>
        public List<Element> GetElementsByIds(int[] elementIds)
        {
            if (elementIds == null || elementIds.Length == 0)
                return new List<Element>();

            var query = from el in _elementRepository.Table
                        where elementIds.Contains(el.Id)
                        select el;

            return query.ToList();
        }

        /// <summary>
        /// Gets all elements
        /// </summary>
        /// <param name="elementType">Element type</param>
        /// <param name="elementName">Element name</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <returns>Categories</returns>
        public IPagedList<Element> GetAllElements(ElementType? elementType = null, string elementName = null, int pageIndex = 0, int pageSize = int.MaxValue)
        {
            var query = _elementRepository.Table;
            if (!string.IsNullOrWhiteSpace(elementName))
            {
                query = query.Where(el => el.Name.Contains(elementName));
            }

            if (elementType.HasValue)
            {
                query = query.Where(el => el.ElementType == elementType);
            }

            query = query.OrderBy(el => el.Id);

            var elements = new PagedList<Element>(query, pageIndex, pageSize);
            return elements;
        }

        /// <summary>
        /// Gets all elements
        /// </summary>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>Elements</returns>
        public IList<Element> GetAllElementsList(bool showHidden = true)
        {
            var query = _elementRepository.Table;

            if (!showHidden)
            {
                query = query.Where(p => p.Visible);
            }

            return query.OrderBy(p => p.Name).ToList();
        }

        /// <summary>
        /// Inserts element
        /// </summary>
        /// <param name="element">Element</param>
        public void InsertElement(Element element)
        {
            if (element == null)
                throw new ArgumentNullException(nameof(element));

            _elementRepository.Insert(element);

            //cache
            _cacheManager.RemoveByPattern(NopCatalogDefaults.ElementsPatternCacheKey);
            _staticCacheManager.RemoveByPattern(NopCatalogDefaults.CategoriesPatternCacheKey);

            //event notification
            _eventPublisher.EntityInserted(element);
        }

        /// <summary>
        /// Updates the element
        /// </summary>
        /// <param name="element">Element</param>
        public void UpdateElement(Element element)
        {
            if (element == null)
                throw new ArgumentNullException(nameof(element));

            _elementRepository.Update(element);

            //cache
            _cacheManager.RemoveByPattern(NopCatalogDefaults.ElementsPatternCacheKey);
            _staticCacheManager.RemoveByPattern(NopCatalogDefaults.CategoriesPatternCacheKey);

            //event notification
            _eventPublisher.EntityUpdated(element);
        }

        /// <summary>
        /// Delete element
        /// </summary>
        /// <param name="element">Element</param>
        public void DeleteElement(Element element)
        {
            if (element == null)
                throw new ArgumentNullException(nameof(element));

            _elementRepository.Delete(element);

            //event notification
            _eventPublisher.EntityDeleted(element);

            var connections = (from cc in _connectionsRepository.Table
                               where (cc.ConnectionToType == ConnectionToType.desc || cc.ConnectionToType == ConnectionToType.sep)
                                    && cc.To == element.Id
                               select cc).Distinct().ToList();

            _connectionsRepository.Delete(connections);

            foreach (var conn in connections)
            {
                //event notification
                _eventPublisher.EntityDeleted(conn);
            }

            //cache
            _cacheManager.RemoveByPattern(NopCatalogDefaults.ConnectionsPatternCacheKey);

            _megaMenuTabItemService.DeleteConnectionMegaMenuTabItem(RowConnectionType.element, new List<int>() { element.Id });
            _menuItemService.DeleteConnectionMenuItem(RowConnectionType.element, new List<int>() { element.Id });
        }

        #endregion
    }
}