using System.Collections.Generic;
using Nop.Core;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Topics;

namespace Nop.Services.Catalog
{
    /// <summary>
    /// Mega menu tab item service interface
    /// </summary>
    public partial interface IMegaMenuTabItemService
    {
        /// <summary>
        /// Gets mega menu tab items collection
        /// </summary>
        /// <param name="megaMenuTabId">Mega menu tab identifier</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <returns>Mega menu tab item collection</returns>
        IPagedList<MegaMenuTabItem> GetColumnsHeadersByMegaMenuTabId(int megaMenuTabId,
            int pageIndex = 0,
            int pageSize = int.MaxValue);

        /// <summary>
        /// Gets mega menu tab items collection
        /// </summary>
        /// <param name="megaMenuTabItem">Mega menu tab item</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <returns>Mega menu tab item collection</returns>
        IPagedList<MegaMenuTabItem> GetRowsByMegaMenuTabItem(MegaMenuTabItem megaMenuTabItem,
            int pageIndex = 0,
            int pageSize = int.MaxValue);

        /// <summary>
        /// Inserts mega menu tab item
        /// </summary>
        /// <param name="megaMenuTabItem">Mega menu tab item</param>
        void InsertMegaMenuTabItem(MegaMenuTabItem megaMenuTabItem);

        /// <summary>
        /// Gets a mega menu tab item
        /// </summary>
        /// <param name="megaMenuTabItemId">Mega menu tab item identifier</param>
        /// <returns>Mega menu tab item</returns>
        MegaMenuTabItem GetMegaMenuTabItemById(int megaMenuTabItemId);

        /// <summary>
        /// Delete a mega menu tab item
        /// </summary>
        /// <param name="megaMenuTabItem">Mega menu tab item</param>
        void DeleteMegaMenuTabItem(MegaMenuTabItem megaMenuTabItem);

        /// <summary>
        /// Delete a mega menu tab item
        /// </summary>
        /// <param name="connectionType">Row connection type</param>
        /// <param name="rowConnectionIds">Row connection identifiers</param>
        void DeleteConnectionMegaMenuTabItem(RowConnectionType connectionType, IEnumerable<int> rowConnectionIds);

        /// <summary>
        /// Gets mega menu tab items collection
        /// </summary>
        /// <param name="megaMenuTabId">Mega menu tab identifier</param>
        /// <returns>Columns</returns>
        IList<MegaMenuTabItem> GetColumnsByMegaMenuTabId(int megaMenuTabId);

        /// <summary>
        /// Inserts mega menu tab item rows
        /// </summary>
        /// <param name="selectedCategories">Selected categories</param>
        /// <param name="megaMenuTabItemId">Mega menu tab column identifier</param>
        /// <param name="megaMenuTabId">Menu tab column identifier</param>
        void InsertCategoryRows(List<Category> selectedCategories, int megaMenuTabItemId, int megaMenuTabId);

        /// <summary>
        /// Inserts mega menu tab item rows
        /// </summary>
        /// <param name="selectedProducts">Selected products</param>
        /// <param name="megaMenuTabItemId">Mega menu tab column identifier</param>
        /// <param name="megaMenuTabId">Menu tab column identifier</param>
        void InsertProductRows(List<Product> selectedProducts, int megaMenuTabItemId, int megaMenuTabId);

        /// <summary>
        /// Inserts mega menu tab item rows
        /// </summary>
        /// <param name="selectedElements">Selected elements</param>
        /// <param name="megaMenuTabItemId">Mega menu tab column identifier</param>
        /// <param name="megaMenuTabId">Menu tab column identifier</param>
        void InsertElementRows(List<Element> selectedElements, int megaMenuTabItemId, int megaMenuTabId);

        /// <summary>
        /// Inserts mega menu tab item rows
        /// </summary>
        /// <param name="selectedTopics">Selected topics</param>
        /// <param name="megaMenuTabItemId">Mega menu tab column identifier</param>
        /// <param name="megaMenuTabId">Menu tab column identifier</param>
        void InsertTopicsRows(List<Topic> selectedTopics, int megaMenuTabItemId, int megaMenuTabId);

        /// <summary>
        /// Updates the mega menu tab item
        /// </summary>
        /// <param name="megaMenuTabItem">Mega menu tab item</param>
        void UpdateMegaMenuTabItem(MegaMenuTabItem megaMenuTabItem);

        /// <summary>
        /// Reorder mega menu tab item
        /// </summary>
        /// <param name="newDisplayOrder">New display order</param>
        /// <param name="oldDisplayOrder">Old display order</param>
        /// <param name="dragItemId">Item that we are moving</param>
        /// <param name="isColumn">A value indicating whether drag item is column</param>
        void ReOrderMegaMenuTabItem(int newDisplayOrder, int oldDisplayOrder, int dragItemId, bool isColumn);

        /// <summary>
        /// Gets mega menu tab items collection
        /// </summary>
        /// <param name="megaMenuTabId">Mega menu tab identifier</param>
        /// <param name="visible">Show only visible</param>
        /// <returns>Mega menu tab items</returns>
        IList<MegaMenuTabItem> GetItemsByMegaMenuTabId(int megaMenuTabId, bool visible = true);

        /// <summary>
        /// Inserts mega menu tab item rows
        /// </summary>
        /// <param name="selectedSliders">Selected sliders</param>
        /// <param name="megaMenuTabItemId">Mega menu tab item column identifier</param>
        /// <param name="megaMenuTabId">Mega menu tab identifier</param>
        void InsertSliderRows(List<Slider> selectedSliders, int megaMenuTabItemId, int megaMenuTabId);
    }
}