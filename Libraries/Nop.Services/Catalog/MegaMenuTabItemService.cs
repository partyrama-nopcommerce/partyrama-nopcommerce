using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Data;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Topics;
using Nop.Services.Events;

namespace Nop.Services.Catalog
{
    /// <summary>
    /// Mega menu tab item service
    /// </summary>
    public partial class MegaMenuTabItemService : IMegaMenuTabItemService
    {
        #region Fields

        private readonly IRepository<MegaMenuTabItem> _megaMenuTabItemRepository;
        private readonly ICacheManager _cacheManager;
        private readonly IEventPublisher _eventPublisher;

        #endregion

        #region Ctor

        public MegaMenuTabItemService(IRepository<MegaMenuTabItem> megaMenuTabItemRepository,
            ICacheManager cacheManager,
            IEventPublisher eventPublisher)
        {
            _cacheManager = cacheManager;
            _eventPublisher = eventPublisher;
            _megaMenuTabItemRepository = megaMenuTabItemRepository;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Gets mega menu tab items collection
        /// </summary>
        /// <param name="megaMenuTabId">Mega menu tab identifier</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <returns>Mega menu tab item collection</returns>
        public IPagedList<MegaMenuTabItem> GetColumnsHeadersByMegaMenuTabId(int megaMenuTabId,
            int pageIndex = 0,
            int pageSize = int.MaxValue)
        {
            var query = from mti in _megaMenuTabItemRepository.Table
                        where ((mti.RowType == RowType.textcolumn
                                || mti.RowType == RowType.header
                                || mti.RowType == RowType.imagecolumn
                                || mti.RowType == RowType.title)
                            && mti.MegaMenuTabId == megaMenuTabId)
                        orderby mti.DisplayOrder ascending, mti.ColumnId ascending, mti.Id ascending
                        select mti;

            var items = new PagedList<MegaMenuTabItem>(query, pageIndex, pageSize);
            return items;
        }

        /// <summary>
        /// Gets mega menu tab items collection
        /// </summary>
        /// <param name="megaMenuTabItem">Mega menu tab item</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <returns>Mega menu tab item collection</returns>
        public IPagedList<MegaMenuTabItem> GetRowsByMegaMenuTabItem(MegaMenuTabItem megaMenuTabItem,
            int pageIndex = 0,
            int pageSize = int.MaxValue)
        {
            var query = from mti in _megaMenuTabItemRepository.Table
                        where (mti.RowType == RowType.data
                            && mti.MegaMenuTabId == megaMenuTabItem.MegaMenuTabId
                            && mti.ColumnId == megaMenuTabItem.ColumnId)
                        orderby mti.DisplayOrder ascending, mti.ColumnId ascending, mti.Id ascending
                        select mti;

            var items = new PagedList<MegaMenuTabItem>(query, pageIndex, pageSize);
            return items;
        }

        /// <summary>
        /// Gets mega menu tab items collection
        /// </summary>
        /// <param name="megaMenuTabId">Mega menu tab identifier</param>
        /// <returns>Columns</returns>
        public IList<MegaMenuTabItem> GetColumnsByMegaMenuTabId(int megaMenuTabId)
        {
            var query = from mti in _megaMenuTabItemRepository.Table
                        where ((mti.RowType == RowType.textcolumn
                            || mti.RowType == RowType.imagecolumn)
                            && mti.MegaMenuTabId == megaMenuTabId)
                        orderby mti.DisplayOrder ascending, mti.ColumnId ascending, mti.Id ascending
                        select mti;

            var columns = query.ToList();
            return columns;
        }

        /// <summary>
        /// Inserts mega menu tab item
        /// </summary>
        /// <param name="megaMenuTabItem">Mega menu tab item</param>
        public void InsertMegaMenuTabItem(MegaMenuTabItem megaMenuTabItem)
        {
            if (megaMenuTabItem == null)
                throw new ArgumentNullException(nameof(megaMenuTabItem));

            if (megaMenuTabItem.RowType == RowType.textcolumn
                || megaMenuTabItem.RowType == RowType.header
                || megaMenuTabItem.RowType == RowType.imagecolumn
                || megaMenuTabItem.RowType == RowType.title)
            {
                var maxColumnId = _megaMenuTabItemRepository.Table
                                        .Where(item => item.MegaMenuTabId == megaMenuTabItem.MegaMenuTabId
                                            && (item.RowType == RowType.textcolumn
                                                || item.RowType == RowType.header
                                                || item.RowType == RowType.imagecolumn
                                                || item.RowType == RowType.title))
                                        .OrderByDescending(item => item.ColumnId)
                                        .Select(cc => cc.ColumnId)
                                        .DefaultIfEmpty(0)
                                        .First();

                megaMenuTabItem.ColumnId = maxColumnId + 1;

                var maxDisplayOrder = _megaMenuTabItemRepository.Table
                                        .Where(item => item.MegaMenuTabId == megaMenuTabItem.MegaMenuTabId
                                            && (item.RowType == RowType.textcolumn
                                                || item.RowType == RowType.header
                                                || item.RowType == RowType.imagecolumn
                                                || item.RowType == RowType.title))
                                        .OrderByDescending(item => item.DisplayOrder)
                                        .Select(cc => cc.DisplayOrder)
                                        .DefaultIfEmpty(-1)
                                        .First();

                megaMenuTabItem.DisplayOrder = maxDisplayOrder + 1;
            }

            _megaMenuTabItemRepository.Insert(megaMenuTabItem);

            //cache
            _cacheManager.RemoveByPattern(NopCatalogDefaults.MegaMenuTabsPatternCacheKey);
            _cacheManager.RemoveByPattern(NopCatalogDefaults.MegaMenuTabItemsPatternCacheKey);

            //event notification
            _eventPublisher.EntityInserted(megaMenuTabItem);
        }

        /// <summary>
        /// Gets a mega menu tab item
        /// </summary>
        /// <param name="megaMenuTabItemId">Mega menu tab item identifier</param>
        /// <returns>Mega menu tab item</returns>
        public MegaMenuTabItem GetMegaMenuTabItemById(int megaMenuTabItemId)
        {
            if (megaMenuTabItemId == 0)
                return null;

            var key = string.Format(NopCatalogDefaults.MegaMenuTabItemByIdCacheKey, megaMenuTabItemId);
            return _cacheManager.Get(key, () => _megaMenuTabItemRepository.GetById(megaMenuTabItemId));
        }

        /// <summary>
        /// Delete a mega menu tab item
        /// </summary>
        /// <param name="connectionType">Row connection type</param>
        /// <param name="rowConnectionIds">Row connection identifiers</param>
        public void DeleteConnectionMegaMenuTabItem(RowConnectionType connectionType, IEnumerable<int> rowConnectionIds)
        {
            var items = (from item in _megaMenuTabItemRepository.Table
                                 where item.RowConnectionType == connectionType && rowConnectionIds.Contains(item.RowConnectionId.GetValueOrDefault())
                                 select item).Distinct().ToList();
            var rowsToDelete = new List<MegaMenuTabItem>();

            foreach (var item in items.Where(m => m.RowType == RowType.textcolumn || m.RowType == RowType.imagecolumn))
            {
                var rows = _megaMenuTabItemRepository.Table.Where(row => row.RowType == RowType.data && row.ColumnId == item.ColumnId).ToList();

                if (rows.Any())
                {
                    rowsToDelete.AddRange(rows);
                }
            }

            if (rowsToDelete.Any())
            {
                items.AddRange(rowsToDelete);
            }

            _megaMenuTabItemRepository.Delete(items);

            foreach (var item in items)
            {
                //event notification
                _eventPublisher.EntityDeleted(item);
            }

            //cache
            _cacheManager.RemoveByPattern(NopCatalogDefaults.MegaMenuTabItemsPatternCacheKey);
            _cacheManager.RemoveByPattern(NopCatalogDefaults.MegaMenuTabsPatternCacheKey);
        }

        /// <summary>
        /// Delete a mega menu tab item
        /// </summary>
        /// <param name="megaMenuTabItem">Mega menu tab item</param>
        public void DeleteMegaMenuTabItem(MegaMenuTabItem megaMenuTabItem)
        {
            if (megaMenuTabItem == null)
                throw new ArgumentNullException(nameof(megaMenuTabItem));

            var itemsToDelete = new List<MegaMenuTabItem>
            {
                megaMenuTabItem
            };

            //remove rows for column
            if (megaMenuTabItem.RowType == RowType.textcolumn
                || megaMenuTabItem.RowType == RowType.imagecolumn)
            {
                var rows = _megaMenuTabItemRepository.Table.Where(row => row.RowType == RowType.data && row.ColumnId == megaMenuTabItem.ColumnId).ToList();

                if (rows.Any())
                {
                    itemsToDelete.AddRange(rows);
                }
            }

            _megaMenuTabItemRepository.Delete(itemsToDelete);

            foreach (var item in itemsToDelete)
            {
                //event notification
                _eventPublisher.EntityDeleted(item);
            }

            //cache
            _cacheManager.RemoveByPattern(NopCatalogDefaults.MegaMenuTabItemsPatternCacheKey);
            _cacheManager.RemoveByPattern(NopCatalogDefaults.MegaMenuTabsPatternCacheKey);
        }

        /// <summary>
        /// Inserts mega menu tab item rows
        /// </summary>
        /// <param name="selectedCategories">Selected categories</param>
        /// <param name="megaMenuTabItemId">Mega menu tab column identifier</param>
        /// <param name="megaMenuTabId">Menu tab column identifier</param>
        public void InsertCategoryRows(List<Category> selectedCategories, int megaMenuTabItemId, int megaMenuTabId)
        {
            if (selectedCategories == null)
                throw new ArgumentNullException(nameof(selectedCategories));

            if (megaMenuTabItemId > 0)
            {
                InsertRows(selectedCategories.Select(c => c.Id), RowConnectionType.category, megaMenuTabItemId);
            }
            else
            {
                InsertTitles(selectedCategories.Select(c => c.Id), RowConnectionType.category, megaMenuTabId);
            }
        }

        /// <summary>
        /// Inserts mega menu tab item rows
        /// </summary>
        /// <param name="selectedProducts">Selected products</param>
        /// <param name="megaMenuTabItemId">Mega menu tab column identifier</param>
        /// <param name="megaMenuTabId">Menu tab column identifier</param>
        public void InsertProductRows(List<Product> selectedProducts, int megaMenuTabItemId, int megaMenuTabId)
        {
            if (selectedProducts == null)
                throw new ArgumentNullException(nameof(selectedProducts));

            if (megaMenuTabItemId > 0)
            {
                InsertRows(selectedProducts.Select(c => c.Id), RowConnectionType.product, megaMenuTabItemId);
            }
            else
            {
                InsertTitles(selectedProducts.Select(c => c.Id), RowConnectionType.product, megaMenuTabId);
            }
        }

        /// <summary>
        /// Inserts mega menu tab item rows
        /// </summary>
        /// <param name="selectedElements">Selected elements</param>
        /// <param name="megaMenuTabItemId">Mega menu tab column identifier</param>
        /// <param name="megaMenuTabId">Menu tab column identifier</param>
        public void InsertElementRows(List<Element> selectedElements, int megaMenuTabItemId, int megaMenuTabId)
        {
            if (selectedElements == null)
                throw new ArgumentNullException(nameof(selectedElements));

            if (megaMenuTabItemId > 0)
            {
                var column = _megaMenuTabItemRepository.GetById(megaMenuTabItemId);
                if (column == null)
                    throw new ArgumentNullException(nameof(column));

                var maxDisplayOrder = _megaMenuTabItemRepository.Table
                                            .Where(item => item.ColumnId == column.ColumnId
                                                    && item.RowType == RowType.data)
                                            .OrderByDescending(item => item.DisplayOrder)
                                            .Select(item => item.DisplayOrder)
                                            .DefaultIfEmpty(-1)
                                            .First();
                var rows = new List<MegaMenuTabItem>();
                var utcDate = DateTime.UtcNow;

                foreach (var element in selectedElements)
                {
                    maxDisplayOrder++;
                    rows.Add(new MegaMenuTabItem
                    {
                        ColumnId = column.ColumnId,
                        CreatedOnUtc = utcDate,
                        CssClass = GetElementRowClass(element),
                        DisplayOrder = maxDisplayOrder,
                        MegaMenuTabId = column.MegaMenuTabId,
                        RowConnectionId = element.Id,
                        RowConnectionType = RowConnectionType.element,
                        RowType = RowType.data,
                        UpdatedOnUtc = utcDate,
                        Visible = true
                    });
                }

                _megaMenuTabItemRepository.Insert(rows);

                //cache
                _cacheManager.RemoveByPattern(NopCatalogDefaults.MegaMenuTabsPatternCacheKey);
                _cacheManager.RemoveByPattern(NopCatalogDefaults.MegaMenuTabItemsPatternCacheKey);

                //event notification
                foreach (var row in rows)
                {
                    //event notification
                    _eventPublisher.EntityInserted(row);
                }
            }
            else
            {
                InsertTitles(selectedElements.Select(c => c.Id), RowConnectionType.element, megaMenuTabId);
            }
        }

        private string GetElementRowClass(Element element)
        {
            switch (element.ElementType)
            {
                case ElementType.Description:
                    return "mm_desc";
                case ElementType.Separator:
                    return "mm_sep";
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        /// <summary>
        /// Inserts mega menu tab item rows
        /// </summary>
        /// <param name="selectedTopics">Selected topics</param>
        /// <param name="megaMenuTabItemId">Mega menu tab column identifier</param>
        /// <param name="megaMenuTabId">Menu tab column identifier</param>
        public void InsertTopicsRows(List<Topic> selectedTopics, int megaMenuTabItemId, int megaMenuTabId)
        {
            if (selectedTopics == null)
                throw new ArgumentNullException(nameof(selectedTopics));

            if (megaMenuTabItemId > 0)
            {
                InsertRows(selectedTopics.Select(c => c.Id), RowConnectionType.topic, megaMenuTabItemId);
            }
            else
            {
                InsertTitles(selectedTopics.Select(c => c.Id), RowConnectionType.topic, megaMenuTabId);
            }
        }

        private void InsertTitles(IEnumerable<int> ids, RowConnectionType connectionType, int megaMenuTabId)
        {
            var maxColumnId = _megaMenuTabItemRepository.Table
                                    .Where(item => item.MegaMenuTabId == megaMenuTabId
                                        && (item.RowType == RowType.textcolumn
                                            || item.RowType == RowType.header
                                            || item.RowType == RowType.imagecolumn
                                            || item.RowType == RowType.title))
                                    .OrderByDescending(item => item.ColumnId)
                                    .Select(cc => cc.ColumnId)
                                    .DefaultIfEmpty(0)
                                    .First();

            var maxDisplayOrder = _megaMenuTabItemRepository.Table
                                    .Where(item => item.MegaMenuTabId == megaMenuTabId
                                        && (item.RowType == RowType.textcolumn
                                            || item.RowType == RowType.header
                                            || item.RowType == RowType.imagecolumn
                                            || item.RowType == RowType.title))
                                    .OrderByDescending(item => item.DisplayOrder)
                                    .Select(cc => cc.DisplayOrder)
                                    .DefaultIfEmpty(-1)
                                    .First();

            var titles = new List<MegaMenuTabItem>();
            var utcDate = DateTime.UtcNow;

            foreach (var id in ids)
            {
                maxDisplayOrder++;
                maxColumnId++;
                titles.Add(new MegaMenuTabItem
                {
                    ColumnId = maxColumnId,
                    CreatedOnUtc = utcDate,
                    CssClass = "mm_title",
                    DisplayOrder = maxDisplayOrder,
                    MegaMenuTabId = megaMenuTabId,
                    RowConnectionId = id,
                    RowConnectionType = connectionType,
                    RowType = RowType.title,
                    UpdatedOnUtc = utcDate,
                    Visible = true
                });
            }

            _megaMenuTabItemRepository.Insert(titles);

            //cache
            _cacheManager.RemoveByPattern(NopCatalogDefaults.MegaMenuTabsPatternCacheKey);
            _cacheManager.RemoveByPattern(NopCatalogDefaults.MegaMenuTabItemsPatternCacheKey);

            //event notification
            foreach (var row in titles)
            {
                //event notification
                _eventPublisher.EntityInserted(row);
            }
        }

        private void InsertRows(IEnumerable<int> ids, RowConnectionType connectionType, int megaMenuTabItemId)
        {
            var column = _megaMenuTabItemRepository.GetById(megaMenuTabItemId);
            if (column == null)
                throw new ArgumentNullException(nameof(column));

            var maxDisplayOrder = _megaMenuTabItemRepository.Table
                                        .Where(item => item.ColumnId == column.ColumnId
                                                && item.RowType == RowType.data)
                                        .OrderByDescending(item => item.DisplayOrder)
                                        .Select(item => item.DisplayOrder)
                                        .DefaultIfEmpty(-1)
                                        .First();
            var rows = new List<MegaMenuTabItem>();
            var utcDate = DateTime.UtcNow;

            foreach (var id in ids)
            {
                maxDisplayOrder++;
                rows.Add(new MegaMenuTabItem
                {
                    ColumnId = column.ColumnId,
                    CreatedOnUtc = utcDate,
                    CssClass = column.RowType == RowType.imagecolumn ? "mm_image": "mm_link",
                    DisplayOrder = maxDisplayOrder,
                    MegaMenuTabId = column.MegaMenuTabId,
                    RowConnectionId = id,
                    RowConnectionType = connectionType,
                    RowType = RowType.data,
                    UpdatedOnUtc = utcDate,
                    Visible = true
                });
            }

            _megaMenuTabItemRepository.Insert(rows);

            //cache
            _cacheManager.RemoveByPattern(NopCatalogDefaults.MegaMenuTabsPatternCacheKey);
            _cacheManager.RemoveByPattern(NopCatalogDefaults.MegaMenuTabItemsPatternCacheKey);

            //event notification
            foreach (var row in rows)
            {
                //event notification
                _eventPublisher.EntityInserted(row);
            }
        }

        /// <summary>
        /// Updates the mega menu tab item
        /// </summary>
        /// <param name="megaMenuTabItem">Mega menu tab item</param>
        public void UpdateMegaMenuTabItem(MegaMenuTabItem megaMenuTabItem)
        {
            if (megaMenuTabItem == null)
                throw new ArgumentNullException(nameof(megaMenuTabItem));

            _megaMenuTabItemRepository.Update(megaMenuTabItem);

            //cache
            _cacheManager.RemoveByPattern(NopCatalogDefaults.MegaMenuTabsPatternCacheKey);
            _cacheManager.RemoveByPattern(NopCatalogDefaults.MegaMenuTabItemsPatternCacheKey);

            //event notification
            _eventPublisher.EntityUpdated(megaMenuTabItem);
        }

        /// <summary>
        /// Reorder mega menu tab item
        /// </summary>
        /// <param name="newDisplayOrder">New display order</param>
        /// <param name="oldDisplayOrder">Old display order</param>
        /// <param name="dragItemId">Item that we are moving</param>
        /// <param name="isColumn">A value indicating whether drag item is column</param>
        public void ReOrderMegaMenuTabItem(int newDisplayOrder, int oldDisplayOrder, int dragItemId, bool isColumn)
        {
            if (newDisplayOrder == oldDisplayOrder)
                return;

            var item = _megaMenuTabItemRepository.GetById(dragItemId);
            if (item == null)
                throw new ArgumentNullException(nameof(item));

            var query = _megaMenuTabItemRepository.Table.Where(i => i.MegaMenuTabId == item.MegaMenuTabId);
            if (isColumn)
            {
                query = query.Where(i => i.RowType != RowType.data);
            }
            else
            {
                query = query.Where(i => i.RowType == RowType.data && i.ColumnId == item.ColumnId);
            }

            var items = query.OrderBy(i => i.DisplayOrder).ThenBy(i => i.ColumnId).ThenBy(i => i.Id).ToList();
            if (items[oldDisplayOrder].Id != item.Id)
                return;

            var displayOrder = -1;

            if (newDisplayOrder > oldDisplayOrder)
            {
                for (var i = 0; i <= newDisplayOrder; i++)
                {
                    var currentItem = items[i];
                    if (currentItem.Id == item.Id)
                    {
                        currentItem.DisplayOrder = newDisplayOrder;
                    }
                    else
                    {
                        displayOrder++;
                        currentItem.DisplayOrder = displayOrder;
                    }
                }

                for (var i = newDisplayOrder + 1; i < items.Count(); i++)
                {
                    items[i].DisplayOrder = i;
                }
            }
            else
            {
                for (var i = 0; i < newDisplayOrder; i++)
                {
                    items[i].DisplayOrder = i;
                }

                displayOrder = newDisplayOrder;
                for (var i = newDisplayOrder; i < items.Count(); i++)
                {
                    var currentItem = items[i];
                    if (currentItem.Id == item.Id)
                    {
                        currentItem.DisplayOrder = newDisplayOrder;
                    }
                    else
                    {
                        displayOrder++;
                        currentItem.DisplayOrder = displayOrder;
                    }
                }
            }

            _megaMenuTabItemRepository.Update(items);

            //cache
            _cacheManager.RemoveByPattern(NopCatalogDefaults.MegaMenuTabsPatternCacheKey);
            _cacheManager.RemoveByPattern(NopCatalogDefaults.MegaMenuTabItemsPatternCacheKey);

            //event notification
            foreach (var itemUpdated in items)
            {
                //event notification
                _eventPublisher.EntityUpdated(itemUpdated);
            }
        }

        /// <summary>
        /// Gets mega menu tab items collection
        /// </summary>
        /// <param name="megaMenuTabId">Mega menu tab identifier</param>
        /// <param name="visible">Show only visible</param>
        /// <returns>Mega menu tab items</returns>
        public IList<MegaMenuTabItem> GetItemsByMegaMenuTabId(int megaMenuTabId, bool visible = true)
        {
            var query = _megaMenuTabItemRepository.Table.Where(item => item.MegaMenuTabId == megaMenuTabId);
            if (visible)
            {
                query = query.Where(item => item.Visible
                    && (!item.StartDateUtc.HasValue || DateTime.UtcNow >= item.StartDateUtc.Value)
                    && (!item.EndDateUtc.HasValue || DateTime.UtcNow < item.EndDateUtc.Value));
            }

            return query.OrderBy(item => item.ColumnId).ThenBy(item => item.DisplayOrder).ToList();
        }

        /// <summary>
        /// Inserts mega menu tab item rows
        /// </summary>
        /// <param name="selectedSliders">Selected sliders</param>
        /// <param name="megaMenuTabItemId">Mega menu tab item column identifier</param>
        /// <param name="megaMenuTabId">Mega menu tab identifier</param>
        public void InsertSliderRows(List<Slider> selectedSliders, int megaMenuTabItemId, int megaMenuTabId)
        {
            if (selectedSliders == null)
                throw new ArgumentNullException(nameof(selectedSliders));

            if (megaMenuTabItemId > 0)
            {
                InsertRows(selectedSliders.Select(c => c.Id), RowConnectionType.slider, megaMenuTabItemId);
            }
            else
            {
                InsertTitles(selectedSliders.Select(c => c.Id), RowConnectionType.slider, megaMenuTabId);
            }
        }

        #endregion
    }
}