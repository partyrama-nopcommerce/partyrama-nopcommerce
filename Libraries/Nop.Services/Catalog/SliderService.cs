using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Data;
using Nop.Core.Domain.Catalog;
using Nop.Services.Events;

namespace Nop.Services.Catalog
{
    /// <summary>
    /// Slider service
    /// </summary>
    public partial class SliderService : ISliderService
    {
        #region Fields

        private readonly IRepository<Slider> _sliderRepository;
        private readonly ICacheManager _cacheManager;
        private readonly IEventPublisher _eventPublisher;
        private readonly IRepository<SliderItem> _sliderItemRepository;

        #endregion

        #region Ctor

        public SliderService(IRepository<Slider> sliderRepository,
            ICacheManager cacheManager,
            IEventPublisher eventPublisher,
            IRepository<SliderItem> sliderItemRepository)
        {
            _sliderRepository = sliderRepository;
            _cacheManager = cacheManager;
            _eventPublisher = eventPublisher;
            _sliderItemRepository = sliderItemRepository;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Delete slider
        /// </summary>
        /// <param name="slider">Slider</param>
        public void DeleteSlider(Slider slider)
        {
            if (slider == null)
                throw new ArgumentNullException(nameof(slider));

            _sliderRepository.Delete(slider);

            //cache
            _cacheManager.RemoveByPattern(NopCatalogDefaults.SlidersPatternCacheKey);

            //event notification
            _eventPublisher.EntityDeleted(slider);
        }

        /// <summary>
        /// Inserts slider
        /// </summary>
        /// <param name="slider">Slider</param>
        public void InsertSlider(Slider slider)
        {
            if (slider == null)
                throw new ArgumentNullException(nameof(slider));

            _sliderRepository.Insert(slider);

            //cache
            _cacheManager.RemoveByPattern(NopCatalogDefaults.SlidersPatternCacheKey);

            //event notification
            _eventPublisher.EntityInserted(slider);
        }

        /// <summary>
        /// Updates the slider
        /// </summary>
        /// <param name="slider">Slider</param>
        public void UpdateSlider(Slider slider)
        {
            if (slider == null)
                throw new ArgumentNullException(nameof(slider));

            _sliderRepository.Update(slider);

            //cache
            _cacheManager.RemoveByPattern(NopCatalogDefaults.SlidersPatternCacheKey);

            //event notification
            _eventPublisher.EntityUpdated(slider);
        }

        /// <summary>
        /// Gets a slider
        /// </summary>
        /// <param name="sliderId">Slider identifier</param>
        /// <returns>Slider</returns>
        public Slider GetSliderById(int sliderId)
        {
            if (sliderId == 0)
                return null;

            var key = string.Format(NopCatalogDefaults.SliderByIdCacheKey, sliderId);
            return _cacheManager.Get(key, () => _sliderRepository.GetById(sliderId));
        }

        /// <summary>
        /// Gets all sliders
        /// </summary>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>Sliders</returns>
        public IList<Slider> GetAll(bool showHidden = true)
        {
            var query = _sliderRepository.Table;

            if (!showHidden)
            {
                query = query.Where(mmt => mmt.Visible);
            }

            return query.OrderBy(mmt => mmt.Id).ToList();
        }

        /// <summary>
        /// Gets sliders by identifier
        /// </summary>
        /// <param name="sliderIds">Slider identifiers</param>
        /// <returns>Sliders</returns>
        public List<Slider> GetSlidersByIds(int[] sliderIds)
        {
            if (sliderIds == null || sliderIds.Length == 0)
                return new List<Slider>();

            var query = from sl in _sliderRepository.Table
                        where sliderIds.Contains(sl.Id)
                        select sl;

            return query.ToList();
        }

        /// <summary>
        /// Gets all sliders
        /// </summary>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <param name="sliderName">Slider name</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <returns>Sliders</returns>
        public IPagedList<Slider> GetAllSliders(bool showHidden = true, string sliderName = null, int pageIndex = 0, int pageSize = int.MaxValue)
        {
            var query = _sliderRepository.Table;
            if (!showHidden)
            {
                query = query.Where(sl => sl.Visible);
            }

            if (!string.IsNullOrWhiteSpace(sliderName))
            {
                query = query.Where(sl => sl.Name.Contains(sliderName));
            }

            query = query.OrderBy(sl => sl.Id);

            var sliders = new PagedList<Slider>(query, pageIndex, pageSize);
            return sliders;
        }

        #endregion
    }
}