using System.Collections.Generic;
using Nop.Core.Domain.Catalog;

namespace Nop.Services.Catalog
{
    /// <summary>
    /// Home page service interface
    /// </summary>
    public partial interface IHomePageService
    {
        /// <summary>
        /// Delete home page
        /// </summary>
        /// <param name="homePage">Home page</param>
        void DeleteHomePage(HomePage homePage);

        /// <summary>
        /// Inserts home page
        /// </summary>
        /// <param name="homePage">Home page</param>
        void InsertHomePage(HomePage homePage);

        /// <summary>
        /// Updates the home page
        /// </summary>
        /// <param name="homePage">Home page</param>
        void UpdateHomePage(HomePage homePage);

        /// <summary>
        /// Gets a home page
        /// </summary>
        /// <param name="homePageId">Home page identifier</param>
        /// <returns>Home page</returns>
        HomePage GetHomePageById(int homePageId);

        /// <summary>
        /// Gets all home pages
        /// </summary>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>Home pages</returns>
        IList<HomePage> GetAll(bool showHidden = true);
    }
}