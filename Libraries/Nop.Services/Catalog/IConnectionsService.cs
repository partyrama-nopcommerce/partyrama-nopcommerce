using System.Collections.Generic;
using Nop.Core;
using Nop.Core.Domain.Catalog;

namespace Nop.Services.Catalog
{
    /// <summary>
    /// Product service
    /// </summary>
    public partial interface IConnectionsService
    {
        /// <summary>
        /// Delete a connection
        /// </summary>
        /// <param name="connections">Connections</param>
        void DeleteConnection(Connections connection);
        
        /// <summary>
        /// Delete connections
        /// </summary>
        /// <param name="connections">Connections</param>
        void DeleteConnections(IList<Connections> connections);

        /// <summary>
        /// Inserts a connection
        /// </summary>
        /// <param name="connections">Connections</param>
        void InsertConnection(Connections connection);

        /// <summary>
        /// Updates the connection
        /// </summary>
        /// <param name="connections">Connections</param>
        void UpdateConnection(Connections connection);

        /// <summary>
        /// Updates the connections
        /// </summary>
        /// <param name="connections">Connections</param>
        void UpdateConnections(IList<Connections> connections);

        /// <summary>
        /// Gets connections collection
        /// </summary>
        /// <param name="categoryId">Category identifier</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <returns>Connections collection</returns>
        IPagedList<Connections> GetConnectionsByCategoryId(int categoryId,
            int pageIndex = 0,
            int pageSize = int.MaxValue);

        /// <summary>
        /// Gets connections collection
        /// </summary>
        /// <param name="productId">Product identifier</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <returns>Connections collection</returns>
        IPagedList<Connections> GetConnectionsByProductId(int productId,
            int pageIndex = 0,
            int pageSize = int.MaxValue);

        /// <summary>
        /// Gets a connection
        /// </summary>
        /// <param name="connectionId">Connection identifier</param>
        /// <returns>Connection</returns>
        Connections GetConnectionById(int connectionId);

        /// <summary>
        /// Reorder connections
        /// </summary>
        /// <param name="numberPositions">Number position to move</param>
        /// <param name="fromId">Product or category identifier</param>
        /// <param name="dragItemId">Item that we are moving</param>
        /// <param name="dropItemId">Item to which we are dropping</param>
        void ReOrderConnections(int numberPositions, int fromId, int dropItemId, int dragItemId);

        /// <summary>
        /// Gets product identifiers connect with object.
        /// </summary>
        /// <param name="fromId">Object identifier (product or category)</param>
        /// <param name="fromType">Connection type</param>
        /// <param name="limit">Number of items to read</param>
        /// <param name="toIdExcluded">Object identifier to excluded</param>
        /// <returns></returns>
        int[] GetProductConnectionsByObjectId(int fromId, ConnectionFromType fromType, int? limit = null, int? toIdExcluded = null);

        /// <summary>
        /// Gets category identifiers connect with object.
        /// </summary>
        /// <param name="fromId">Object identifier (product or category)</param>
        /// <param name="fromType">Connection type</param>
        /// <param name="limit">Number of items to read</param>
        /// <param name="toIdExcluded">Object identifier to excluded</param>
        /// <returns></returns>
        int[] GetCategoryConnectionsByObjectId(int fromId, ConnectionFromType fromType, int? limit = null, int? toIdExcluded = null);

        /// <summary>
        /// Gets element identifiers connect with object.
        /// </summary>
        /// <param name="fromId">Object identifier (product or category)</param>
        /// <param name="fromType">Connection from type</param>
        /// <param name="toType">Connection to Type</param>
        /// <param name="limit">Number of items to read</param>
        /// <param name="toIdExcluded">Object identifier to excluded</param>
        /// <returns></returns>
        int[] GetElementConnectionsByObjectId(int fromId, ConnectionFromType fromType, ConnectionToType toType, int? limit = null, int? toIdExcluded = null);

        /// <summary>
        /// Gets dictionary, key - product identifiers connect with object, value - display index.
        /// </summary>
        /// <param name="fromId">Object identifier (product or category)</param>
        /// <param name="fromType">Connection type</param>
        /// <param name="limit">Number of items to read</param>
        /// <param name="toIdExcluded">Object identifier to excluded</param>
        /// <returns></returns>
        Dictionary<int, int> GetProductIdsWithIndexByObjectId(int fromId, ConnectionFromType fromType, int? limit = null, int? toIdExcluded = null);

        /// <summary>
        /// Inserts connections
        /// </summary>
        /// <param name="connectionsToInsert">Connections</param>
        void InsertConnections(List<Connections> connectionsToInsert);

        /// <summary>
        /// Gets connections count from object
        /// </summary>
        /// <param name="fromIds">Categories or products identifiers count</param>
        /// <param name="fromType">Connection from type</param>
        /// <returns></returns>
        Dictionary<int, int> GetConnectionsCount(int[] fromIds, ConnectionFromType fromType);

        /// <summary>
        /// Gets slider identifiers connect with object.
        /// </summary>
        /// <param name="fromId">Object identifier (product or category)</param>
        /// <param name="fromType">Connection type</param>
        /// <param name="limit">Number of items to read</param>
        /// <param name="toIdExcluded">Object identifier to excluded</param>
        /// <returns></returns>
        int[] GetSliderConnectionsByObjectId(int fromId, ConnectionFromType fromType, int? limit = null, int? toIdExcluded = null);
    }
}