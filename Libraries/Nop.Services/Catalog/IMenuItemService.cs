using System.Collections.Generic;
using Nop.Core;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Topics;

namespace Nop.Services.Catalog
{
    /// <summary>
    /// Menu item service interface
    /// </summary>
    public partial interface IMenuItemService
    {
        /// <summary>
        /// Gets menu items collection
        /// </summary>
        /// <param name="menuId">Menu identifier</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <returns>Menu item collection</returns>
        IPagedList<MenuItem> GetColumnsHeadersByMenuId(int menuId,
            int pageIndex = 0,
            int pageSize = int.MaxValue);

        /// <summary>
        /// Gets menu items collection
        /// </summary>
        /// <param name="menuItem">Menu item</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <returns>Menu item collection</returns>
        IPagedList<MenuItem> GetRowsByMenuItem(MenuItem menuItem,
            int pageIndex = 0,
            int pageSize = int.MaxValue);

        /// <summary>
        /// Inserts menu item
        /// </summary>
        /// <param name="menuItem">Menu item</param>
        void InsertMenuItem(MenuItem menuItem);

        /// <summary>
        /// Gets a menu item
        /// </summary>
        /// <param name="menuItemId">Menu item identifier</param>
        /// <returns>Menu item</returns>
        MenuItem GetMenuItemById(int menuItemId);

        /// <summary>
        /// Delete a menu item
        /// </summary>
        /// <param name="menuItem">Menu item</param>
        void DeleteMenuItem(MenuItem menuItem);

        /// <summary>
        /// Delete a menu item
        /// </summary>
        /// <param name="connectionType">Row connection type</param>
        /// <param name="rowConnectionIds">Row connection identifiers</param>
        void DeleteConnectionMenuItem(RowConnectionType connectionType, IEnumerable<int> rowConnectionIds);

        /// <summary>
        /// Gets menu items collection
        /// </summary>
        /// <param name="menuId">Menu identifier</param>
        /// <returns>Columns</returns>
        IList<MenuItem> GetColumnsByMenuId(int menuId);

        /// <summary>
        /// Inserts menu item rows
        /// </summary>
        /// <param name="selectedCategories">Selected categories</param>
        /// <param name="menuItemId">Menu item column identifier</param>
        /// <param name="menuId">Menu identifier</param>
        void InsertCategoryRows(List<Category> selectedCategories, int menuItemId, int menuId);

        /// <summary>
        /// Inserts menu item rows
        /// </summary>
        /// <param name="selectedProducts">Selected products</param>
        /// <param name="menuItemId">Menu item column identifier</param>
        /// <param name="menuId">Menu identifier</param>
        void InsertProductRows(List<Product> selectedProducts, int menuItemId, int menuId);

        /// <summary>
        /// Inserts menu item rows
        /// </summary>
        /// <param name="selectedElements">Selected elements</param>
        /// <param name="menuItemId">Menu item column identifier</param>
        /// <param name="menuId">Menu identifier</param>
        void InsertElementRows(List<Element> selectedElements, int menuItemId, int menuId);

        /// <summary>
        /// Inserts menu item rows
        /// </summary>
        /// <param name="selectedTopics">Selected topics</param>
        /// <param name="menuItemId">Menu item column identifier</param>
        /// <param name="menuId">Menu identifier</param>
        void InsertTopicsRows(List<Topic> selectedTopics, int menuItemId, int menuId);

        /// <summary>
        /// Updates the menu item
        /// </summary>
        /// <param name="menuItem">Menu item</param>
        void UpdateMenuItem(MenuItem menuItem);

        /// <summary>
        /// Reorder menu item
        /// </summary>
        /// <param name="newDisplayOrder">New display order</param>
        /// <param name="oldDisplayOrder">Old display order</param>
        /// <param name="dragItemId">Item that we are moving</param>
        /// <param name="isColumn">A value indicating whether drag item is column</param>
        void ReOrderMenuItem(int newDisplayOrder, int oldDisplayOrder, int dragItemId, bool isColumn);

        /// <summary>
        /// Gets menu items collection
        /// </summary>
        /// <param name="menuId">Menu identifier</param>
        /// <param name="visible">Show only visible</param>
        /// <returns>Menu items</returns>
        IList<MenuItem> GetItemsByMenuId(int menuId, bool visible = true);
    }
}