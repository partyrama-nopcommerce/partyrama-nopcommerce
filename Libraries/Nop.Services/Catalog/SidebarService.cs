using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core.Caching;
using Nop.Core.Data;
using Nop.Core.Domain.Catalog;
using Nop.Services.Events;

namespace Nop.Services.Catalog
{
    /// <summary>
    /// Sidebar service
    /// </summary>
    public partial class SidebarService : ISidebarService
    {
        #region Fields

        private readonly IRepository<Sidebar> _sidebarRepository;
        private readonly ICacheManager _cacheManager;
        private readonly IEventPublisher _eventPublisher;
        private readonly IRepository<SidebarItem> _sidebarItemRepository;

        #endregion

        #region Ctor

        public SidebarService(IRepository<Sidebar> sidebarRepository,
            ICacheManager cacheManager,
            IEventPublisher eventPublisher,
            IRepository<SidebarItem> sidebarItemRepository)
        {
            _sidebarRepository = sidebarRepository;
            _cacheManager = cacheManager;
            _eventPublisher = eventPublisher;
            _sidebarItemRepository = sidebarItemRepository;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Delete sidebar
        /// </summary>
        /// <param name="sidebar">Sidebar</param>
        public void DeleteSidebar(Sidebar sidebar)
        {
            if (sidebar == null)
                throw new ArgumentNullException(nameof(sidebar));

            _sidebarRepository.Delete(sidebar);

            //cache
            _cacheManager.RemoveByPattern(NopCatalogDefaults.SidebarsPatternCacheKey);

            //event notification
            _eventPublisher.EntityDeleted(sidebar);
        }

        /// <summary>
        /// Inserts sidebar
        /// </summary>
        /// <param name="sidebar">Sidebar</param>
        public void InsertSidebar(Sidebar sidebar)
        {
            if (sidebar == null)
                throw new ArgumentNullException(nameof(sidebar));

            _sidebarRepository.Insert(sidebar);

            //cache
            _cacheManager.RemoveByPattern(NopCatalogDefaults.SidebarsPatternCacheKey);

            //event notification
            _eventPublisher.EntityInserted(sidebar);
        }

        /// <summary>
        /// Updates the sidebar
        /// </summary>
        /// <param name="sidebar">Sidebar</param>
        public void UpdateSidebar(Sidebar sidebar)
        {
            if (sidebar == null)
                throw new ArgumentNullException(nameof(sidebar));

            _sidebarRepository.Update(sidebar);

            //cache
            _cacheManager.RemoveByPattern(NopCatalogDefaults.SidebarsPatternCacheKey);

            //event notification
            _eventPublisher.EntityUpdated(sidebar);
        }

        /// <summary>
        /// Gets a sidebar
        /// </summary>
        /// <param name="sidebarId">Sidebar identifier</param>
        /// <returns>Sidebar</returns>
        public Sidebar GetSidebarById(int sidebarId)
        {
            if (sidebarId == 0)
                return null;

            var key = string.Format(NopCatalogDefaults.SidebarByIdCacheKey, sidebarId);
            return _cacheManager.Get(key, () => _sidebarRepository.GetById(sidebarId));
        }

        /// <summary>
        /// Gets all sidebars
        /// </summary>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>Sidebars</returns>
        public IList<Sidebar> GetAll(bool showHidden = true)
        {
            var query = _sidebarRepository.Table;

            if (!showHidden)
            {
                query = query.Where(mmt => mmt.Visible);
            }

            return query.OrderBy(mmt => mmt.Id).ToList();
        }

        #endregion
    }
}