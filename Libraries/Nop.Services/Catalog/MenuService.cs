using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core.Caching;
using Nop.Core.Data;
using Nop.Core.Domain.Catalog;
using Nop.Services.Events;

namespace Nop.Services.Catalog
{
    /// <summary>
    /// Menu service
    /// </summary>
    public partial class MenuService : IMenuService
    {
        #region Fields

        private readonly IRepository<Menu> _menuRepository;
        private readonly ICacheManager _cacheManager;
        private readonly IEventPublisher _eventPublisher;
        private readonly IRepository<MenuItem> _menuItemRepository;

        #endregion

        #region Ctor

        public MenuService(IRepository<Menu> menuRepository,
            ICacheManager cacheManager,
            IEventPublisher eventPublisher,
            IRepository<MenuItem> menuItemRepository)
        {
            _menuRepository = menuRepository;
            _cacheManager = cacheManager;
            _eventPublisher = eventPublisher;
            _menuItemRepository = menuItemRepository;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Delete menu
        /// </summary>
        /// <param name="menu">Menu</param>
        public void DeleteMenu(Menu menu)
        {
            if (menu == null)
                throw new ArgumentNullException(nameof(menu));

            _menuRepository.Delete(menu);

            //cache
            _cacheManager.RemoveByPattern(NopCatalogDefaults.MenusPatternCacheKey);

            //event notification
            _eventPublisher.EntityDeleted(menu);
        }

        /// <summary>
        /// Inserts menu
        /// </summary>
        /// <param name="menu">Menu</param>
        public void InsertMenu(Menu menu)
        {
            if (menu == null)
                throw new ArgumentNullException(nameof(menu));

            _menuRepository.Insert(menu);

            //cache
            _cacheManager.RemoveByPattern(NopCatalogDefaults.MenusPatternCacheKey);

            //event notification
            _eventPublisher.EntityInserted(menu);
        }

        /// <summary>
        /// Updates the menu
        /// </summary>
        /// <param name="menu">Menu</param>
        public void UpdateMenu(Menu menu)
        {
            if (menu == null)
                throw new ArgumentNullException(nameof(menu));

            _menuRepository.Update(menu);

            //cache
            _cacheManager.RemoveByPattern(NopCatalogDefaults.MenusPatternCacheKey);

            //event notification
            _eventPublisher.EntityUpdated(menu);
        }

        /// <summary>
        /// Gets a menu
        /// </summary>
        /// <param name="menuId">Menu identifier</param>
        /// <returns>Menu</returns>
        public Menu GetMenuById(int menuId)
        {
            if (menuId == 0)
                return null;

            var key = string.Format(NopCatalogDefaults.MenuByIdCacheKey, menuId);
            return _cacheManager.Get(key, () => _menuRepository.GetById(menuId));
        }

        /// <summary>
        /// Gets all menus
        /// </summary>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>Menus</returns>
        public IList<Menu> GetAll(bool showHidden = true)
        {
            var query = _menuRepository.Table;

            if (!showHidden)
            {
                query = query.Where(item => item.Visible
                    && (!item.StartDateUtc.HasValue || DateTime.UtcNow >= item.StartDateUtc.Value)
                    && (!item.EndDateUtc.HasValue || DateTime.UtcNow < item.EndDateUtc.Value));
            }

            return query.OrderBy(mmt => mmt.Id).ToList();
        }

        #endregion
    }
}