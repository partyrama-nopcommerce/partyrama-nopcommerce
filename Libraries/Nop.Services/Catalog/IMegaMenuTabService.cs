using System.Collections.Generic;
using Nop.Core;
using Nop.Core.Domain.Catalog;

namespace Nop.Services.Catalog
{
    /// <summary>
    /// Mega menu tab service interface
    /// </summary>
    public partial interface IMegaMenuTabService
    {
        /// <summary>
        /// Delete mega menu tab
        /// </summary>
        /// <param name="megaMenuTab">Mega menu tab</param>
        void DeleteMegaMenuTab(MegaMenuTab megaMenuTab);

        /// <summary>
        /// Inserts mega menu tab
        /// </summary>
        /// <param name="megaMenuTab">Mega menu tab</param>
        void InsertMegaMenuTab(MegaMenuTab megaMenuTab);

        /// <summary>
        /// Updates the mega menu tab
        /// </summary>
        /// <param name="megaMenuTab">Mega menu tab</param>
        void UpdateMegaMenuTab(MegaMenuTab megaMenuTab);

        /// <summary>
        /// Gets a mega menu tab
        /// </summary>
        /// <param name="megaMenuTabId">Mega menu tab identifier</param>
        /// <returns>Mega menu tab</returns>
        MegaMenuTab GetMegaMenuTabById(int megaMenuTabId);

        /// <summary>
        /// Gets all mega menu tabs
        /// </summary>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>Mega menu tabs</returns>
        IList<MegaMenuTab> GetAll(bool showHidden = true);
    }
}