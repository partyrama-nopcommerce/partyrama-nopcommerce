using System.Collections.Generic;
using Nop.Core;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Topics;

namespace Nop.Services.Catalog
{
    /// <summary>
    /// Slider item service interface
    /// </summary>
    public partial interface ISliderItemService
    {
        /// <summary>
        /// Gets slider items collection
        /// </summary>
        /// <param name="sliderId">Slider identifier</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <returns>Slider item collection</returns>
        IPagedList<SliderItem> GetColumnsHeadersBySliderId(int sliderId,
            int pageIndex = 0,
            int pageSize = int.MaxValue);

        /// <summary>
        /// Gets slider items collection
        /// </summary>
        /// <param name="sliderItem">Slider item</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <returns>Slider item collection</returns>
        IPagedList<SliderItem> GetRowsBySliderItem(SliderItem sliderItem,
            int pageIndex = 0,
            int pageSize = int.MaxValue);

        /// <summary>
        /// Inserts slider item
        /// </summary>
        /// <param name="sliderItem">Slider item</param>
        void InsertSliderItem(SliderItem sliderItem);

        /// <summary>
        /// Gets a slider item
        /// </summary>
        /// <param name="sliderItemId">Slider item identifier</param>
        /// <returns>Slider item</returns>
        SliderItem GetSliderItemById(int sliderItemId);

        /// <summary>
        /// Delete a slider item
        /// </summary>
        /// <param name="sliderItem">Slider item</param>
        void DeleteSliderItem(SliderItem sliderItem);

        /// <summary>
        /// Delete a slider item
        /// </summary>
        /// <param name="connectionType">Row connection type</param>
        /// <param name="rowConnectionIds">Row connection identifiers</param>
        void DeleteConnectionSliderItem(RowConnectionType connectionType, IEnumerable<int> rowConnectionIds);

        /// <summary>
        /// Gets slider items collection
        /// </summary>
        /// <param name="sliderId">Slider identifier</param>
        /// <returns>Columns</returns>
        IList<SliderItem> GetColumnsBySliderId(int sliderId);

        /// <summary>
        /// Inserts slider item rows
        /// </summary>
        /// <param name="selectedCategories">Selected categories</param>
        /// <param name="sliderItemId">Slider item column identifier</param>
        /// <param name="sliderId">Slider identifier</param>
        void InsertCategoryRows(List<Category> selectedCategories, int sliderItemId, int sliderId);

        /// <summary>
        /// Inserts slider item rows
        /// </summary>
        /// <param name="selectedProducts">Selected products</param>
        /// <param name="sliderItemId">Slider item column identifier</param>
        /// <param name="sliderId">Slider identifier</param>
        void InsertProductRows(List<Product> selectedProducts, int sliderItemId, int sliderId);

        /// <summary>
        /// Inserts slider item rows
        /// </summary>
        /// <param name="selectedElements">Selected elements</param>
        /// <param name="sliderItemId">Slider item column identifier</param>
        /// <param name="sliderId">Slider identifier</param>
        void InsertElementRows(List<Element> selectedElements, int sliderItemId, int sliderId);

        /// <summary>
        /// Inserts slider item rows
        /// </summary>
        /// <param name="selectedTopics">Selected topics</param>
        /// <param name="sliderItemId">Slider item column identifier</param>
        /// <param name="sliderId">Slider identifier</param>
        void InsertTopicsRows(List<Topic> selectedTopics, int sliderItemId, int sliderId);

        /// <summary>
        /// Updates the slider item
        /// </summary>
        /// <param name="sliderItem">Slider item</param>
        void UpdateSliderItem(SliderItem sliderItem);

        /// <summary>
        /// Reorder slider item
        /// </summary>
        /// <param name="newDisplayOrder">New display order</param>
        /// <param name="oldDisplayOrder">Old display order</param>
        /// <param name="dragItemId">Item that we are moving</param>
        /// <param name="isColumn">A value indicating whether drag item is column</param>
        void ReOrderSliderItem(int newDisplayOrder, int oldDisplayOrder, int dragItemId, bool isColumn);

        /// <summary>
        /// Gets slider items collection
        /// </summary>
        /// <param name="sliderId">Slider identifier</param>
        /// <param name="visible">Show only visible</param>
        /// <returns>Slider items</returns>
        IList<SliderItem> GetItemsBySliderId(int sliderId, bool visible = true);
    }
}