using System.Collections.Generic;
using Nop.Core;
using Nop.Core.Domain.Catalog;

namespace Nop.Services.Catalog
{
    /// <summary>
    /// Element service interface
    /// </summary>
    public partial interface IElementService
    {
        /// <summary>
        /// Gets an element
        /// </summary>
        /// <param name="elementId">Element identifier</param>
        /// <returns>Element</returns>
        Element GetElementById(int elementId);

        /// <summary>
        /// Gets elements by identifier
        /// </summary>
        /// <param name="elementIds">Element identifiers</param>
        /// <returns>Elements</returns>
        List<Element> GetElementsByIds(int[] elementIds);

        /// <summary>
        /// Gets all elements
        /// </summary>
        /// <param name="elementType">Element type</param>
        /// <param name="elementName">Element name</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <returns>Categories</returns>
        IPagedList<Element> GetAllElements(ElementType? elementType = null, string elementName = null, int pageIndex = 0, int pageSize = int.MaxValue);

        /// <summary>
        /// Gets all elements
        /// </summary>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>Elements</returns>
        IList<Element> GetAllElementsList(bool showHidden = true);

        /// <summary>
        /// Inserts element
        /// </summary>
        /// <param name="element">Element</param>
        void InsertElement(Element element);

        /// <summary>
        /// Updates the element
        /// </summary>
        /// <param name="element">Element</param>
        void UpdateElement(Element element);

        /// <summary>
        /// Delete element
        /// </summary>
        /// <param name="element">Element</param>
        void DeleteElement(Element element);
    }
}