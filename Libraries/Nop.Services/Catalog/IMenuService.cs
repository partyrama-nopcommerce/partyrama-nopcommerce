using System.Collections.Generic;
using Nop.Core;
using Nop.Core.Domain.Catalog;

namespace Nop.Services.Catalog
{
    /// <summary>
    /// Menu service interface
    /// </summary>
    public partial interface IMenuService
    {
        /// <summary>
        /// Delete menu
        /// </summary>
        /// <param name="menu">Menu</param>
        void DeleteMenu(Menu menu);

        /// <summary>
        /// Inserts menu
        /// </summary>
        /// <param name="menu">Menu</param>
        void InsertMenu(Menu menu);

        /// <summary>
        /// Updates the menu
        /// </summary>
        /// <param name="menu">Menu</param>
        void UpdateMenu(Menu menu);

        /// <summary>
        /// Gets a menu
        /// </summary>
        /// <param name="menuId">Menu identifier</param>
        /// <returns>Menu</returns>
        Menu GetMenuById(int menuId);

        /// <summary>
        /// Gets all menus
        /// </summary>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>Menus</returns>
        IList<Menu> GetAll(bool showHidden = true);
    }
}