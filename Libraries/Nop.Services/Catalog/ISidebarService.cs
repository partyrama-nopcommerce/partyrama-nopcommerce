using System.Collections.Generic;
using Nop.Core.Domain.Catalog;

namespace Nop.Services.Catalog
{
    /// <summary>
    /// Sidebar service interface
    /// </summary>
    public partial interface ISidebarService
    {
        /// <summary>
        /// Delete sidebar
        /// </summary>
        /// <param name="sidebar">Sidebar</param>
        void DeleteSidebar(Sidebar sidebar);

        /// <summary>
        /// Inserts sidebar
        /// </summary>
        /// <param name="sidebar">Sidebar</param>
        void InsertSidebar(Sidebar sidebar);

        /// <summary>
        /// Updates the sidebar
        /// </summary>
        /// <param name="sidebar">Sidebar</param>
        void UpdateSidebar(Sidebar sidebar);

        /// <summary>
        /// Gets a sidebar
        /// </summary>
        /// <param name="sidebarId">Sidebar identifier</param>
        /// <returns>Sidebar</returns>
        Sidebar GetSidebarById(int sidebarId);

        /// <summary>
        /// Gets all sidebars
        /// </summary>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>Sidebars</returns>
        IList<Sidebar> GetAll(bool showHidden = true);
    }
}