using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core.Caching;
using Nop.Core.Data;
using Nop.Core.Domain.Catalog;
using Nop.Services.Events;

namespace Nop.Services.Catalog
{
    /// <summary>
    /// Home page service
    /// </summary>
    public partial class HomePageService : IHomePageService
    {
        #region Fields

        private readonly IRepository<HomePage> _homePageRepository;
        private readonly ICacheManager _cacheManager;
        private readonly IEventPublisher _eventPublisher;
        private readonly IRepository<HomePageItem> _homePageItemRepository;

        #endregion

        #region Ctor

        public HomePageService(IRepository<HomePage> homePageRepository,
            ICacheManager cacheManager,
            IEventPublisher eventPublisher,
            IRepository<HomePageItem> homePageItemRepository)
        {
            _homePageRepository = homePageRepository;
            _cacheManager = cacheManager;
            _eventPublisher = eventPublisher;
            _homePageItemRepository = homePageItemRepository;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Delete home page
        /// </summary>
        /// <param name="homePage">Home page</param>
        public void DeleteHomePage(HomePage homePage)
        {
            if (homePage == null)
                throw new ArgumentNullException(nameof(homePage));

            _homePageRepository.Delete(homePage);

            //cache
            _cacheManager.RemoveByPattern(NopCatalogDefaults.HomePagesPatternCacheKey);

            //event notification
            _eventPublisher.EntityDeleted(homePage);
        }

        /// <summary>
        /// Inserts home page
        /// </summary>
        /// <param name="homePage">Home page</param>
        public void InsertHomePage(HomePage homePage)
        {
            if (homePage == null)
                throw new ArgumentNullException(nameof(homePage));

            _homePageRepository.Insert(homePage);

            //cache
            _cacheManager.RemoveByPattern(NopCatalogDefaults.HomePagesPatternCacheKey);

            //event notification
            _eventPublisher.EntityInserted(homePage);
        }

        /// <summary>
        /// Updates the home page
        /// </summary>
        /// <param name="homePage">Home page</param>
        public void UpdateHomePage(HomePage homePage)
        {
            if (homePage == null)
                throw new ArgumentNullException(nameof(homePage));

            _homePageRepository.Update(homePage);

            //cache
            _cacheManager.RemoveByPattern(NopCatalogDefaults.HomePagesPatternCacheKey);

            //event notification
            _eventPublisher.EntityUpdated(homePage);
        }

        /// <summary>
        /// Gets a home page
        /// </summary>
        /// <param name="homePageId">Home page identifier</param>
        /// <returns>Home page</returns>
        public HomePage GetHomePageById(int homePageId)
        {
            if (homePageId == 0)
                return null;

            var key = string.Format(NopCatalogDefaults.HomePageByIdCacheKey, homePageId);
            return _cacheManager.Get(key, () => _homePageRepository.GetById(homePageId));
        }

        /// <summary>
        /// Gets all home pages
        /// </summary>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>Home pages</returns>
        public IList<HomePage> GetAll(bool showHidden = true)
        {
            var query = _homePageRepository.Table;

            if (!showHidden)
            {
                query = query.Where(item => item.Visible
                    && (!item.StartDateUtc.HasValue || DateTime.UtcNow >= item.StartDateUtc.Value)
                    && (!item.EndDateUtc.HasValue || DateTime.UtcNow < item.EndDateUtc.Value));
            }

            return query.OrderBy(mmt => mmt.Id).ToList();
        }

        #endregion
    }
}