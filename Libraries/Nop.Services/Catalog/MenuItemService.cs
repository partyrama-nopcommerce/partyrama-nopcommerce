using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Data;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Topics;
using Nop.Services.Events;

namespace Nop.Services.Catalog
{
    /// <summary>
    /// Menu item service
    /// </summary>
    public partial class MenuItemService : IMenuItemService
    {
        #region Fields

        private readonly IRepository<MenuItem> _menuItemRepository;
        private readonly ICacheManager _cacheManager;
        private readonly IEventPublisher _eventPublisher;

        #endregion

        #region Ctor

        public MenuItemService(IRepository<MenuItem> menuItemRepository,
            ICacheManager cacheManager,
            IEventPublisher eventPublisher)
        {
            _cacheManager = cacheManager;
            _eventPublisher = eventPublisher;
            _menuItemRepository = menuItemRepository;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Gets menu items collection
        /// </summary>
        /// <param name="menuId">Menu identifier</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <returns>Menu item collection</returns>
        public IPagedList<MenuItem> GetColumnsHeadersByMenuId(int menuId,
            int pageIndex = 0,
            int pageSize = int.MaxValue)
        {
            var query = from mi in _menuItemRepository.Table
                        where ((mi.RowType == RowType.textcolumn
                                || mi.RowType == RowType.header
                                || mi.RowType == RowType.imagecolumn
                                || mi.RowType == RowType.title)
                            && mi.MenuId == menuId)
                        orderby mi.DisplayOrder ascending, mi.ColumnId ascending, mi.Id ascending
                        select mi;

            var items = new PagedList<MenuItem>(query, pageIndex, pageSize);
            return items;
        }

        /// <summary>
        /// Gets menu items collection
        /// </summary>
        /// <param name="menuItem">Menu item</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <returns>Menu item collection</returns>
        public IPagedList<MenuItem> GetRowsByMenuItem(MenuItem menuItem,
            int pageIndex = 0,
            int pageSize = int.MaxValue)
        {
            var query = from mi in _menuItemRepository.Table
                        where (mi.RowType == RowType.data
                            && mi.MenuId == menuItem.MenuId
                            && mi.ColumnId == menuItem.ColumnId)
                        orderby mi.DisplayOrder ascending, mi.ColumnId ascending, mi.Id ascending
                        select mi;

            var items = new PagedList<MenuItem>(query, pageIndex, pageSize);
            return items;
        }

        /// <summary>
        /// Gets menu items collection
        /// </summary>
        /// <param name="menuId">Menu identifier</param>
        /// <returns>Columns</returns>
        public IList<MenuItem> GetColumnsByMenuId(int menuId)
        {
            var query = from mi in _menuItemRepository.Table
                        where ((mi.RowType == RowType.textcolumn
                            || mi.RowType == RowType.imagecolumn)
                            && mi.MenuId == menuId)
                        orderby mi.DisplayOrder ascending, mi.ColumnId ascending, mi.Id ascending
                        select mi;

            var columns = query.ToList();
            return columns;
        }

        /// <summary>
        /// Inserts menu item
        /// </summary>
        /// <param name="menuItem">Menu item</param>
        public void InsertMenuItem(MenuItem menuItem)
        {
            if (menuItem == null)
                throw new ArgumentNullException(nameof(menuItem));

            if (menuItem.RowType == RowType.textcolumn
                || menuItem.RowType == RowType.header
                || menuItem.RowType == RowType.imagecolumn
                || menuItem.RowType == RowType.title)
            {
                var maxColumnId = _menuItemRepository.Table
                                        .Where(item => item.MenuId == menuItem.MenuId
                                            && (item.RowType == RowType.textcolumn
                                                || item.RowType == RowType.header
                                                || item.RowType == RowType.imagecolumn
                                                || item.RowType == RowType.title))
                                        .OrderByDescending(item => item.ColumnId)
                                        .Select(cc => cc.ColumnId)
                                        .DefaultIfEmpty(0)
                                        .First();

                menuItem.ColumnId = maxColumnId + 1;

                var maxDisplayOrder = _menuItemRepository.Table
                                        .Where(item => item.MenuId == menuItem.MenuId
                                            && (item.RowType == RowType.textcolumn
                                                || item.RowType == RowType.header
                                                || item.RowType == RowType.imagecolumn
                                                || item.RowType == RowType.title))
                                        .OrderByDescending(item => item.DisplayOrder)
                                        .Select(cc => cc.DisplayOrder)
                                        .DefaultIfEmpty(-1)
                                        .First();

                menuItem.DisplayOrder = maxDisplayOrder + 1;
            }

            _menuItemRepository.Insert(menuItem);

            //cache
            _cacheManager.RemoveByPattern(NopCatalogDefaults.MenusPatternCacheKey);
            _cacheManager.RemoveByPattern(NopCatalogDefaults.MenuItemsPatternCacheKey);

            //event notification
            _eventPublisher.EntityInserted(menuItem);
        }

        /// <summary>
        /// Gets a menu item
        /// </summary>
        /// <param name="menuItemId">Menu item identifier</param>
        /// <returns>Menu item</returns>
        public MenuItem GetMenuItemById(int menuItemId)
        {
            if (menuItemId == 0)
                return null;

            var key = string.Format(NopCatalogDefaults.MenuItemByIdCacheKey, menuItemId);
            return _cacheManager.Get(key, () => _menuItemRepository.GetById(menuItemId));
        }

        /// <summary>
        /// Delete a menu item
        /// </summary>
        /// <param name="connectionType">Row connection type</param>
        /// <param name="rowConnectionIds">Row connection identifiers</param>
        public void DeleteConnectionMenuItem(RowConnectionType connectionType, IEnumerable<int> rowConnectionIds)
        {
            var items = (from item in _menuItemRepository.Table
                         where item.RowConnectionType == connectionType && rowConnectionIds.Contains(item.RowConnectionId.GetValueOrDefault())
                         select item).Distinct().ToList();
            var rowsToDelete = new List<MenuItem>();

            foreach (var item in items.Where(m => m.RowType == RowType.textcolumn || m.RowType == RowType.imagecolumn))
            {
                var rows = _menuItemRepository.Table.Where(row => row.RowType == RowType.data && row.ColumnId == item.ColumnId).ToList();

                if (rows.Any())
                {
                    rowsToDelete.AddRange(rows);
                }
            }

            if (rowsToDelete.Any())
            {
                items.AddRange(rowsToDelete);
            }

            _menuItemRepository.Delete(items);

            foreach (var item in items)
            {
                //event notification
                _eventPublisher.EntityDeleted(item);
            }

            //cache
            _cacheManager.RemoveByPattern(NopCatalogDefaults.MenuItemsPatternCacheKey);
            _cacheManager.RemoveByPattern(NopCatalogDefaults.MenusPatternCacheKey);
        }

        /// <summary>
        /// Delete a menu item
        /// </summary>
        /// <param name="menuItem">Menu item</param>
        public void DeleteMenuItem(MenuItem menuItem)
        {
            if (menuItem == null)
                throw new ArgumentNullException(nameof(menuItem));

            var itemsToDelete = new List<MenuItem>
            {
                menuItem
            };

            //remove rows for column
            if (menuItem.RowType == RowType.textcolumn
                || menuItem.RowType == RowType.imagecolumn)
            {
                var rows = _menuItemRepository.Table.Where(row => row.RowType == RowType.data && row.ColumnId == menuItem.ColumnId).ToList();

                if (rows.Any())
                {
                    itemsToDelete.AddRange(rows);
                }
            }

            _menuItemRepository.Delete(itemsToDelete);

            foreach (var item in itemsToDelete)
            {
                //event notification
                _eventPublisher.EntityDeleted(item);
            }

            //cache
            _cacheManager.RemoveByPattern(NopCatalogDefaults.MenuItemsPatternCacheKey);
            _cacheManager.RemoveByPattern(NopCatalogDefaults.MenusPatternCacheKey);
        }

        /// <summary>
        /// Inserts menu item rows
        /// </summary>
        /// <param name="selectedCategories">Selected categories</param>
        /// <param name="menuItemId">Menu item column identifier</param>
        /// <param name="menuId">Menu identifier</param>
        public void InsertCategoryRows(List<Category> selectedCategories, int menuItemId, int menuId)
        {
            if (selectedCategories == null)
                throw new ArgumentNullException(nameof(selectedCategories));

            if (menuItemId > 0)
            {
                InsertRows(selectedCategories.Select(c => c.Id), RowConnectionType.category, menuItemId);
            }
            else
            {
                InsertTitles(selectedCategories.Select(c => c.Id), RowConnectionType.category, menuId);
            }
        }

        /// <summary>
        /// Inserts menu item rows
        /// </summary>
        /// <param name="selectedProducts">Selected products</param>
        /// <param name="menuItemId">Menu item column identifier</param>
        /// <param name="menuId">Menu identifier</param>
        public void InsertProductRows(List<Product> selectedProducts, int menuItemId, int menuId)
        {
            if (selectedProducts == null)
                throw new ArgumentNullException(nameof(selectedProducts));

            if (menuItemId > 0)
            {
                InsertRows(selectedProducts.Select(c => c.Id), RowConnectionType.product, menuItemId);
            }
            else
            {
                InsertTitles(selectedProducts.Select(c => c.Id), RowConnectionType.product, menuId);
            }
        }

        /// <summary>
        /// Inserts menu item rows
        /// </summary>
        /// <param name="selectedElements">Selected elements</param>
        /// <param name="menuItemId">Menu item column identifier</param>
        /// <param name="menuId">Menu identifier</param>
        public void InsertElementRows(List<Element> selectedElements, int menuItemId, int menuId)
        {
            if (selectedElements == null)
                throw new ArgumentNullException(nameof(selectedElements));

            if (menuItemId > 0)
            {
                var column = _menuItemRepository.GetById(menuItemId);
                if (column == null)
                    throw new ArgumentNullException(nameof(column));

                var maxDisplayOrder = _menuItemRepository.Table
                                            .Where(item => item.ColumnId == column.ColumnId
                                                    && item.RowType == RowType.data)
                                            .OrderByDescending(item => item.DisplayOrder)
                                            .Select(item => item.DisplayOrder)
                                            .DefaultIfEmpty(-1)
                                            .First();
                var rows = new List<MenuItem>();
                var utcDate = DateTime.UtcNow;

                foreach (var element in selectedElements)
                {
                    maxDisplayOrder++;
                    rows.Add(new MenuItem
                    {
                        ColumnId = column.ColumnId,
                        CreatedOnUtc = utcDate,
                        CssClass = GetElementRowClass(element),
                        DisplayOrder = maxDisplayOrder,
                        MenuId = column.MenuId,
                        RowConnectionId = element.Id,
                        RowConnectionType = RowConnectionType.element,
                        RowType = RowType.data,
                        UpdatedOnUtc = utcDate,
                        Visible = true
                    });
                }

                _menuItemRepository.Insert(rows);

                //cache
                _cacheManager.RemoveByPattern(NopCatalogDefaults.MenusPatternCacheKey);
                _cacheManager.RemoveByPattern(NopCatalogDefaults.MenuItemsPatternCacheKey);

                //event notification
                foreach (var row in rows)
                {
                    //event notification
                    _eventPublisher.EntityInserted(row);
                }
            }
            else
            {
                InsertTitles(selectedElements.Select(c => c.Id), RowConnectionType.element, menuId);
            }
        }

        private string GetElementRowClass(Element element)
        {
            switch (element.ElementType)
            {
                case ElementType.Description:
                    return "m_desc";
                case ElementType.Separator:
                    return "m_sep";
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        /// <summary>
        /// Inserts menu item rows
        /// </summary>
        /// <param name="selectedTopics">Selected topics</param>
        /// <param name="menuItemId">Menu item column identifier</param>
        /// <param name="menuId">Menu identifier</param>
        public void InsertTopicsRows(List<Topic> selectedTopics, int menuItemId, int menuId)
        {
            if (selectedTopics == null)
                throw new ArgumentNullException(nameof(selectedTopics));

            if (menuItemId > 0)
            {
                InsertRows(selectedTopics.Select(c => c.Id), RowConnectionType.topic, menuItemId);
            }
            else
            {
                InsertTitles(selectedTopics.Select(c => c.Id), RowConnectionType.topic, menuId);
            }
        }

        private void InsertTitles(IEnumerable<int> ids, RowConnectionType connectionType, int menuId)
        {
            var maxColumnId = _menuItemRepository.Table
                                    .Where(item => item.MenuId == menuId
                                        && (item.RowType == RowType.textcolumn
                                            || item.RowType == RowType.header
                                            || item.RowType == RowType.imagecolumn
                                            || item.RowType == RowType.title))
                                    .OrderByDescending(item => item.ColumnId)
                                    .Select(cc => cc.ColumnId)
                                    .DefaultIfEmpty(0)
                                    .First();

            var maxDisplayOrder = _menuItemRepository.Table
                                    .Where(item => item.MenuId == menuId
                                        && (item.RowType == RowType.textcolumn
                                            || item.RowType == RowType.header
                                            || item.RowType == RowType.imagecolumn
                                            || item.RowType == RowType.title))
                                    .OrderByDescending(item => item.DisplayOrder)
                                    .Select(cc => cc.DisplayOrder)
                                    .DefaultIfEmpty(-1)
                                    .First();

            var titles = new List<MenuItem>();
            var utcDate = DateTime.UtcNow;

            foreach (var id in ids)
            {
                maxDisplayOrder++;
                maxColumnId++;
                titles.Add(new MenuItem
                {
                    ColumnId = maxColumnId,
                    CreatedOnUtc = utcDate,
                    CssClass = "m_title",
                    DisplayOrder = maxDisplayOrder,
                    MenuId = menuId,
                    RowConnectionId = id,
                    RowConnectionType = connectionType,
                    RowType = RowType.title,
                    UpdatedOnUtc = utcDate,
                    Visible = true
                });
            }

            _menuItemRepository.Insert(titles);

            //cache
            _cacheManager.RemoveByPattern(NopCatalogDefaults.MenusPatternCacheKey);
            _cacheManager.RemoveByPattern(NopCatalogDefaults.MenuItemsPatternCacheKey);

            //event notification
            foreach (var row in titles)
            {
                //event notification
                _eventPublisher.EntityInserted(row);
            }
        }

        private void InsertRows(IEnumerable<int> ids, RowConnectionType connectionType, int menuItemId)
        {
            var column = _menuItemRepository.GetById(menuItemId);
            if (column == null)
                throw new ArgumentNullException(nameof(column));

            var maxDisplayOrder = _menuItemRepository.Table
                                        .Where(item => item.ColumnId == column.ColumnId
                                                && item.RowType == RowType.data)
                                        .OrderByDescending(item => item.DisplayOrder)
                                        .Select(item => item.DisplayOrder)
                                        .DefaultIfEmpty(-1)
                                        .First();
            var rows = new List<MenuItem>();
            var utcDate = DateTime.UtcNow;

            foreach (var id in ids)
            {
                maxDisplayOrder++;
                rows.Add(new MenuItem
                {
                    ColumnId = column.ColumnId,
                    CreatedOnUtc = utcDate,
                    CssClass = column.RowType == RowType.imagecolumn ? "m_image": "m_link",
                    DisplayOrder = maxDisplayOrder,
                    MenuId = column.MenuId,
                    RowConnectionId = id,
                    RowConnectionType = connectionType,
                    RowType = RowType.data,
                    UpdatedOnUtc = utcDate,
                    Visible = true
                });
            }

            _menuItemRepository.Insert(rows);

            //cache
            _cacheManager.RemoveByPattern(NopCatalogDefaults.MenusPatternCacheKey);
            _cacheManager.RemoveByPattern(NopCatalogDefaults.MenuItemsPatternCacheKey);

            //event notification
            foreach (var row in rows)
            {
                //event notification
                _eventPublisher.EntityInserted(row);
            }
        }

        /// <summary>
        /// Updates the menu item
        /// </summary>
        /// <param name="menuItem">Menu item</param>
        public void UpdateMenuItem(MenuItem menuItem)
        {
            if (menuItem == null)
                throw new ArgumentNullException(nameof(menuItem));

            _menuItemRepository.Update(menuItem);

            //cache
            _cacheManager.RemoveByPattern(NopCatalogDefaults.MenusPatternCacheKey);
            _cacheManager.RemoveByPattern(NopCatalogDefaults.MenuItemsPatternCacheKey);

            //event notification
            _eventPublisher.EntityUpdated(menuItem);
        }

        /// <summary>
        /// Reorder menu item
        /// </summary>
        /// <param name="newDisplayOrder">New display order</param>
        /// <param name="oldDisplayOrder">Old display order</param>
        /// <param name="dragItemId">Item that we are moving</param>
        /// <param name="isColumn">A value indicating whether drag item is column</param>
        public void ReOrderMenuItem(int newDisplayOrder, int oldDisplayOrder, int dragItemId, bool isColumn)
        {
            if (newDisplayOrder == oldDisplayOrder)
                return;

            var item = _menuItemRepository.GetById(dragItemId);
            if (item == null)
                throw new ArgumentNullException(nameof(item));

            var query = _menuItemRepository.Table.Where(i => i.MenuId == item.MenuId);
            if (isColumn)
            {
                query = query.Where(i => i.RowType != RowType.data);
            }
            else
            {
                query = query.Where(i => i.RowType == RowType.data && i.ColumnId == item.ColumnId);
            }

            var items = query.OrderBy(i => i.DisplayOrder).ThenBy(i => i.ColumnId).ThenBy(i => i.Id).ToList();
            if (items[oldDisplayOrder].Id != item.Id)
                return;

            var displayOrder = -1;

            if (newDisplayOrder > oldDisplayOrder)
            {
                for (var i = 0; i <= newDisplayOrder; i++)
                {
                    var currentItem = items[i];
                    if (currentItem.Id == item.Id)
                    {
                        currentItem.DisplayOrder = newDisplayOrder;
                    }
                    else
                    {
                        displayOrder++;
                        currentItem.DisplayOrder = displayOrder;
                    }
                }

                for (var i = newDisplayOrder + 1; i < items.Count(); i++)
                {
                    items[i].DisplayOrder = i;
                }
            }
            else
            {
                for (var i = 0; i < newDisplayOrder; i++)
                {
                    items[i].DisplayOrder = i;
                }

                displayOrder = newDisplayOrder;
                for (var i = newDisplayOrder; i < items.Count(); i++)
                {
                    var currentItem = items[i];
                    if (currentItem.Id == item.Id)
                    {
                        currentItem.DisplayOrder = newDisplayOrder;
                    }
                    else
                    {
                        displayOrder++;
                        currentItem.DisplayOrder = displayOrder;
                    }
                }
            }

            _menuItemRepository.Update(items);

            //cache
            _cacheManager.RemoveByPattern(NopCatalogDefaults.MenusPatternCacheKey);
            _cacheManager.RemoveByPattern(NopCatalogDefaults.MenuItemsPatternCacheKey);

            //event notification
            foreach (var itemUpdated in items)
            {
                //event notification
                _eventPublisher.EntityUpdated(itemUpdated);
            }
        }

        /// <summary>
        /// Gets menu items collection
        /// </summary>
        /// <param name="menuId">Menu identifier</param>
        /// <param name="visible">Show only visible</param>
        /// <returns>Menu items</returns>
        public IList<MenuItem> GetItemsByMenuId(int menuId, bool visible = true)
        {
            var query = _menuItemRepository.Table.Where(item => item.MenuId == menuId);
            if (visible)
            {
                query = query.Where(item => item.Visible
                    && (!item.StartDateUtc.HasValue || DateTime.UtcNow >= item.StartDateUtc.Value)
                    && (!item.EndDateUtc.HasValue || DateTime.UtcNow < item.EndDateUtc.Value));
            }

            return query.OrderBy(item => item.ColumnId).ThenBy(item => item.DisplayOrder).ToList();
        }

        #endregion
    }
}