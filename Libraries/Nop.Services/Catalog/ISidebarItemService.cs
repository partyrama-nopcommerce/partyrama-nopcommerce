using System.Collections.Generic;
using Nop.Core;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Topics;

namespace Nop.Services.Catalog
{
    /// <summary>
    /// Sidebar item service interface
    /// </summary>
    public partial interface ISidebarItemService
    {
        /// <summary>
        /// Gets sidebar items collection
        /// </summary>
        /// <param name="sidebarId">Sidebar identifier</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <returns>Sidebar item collection</returns>
        IPagedList<SidebarItem> GetColumnsHeadersBySidebarId(int sidebarId,
            int pageIndex = 0,
            int pageSize = int.MaxValue);

        /// <summary>
        /// Gets sidebar items collection
        /// </summary>
        /// <param name="sidebarItem">Sidebar item</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <returns>Sidebar item collection</returns>
        IPagedList<SidebarItem> GetRowsBySidebarItem(SidebarItem sidebarItem,
            int pageIndex = 0,
            int pageSize = int.MaxValue);

        /// <summary>
        /// Inserts sidebar item
        /// </summary>
        /// <param name="sidebarItem">Sidebar item</param>
        void InsertSidebarItem(SidebarItem sidebarItem);

        /// <summary>
        /// Gets a sidebar item
        /// </summary>
        /// <param name="sidebarItemId">Sidebar item identifier</param>
        /// <returns>Sidebar item</returns>
        SidebarItem GetSidebarItemById(int sidebarItemId);

        /// <summary>
        /// Delete a sidebar item
        /// </summary>
        /// <param name="sidebarItem">Sidebar item</param>
        void DeleteSidebarItem(SidebarItem sidebarItem);

        /// <summary>
        /// Delete a sidebar item
        /// </summary>
        /// <param name="connectionType">Row connection type</param>
        /// <param name="rowConnectionIds">Row connection identifiers</param>
        void DeleteConnectionSidebarItem(RowConnectionType connectionType, IEnumerable<int> rowConnectionIds);

        /// <summary>
        /// Gets sidebar items collection
        /// </summary>
        /// <param name="sidebarId">Sidebar identifier</param>
        /// <returns>Columns</returns>
        IList<SidebarItem> GetColumnsBySidebarId(int sidebarId);

        /// <summary>
        /// Inserts sidebar item rows
        /// </summary>
        /// <param name="selectedCategories">Selected categories</param>
        /// <param name="sidebarItemId">Sidebar item column identifier</param>
        /// <param name="sidebarId">Sidebar identifier</param>
        void InsertCategoryRows(List<Category> selectedCategories, int sidebarItemId, int sidebarId);

        /// <summary>
        /// Inserts sidebar item rows
        /// </summary>
        /// <param name="selectedProducts">Selected products</param>
        /// <param name="sidebarItemId">Sidebar item column identifier</param>
        /// <param name="sidebarId">Sidebar identifier</param>
        void InsertProductRows(List<Product> selectedProducts, int sidebarItemId, int sidebarId);

        /// <summary>
        /// Inserts sidebar item rows
        /// </summary>
        /// <param name="selectedElements">Selected elements</param>
        /// <param name="sidebarItemId">Sidebar item column identifier</param>
        /// <param name="sidebarId">Sidebar identifier</param>
        void InsertElementRows(List<Element> selectedElements, int sidebarItemId, int sidebarId);

        /// <summary>
        /// Inserts sidebar item rows
        /// </summary>
        /// <param name="selectedTopics">Selected topics</param>
        /// <param name="sidebarItemId">Sidebar item column identifier</param>
        /// <param name="sidebarId">Sidebar identifier</param>
        void InsertTopicsRows(List<Topic> selectedTopics, int sidebarItemId, int sidebarId);

        /// <summary>
        /// Updates the sidebar item
        /// </summary>
        /// <param name="sidebarItem">Sidebar item</param>
        void UpdateSidebarItem(SidebarItem sidebarItem);

        /// <summary>
        /// Reorder sidebar item
        /// </summary>
        /// <param name="newDisplayOrder">New display order</param>
        /// <param name="oldDisplayOrder">Old display order</param>
        /// <param name="dragItemId">Item that we are moving</param>
        /// <param name="isColumn">A value indicating whether drag item is column</param>
        void ReOrderSidebarItem(int newDisplayOrder, int oldDisplayOrder, int dragItemId, bool isColumn);

        /// <summary>
        /// Gets sidebar items collection
        /// </summary>
        /// <param name="sidebarId">Sidebar identifier</param>
        /// <param name="visible">Show only visible</param>
        /// <returns>Sidebar items</returns>
        IList<SidebarItem> GetItemsBySidebarId(int sidebarId, bool visible = true);

        /// <summary>
        /// Inserts sidebar item rows
        /// </summary>
        /// <param name="selectedSliders">Selected sliders</param>
        /// <param name="sidebarItemId">Sidebar item column identifier</param>
        /// <param name="sidebarId">Sidebar identifier</param>
        void InsertSliderRows(List<Slider> selectedSliders, int sidebarItemId, int sidebarId);
    }
}