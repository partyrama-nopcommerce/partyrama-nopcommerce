using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core.Caching;
using Nop.Core.Data;
using Nop.Core.Domain.Catalog;
using Nop.Services.Events;

namespace Nop.Services.Catalog
{
    /// <summary>
    /// Mega menu service
    /// </summary>
    public partial class MegaMenuTabService : IMegaMenuTabService
    {
        #region Fields

        private readonly IRepository<MegaMenuTab> _megaMenuTabRepository;
        private readonly ICacheManager _cacheManager;
        private readonly IEventPublisher _eventPublisher;
        private readonly IRepository<MegaMenuTabItem> _megaMenuTabItemRepository;

        #endregion

        #region Ctor

        public MegaMenuTabService(IRepository<MegaMenuTab> megaMenuTabRepository,
            ICacheManager cacheManager,
            IEventPublisher eventPublisher,
            IRepository<MegaMenuTabItem> megaMenuTabItemRepository)
        {
            _megaMenuTabRepository = megaMenuTabRepository;
            _cacheManager = cacheManager;
            _eventPublisher = eventPublisher;
            _megaMenuTabItemRepository = megaMenuTabItemRepository;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Delete mega menu tab
        /// </summary>
        /// <param name="megaMenuTab">Mega menu tab</param>
        public void DeleteMegaMenuTab(MegaMenuTab megaMenuTab)
        {
            if (megaMenuTab == null)
                throw new ArgumentNullException(nameof(megaMenuTab));

            _megaMenuTabRepository.Delete(megaMenuTab);

            //cache
            _cacheManager.RemoveByPattern(NopCatalogDefaults.MegaMenuTabsPatternCacheKey);

            //event notification
            _eventPublisher.EntityDeleted(megaMenuTab);
        }

        /// <summary>
        /// Inserts mega menu tab
        /// </summary>
        /// <param name="megaMenuTab">Mega menu tab</param>
        public void InsertMegaMenuTab(MegaMenuTab megaMenuTab)
        {
            if (megaMenuTab == null)
                throw new ArgumentNullException(nameof(megaMenuTab));

            _megaMenuTabRepository.Insert(megaMenuTab);

            //cache
            _cacheManager.RemoveByPattern(NopCatalogDefaults.MegaMenuTabsPatternCacheKey);

            //event notification
            _eventPublisher.EntityInserted(megaMenuTab);
        }


        /// <summary>
        /// Updates the mega menu tab
        /// </summary>
        /// <param name="megaMenuTab">Mega menu tab</param>
        public void UpdateMegaMenuTab(MegaMenuTab megaMenuTab)
        {
            if (megaMenuTab == null)
                throw new ArgumentNullException(nameof(megaMenuTab));

            _megaMenuTabRepository.Update(megaMenuTab);

            //cache
            _cacheManager.RemoveByPattern(NopCatalogDefaults.MegaMenuTabsPatternCacheKey);

            //event notification
            _eventPublisher.EntityUpdated(megaMenuTab);
        }

        /// <summary>
        /// Gets a mega menu tab
        /// </summary>
        /// <param name="megaMenuTabId">Mega menu tab identifier</param>
        /// <returns>Mega menu tab</returns>
        public MegaMenuTab GetMegaMenuTabById(int megaMenuTabId)
        {
            if (megaMenuTabId == 0)
                return null;

            var key = string.Format(NopCatalogDefaults.MegaMenuTabByIdCacheKey, megaMenuTabId);
            return _cacheManager.Get(key, () => _megaMenuTabRepository.GetById(megaMenuTabId));
        }

        /// <summary>
        /// Gets all mega menu tabs
        /// </summary>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>Mega menu tabs</returns>
        public IList<MegaMenuTab> GetAll(bool showHidden = true)
        {
            var query = _megaMenuTabRepository.Table;

            if (!showHidden)
            {
                query = query.Where(mmt => mmt.Visible);
            }

            return query.OrderBy(mmt => mmt.Id).ToList();
        }

        #endregion
    }
}