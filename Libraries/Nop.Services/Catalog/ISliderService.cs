using System.Collections.Generic;
using Nop.Core;
using Nop.Core.Domain.Catalog;

namespace Nop.Services.Catalog
{
    /// <summary>
    /// Slider service interface
    /// </summary>
    public partial interface ISliderService
    {
        /// <summary>
        /// Delete slider
        /// </summary>
        /// <param name="slider">Slider</param>
        void DeleteSlider(Slider slider);

        /// <summary>
        /// Inserts slider
        /// </summary>
        /// <param name="slider">Slider</param>
        void InsertSlider(Slider slider);

        /// <summary>
        /// Updates the slider
        /// </summary>
        /// <param name="slider">Slider</param>
        void UpdateSlider(Slider slider);

        /// <summary>
        /// Gets a slider
        /// </summary>
        /// <param name="sliderId">Slider identifier</param>
        /// <returns>Slider</returns>
        Slider GetSliderById(int sliderId);

        /// <summary>
        /// Gets all sliders
        /// </summary>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>Sliders</returns>
        IList<Slider> GetAll(bool showHidden = true);

        /// <summary>
        /// Gets sliders by identifier
        /// </summary>
        /// <param name="sliderIds">Slider identifiers</param>
        /// <returns>Sliders</returns>
        List<Slider> GetSlidersByIds(int[] sliderIds);

        /// <summary>
        /// Gets all sliders
        /// </summary>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <param name="sliderName">Slider name</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <returns>Sliders</returns>
        IPagedList<Slider> GetAllSliders(bool showHidden = true, string sliderName = null, int pageIndex = 0, int pageSize = int.MaxValue);
    }
}