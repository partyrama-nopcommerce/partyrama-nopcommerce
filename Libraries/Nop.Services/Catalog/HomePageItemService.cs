using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Data;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Topics;
using Nop.Services.Events;

namespace Nop.Services.Catalog
{
    /// <summary>
    /// Home page item service
    /// </summary>
    public partial class HomePageItemService: IHomePageItemService
    {
        #region Fields

        private readonly IRepository<HomePageItem> _homePageItemRepository;
        private readonly ICacheManager _cacheManager;
        private readonly IEventPublisher _eventPublisher;

        #endregion

        #region Ctor

        public HomePageItemService(IRepository<HomePageItem> homePageItemRepository,
            ICacheManager cacheManager,
            IEventPublisher eventPublisher)
        {
            _cacheManager = cacheManager;
            _eventPublisher = eventPublisher;
            _homePageItemRepository = homePageItemRepository;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Gets home page items collection
        /// </summary>
        /// <param name="homePageId">Home page identifier</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <returns>Home page item collection</returns>
        public IPagedList<HomePageItem> GetColumnsHeadersByHomePageId(int homePageId,
            int pageIndex = 0,
            int pageSize = int.MaxValue)
        {
            var query = from hpi in _homePageItemRepository.Table
                        where ((hpi.RowType == RowType.textcolumn
                                || hpi.RowType == RowType.header
                                || hpi.RowType == RowType.imagecolumn
                                || hpi.RowType == RowType.title)
                            && hpi.HomePageId == homePageId)
                        orderby hpi.DisplayOrder ascending, hpi.ColumnId ascending, hpi.Id ascending
                        select hpi;

            var items = new PagedList<HomePageItem>(query, pageIndex, pageSize);
            return items;
        }

        /// <summary>
        /// Gets home page items collection
        /// </summary>
        /// <param name="homePageItem">Home page item</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <returns>Home page item collection</returns>
        public IPagedList<HomePageItem> GetRowsByHomePageItem(HomePageItem homePageItem,
            int pageIndex = 0,
            int pageSize = int.MaxValue)
        {
            var query = from hpi in _homePageItemRepository.Table
                        where (hpi.RowType == RowType.data
                            && hpi.HomePageId == homePageItem.HomePageId
                            && hpi.ColumnId == homePageItem.ColumnId)
                        orderby hpi.DisplayOrder ascending, hpi.ColumnId ascending, hpi.Id ascending
                        select hpi;

            var items = new PagedList<HomePageItem>(query, pageIndex, pageSize);
            return items;
        }

        /// <summary>
        /// Gets home page items collection
        /// </summary>
        /// <param name="homePageId">Home page identifier</param>
        /// <returns>Columns</returns>
        public IList<HomePageItem> GetColumnsByHomePageId(int homePageId)
        {
            var query = from hpi in _homePageItemRepository.Table
                        where ((hpi.RowType == RowType.textcolumn
                            || hpi.RowType == RowType.imagecolumn)
                            && hpi.HomePageId == homePageId)
                        orderby hpi.DisplayOrder ascending, hpi.ColumnId ascending, hpi.Id ascending
                        select hpi;

            var columns = query.ToList();
            return columns;
        }

        /// <summary>
        /// Inserts home page item
        /// </summary>
        /// <param name="homePageItem">Home page item</param>
        public void InsertHomePageItem(HomePageItem homePageItem)
        {
            if (homePageItem == null)
                throw new ArgumentNullException(nameof(homePageItem));

            if (homePageItem.RowType == RowType.textcolumn
                || homePageItem.RowType == RowType.header
                || homePageItem.RowType == RowType.imagecolumn
                || homePageItem.RowType == RowType.title)
            {
                var maxColumnId = _homePageItemRepository.Table
                                        .Where(item => item.HomePageId == homePageItem.HomePageId
                                            && (item.RowType == RowType.textcolumn
                                                || item.RowType == RowType.header
                                                || item.RowType == RowType.imagecolumn
                                                || item.RowType == RowType.title))
                                        .OrderByDescending(item => item.ColumnId)
                                        .Select(cc => cc.ColumnId)
                                        .DefaultIfEmpty(0)
                                        .First();

                homePageItem.ColumnId = maxColumnId + 1;

                var maxDisplayOrder = _homePageItemRepository.Table
                                        .Where(item => item.HomePageId == homePageItem.HomePageId
                                            && (item.RowType == RowType.textcolumn
                                                || item.RowType == RowType.header
                                                || item.RowType == RowType.imagecolumn
                                                || item.RowType == RowType.title))
                                        .OrderByDescending(item => item.DisplayOrder)
                                        .Select(cc => cc.DisplayOrder)
                                        .DefaultIfEmpty(-1)
                                        .First();

                homePageItem.DisplayOrder = maxDisplayOrder + 1;
            }

            _homePageItemRepository.Insert(homePageItem);

            //cache
            _cacheManager.RemoveByPattern(NopCatalogDefaults.HomePagesPatternCacheKey);
            _cacheManager.RemoveByPattern(NopCatalogDefaults.HomePageItemsPatternCacheKey);

            //event notification
            _eventPublisher.EntityInserted(homePageItem);
        }

        /// <summary>
        /// Gets a home page item
        /// </summary>
        /// <param name="homePageItemId">Home page item identifier</param>
        /// <returns>Home page item</returns>
        public HomePageItem GetHomePageItemById(int homePageItemId)
        {
            if (homePageItemId == 0)
                return null;

            var key = string.Format(NopCatalogDefaults.HomePageItemByIdCacheKey, homePageItemId);
            return _cacheManager.Get(key, () => _homePageItemRepository.GetById(homePageItemId));
        }

        /// <summary>
        /// Delete a home page item
        /// </summary>
        /// <param name="connectionType">Row connection type</param>
        /// <param name="rowConnectionIds">Row connection identifiers</param>
        public void DeleteConnectionHomePageItem(RowConnectionType connectionType, IEnumerable<int> rowConnectionIds)
        {
            var items = (from item in _homePageItemRepository.Table
                         where item.RowConnectionType == connectionType && rowConnectionIds.Contains(item.RowConnectionId.GetValueOrDefault())
                         select item).Distinct().ToList();
            var rowsToDelete = new List<HomePageItem>();

            foreach (var item in items.Where(m => m.RowType == RowType.textcolumn || m.RowType == RowType.imagecolumn))
            {
                var rows = _homePageItemRepository.Table.Where(row => row.RowType == RowType.data && row.ColumnId == item.ColumnId).ToList();

                if (rows.Any())
                {
                    rowsToDelete.AddRange(rows);
                }
            }

            if (rowsToDelete.Any())
            {
                items.AddRange(rowsToDelete);
            }

            _homePageItemRepository.Delete(items);

            foreach (var item in items)
            {
                //event notification
                _eventPublisher.EntityDeleted(item);
            }

            //cache
            _cacheManager.RemoveByPattern(NopCatalogDefaults.HomePageItemsPatternCacheKey);
            _cacheManager.RemoveByPattern(NopCatalogDefaults.HomePagesPatternCacheKey);
        }

        /// <summary>
        /// Delete a home page item
        /// </summary>
        /// <param name="homePageItem">Home page item</param>
        public void DeleteHomePageItem(HomePageItem homePageItem)
        {
            if (homePageItem == null)
                throw new ArgumentNullException(nameof(homePageItem));

            var itemsToDelete = new List<HomePageItem>
            {
                homePageItem
            };

            //remove rows for column
            if (homePageItem.RowType == RowType.textcolumn
                || homePageItem.RowType == RowType.imagecolumn)
            {
                var rows = _homePageItemRepository.Table.Where(row => row.RowType == RowType.data && row.ColumnId == homePageItem.ColumnId).ToList();

                if (rows.Any())
                {
                    itemsToDelete.AddRange(rows);
                }
            }

            _homePageItemRepository.Delete(itemsToDelete);

            foreach (var item in itemsToDelete)
            {
                //event notification
                _eventPublisher.EntityDeleted(item);
            }

            //cache
            _cacheManager.RemoveByPattern(NopCatalogDefaults.HomePageItemsPatternCacheKey);
            _cacheManager.RemoveByPattern(NopCatalogDefaults.HomePagesPatternCacheKey);
        }

        /// <summary>
        /// Inserts home page item rows
        /// </summary>
        /// <param name="selectedCategories">Selected categories</param>
        /// <param name="homePageItemId">Home page item column identifier</param>
        /// <param name="homePageId">Home page identifier</param>
        public void InsertCategoryRows(List<Category> selectedCategories, int homePageItemId, int homePageId)
        {
            if (selectedCategories == null)
                throw new ArgumentNullException(nameof(selectedCategories));

            if (homePageItemId > 0)
            {
                InsertRows(selectedCategories.Select(c => c.Id), RowConnectionType.category, homePageItemId);
            }
            else
            {
                InsertTitles(selectedCategories.Select(c => c.Id), RowConnectionType.category, homePageId);
            }
        }

        /// <summary>
        /// Inserts home page item rows
        /// </summary>
        /// <param name="selectedProducts">Selected products</param>
        /// <param name="homePageItemId">Home page item column identifier</param>
        /// <param name="homePageId">Home page identifier</param>
        public void InsertProductRows(List<Product> selectedProducts, int homePageItemId, int homePageId)
        {
            if (selectedProducts == null)
                throw new ArgumentNullException(nameof(selectedProducts));

            if (homePageItemId > 0)
            {
                InsertRows(selectedProducts.Select(c => c.Id), RowConnectionType.product, homePageItemId);
            }
            else
            {
                InsertTitles(selectedProducts.Select(c => c.Id), RowConnectionType.product, homePageId);
            }
        }

        /// <summary>
        /// Inserts home page item rows
        /// </summary>
        /// <param name="selectedElements">Selected elements</param>
        /// <param name="homePageItemId">Home page item column identifier</param>
        /// <param name="homePageId">Home page identifier</param>
        public void InsertElementRows(List<Element> selectedElements, int homePageItemId, int homePageId)
        {
            if (homePageItemId > 0)
            {
                if (selectedElements == null)
                    throw new ArgumentNullException(nameof(selectedElements));

                var column = _homePageItemRepository.GetById(homePageItemId);
                if (column == null)
                    throw new ArgumentNullException(nameof(column));

                var maxDisplayOrder = _homePageItemRepository.Table
                                            .Where(item => item.ColumnId == column.ColumnId
                                                    && item.RowType == RowType.data)
                                            .OrderByDescending(item => item.DisplayOrder)
                                            .Select(item => item.DisplayOrder)
                                            .DefaultIfEmpty(-1)
                                            .First();
                var rows = new List<HomePageItem>();
                var utcDate = DateTime.UtcNow;

                foreach (var element in selectedElements)
                {
                    maxDisplayOrder++;
                    rows.Add(new HomePageItem
                    {
                        ColumnId = column.ColumnId,
                        CreatedOnUtc = utcDate,
                        CssClass = GetElementRowClass(element),
                        DisplayOrder = maxDisplayOrder,
                        HomePageId = column.HomePageId,
                        RowConnectionId = element.Id,
                        RowConnectionType = RowConnectionType.element,
                        RowType = RowType.data,
                        UpdatedOnUtc = utcDate,
                        Visible = true,
                        ShowDescription = true,
                        ShowName = true,
                        ShowPicture = true
                    });
                }

                _homePageItemRepository.Insert(rows);

                //cache
                _cacheManager.RemoveByPattern(NopCatalogDefaults.HomePagesPatternCacheKey);
                _cacheManager.RemoveByPattern(NopCatalogDefaults.HomePageItemsPatternCacheKey);

                //event notification
                foreach (var row in rows)
                {
                    //event notification
                    _eventPublisher.EntityInserted(row);
                }
            }
            else
            {
                InsertTitles(selectedElements.Select(c => c.Id), RowConnectionType.element, homePageId);
            }
        }

        private string GetElementRowClass(Element element)
        {
            switch (element.ElementType)
            {
                case ElementType.Description:
                    return "hp_desc";
                case ElementType.Separator:
                    return "hp_sep";
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        /// <summary>
        /// Inserts home page item rows
        /// </summary>
        /// <param name="selectedTopics">Selected topics</param>
        /// <param name="homePageItemId">Home page item column identifier</param>
        /// <param name="homePageId">Home page identifier</param>
        public void InsertTopicsRows(List<Topic> selectedTopics, int homePageItemId, int homePageId)
        {
            if (selectedTopics == null)
                throw new ArgumentNullException(nameof(selectedTopics));

            if (homePageItemId > 0)
            {
                InsertRows(selectedTopics.Select(c => c.Id), RowConnectionType.topic, homePageItemId);
            }
            else
            {
                InsertTitles(selectedTopics.Select(c => c.Id), RowConnectionType.topic, homePageId);
            }
        }

        private void InsertRows(IEnumerable<int> ids, RowConnectionType connectionType, int homePageItemId)
        {
            var column = _homePageItemRepository.GetById(homePageItemId);
            if (column == null)
                throw new ArgumentNullException(nameof(column));

            var maxDisplayOrder = _homePageItemRepository.Table
                                        .Where(item => item.ColumnId == column.ColumnId
                                                && item.RowType == RowType.data)
                                        .OrderByDescending(item => item.DisplayOrder)
                                        .Select(item => item.DisplayOrder)
                                        .DefaultIfEmpty(-1)
                                        .First();
            var rows = new List<HomePageItem>();
            var utcDate = DateTime.UtcNow;

            foreach (var id in ids)
            {
                maxDisplayOrder++;
                rows.Add(new HomePageItem
                {
                    ColumnId = column.ColumnId,
                    CreatedOnUtc = utcDate,
                    CssClass = column.RowType == RowType.imagecolumn ? "hp_image" : "hp_link",
                    DisplayOrder = maxDisplayOrder,
                    HomePageId = column.HomePageId,
                    RowConnectionId = id,
                    RowConnectionType = connectionType,
                    RowType = RowType.data,
                    UpdatedOnUtc = utcDate,
                    ShowDescription = true,
                    ShowName = true,
                    ShowPicture = true,
                    Visible = true
                });
            }

            _homePageItemRepository.Insert(rows);

            //cache
            _cacheManager.RemoveByPattern(NopCatalogDefaults.HomePagesPatternCacheKey);
            _cacheManager.RemoveByPattern(NopCatalogDefaults.HomePageItemsPatternCacheKey);

            //event notification
            foreach (var row in rows)
            {
                //event notification
                _eventPublisher.EntityInserted(row);
            }
        }

        private void InsertTitles(IEnumerable<int> ids, RowConnectionType connectionType, int homePageId)
        {
            var maxColumnId = _homePageItemRepository.Table
                                    .Where(item => item.HomePageId == homePageId
                                        && (item.RowType == RowType.textcolumn
                                            || item.RowType == RowType.header
                                            || item.RowType == RowType.imagecolumn
                                            || item.RowType == RowType.title))
                                    .OrderByDescending(item => item.ColumnId)
                                    .Select(cc => cc.ColumnId)
                                    .DefaultIfEmpty(0)
                                    .First();

            var maxDisplayOrder = _homePageItemRepository.Table
                                    .Where(item => item.HomePageId == homePageId
                                        && (item.RowType == RowType.textcolumn
                                            || item.RowType == RowType.header
                                            || item.RowType == RowType.imagecolumn
                                            || item.RowType == RowType.title))
                                    .OrderByDescending(item => item.DisplayOrder)
                                    .Select(cc => cc.DisplayOrder)
                                    .DefaultIfEmpty(-1)
                                    .First();

            var titles = new List<HomePageItem>();
            var utcDate = DateTime.UtcNow;

            foreach (var id in ids)
            {
                maxDisplayOrder++;
                maxColumnId++;
                titles.Add(new HomePageItem
                {
                    ColumnId = maxColumnId,
                    CreatedOnUtc = utcDate,
                    CssClass = "hp_title",
                    DisplayOrder = maxDisplayOrder,
                    HomePageId = homePageId,
                    RowConnectionId = id,
                    RowConnectionType = connectionType,
                    RowType = RowType.title,
                    UpdatedOnUtc = utcDate,
                    Visible = true,
                    ShowDescription = true,
                    ShowName = true,
                    ShowPicture = true
                });
            }

            _homePageItemRepository.Insert(titles);

            //cache
            _cacheManager.RemoveByPattern(NopCatalogDefaults.HomePagesPatternCacheKey);
            _cacheManager.RemoveByPattern(NopCatalogDefaults.HomePageItemsPatternCacheKey);

            //event notification
            foreach (var row in titles)
            {
                //event notification
                _eventPublisher.EntityInserted(row);
            }
        }

        /// <summary>
        /// Updates the home page item
        /// </summary>
        /// <param name="homePageItem">Home page item</param>
        public void UpdateHomePageItem(HomePageItem homePageItem)
        {
            if (homePageItem == null)
                throw new ArgumentNullException(nameof(homePageItem));

            _homePageItemRepository.Update(homePageItem);

            //cache
            _cacheManager.RemoveByPattern(NopCatalogDefaults.HomePagesPatternCacheKey);
            _cacheManager.RemoveByPattern(NopCatalogDefaults.HomePageItemsPatternCacheKey);

            //event notification
            _eventPublisher.EntityUpdated(homePageItem);
        }

        /// <summary>
        /// Reorder home page item
        /// </summary>
        /// <param name="newDisplayOrder">New display order</param>
        /// <param name="oldDisplayOrder">Old display order</param>
        /// <param name="dragItemId">Item that we are moving</param>
        /// <param name="isColumn">A value indicating whether drag item is column</param>
        public void ReOrderHomePageItem(int newDisplayOrder, int oldDisplayOrder, int dragItemId, bool isColumn)
        {
            if (newDisplayOrder == oldDisplayOrder)
                return;

            var item = _homePageItemRepository.GetById(dragItemId);
            if (item == null)
                throw new ArgumentNullException(nameof(item));

            var query = _homePageItemRepository.Table.Where(i => i.HomePageId == item.HomePageId);
            if (isColumn)
            {
                query = query.Where(i => i.RowType != RowType.data);
            }
            else
            {
                query = query.Where(i => i.RowType == RowType.data && i.ColumnId == item.ColumnId);
            }

            var items = query.OrderBy(i => i.DisplayOrder).ThenBy(i => i.ColumnId).ThenBy(i => i.Id).ToList();
            if (items[oldDisplayOrder].Id != item.Id)
                return;

            var displayOrder = -1;

            if (newDisplayOrder > oldDisplayOrder)
            {
                for (var i = 0; i <= newDisplayOrder; i++)
                {
                    var currentItem = items[i];
                    if (currentItem.Id == item.Id)
                    {
                        currentItem.DisplayOrder = newDisplayOrder;
                    }
                    else
                    {
                        displayOrder++;
                        currentItem.DisplayOrder = displayOrder;
                    }
                }

                for (var i = newDisplayOrder + 1; i < items.Count(); i++)
                {
                    items[i].DisplayOrder = i;
                }
            }
            else
            {
                for (var i = 0; i < newDisplayOrder; i++)
                {
                    items[i].DisplayOrder = i;
                }

                displayOrder = newDisplayOrder;
                for (var i = newDisplayOrder; i < items.Count(); i++)
                {
                    var currentItem = items[i];
                    if (currentItem.Id == item.Id)
                    {
                        currentItem.DisplayOrder = newDisplayOrder;
                    }
                    else
                    {
                        displayOrder++;
                        currentItem.DisplayOrder = displayOrder;
                    }
                }
            }

            _homePageItemRepository.Update(items);

            //cache
            _cacheManager.RemoveByPattern(NopCatalogDefaults.HomePagesPatternCacheKey);
            _cacheManager.RemoveByPattern(NopCatalogDefaults.HomePageItemsPatternCacheKey);

            //event notification
            foreach (var itemUpdated in items)
            {
                //event notification
                _eventPublisher.EntityUpdated(itemUpdated);
            }
        }

        /// <summary>
        /// Gets home page items collection
        /// </summary>
        /// <param name="homePageId">Home page identifier</param>
        /// <param name="visible">Show only visible</param>
        /// <returns>Home page items</returns>
        public IList<HomePageItem> GetItemsByHomePageId(int homePageId, bool visible = true)
        {
            var query = _homePageItemRepository.Table.Where(item => item.HomePageId == homePageId);
            if (visible)
            {
                query = query.Where(item => item.Visible
                    && (!item.StartDateUtc.HasValue || DateTime.UtcNow >= item.StartDateUtc.Value)
                    && (!item.EndDateUtc.HasValue || DateTime.UtcNow < item.EndDateUtc.Value));
            }

            return query.OrderBy(item => item.ColumnId).ThenBy(item => item.DisplayOrder).ToList();
        }

        /// <summary>
        /// Inserts home page item rows
        /// </summary>
        /// <param name="selectedSliders">Selected sliders</param>
        /// <param name="homePageItemId">Home page item column identifier</param>
        /// <param name="homePageId">Home page identifier</param>
        public void InsertSliderRows(List<Slider> selectedSliders, int homePageItemId, int homePageId)
        {
            if (selectedSliders == null)
                throw new ArgumentNullException(nameof(selectedSliders));

            if (homePageItemId > 0)
            {
                InsertRows(selectedSliders.Select(c => c.Id), RowConnectionType.slider, homePageItemId);
            }
            else
            {
                InsertTitles(selectedSliders.Select(c => c.Id), RowConnectionType.slider, homePageId);
            }
        }

        #endregion
    }
}