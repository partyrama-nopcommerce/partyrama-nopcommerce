using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Data;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Topics;
using Nop.Services.Events;

namespace Nop.Services.Catalog
{
    /// <summary>
    /// Sidebar item service
    /// </summary>
    public partial class SidebarItemService: ISidebarItemService
    {
        #region Fields

        private readonly IRepository<SidebarItem> _sidebarItemRepository;
        private readonly ICacheManager _cacheManager;
        private readonly IEventPublisher _eventPublisher;

        #endregion

        #region Ctor

        public SidebarItemService(IRepository<SidebarItem> sidebarItemRepository,
            ICacheManager cacheManager,
            IEventPublisher eventPublisher)
        {
            _cacheManager = cacheManager;
            _eventPublisher = eventPublisher;
            _sidebarItemRepository = sidebarItemRepository;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Gets sidebar items collection
        /// </summary>
        /// <param name="sidebarId">Sidebar identifier</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <returns>Sidebar item collection</returns>
        public IPagedList<SidebarItem> GetColumnsHeadersBySidebarId(int sidebarId,
            int pageIndex = 0,
            int pageSize = int.MaxValue)
        {
            var query = from si in _sidebarItemRepository.Table
                        where ((si.RowType == RowType.textcolumn
                                || si.RowType == RowType.header
                                || si.RowType == RowType.imagecolumn
                                || si.RowType == RowType.title)
                            && si.SidebarId == sidebarId)
                        orderby si.DisplayOrder ascending, si.ColumnId ascending, si.Id ascending
                        select si;

            var items = new PagedList<SidebarItem>(query, pageIndex, pageSize);
            return items;
        }

        /// <summary>
        /// Gets sidebar items collection
        /// </summary>
        /// <param name="sidebarItem">Sidebar item</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <returns>Sidebar item collection</returns>
        public IPagedList<SidebarItem> GetRowsBySidebarItem(SidebarItem sidebarItem,
            int pageIndex = 0,
            int pageSize = int.MaxValue)
        {
            var query = from si in _sidebarItemRepository.Table
                        where (si.RowType == RowType.data
                            && si.SidebarId == sidebarItem.SidebarId
                            && si.ColumnId == sidebarItem.ColumnId)
                        orderby si.DisplayOrder ascending, si.ColumnId ascending, si.Id ascending
                        select si;

            var items = new PagedList<SidebarItem>(query, pageIndex, pageSize);
            return items;
        }

        /// <summary>
        /// Gets sidebar items collection
        /// </summary>
        /// <param name="sidebarId">Sidebar identifier</param>
        /// <returns>Columns</returns>
        public IList<SidebarItem> GetColumnsBySidebarId(int sidebarId)
        {
            var query = from si in _sidebarItemRepository.Table
                        where ((si.RowType == RowType.textcolumn
                            || si.RowType == RowType.imagecolumn)
                            && si.SidebarId == sidebarId)
                        orderby si.DisplayOrder ascending, si.ColumnId ascending, si.Id ascending
                        select si;

            var columns = query.ToList();
            return columns;
        }

        /// <summary>
        /// Inserts sidebar item
        /// </summary>
        /// <param name="sidebarItem">Sidebar item</param>
        public void InsertSidebarItem(SidebarItem sidebarItem)
        {
            if (sidebarItem == null)
                throw new ArgumentNullException(nameof(sidebarItem));

            if (sidebarItem.RowType == RowType.textcolumn
                || sidebarItem.RowType == RowType.header
                || sidebarItem.RowType == RowType.imagecolumn
                || sidebarItem.RowType == RowType.title)
            {
                var maxColumnId = _sidebarItemRepository.Table
                                        .Where(item => item.SidebarId == sidebarItem.SidebarId
                                            && (item.RowType == RowType.textcolumn
                                                || item.RowType == RowType.header
                                                || item.RowType == RowType.imagecolumn
                                                || item.RowType == RowType.title))
                                        .OrderByDescending(item => item.ColumnId)
                                        .Select(cc => cc.ColumnId)
                                        .DefaultIfEmpty(0)
                                        .First();

                sidebarItem.ColumnId = maxColumnId + 1;

                var maxDisplayOrder = _sidebarItemRepository.Table
                                        .Where(item => item.SidebarId == sidebarItem.SidebarId
                                            && (item.RowType == RowType.textcolumn
                                                || item.RowType == RowType.header
                                                || item.RowType == RowType.imagecolumn
                                                || item.RowType == RowType.title))
                                        .OrderByDescending(item => item.DisplayOrder)
                                        .Select(cc => cc.DisplayOrder)
                                        .DefaultIfEmpty(-1)
                                        .First();

                sidebarItem.DisplayOrder = maxDisplayOrder + 1;
            }

            _sidebarItemRepository.Insert(sidebarItem);

            //cache
            _cacheManager.RemoveByPattern(NopCatalogDefaults.SidebarsPatternCacheKey);
            _cacheManager.RemoveByPattern(NopCatalogDefaults.SidebarItemsPatternCacheKey);

            //event notification
            _eventPublisher.EntityInserted(sidebarItem);
        }

        /// <summary>
        /// Gets a sidebar item
        /// </summary>
        /// <param name="sidebarItemId">Sidebar item identifier</param>
        /// <returns>Sidebar item</returns>
        public SidebarItem GetSidebarItemById(int sidebarItemId)
        {
            if (sidebarItemId == 0)
                return null;

            var key = string.Format(NopCatalogDefaults.SidebarItemByIdCacheKey, sidebarItemId);
            return _cacheManager.Get(key, () => _sidebarItemRepository.GetById(sidebarItemId));
        }

        /// <summary>
        /// Delete a sidebar item
        /// </summary>
        /// <param name="connectionType">Row connection type</param>
        /// <param name="rowConnectionIds">Row connection identifiers</param>
        public void DeleteConnectionSidebarItem(RowConnectionType connectionType, IEnumerable<int> rowConnectionIds)
        {
            var items = (from item in _sidebarItemRepository.Table
                         where item.RowConnectionType == connectionType && rowConnectionIds.Contains(item.RowConnectionId.GetValueOrDefault())
                         select item).Distinct().ToList();
            var rowsToDelete = new List<SidebarItem>();

            foreach (var item in items.Where(m => m.RowType == RowType.textcolumn || m.RowType == RowType.imagecolumn))
            {
                var rows = _sidebarItemRepository.Table.Where(row => row.RowType == RowType.data && row.ColumnId == item.ColumnId).ToList();

                if (rows.Any())
                {
                    rowsToDelete.AddRange(rows);
                }
            }

            if (rowsToDelete.Any())
            {
                items.AddRange(rowsToDelete);
            }

            _sidebarItemRepository.Delete(items);

            foreach (var item in items)
            {
                //event notification
                _eventPublisher.EntityDeleted(item);
            }

            //cache
            _cacheManager.RemoveByPattern(NopCatalogDefaults.SidebarItemsPatternCacheKey);
            _cacheManager.RemoveByPattern(NopCatalogDefaults.SidebarsPatternCacheKey);
        }

        /// <summary>
        /// Delete a sidebar item
        /// </summary>
        /// <param name="sidebarItem">Sidebar item</param>
        public void DeleteSidebarItem(SidebarItem sidebarItem)
        {
            if (sidebarItem == null)
                throw new ArgumentNullException(nameof(sidebarItem));

            var itemsToDelete = new List<SidebarItem>
            {
                sidebarItem
            };

            //remove rows for column
            if (sidebarItem.RowType == RowType.textcolumn
                || sidebarItem.RowType == RowType.imagecolumn)
            {
                var rows = _sidebarItemRepository.Table.Where(row => row.RowType == RowType.data && row.ColumnId == sidebarItem.ColumnId).ToList();

                if (rows.Any())
                {
                    itemsToDelete.AddRange(rows);
                }
            }

            _sidebarItemRepository.Delete(itemsToDelete);

            foreach (var item in itemsToDelete)
            {
                //event notification
                _eventPublisher.EntityDeleted(item);
            }

            //cache
            _cacheManager.RemoveByPattern(NopCatalogDefaults.SidebarItemsPatternCacheKey);
            _cacheManager.RemoveByPattern(NopCatalogDefaults.SidebarsPatternCacheKey);
        }

        /// <summary>
        /// Inserts sidebar item rows
        /// </summary>
        /// <param name="selectedCategories">Selected categories</param>
        /// <param name="sidebarItemId">Sidebar item column identifier</param>
        /// <param name="sidebarId">Sidebar identifier</param>
        public void InsertCategoryRows(List<Category> selectedCategories, int sidebarItemId, int sidebarId)
        {
            if (selectedCategories == null)
                throw new ArgumentNullException(nameof(selectedCategories));

            if (sidebarItemId > 0)
            {
                InsertRows(selectedCategories.Select(c => c.Id), RowConnectionType.category, sidebarItemId);
            }
            else
            {
                InsertTitles(selectedCategories.Select(c => c.Id), RowConnectionType.category, sidebarId);
            }
        }

        /// <summary>
        /// Inserts sidebar item rows
        /// </summary>
        /// <param name="selectedProducts">Selected products</param>
        /// <param name="sidebarItemId">Sidebar item column identifier</param>
        /// <param name="sidebarId">Sidebar identifier</param>
        public void InsertProductRows(List<Product> selectedProducts, int sidebarItemId, int sidebarId)
        {
            if (selectedProducts == null)
                throw new ArgumentNullException(nameof(selectedProducts));

            if (sidebarItemId > 0)
            {
                InsertRows(selectedProducts.Select(c => c.Id), RowConnectionType.product, sidebarItemId);
            }
            else
            {
                InsertTitles(selectedProducts.Select(c => c.Id), RowConnectionType.product, sidebarId);
            }
        }

        /// <summary>
        /// Inserts sidebar item rows
        /// </summary>
        /// <param name="selectedElements">Selected elements</param>
        /// <param name="sidebarItemId">Sidebar item column identifier</param>
        /// <param name="sidebarId">Sidebar identifier</param>
        public void InsertElementRows(List<Element> selectedElements, int sidebarItemId, int sidebarId)
        {
            if (sidebarItemId > 0)
            {
                if (selectedElements == null)
                    throw new ArgumentNullException(nameof(selectedElements));

                var column = _sidebarItemRepository.GetById(sidebarItemId);
                if (column == null)
                    throw new ArgumentNullException(nameof(column));

                var maxDisplayOrder = _sidebarItemRepository.Table
                                            .Where(item => item.ColumnId == column.ColumnId
                                                    && item.RowType == RowType.data)
                                            .OrderByDescending(item => item.DisplayOrder)
                                            .Select(item => item.DisplayOrder)
                                            .DefaultIfEmpty(-1)
                                            .First();
                var rows = new List<SidebarItem>();
                var utcDate = DateTime.UtcNow;

                foreach (var element in selectedElements)
                {
                    maxDisplayOrder++;
                    rows.Add(new SidebarItem
                    {
                        ColumnId = column.ColumnId,
                        CreatedOnUtc = utcDate,
                        CssClass = GetElementRowClass(element),
                        DisplayOrder = maxDisplayOrder,
                        SidebarId = column.SidebarId,
                        RowConnectionId = element.Id,
                        RowConnectionType = RowConnectionType.element,
                        RowType = RowType.data,
                        UpdatedOnUtc = utcDate,
                        Visible = true
                    });
                }

                _sidebarItemRepository.Insert(rows);

                //cache
                _cacheManager.RemoveByPattern(NopCatalogDefaults.SidebarsPatternCacheKey);
                _cacheManager.RemoveByPattern(NopCatalogDefaults.SidebarItemsPatternCacheKey);

                //event notification
                foreach (var row in rows)
                {
                    //event notification
                    _eventPublisher.EntityInserted(row);
                }
            }
            else
            {
                InsertTitles(selectedElements.Select(c => c.Id), RowConnectionType.element, sidebarId);
            }
        }

        private string GetElementRowClass(Element element)
        {
            switch (element.ElementType)
            {
                case ElementType.Description:
                    return "sd_desc";
                case ElementType.Separator:
                    return "sd_sep";
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        /// <summary>
        /// Inserts sidebar item rows
        /// </summary>
        /// <param name="selectedTopics">Selected topics</param>
        /// <param name="sidebarItemId">Sidebar item column identifier</param>
        /// <param name="sidebarId">Sidebar identifier</param>
        public void InsertTopicsRows(List<Topic> selectedTopics, int sidebarItemId, int sidebarId)
        {
            if (selectedTopics == null)
                throw new ArgumentNullException(nameof(selectedTopics));

            if (sidebarItemId > 0)
            {
                InsertRows(selectedTopics.Select(c => c.Id), RowConnectionType.topic, sidebarItemId);
            }
            else
            {
                InsertTitles(selectedTopics.Select(c => c.Id), RowConnectionType.topic, sidebarId);
            }
        }

        private void InsertTitles(IEnumerable<int> ids, RowConnectionType connectionType, int sidebarId)
        {
            var maxColumnId = _sidebarItemRepository.Table
                                    .Where(item => item.SidebarId == sidebarId
                                        && (item.RowType == RowType.textcolumn
                                            || item.RowType == RowType.header
                                            || item.RowType == RowType.imagecolumn
                                            || item.RowType == RowType.title))
                                    .OrderByDescending(item => item.ColumnId)
                                    .Select(cc => cc.ColumnId)
                                    .DefaultIfEmpty(0)
                                    .First();

            var maxDisplayOrder = _sidebarItemRepository.Table
                                    .Where(item => item.SidebarId == sidebarId
                                        && (item.RowType == RowType.textcolumn
                                            || item.RowType == RowType.header
                                            || item.RowType == RowType.imagecolumn
                                            || item.RowType == RowType.title))
                                    .OrderByDescending(item => item.DisplayOrder)
                                    .Select(cc => cc.DisplayOrder)
                                    .DefaultIfEmpty(-1)
                                    .First();

            var titles = new List<SidebarItem>();
            var utcDate = DateTime.UtcNow;

            foreach (var id in ids)
            {
                maxDisplayOrder++;
                maxColumnId++;
                titles.Add(new SidebarItem
                {
                    ColumnId = maxColumnId,
                    CreatedOnUtc = utcDate,
                    CssClass = "sd_title",
                    DisplayOrder = maxDisplayOrder,
                    SidebarId = sidebarId,
                    RowConnectionId = id,
                    RowConnectionType = connectionType,
                    RowType = RowType.title,
                    UpdatedOnUtc = utcDate,
                    Visible = true
                });
            }

            _sidebarItemRepository.Insert(titles);

            //cache
            _cacheManager.RemoveByPattern(NopCatalogDefaults.SidebarsPatternCacheKey);
            _cacheManager.RemoveByPattern(NopCatalogDefaults.SidebarItemsPatternCacheKey);

            //event notification
            foreach (var row in titles)
            {
                //event notification
                _eventPublisher.EntityInserted(row);
            }
        }

        private void InsertRows(IEnumerable<int> ids, RowConnectionType connectionType, int sidebarItemId)
        {
            var column = _sidebarItemRepository.GetById(sidebarItemId);
            if (column == null)
                throw new ArgumentNullException(nameof(column));

            var maxDisplayOrder = _sidebarItemRepository.Table
                                        .Where(item => item.ColumnId == column.ColumnId
                                                && item.RowType == RowType.data)
                                        .OrderByDescending(item => item.DisplayOrder)
                                        .Select(item => item.DisplayOrder)
                                        .DefaultIfEmpty(-1)
                                        .First();
            var rows = new List<SidebarItem>();
            var utcDate = DateTime.UtcNow;

            foreach (var id in ids)
            {
                maxDisplayOrder++;
                rows.Add(new SidebarItem
                {
                    ColumnId = column.ColumnId,
                    CreatedOnUtc = utcDate,
                    CssClass = column.RowType == RowType.imagecolumn ? "sd_image": "sd_link",
                    DisplayOrder = maxDisplayOrder,
                    SidebarId = column.SidebarId,
                    RowConnectionId = id,
                    RowConnectionType = connectionType,
                    RowType = RowType.data,
                    UpdatedOnUtc = utcDate,
                    Visible = true
                });
            }

            _sidebarItemRepository.Insert(rows);

            //cache
            _cacheManager.RemoveByPattern(NopCatalogDefaults.SidebarsPatternCacheKey);
            _cacheManager.RemoveByPattern(NopCatalogDefaults.SidebarItemsPatternCacheKey);

            //event notification
            foreach (var row in rows)
            {
                //event notification
                _eventPublisher.EntityInserted(row);
            }
        }

        /// <summary>
        /// Updates the sidebar item
        /// </summary>
        /// <param name="sidebarItem">Sidebar item</param>
        public void UpdateSidebarItem(SidebarItem sidebarItem)
        {
            if (sidebarItem == null)
                throw new ArgumentNullException(nameof(sidebarItem));

            _sidebarItemRepository.Update(sidebarItem);

            //cache
            _cacheManager.RemoveByPattern(NopCatalogDefaults.SidebarsPatternCacheKey);
            _cacheManager.RemoveByPattern(NopCatalogDefaults.SidebarItemsPatternCacheKey);

            //event notification
            _eventPublisher.EntityUpdated(sidebarItem);
        }

        /// <summary>
        /// Reorder sidebar item
        /// </summary>
        /// <param name="newDisplayOrder">New display order</param>
        /// <param name="oldDisplayOrder">Old display order</param>
        /// <param name="dragItemId">Item that we are moving</param>
        /// <param name="isColumn">A value indicating whether drag item is column</param>
        public void ReOrderSidebarItem(int newDisplayOrder, int oldDisplayOrder, int dragItemId, bool isColumn)
        {
            if (newDisplayOrder == oldDisplayOrder)
                return;

            var item = _sidebarItemRepository.GetById(dragItemId);
            if (item == null)
                throw new ArgumentNullException(nameof(item));

            var query = _sidebarItemRepository.Table.Where(i => i.SidebarId == item.SidebarId);
            if (isColumn)
            {
                query = query.Where(i => i.RowType != RowType.data);
            }
            else
            {
                query = query.Where(i => i.RowType == RowType.data && i.ColumnId == item.ColumnId);
            }

            var items = query.OrderBy(i => i.DisplayOrder).ThenBy(i => i.ColumnId).ThenBy(i => i.Id).ToList();
            if (items[oldDisplayOrder].Id != item.Id)
                return;

            var displayOrder = -1;

            if (newDisplayOrder > oldDisplayOrder)
            {
                for (var i = 0; i <= newDisplayOrder; i++)
                {
                    var currentItem = items[i];
                    if (currentItem.Id == item.Id)
                    {
                        currentItem.DisplayOrder = newDisplayOrder;
                    }
                    else
                    {
                        displayOrder++;
                        currentItem.DisplayOrder = displayOrder;
                    }
                }

                for (var i = newDisplayOrder + 1; i < items.Count(); i++)
                {
                    items[i].DisplayOrder = i;
                }
            }
            else
            {
                for (var i = 0; i < newDisplayOrder; i++)
                {
                    items[i].DisplayOrder = i;
                }

                displayOrder = newDisplayOrder;
                for (var i = newDisplayOrder; i < items.Count(); i++)
                {
                    var currentItem = items[i];
                    if (currentItem.Id == item.Id)
                    {
                        currentItem.DisplayOrder = newDisplayOrder;
                    }
                    else
                    {
                        displayOrder++;
                        currentItem.DisplayOrder = displayOrder;
                    }
                }
            }

            _sidebarItemRepository.Update(items);

            //cache
            _cacheManager.RemoveByPattern(NopCatalogDefaults.SidebarsPatternCacheKey);
            _cacheManager.RemoveByPattern(NopCatalogDefaults.SidebarItemsPatternCacheKey);

            //event notification
            foreach (var itemUpdated in items)
            {
                //event notification
                _eventPublisher.EntityUpdated(itemUpdated);
            }
        }

        /// <summary>
        /// Gets sidebar items collection
        /// </summary>
        /// <param name="sidebarId">Sidebar identifier</param>
        /// <param name="visible">Show only visible</param>
        /// <returns>Sidebar items</returns>
        public IList<SidebarItem> GetItemsBySidebarId(int sidebarId, bool visible = true)
        {
            var query = _sidebarItemRepository.Table.Where(item => item.SidebarId == sidebarId);
            if (visible)
            {
                query = query.Where(item => item.Visible
                    && (!item.StartDateUtc.HasValue || DateTime.UtcNow >= item.StartDateUtc.Value)
                    && (!item.EndDateUtc.HasValue || DateTime.UtcNow < item.EndDateUtc.Value));
            }

            return query.OrderBy(item => item.ColumnId).ThenBy(item => item.DisplayOrder).ToList();
        }

        /// <summary>
        /// Inserts sidebar item rows
        /// </summary>
        /// <param name="selectedSliders">Selected sliders</param>
        /// <param name="sidebarItemId">Sidebar item column identifier</param>
        /// <param name="sidebarId">Sidebar identifier</param>
        public void InsertSliderRows(List<Slider> selectedSliders, int sidebarItemId, int sidebarId)
        {
            if (selectedSliders == null)
                throw new ArgumentNullException(nameof(selectedSliders));

            if (sidebarItemId > 0)
            {
                InsertRows(selectedSliders.Select(c => c.Id), RowConnectionType.slider, sidebarItemId);
            }
            else
            {
                InsertTitles(selectedSliders.Select(c => c.Id), RowConnectionType.slider, sidebarId);
            }
        }

        #endregion
    }
}