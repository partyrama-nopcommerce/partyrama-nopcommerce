using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Data;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Topics;
using Nop.Services.Events;

namespace Nop.Services.Catalog
{
    /// <summary>
    /// Slider item service
    /// </summary>
    public partial class SliderItemService: ISliderItemService
    {
        #region Fields

        private readonly IRepository<SliderItem> _sliderItemRepository;
        private readonly ICacheManager _cacheManager;
        private readonly IEventPublisher _eventPublisher;

        #endregion

        #region Ctor

        public SliderItemService(IRepository<SliderItem> sliderItemRepository,
            ICacheManager cacheManager,
            IEventPublisher eventPublisher)
        {
            _cacheManager = cacheManager;
            _eventPublisher = eventPublisher;
            _sliderItemRepository = sliderItemRepository;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Gets slider items collection
        /// </summary>
        /// <param name="sliderId">Slider identifier</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <returns>Slider item collection</returns>
        public IPagedList<SliderItem> GetColumnsHeadersBySliderId(int sliderId,
            int pageIndex = 0,
            int pageSize = int.MaxValue)
        {
            var query = from si in _sliderItemRepository.Table
                        where ((si.RowType == RowType.textcolumn
                                || si.RowType == RowType.header
                                || si.RowType == RowType.imagecolumn
                                || si.RowType == RowType.title)
                            && si.SliderId == sliderId)
                        orderby si.DisplayOrder ascending, si.ColumnId ascending, si.Id ascending
                        select si;

            var items = new PagedList<SliderItem>(query, pageIndex, pageSize);
            return items;
        }

        /// <summary>
        /// Gets slider items collection
        /// </summary>
        /// <param name="sliderItem">Slider item</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <returns>Slider item collection</returns>
        public IPagedList<SliderItem> GetRowsBySliderItem(SliderItem sliderItem,
            int pageIndex = 0,
            int pageSize = int.MaxValue)
        {
            var query = from si in _sliderItemRepository.Table
                        where (si.RowType == RowType.data
                            && si.SliderId == sliderItem.SliderId
                            && si.ColumnId == sliderItem.ColumnId)
                        orderby si.DisplayOrder ascending, si.ColumnId ascending, si.Id ascending
                        select si;

            var items = new PagedList<SliderItem>(query, pageIndex, pageSize);
            return items;
        }

        /// <summary>
        /// Gets slider items collection
        /// </summary>
        /// <param name="sliderId">Slider identifier</param>
        /// <returns>Columns</returns>
        public IList<SliderItem> GetColumnsBySliderId(int sliderId)
        {
            var query = from si in _sliderItemRepository.Table
                        where ((si.RowType == RowType.textcolumn
                            || si.RowType == RowType.imagecolumn)
                            && si.SliderId == sliderId)
                        orderby si.DisplayOrder ascending, si.ColumnId ascending, si.Id ascending
                        select si;

            var columns = query.ToList();
            return columns;
        }

        /// <summary>
        /// Inserts slider item
        /// </summary>
        /// <param name="sliderItem">Slider item</param>
        public void InsertSliderItem(SliderItem sliderItem)
        {
            if (sliderItem == null)
                throw new ArgumentNullException(nameof(sliderItem));

            if (sliderItem.RowType == RowType.textcolumn
                || sliderItem.RowType == RowType.header
                || sliderItem.RowType == RowType.imagecolumn
                || sliderItem.RowType == RowType.title)
            {
                var maxColumnId = _sliderItemRepository.Table
                                        .Where(item => item.SliderId == sliderItem.SliderId
                                            && (item.RowType == RowType.textcolumn
                                                || item.RowType == RowType.header
                                                || item.RowType == RowType.imagecolumn
                                                || item.RowType == RowType.title))
                                        .OrderByDescending(item => item.ColumnId)
                                        .Select(cc => cc.ColumnId)
                                        .DefaultIfEmpty(0)
                                        .First();

                sliderItem.ColumnId = maxColumnId + 1;

                var maxDisplayOrder = _sliderItemRepository.Table
                                        .Where(item => item.SliderId == sliderItem.SliderId
                                            && (item.RowType == RowType.textcolumn
                                                || item.RowType == RowType.header
                                                || item.RowType == RowType.imagecolumn
                                                || item.RowType == RowType.title))
                                        .OrderByDescending(item => item.DisplayOrder)
                                        .Select(cc => cc.DisplayOrder)
                                        .DefaultIfEmpty(-1)
                                        .First();

                sliderItem.DisplayOrder = maxDisplayOrder + 1;
            }

            _sliderItemRepository.Insert(sliderItem);

            //cache
            _cacheManager.RemoveByPattern(NopCatalogDefaults.SlidersPatternCacheKey);
            _cacheManager.RemoveByPattern(NopCatalogDefaults.SliderItemsPatternCacheKey);

            //event notification
            _eventPublisher.EntityInserted(sliderItem);
        }

        /// <summary>
        /// Gets a slider item
        /// </summary>
        /// <param name="sliderItemId">Slider item identifier</param>
        /// <returns>Slider item</returns>
        public SliderItem GetSliderItemById(int sliderItemId)
        {
            if (sliderItemId == 0)
                return null;

            var key = string.Format(NopCatalogDefaults.SliderItemByIdCacheKey, sliderItemId);
            return _cacheManager.Get(key, () => _sliderItemRepository.GetById(sliderItemId));
        }

        /// <summary>
        /// Delete a slider item
        /// </summary>
        /// <param name="connectionType">Row connection type</param>
        /// <param name="rowConnectionIds">Row connection identifiers</param>
        public void DeleteConnectionSliderItem(RowConnectionType connectionType, IEnumerable<int> rowConnectionIds)
        {
            var items = (from item in _sliderItemRepository.Table
                         where item.RowConnectionType == connectionType && rowConnectionIds.Contains(item.RowConnectionId.GetValueOrDefault())
                         select item).Distinct().ToList();
            var rowsToDelete = new List<SliderItem>();

            foreach (var item in items.Where(m => m.RowType == RowType.textcolumn || m.RowType == RowType.imagecolumn))
            {
                var rows = _sliderItemRepository.Table.Where(row => row.RowType == RowType.data && row.ColumnId == item.ColumnId).ToList();

                if (rows.Any())
                {
                    rowsToDelete.AddRange(rows);
                }
            }

            if (rowsToDelete.Any())
            {
                items.AddRange(rowsToDelete);
            }

            _sliderItemRepository.Delete(items);

            foreach (var item in items)
            {
                //event notification
                _eventPublisher.EntityDeleted(item);
            }

            //cache
            _cacheManager.RemoveByPattern(NopCatalogDefaults.SliderItemsPatternCacheKey);
            _cacheManager.RemoveByPattern(NopCatalogDefaults.SlidersPatternCacheKey);
        }

        /// <summary>
        /// Delete a slider item
        /// </summary>
        /// <param name="sliderItem">Slider item</param>
        public void DeleteSliderItem(SliderItem sliderItem)
        {
            if (sliderItem == null)
                throw new ArgumentNullException(nameof(sliderItem));

            var itemsToDelete = new List<SliderItem>
            {
                sliderItem
            };

            //remove rows for column
            if (sliderItem.RowType == RowType.textcolumn
                || sliderItem.RowType == RowType.imagecolumn)
            {
                var rows = _sliderItemRepository.Table.Where(row => row.RowType == RowType.data && row.ColumnId == sliderItem.ColumnId).ToList();

                if (rows.Any())
                {
                    itemsToDelete.AddRange(rows);
                }
            }

            _sliderItemRepository.Delete(itemsToDelete);

            foreach (var item in itemsToDelete)
            {
                //event notification
                _eventPublisher.EntityDeleted(item);
            }

            //cache
            _cacheManager.RemoveByPattern(NopCatalogDefaults.SliderItemsPatternCacheKey);
            _cacheManager.RemoveByPattern(NopCatalogDefaults.SlidersPatternCacheKey);
        }

        /// <summary>
        /// Inserts slider item rows
        /// </summary>
        /// <param name="selectedCategories">Selected categories</param>
        /// <param name="sliderItemId">Slider item column identifier</param>
        /// <param name="sliderId">Slider identifier</param>
        public void InsertCategoryRows(List<Category> selectedCategories, int sliderItemId, int sliderId)
        {
            if (selectedCategories == null)
                throw new ArgumentNullException(nameof(selectedCategories));

            if (sliderItemId > 0)
            {
                InsertRows(selectedCategories.Select(c => c.Id), RowConnectionType.category, sliderItemId);
            }
            else
            {
                InsertTitles(selectedCategories.Select(c => c.Id), RowConnectionType.category, sliderId);
            }
        }

        /// <summary>
        /// Inserts slider item rows
        /// </summary>
        /// <param name="selectedProducts">Selected products</param>
        /// <param name="sliderItemId">Slider item column identifier</param>
        /// <param name="sliderId">Slider identifier</param>
        public void InsertProductRows(List<Product> selectedProducts, int sliderItemId, int sliderId)
        {
            if (selectedProducts == null)
                throw new ArgumentNullException(nameof(selectedProducts));

            if (sliderItemId > 0)
            {
                InsertRows(selectedProducts.Select(c => c.Id), RowConnectionType.product, sliderItemId);
            }
            else
            {
                InsertTitles(selectedProducts.Select(c => c.Id), RowConnectionType.product, sliderId);
            }
        }

        /// <summary>
        /// Inserts slider item rows
        /// </summary>
        /// <param name="selectedElements">Selected elements</param>
        /// <param name="sliderItemId">Slider item column identifier</param>
        /// <param name="sliderId">Slider identifier</param>
        public void InsertElementRows(List<Element> selectedElements, int sliderItemId, int sliderId)
        {
            if (selectedElements == null)
                throw new ArgumentNullException(nameof(selectedElements));

            if (sliderItemId > 0)
            {
                var column = _sliderItemRepository.GetById(sliderItemId);
                if (column == null)
                    throw new ArgumentNullException(nameof(column));

                var maxDisplayOrder = _sliderItemRepository.Table
                                            .Where(item => item.ColumnId == column.ColumnId
                                                    && item.RowType == RowType.data)
                                            .OrderByDescending(item => item.DisplayOrder)
                                            .Select(item => item.DisplayOrder)
                                            .DefaultIfEmpty(-1)
                                            .First();
                var rows = new List<SliderItem>();
                var utcDate = DateTime.UtcNow;

                foreach (var element in selectedElements)
                {
                    maxDisplayOrder++;
                    rows.Add(new SliderItem
                    {
                        ColumnId = column.ColumnId,
                        CreatedOnUtc = utcDate,
                        CssClass = GetElementRowClass(element),
                        DisplayOrder = maxDisplayOrder,
                        SliderId = column.SliderId,
                        RowConnectionId = element.Id,
                        RowConnectionType = RowConnectionType.element,
                        RowType = RowType.data,
                        UpdatedOnUtc = utcDate,
                        Visible = true
                    });
                }

                _sliderItemRepository.Insert(rows);

                //cache
                _cacheManager.RemoveByPattern(NopCatalogDefaults.SlidersPatternCacheKey);
                _cacheManager.RemoveByPattern(NopCatalogDefaults.SliderItemsPatternCacheKey);

                //event notification
                foreach (var row in rows)
                {
                    //event notification
                    _eventPublisher.EntityInserted(row);
                }
            }
            else
            {
                InsertTitles(selectedElements.Select(c => c.Id), RowConnectionType.element, sliderId);
            }
        }

        private string GetElementRowClass(Element element)
        {
            switch (element.ElementType)
            {
                case ElementType.Description:
                    return "sl_desc";
                case ElementType.Separator:
                    return "sl_sep";
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        /// <summary>
        /// Inserts slider item rows
        /// </summary>
        /// <param name="selectedTopics">Selected topics</param>
        /// <param name="sliderItemId">Slider item column identifier</param>
        /// <param name="sliderId">Slider identifier</param>
        public void InsertTopicsRows(List<Topic> selectedTopics, int sliderItemId, int sliderId)
        {
            if (selectedTopics == null)
                throw new ArgumentNullException(nameof(selectedTopics));


            if (sliderItemId > 0)
            {
                InsertRows(selectedTopics.Select(c => c.Id), RowConnectionType.topic, sliderItemId);
            }
            else
            {
                InsertTitles(selectedTopics.Select(c => c.Id), RowConnectionType.topic, sliderId);
            }
        }

        private void InsertTitles(IEnumerable<int> ids, RowConnectionType connectionType, int sliderId)
        {
            var maxColumnId = _sliderItemRepository.Table
                                    .Where(item => item.SliderId == sliderId
                                        && (item.RowType == RowType.textcolumn
                                            || item.RowType == RowType.header
                                            || item.RowType == RowType.imagecolumn
                                            || item.RowType == RowType.title))
                                    .OrderByDescending(item => item.ColumnId)
                                    .Select(cc => cc.ColumnId)
                                    .DefaultIfEmpty(0)
                                    .First();

            var maxDisplayOrder = _sliderItemRepository.Table
                                    .Where(item => item.SliderId == sliderId
                                        && (item.RowType == RowType.textcolumn
                                            || item.RowType == RowType.header
                                            || item.RowType == RowType.imagecolumn
                                            || item.RowType == RowType.title))
                                    .OrderByDescending(item => item.DisplayOrder)
                                    .Select(cc => cc.DisplayOrder)
                                    .DefaultIfEmpty(-1)
                                    .First();

            var titles = new List<SliderItem>();
            var utcDate = DateTime.UtcNow;

            foreach (var id in ids)
            {
                maxDisplayOrder++;
                maxColumnId++;
                titles.Add(new SliderItem
                {
                    ColumnId = maxColumnId,
                    CreatedOnUtc = utcDate,
                    CssClass = "sl_title",
                    DisplayOrder = maxDisplayOrder,
                    SliderId = sliderId,
                    RowConnectionId = id,
                    RowConnectionType = connectionType,
                    RowType = RowType.title,
                    UpdatedOnUtc = utcDate,
                    Visible = true
                });
            }

            _sliderItemRepository.Insert(titles);

            //cache
            _cacheManager.RemoveByPattern(NopCatalogDefaults.SlidersPatternCacheKey);
            _cacheManager.RemoveByPattern(NopCatalogDefaults.SliderItemsPatternCacheKey);

            //event notification
            foreach (var row in titles)
            {
                //event notification
                _eventPublisher.EntityInserted(row);
            }
        }

        private void InsertRows(IEnumerable<int> ids, RowConnectionType connectionType, int sliderItemId)
        {
            var column = _sliderItemRepository.GetById(sliderItemId);
            if (column == null)
                throw new ArgumentNullException(nameof(column));

            var maxDisplayOrder = _sliderItemRepository.Table
                                        .Where(item => item.ColumnId == column.ColumnId
                                                && item.RowType == RowType.data)
                                        .OrderByDescending(item => item.DisplayOrder)
                                        .Select(item => item.DisplayOrder)
                                        .DefaultIfEmpty(-1)
                                        .First();
            var rows = new List<SliderItem>();
            var utcDate = DateTime.UtcNow;

            foreach (var id in ids)
            {
                maxDisplayOrder++;
                rows.Add(new SliderItem
                {
                    ColumnId = column.ColumnId,
                    CreatedOnUtc = utcDate,
                    CssClass = column.RowType == RowType.imagecolumn ? "sl_image": "sl_link",
                    DisplayOrder = maxDisplayOrder,
                    SliderId = column.SliderId,
                    RowConnectionId = id,
                    RowConnectionType = connectionType,
                    RowType = RowType.data,
                    UpdatedOnUtc = utcDate,
                    Visible = true
                });
            }

            _sliderItemRepository.Insert(rows);

            //cache
            _cacheManager.RemoveByPattern(NopCatalogDefaults.SlidersPatternCacheKey);
            _cacheManager.RemoveByPattern(NopCatalogDefaults.SliderItemsPatternCacheKey);

            //event notification
            foreach (var row in rows)
            {
                //event notification
                _eventPublisher.EntityInserted(row);
            }
        }

        /// <summary>
        /// Updates the slider item
        /// </summary>
        /// <param name="sliderItem">Slider item</param>
        public void UpdateSliderItem(SliderItem sliderItem)
        {
            if (sliderItem == null)
                throw new ArgumentNullException(nameof(sliderItem));

            _sliderItemRepository.Update(sliderItem);

            //cache
            _cacheManager.RemoveByPattern(NopCatalogDefaults.SlidersPatternCacheKey);
            _cacheManager.RemoveByPattern(NopCatalogDefaults.SliderItemsPatternCacheKey);

            //event notification
            _eventPublisher.EntityUpdated(sliderItem);
        }

        /// <summary>
        /// Reorder slider item
        /// </summary>
        /// <param name="newDisplayOrder">New display order</param>
        /// <param name="oldDisplayOrder">Old display order</param>
        /// <param name="dragItemId">Item that we are moving</param>
        /// <param name="isColumn">A value indicating whether drag item is column</param>
        public void ReOrderSliderItem(int newDisplayOrder, int oldDisplayOrder, int dragItemId, bool isColumn)
        {
            if (newDisplayOrder == oldDisplayOrder)
                return;

            var item = _sliderItemRepository.GetById(dragItemId);
            if (item == null)
                throw new ArgumentNullException(nameof(item));

            var query = _sliderItemRepository.Table.Where(i => i.SliderId == item.SliderId);
            if (isColumn)
            {
                query = query.Where(i => i.RowType != RowType.data);
            }
            else
            {
                query = query.Where(i => i.RowType == RowType.data && i.ColumnId == item.ColumnId);
            }

            var items = query.OrderBy(i => i.DisplayOrder).ThenBy(i => i.ColumnId).ThenBy(i => i.Id).ToList();
            if (items[oldDisplayOrder].Id != item.Id)
                return;

            var displayOrder = -1;

            if (newDisplayOrder > oldDisplayOrder)
            {
                for (var i = 0; i <= newDisplayOrder; i++)
                {
                    var currentItem = items[i];
                    if (currentItem.Id == item.Id)
                    {
                        currentItem.DisplayOrder = newDisplayOrder;
                    }
                    else
                    {
                        displayOrder++;
                        currentItem.DisplayOrder = displayOrder;
                    }
                }

                for (var i = newDisplayOrder + 1; i < items.Count(); i++)
                {
                    items[i].DisplayOrder = i;
                }
            }
            else
            {
                for (var i = 0; i < newDisplayOrder; i++)
                {
                    items[i].DisplayOrder = i;
                }

                displayOrder = newDisplayOrder;
                for (var i = newDisplayOrder; i < items.Count(); i++)
                {
                    var currentItem = items[i];
                    if (currentItem.Id == item.Id)
                    {
                        currentItem.DisplayOrder = newDisplayOrder;
                    }
                    else
                    {
                        displayOrder++;
                        currentItem.DisplayOrder = displayOrder;
                    }
                }
            }

            _sliderItemRepository.Update(items);

            //cache
            _cacheManager.RemoveByPattern(NopCatalogDefaults.SlidersPatternCacheKey);
            _cacheManager.RemoveByPattern(NopCatalogDefaults.SliderItemsPatternCacheKey);

            //event notification
            foreach (var itemUpdated in items)
            {
                //event notification
                _eventPublisher.EntityUpdated(itemUpdated);
            }
        }

        /// <summary>
        /// Gets slider items collection
        /// </summary>
        /// <param name="sliderId">Slider identifier</param>
        /// <param name="visible">Show only visible</param>
        /// <returns>Slider items</returns>
        public IList<SliderItem> GetItemsBySliderId(int sliderId, bool visible = true)
        {
            var query = _sliderItemRepository.Table.Where(item => item.SliderId == sliderId);
            if (visible)
            {
                query = query.Where(item => item.Visible
                    && (!item.StartDateUtc.HasValue || DateTime.UtcNow >= item.StartDateUtc.Value)
                    && (!item.EndDateUtc.HasValue || DateTime.UtcNow < item.EndDateUtc.Value));
            }

            return query.OrderBy(item => item.ColumnId).ThenBy(item => item.DisplayOrder).ToList();
        }

        #endregion
    }
}