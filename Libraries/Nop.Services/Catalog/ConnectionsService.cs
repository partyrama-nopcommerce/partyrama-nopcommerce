using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Data;
using Nop.Core.Domain.Catalog;
using Nop.Data;
using Nop.Services.Events;

namespace Nop.Services.Catalog
{
    /// <summary>
    /// Connections service
    /// </summary>
    public partial class ConnectionsService : IConnectionsService
    {
        #region Fields

        private readonly IDbContext _dbContext;
        private readonly IDataProvider _dataProvider;
        private readonly IStoreContext _storeContext;
        private readonly ICacheManager _cacheManager;
        private readonly IWorkContext _workContext;
        private readonly IRepository<Connections> _connectionsRepository;
        private readonly IEventPublisher _eventPublisher;
        private readonly string _entityName;

        #endregion

        #region Ctor

        public ConnectionsService(
            IDbContext dbContext,
            IDataProvider dataProvider,
            IWorkContext workContext,
            IStoreContext storeContext,
            ICacheManager cacheManager,
            IEventPublisher eventPublisher,
            IRepository<Connections> connectionsRepository,
            IRepository<Category> categoryRepository)
        {
            _dbContext = dbContext;
            _dataProvider = dataProvider;
            _workContext = workContext;
            _storeContext = storeContext;
            _cacheManager = cacheManager;
            _connectionsRepository = connectionsRepository;
            _eventPublisher = eventPublisher;
            _entityName = typeof(Connections).Name;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Delete a connection
        /// </summary>
        /// <param name="connections">Connections</param>
        public void DeleteConnection(Connections connection)
        {
            if (connection == null)
                throw new ArgumentNullException(nameof(connection));

            _connectionsRepository.Delete(connection);

            //cache
            _cacheManager.RemoveByPattern(NopCatalogDefaults.ConnectionsPatternCacheKey);

            //event notification
            _eventPublisher.EntityDeleted(connection);
        }

        public void DeleteConnections(IList<Connections> connections)
        {
            throw new NotImplementedException();
        }

        public void InsertConnection(Connections connection)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets connections mapping collection
        /// </summary>
        /// <param name="categoryId">Category identifier</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <returns>Connections mapping collection</returns>
        public virtual IPagedList<Connections> GetConnectionsByCategoryId(int categoryId,
            int pageIndex = 0,
            int pageSize = int.MaxValue)
        {
            var key = string.Format(NopCatalogDefaults.ConnectionsByCategoryIdCacheKey, categoryId, pageIndex, pageSize, _workContext.CurrentCustomer.Id, _storeContext.CurrentStore.Id);
            return _cacheManager.Get(key, () =>
            {
                var query = from cc in _connectionsRepository.Table
                            where (cc.ConnectionFromType == ConnectionFromType.cat || cc.ConnectionFromType == ConnectionFromType.tempCat)
                                && cc.From == categoryId
                            orderby cc.DisplayOrder, cc.Id
                            select cc;

                var connections = new PagedList<Connections>(query, pageIndex, pageSize);
                return connections;
            });
        }

        /// <summary>
        /// Gets connections collection
        /// </summary>
        /// <param name="productId">Product identifier</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <returns>Connections collection</returns>
        public virtual IPagedList<Connections> GetConnectionsByProductId(int productId,
            int pageIndex = 0,
            int pageSize = int.MaxValue)
        {
            var key = string.Format(NopCatalogDefaults.ConnectionsByProductIdCacheKey, productId, pageIndex, pageSize, _workContext.CurrentCustomer.Id, _storeContext.CurrentStore.Id);
            return _cacheManager.Get(key, () =>
            {
                var query = from cc in _connectionsRepository.Table
                            where (cc.ConnectionFromType == ConnectionFromType.prod || cc.ConnectionFromType == ConnectionFromType.tempProd)
                                && cc.From == productId
                            orderby cc.DisplayOrder, cc.Id
                            select cc;

                var connections = new PagedList<Connections>(query, pageIndex, pageSize);
                return connections;
            });
        }

        /// <summary>
        /// Gets a connection
        /// </summary>
        /// <param name="connectionId">Connection identifier</param>
        /// <returns>Connection</returns>
        public virtual Connections GetConnectionById(int connectionId)
        {
            if (connectionId == 0)
                return null;

            var key = string.Format(NopCatalogDefaults.ConnectionsByIdCacheKey, connectionId);
            return _cacheManager.Get(key, () => _connectionsRepository.GetById(connectionId));
        }

        /// <summary>
        /// Updates the connection
        /// </summary>
        /// <param name="connections">Connections</param>
        public void UpdateConnection(Connections connection)
        {
            if (connection == null)
                throw new ArgumentNullException(nameof(connection));

            _connectionsRepository.Update(connection);

            //cache
            _cacheManager.RemoveByPattern(NopCatalogDefaults.ConnectionsPatternCacheKey);

            //event notification
            _eventPublisher.EntityUpdated(connection);
        }

        /// <summary>
        /// Updates the product category mapping 
        /// </summary>
        /// <param name="productCategory">>Product category mapping</param>
        public void UpdateConnections(IList<Connections> connections)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Reorder connections
        /// </summary>
        /// <param name="numberPositions">Number position to move</param>
        /// <param name="fromId">Product or category identifier</param>
        /// <param name="dragItemId">Item that we are moving</param>
        /// <param name="dropItemId">Item to which we are dropping</param>
        public void ReOrderConnections(int numberPositions, int fromId, int dropItemId, int dragItemId)
        {
            if (numberPositions == 0)
                return;

            IList<Connections> connectionsToUpdate = null;
            var dragItem = _connectionsRepository.GetById(dragItemId);
            var dropItem = _connectionsRepository.GetById(dropItemId);
            var fromTypeTemp = dragItem.ConnectionFromType == ConnectionFromType.cat ? ConnectionFromType.tempCat : ConnectionFromType.tempProd;

            if (numberPositions > 0)
            {
                var query = from cc in _connectionsRepository.Table
                            where cc.From == fromId
                                && cc.DisplayOrder >= dragItem.DisplayOrder
                                && cc.DisplayOrder <= dropItem.DisplayOrder
                                && cc.ConnectionFromType == dragItem.ConnectionFromType
                            orderby cc.DisplayOrder ascending, cc.Id ascending
                            select cc;

                connectionsToUpdate = query.ToList();
                connectionsToUpdate[0].DisplayOrder = dropItem.DisplayOrder;
                connectionsToUpdate[0].ConnectionFromType = fromTypeTemp;
                for (int i = 1; i < connectionsToUpdate.Count; i++)
                {
                    connectionsToUpdate[i].DisplayOrder--;
                    connectionsToUpdate[i].ConnectionFromType = fromTypeTemp;
                }
            }
            else
            {
                var query = from cc in _connectionsRepository.Table
                            where cc.From == fromId
                                && cc.DisplayOrder <= dragItem.DisplayOrder
                                && cc.DisplayOrder >= dropItem.DisplayOrder
                                && cc.ConnectionFromType == dragItem.ConnectionFromType
                            orderby cc.DisplayOrder descending, cc.Id descending
                            select cc;

                connectionsToUpdate = query.ToList();
                connectionsToUpdate[0].DisplayOrder = dropItem.DisplayOrder;
                connectionsToUpdate[0].ConnectionFromType = fromTypeTemp;
                for (int i = 1; i < connectionsToUpdate.Count; i++)
                {
                    connectionsToUpdate[i].DisplayOrder++;
                    connectionsToUpdate[i].ConnectionFromType = fromTypeTemp;
                }
            }

            //update
            _connectionsRepository.Update(connectionsToUpdate);
            for (int i = 0; i < connectionsToUpdate.Count; i++)
            {
                connectionsToUpdate[i].ConnectionFromType = fromTypeTemp == ConnectionFromType.tempCat ? ConnectionFromType.cat : ConnectionFromType.prod;
            }

            _connectionsRepository.Update(connectionsToUpdate);

            //cache
            _cacheManager.RemoveByPattern(NopCatalogDefaults.ConnectionsPatternCacheKey);

            //event notification
            foreach (var connection in connectionsToUpdate)
            {
                _eventPublisher.EntityUpdated(connection);
            }
        }

        /// <summary>
        /// Gets product identifiers connect with object.
        /// </summary>
        /// <param name="fromId">Object identifier (product or category)</param>
        /// <param name="fromType">Connection type</param>
        /// <param name="limit">Number of items to read</param>
        /// <param name="toIdExcluded">Object identifier to excluded</param>
        /// <returns></returns>
        public int[] GetProductConnectionsByObjectId(int fromId, ConnectionFromType fromType, int? limit = null, int? toIdExcluded = null)
        {
            return GetConnectionsIdsByObjectId(fromId, fromType, ConnectionToType.prod, limit);
        }

        /// <summary>
        /// Gets category identifiers connect with object.
        /// </summary>
        /// <param name="fromId">Object identifier (product or category)</param>
        /// <param name="fromType">Connection type</param>
        /// <param name="limit">Number of items to read</param>
        /// <param name="toIdExcluded">Object identifier to excluded</param>
        /// <returns></returns>
        public int[] GetCategoryConnectionsByObjectId(int fromId, ConnectionFromType fromType, int? limit = null, int? toIdExcluded = null)
        {
            return GetConnectionsIdsByObjectId(fromId, fromType, ConnectionToType.cat, limit, toIdExcluded);
        }

        private int[] GetConnectionsIdsByObjectId(int fromId, ConnectionFromType fromType, ConnectionToType toType, int? limit = null, int? toIdExcluded = null)
        {
            var tempType = fromType == ConnectionFromType.cat ? ConnectionFromType.tempCat : ConnectionFromType.tempProd;

            var query = _connectionsRepository.Table.Where(cc => (cc.ConnectionFromType == fromType || cc.ConnectionFromType == tempType)
                            && cc.ConnectionToType == toType
                            && cc.From == fromId);

            if (toIdExcluded.HasValue)
            {
                query = query.Where(cc => cc.To != toIdExcluded);
            }

            if (limit.HasValue)
            {
                query = query.Take(limit.Value);
            }

            return query.Select(cc => cc.To).ToArray();
        }

        /// <summary>
        /// Gets element identifiers connect with object.
        /// </summary>
        /// <param name="fromId">Object identifier (product or category)</param>
        /// <param name="fromType">Connection from type</param>
        /// <param name="toType">Connection to Type</param>
        /// <param name="limit">Number of items to read</param>
        /// <param name="toIdExcluded">Object identifier to excluded</param>
        /// <returns></returns>
        public int[] GetElementConnectionsByObjectId(int fromId, ConnectionFromType fromType, ConnectionToType toType, int? limit = null,int ? toIdExcluded = null)
        {
            return GetConnectionsIdsByObjectId(fromId, fromType, toType, limit, toIdExcluded);
        }

        /// <summary>
        /// Gets dictionary, key - product identifiers connect with object, value - display index.
        /// </summary>
        /// <param name="fromId">Object identifier (product or category)</param>
        /// <param name="fromType">Connection type</param>
        /// <param name="limit">Number of items to read</param>
        /// <param name="toIdExcluded">Object identifier to excluded</param>
        /// <returns></returns>
        public Dictionary<int, int> GetProductIdsWithIndexByObjectId(int fromId, ConnectionFromType fromType, int? limit = null, int? toIdExcluded = null)
        {
            var tempType = fromType == ConnectionFromType.cat ? ConnectionFromType.tempCat : ConnectionFromType.tempProd;

            var query = _connectionsRepository.Table.Where(cc => (cc.ConnectionFromType == fromType || cc.ConnectionFromType == tempType)
                            && cc.ConnectionToType == ConnectionToType.prod
                            && cc.From == fromId);

            if (toIdExcluded.HasValue)
            {
                query = query.Where(cc => cc.To != toIdExcluded);
            }

            if (limit.HasValue)
            {
                query = query.Take(limit.Value);
            }

            return query.ToDictionary(cc => cc.To, cc => cc.DisplayOrder);
        }

        /// <summary>
        /// Inserts connections
        /// </summary>
        /// <param name="connectionsToInsert">Connections</param>
        public void InsertConnections(List<Connections> connectionsToInsert)
        {
            if (connectionsToInsert == null)
                throw new ArgumentNullException(nameof(connectionsToInsert));

            var fromType = connectionsToInsert[0].ConnectionFromType;
            var tempType = fromType == ConnectionFromType.cat ? ConnectionFromType.tempCat : ConnectionFromType.tempProd;
            var fromId = connectionsToInsert[0].From;

            var maxDisplayOrder = _connectionsRepository.Table
                                        .Where(cc => (cc.ConnectionFromType == fromType || cc.ConnectionFromType == fromType)
                                                && cc.From == fromId)
                                        .OrderByDescending(cc => cc.DisplayOrder)
                                        .Select(cc => cc.DisplayOrder)
                                        .DefaultIfEmpty(-1)
                                        .First();

            foreach(var conn in connectionsToInsert)
            {
                maxDisplayOrder++;
                conn.DisplayOrder = maxDisplayOrder;
            }

            _connectionsRepository.Insert(connectionsToInsert);

            //cache
            _cacheManager.RemoveByPattern(NopCatalogDefaults.ConnectionsPatternCacheKey);

            //event notification
            foreach (var conn in connectionsToInsert)
            {
                //event notification
                _eventPublisher.EntityInserted(conn);
            }
        }

        /// <summary>
        /// Gets connections count from object
        /// </summary>
        /// <param name="fromIds">Categories or products identifiers count</param>
        /// <param name="fromType">Connection from type</param>
        /// <returns></returns>
        public Dictionary<int, int> GetConnectionsCount(int[] fromIds, ConnectionFromType fromType)
        {
            var tempType = fromType == ConnectionFromType.cat ? ConnectionFromType.tempCat : ConnectionFromType.tempProd;

            var query = _connectionsRepository.Table
                .Where(cc => fromIds.Contains(cc.From)
                            && (cc.ConnectionFromType == fromType || cc.ConnectionFromType == tempType))
                .GroupBy(cc => cc.From);
            
            return query.ToDictionary(cc => cc.Key, cc => cc.Count());
        }

        /// <summary>
        /// Gets slider identifiers connect with object.
        /// </summary>
        /// <param name="fromId">Object identifier (product or category)</param>
        /// <param name="fromType">Connection type</param>
        /// <param name="limit">Number of items to read</param>
        /// <param name="toIdExcluded">Object identifier to excluded</param>
        /// <returns></returns>
        public int[] GetSliderConnectionsByObjectId(int fromId, ConnectionFromType fromType, int? limit = null, int? toIdExcluded = null)
        {
            return GetConnectionsIdsByObjectId(fromId, fromType, ConnectionToType.slider, limit);
        }

        #endregion Methods
    }
}