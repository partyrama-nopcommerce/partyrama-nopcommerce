﻿using Nop.Core.Domain.Orders;
using Nop.Services.Localization;
using System;
using System.Net.Http;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Nop.Services.Payments.WorldPay
{
    public class WorldPayService: IWorldPayService
    {
        #region Fields

        private readonly ILocalizationService _localizationService;

        #endregion

        #region Ctor

        public WorldPayService(ILocalizationService localizationService)
        {
            _localizationService = localizationService;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Sends information to the payment system and receives reply
        /// </summary>
        /// <param name="order">The order</param>
        /// <returns>WorldPay reply</returns>
        public WorldPayModel Response(Order order)
        {
            var test = new WorldPayModel()
            {
                Version = WorldPayServiceVersion.Version14,
                Submit = new Submit
                {
                    Order = new OrderPay
                    {
                        Amount = new Amount
                        {
                            CurrencyCode = order.CustomerCurrencyCode,
                            Exponent = AmountExponent.Exponent2,
                            Value = ((int)(order.OrderTotal * 100)).ToString()
                        },
                        InstallationId = "298986",
                        OrderCode = order.OrderGuid.ToString(),
                        Description = _localizationService.GetResource("WorldPay.OrderNumber") + $": {order.CustomOrderNumber}" ,
                        PaymentMethodMask = new PaymentMethodMask
                        {
                            Include = new Include
                            {
                                Code = "ALL"
                            }
                        },
                        BillingAddress = new BillingAddress
                        {
                            Address = new AddressPay
                            {
                                City = order.BillingAddress.City,
                                FirstName = order.BillingAddress.FirstName,
                                LastName = order.BillingAddress.LastName,
                                Address1 = order.BillingAddress.Address1,
                                Address2 = order.BillingAddress.Address2,
                                CountryCode = order.BillingAddress.Country.TwoLetterIsoCode,
                                PostalCode = order.BillingAddress.ZipPostalCode,
                                TelephoneNumber = order.BillingAddress.PhoneNumber
                            }
                        },
                        ShippingAddress = new ShippingAddress
                        {
                            Address = new AddressPay
                            {
                                City = order.ShippingAddress.City,
                                FirstName = order.ShippingAddress.FirstName,
                                LastName = order.ShippingAddress.LastName,
                                Address1 = order.ShippingAddress.Address1,
                                Address2 = order.ShippingAddress.Address2,
                                CountryCode = order.ShippingAddress.Country.TwoLetterIsoCode,
                                PostalCode = order.ShippingAddress.ZipPostalCode,
                                TelephoneNumber = order.ShippingAddress.PhoneNumber
                            }
                        }
                    }
                },
                MerchantCode = "TUTTOPERUNPARTYIT"
            };

            var stringWriter = new Utf8StringWriter();
            var xmlSerializer = new XmlSerializer(typeof(WorldPayModel));

            using (XmlWriter writer = XmlWriter.Create(stringWriter))
            {
                writer.WriteDocType("paymentService", "-//Worldpay//DTD Worldpay PaymentService v1//EN", "http://dtd.worldpay.com/paymentService_v1.dtd", null);
                xmlSerializer.Serialize(writer, test);
            }

            var requestContent = new StringContent(stringWriter.ToString());
            var client = new HttpClient();
            var byteArray = Encoding.ASCII.GetBytes("TUTTOPERUNPARTYIT:bata6489");
            client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));

            var result = client.PostAsync("https://secure-test.worldpay.com/jsp/merchant/xml/paymentService.jsp", requestContent)
                               .Result;

            var model = (WorldPayModel)xmlSerializer.Deserialize(result.Content.ReadAsStreamAsync().Result);

            return model;
        }

        #endregion
    }
}
