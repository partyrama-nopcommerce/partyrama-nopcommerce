using System.Collections.Generic;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Orders;

namespace Nop.Services.Payments.WorldPay
{
    /// <summary>
    /// WorldPay service interface
    /// </summary>
    public partial interface IWorldPayService
    {
        /// <summary>
        /// Sends information to the payment system and receives reply
        /// </summary>
        /// <param name="order">The order</param>
        /// <returns>WorldPay reply</returns>
        WorldPayModel Response(Order order);
    }
}