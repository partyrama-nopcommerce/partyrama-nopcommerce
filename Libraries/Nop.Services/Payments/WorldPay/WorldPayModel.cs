﻿using System;
using System.Xml.Serialization;

namespace Nop.Services.Payments.WorldPay
{
    /// <remarks/>
    [Serializable()]
    [XmlType(AnonymousType = true)]
    [XmlRoot(IsNullable = false, ElementName = "paymentService")]
    public class WorldPayModel
    {
        /// <remarks/>
        [XmlAttribute(AttributeName = "version")]
        public WorldPayServiceVersion Version { get; set; }

        /// <remarks/>
        [XmlAttribute(DataType = "NMTOKEN", AttributeName = "merchantCode")]
        public string MerchantCode { get; set; }

        [XmlElement(ElementName = "submit")]
        public Submit Submit { get; set; }

        [XmlElement(ElementName = "reply")]
        public Reply Reply { get; set; }
    }

    /// <remarks/>
    [Serializable()]
    [XmlType(AnonymousType = true)]
    [XmlRoot(IsNullable = false, ElementName = "reply")]
    public class Reply
    {
        /// <remarks/>
        [XmlElement(ElementName = "orderStatus")]
        public OrderStatus OrderStatus { get; set; }

        /// <remarks/>
        [XmlElement(ElementName = "error")]
        public Error Error { get; set; }
    }

    /// <remarks/>
    [Serializable()]
    [XmlType(AnonymousType = true)]
    [XmlRoot(IsNullable = false, ElementName = "submit")]
    public class Submit
    {
        /// <remarks/>
        [XmlElement(ElementName = "order")]
        public OrderPay Order { get; set; }
    }

    /// <remarks/>
    [Serializable()]
    [XmlType(AnonymousType = true)]
    [XmlRoot(IsNullable = false, ElementName = "order")]
    public class OrderPay
    {
        /// <remarks/>
        [XmlAttribute(AttributeName = "orderCode")]
        public string OrderCode { get; set; }

        /// <remarks/>
        [XmlAttribute(DataType = "NMTOKEN", AttributeName = "installationId")]
        public string InstallationId { get; set; }

        /// <remarks/>
        [XmlElement(ElementName = "description", Order = 0)]
        public string Description { get; set; }

        /// <remarks/>
        [XmlElement(ElementName = "amount", Order = 1)]
        public Amount Amount { get; set; }

        /// <remarks/>
        [XmlElement(ElementName = "paymentMethodMask", Order = 2)]
        public PaymentMethodMask PaymentMethodMask { get; set; }

        /// <remarks/>
        [XmlElement(ElementName = "shopper", Order = 3)]
        public Shopper Shopper { get; set; }

        /// <remarks/>
        [XmlElement(ElementName = "shippingAddress", Order = 4)]
        public ShippingAddress ShippingAddress { get; set; }

        /// <remarks/>
        [XmlElement(ElementName = "billingAddress", Order = 5)]
        public BillingAddress BillingAddress { get; set; }
    }

    [Serializable()]
    [XmlType(AnonymousType = true)]
    [XmlRoot(IsNullable = false, ElementName = "amount")]
    public class Amount
    {
        [XmlAttribute(DataType = "NMTOKEN", AttributeName = "value")]
        public string Value { get; set; }

        /// <remarks/>
        [XmlAttribute(DataType = "NMTOKEN", AttributeName = "currencyCode")]
        public string CurrencyCode { get; set; }

        /// <remarks/>
        [XmlAttribute(AttributeName = "exponent")]
        public AmountExponent Exponent { get; set; }
    }

    [Serializable()]
    [XmlType(AnonymousType = true)]
    [XmlRoot(IsNullable = false, ElementName = "shopper")]
    public class Shopper
    {
        /// <remarks/>
        [XmlElement(ElementName = "shopperEmailAddress")]
        public string ShopperEmailAddress { get; set; }
    }

    [Serializable()]
    [XmlType(AnonymousType = true)]
    [XmlRoot(IsNullable = false, ElementName = "paymentMethodMask")]
    public class PaymentMethodMask
    {
        [XmlElement(ElementName = "include")]
        public Include Include { get; set; }
    }

    [Serializable()]
    [XmlType(AnonymousType = true)]
    [XmlRoot(IsNullable = false, ElementName = "include")]
    public class Include
    {
        /// <remarks/>
        [XmlAttribute(DataType = "NMTOKEN", AttributeName = "code")]
        public string Code { get; set; }
    }

    [Serializable()]
    [XmlType(AnonymousType = true)]
    [XmlRoot(IsNullable = false, ElementName = "billingAddress")]
    public partial class BillingAddress
    {
        /// <remarks/>
        [XmlElement(ElementName = "address")]
        public AddressPay Address { get; set; }
    }

    [Serializable()]
    [XmlType(AnonymousType = true)]
    [XmlRoot(IsNullable = false, ElementName = "shippingAddress")]
    public partial class ShippingAddress
    {
        /// <remarks/>
        [XmlElement(ElementName = "address")]
        public AddressPay Address { get; set; }
    }

    [Serializable()]
    [XmlType(AnonymousType = true)]
    [XmlRoot(IsNullable = false, ElementName = "address")]
    public class AddressPay
    {
        /// <remarks/>
        [XmlElement(ElementName = "firstName")]
        public string FirstName { get; set; }

        /// <remarks/>
        [XmlElement(ElementName = "lastName")]
        public string LastName { get; set; }

        /// <remarks/>
        [XmlElement(ElementName = "address1")]
        public string Address1 { get; set; }

        /// <remarks/>
        [XmlElement(ElementName = "address2")]
        public string Address2 { get; set; }

        /// <remarks/>
        [XmlElement(ElementName = "address3")]
        public string Address3 { get; set; }

        /// <remarks/>
        [XmlElement(ElementName = "houseName")]
        public string HouseName { get; set; }

        /// <remarks/>
        [XmlElement(ElementName = "houseNumber")]
        public string HouseNumber { get; set; }

        /// <remarks/>
        [XmlElement(ElementName = "houseNumberExtension")]
        public string HouseNumberExtension { get; set; }

        /// <remarks/>
        [XmlElement(ElementName = "street")]
        public string Street { get; set; }

        /// <remarks/>
        [XmlElement(ElementName = "postalCode")]
        public string PostalCode { get; set; }

        /// <remarks/>
        [XmlElement(ElementName = "city")]
        public string City { get; set; }

        /// <remarks/>
        [XmlElement(ElementName = "state")]
        public string State { get; set; }

        /// <remarks/>
        [XmlElement(ElementName = "countryCode")]
        public string CountryCode { get; set; }

        /// <remarks/>
        [XmlElement(ElementName = "telephoneNumber")]
        public string TelephoneNumber { get; set; }
    }

    /// <remarks/>
    [Serializable()]
    [XmlType(AnonymousType = true)]
    [XmlRoot(IsNullable = false, ElementName = "orderStatus")]
    public class OrderStatus
    {
        /// <remarks/>
        [XmlElement(ElementName = "reference")]
        public Reference Reference { get; set; }

        /// <remarks/>
        [XmlAttribute(AttributeName = "orderCode")]
        public string OrderCode { get; set; }
    }

    [Serializable()]
    [XmlType(AnonymousType = true)]
    [XmlRoot(IsNullable = false, ElementName = "reference")]
    public class Reference
    {
        /// <remarks/>
        [XmlAttribute(DataType = "NMTOKEN", AttributeName = "id")]
        public string Id { get; set; }

        /// <remarks/>
        [XmlText()]
        public string Value { get; set; }
    }

    /// <remarks/>
    [Serializable()]
    [XmlType(AnonymousType = true)]
    [XmlRoot(IsNullable = false, ElementName = "error")]
    public class Error
    {
        /// <remarks/>
        [XmlAttribute(AttributeName = "code")]
        public ErrorCode Code { get; set; }

        /// <remarks/>
        [XmlText()]
        public string Value { get; set; }
    }

    /// <remarks/>
    [Serializable()]
    [XmlType()]
    public enum WorldPayServiceVersion
    {
        /// <remarks/>
        [XmlEnum("1.4")]
        Version14,

        /// <remarks/>
        [XmlEnum("1.3")]
        Version13,

        /// <remarks/>
        [XmlEnum("1.2")]
        Version12,

        /// <remarks/>
        [XmlEnum("1.1")]
        Version11,

        /// <remarks/>
        [XmlEnum("1.0")]
        Version10
    }

    /// <remarks/>
    [Serializable()]
    [XmlType(AnonymousType = true)]
    public enum AmountExponent
    {

        /// <remarks/>
        [XmlEnum("0")]
        Exponent0,

        /// <remarks/>
        [XmlEnum("2")]
        Exponent2,

        /// <remarks/>
        [XmlEnum("3")]
        Exponent3
    }

    /// <remarks/>
    [Serializable()]
    [XmlType(AnonymousType = true)]
    public enum ErrorCode
    {

        /// <remarks/>
        [XmlEnum("1")]
        Error1,

        /// <remarks/>
        [XmlEnum("2")]
        Error2,

        /// <remarks/>
        [XmlEnum("3")]
        Error3,

        /// <remarks/>
        [XmlEnum("4")]
        Error4,

        /// <remarks/>
        [XmlEnum("5")]
        Error5,

        /// <remarks/>
        [XmlEnum("6")]
        Error6,

        /// <remarks/>
        [XmlEnum("7")]
        Error7,

        /// <remarks/>
        [XmlEnum("8")]
        Error8,

        /// <remarks/>
        [XmlEnum("9")]
        Error9,

        /// <remarks/>
        [XmlEnum("10")]
        Error10,

        /// <remarks/>
        [XmlEnum("11")]
        Error11
    }
}