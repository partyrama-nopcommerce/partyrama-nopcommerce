﻿using System.IO;
using System.Text;

namespace Nop.Services.Payments.WorldPay
{
    public class Utf8StringWriter : StringWriter
    {
        public override Encoding Encoding => Encoding.UTF8;
    }
}