﻿namespace Nop.Core.Domain.Catalog
{
    /// <summary>
    /// Represents the row connection type
    /// </summary>
    public enum RowConnectionType
    {
        /// <summary>
        /// Category
        /// </summary>
        category = 1,

        /// <summary>
        /// Product
        /// </summary>
        product = 2,

        /// <summary>
        /// Element
        /// </summary>
        element = 3,

        /// <summary>
        /// topic
        /// </summary>
        topic = 4,

        /// <summary>
        /// slider
        /// </summary>
        slider = 5
    }
}
