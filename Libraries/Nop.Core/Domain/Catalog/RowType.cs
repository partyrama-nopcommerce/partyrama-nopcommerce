﻿namespace Nop.Core.Domain.Catalog
{
    /// <summary>
    /// Represents the row type
    /// </summary>
    public enum RowType
    {
        /// <summary>
        /// Header
        /// </summary>
        header = 1,

        /// <summary>
        /// Text column
        /// </summary>
        textcolumn = 2,

        /// <summary>
        /// Data
        /// </summary>
        data = 3,

        /// <summary>
        /// Image column
        /// </summary>
        imagecolumn = 4,

        /// <summary>
        /// Title
        /// </summary>
        title = 5
    }
}