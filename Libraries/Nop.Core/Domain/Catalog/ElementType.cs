namespace Nop.Core.Domain.Catalog
{
    /// <summary>
    /// Represents an element type
    /// </summary>
    public enum ElementType
    {
        /// <summary>
        /// Separator
        /// </summary>
        Separator = 1,

        /// <summary>
        /// Description
        /// </summary>
        Description = 2
    }
}
