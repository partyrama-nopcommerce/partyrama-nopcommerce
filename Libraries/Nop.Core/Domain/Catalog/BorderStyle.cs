﻿namespace Nop.Core.Domain.Catalog
{
    /// <summary>
    /// Represents the border style
    /// </summary>
    public enum BorderStyle
    {
        /// <summary>
        /// Solid
        /// </summary>
        Solid = 1,

        /// <summary>
        /// Dotted
        /// </summary>
        Dotted = 2,

        /// <summary>
        /// Dashed
        /// </summary>
        Dashed = 3,

        /// <summary>
        /// Double
        /// </summary>
        Double = 4,
    }
}
