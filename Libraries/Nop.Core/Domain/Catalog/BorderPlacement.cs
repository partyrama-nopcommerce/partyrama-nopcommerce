﻿namespace Nop.Core.Domain.Catalog
{
    /// <summary>
    /// Represents the border placement
    /// </summary>
    public enum BorderPlacement
    {
        /// <summary>
        /// None
        /// </summary>
        None = 1,

        /// <summary>
        /// Top
        /// </summary>
        Top = 2,

        /// <summary>
        /// Bottom
        /// </summary>
        Bottom = 3,

        /// <summary>
        /// Both
        /// </summary>
        Both = 4,
    }
}
