﻿namespace Nop.Core.Domain.Catalog
{
    /// <summary>
    /// Represents the connection type
    /// </summary>
    public enum ConnectionType
    {
        /// <summary>
        /// Category to category
        /// </summary>
        cat_to_cat,

        /// <summary>
        /// Category to product
        /// </summary>
        cat_to_prod,

        /// <summary>
        /// Product to category
        /// </summary>
        prod_to_cat,

        /// <summary>
        /// Product to product
        /// </summary>
        prod_to_prod,

        /// <summary>
        /// Separator to category
        /// </summary>
        sep_to_cat,

        /// <summary>
        /// Separator to category
        /// </summary>
        sep_to_prod,

        /// <summary>
        /// Slider to category
        /// </summary>
        slider_to_cat,

        /// <summary>
        /// Slider to category
        /// </summary>
        slider_to_prod,

        /// <summary>
        /// Description to category
        /// </summary>
        desc_to_prod,

        /// <summary>
        /// Description to category
        /// </summary>
        desc_to_cat
    }

    /// <summary>
    /// Represents the connection to type
    /// </summary>
    public enum ConnectionToType
    {
        /// <summary>
        /// Category
        /// </summary>
        cat,

        /// <summary>
        /// Product
        /// </summary>
        prod,

        /// <summary>
        /// Separator
        /// </summary>
        sep,

        /// <summary>
        /// Slider
        /// </summary>
        slider,

        /// <summary>
        /// Description
        /// </summary>
        desc
    }

    /// <summary>
    /// Represents the connection from type
    /// </summary>
    public enum ConnectionFromType
    {
        /// <summary>
        /// Category
        /// </summary>
        cat,

        /// <summary>
        /// Product
        /// </summary>
        prod,

        /// <summary>
        /// Temporary for reorder
        /// </summary>
        tempProd,

        /// <summary>
        /// Temporary for reorder
        /// </summary>
        tempCat
    }
}