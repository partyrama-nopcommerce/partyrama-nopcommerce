namespace Nop.Core.Domain.Catalog
{
    /// <summary>
    /// Represents a product status
    /// </summary>
    public enum ProductStatus
    {
        /// <summary>
        /// In stock
        /// </summary>
        InStock = 1,
        /// <summary>
        /// Out of stock
        /// </summary>
        OutOfStock = 2,
        /// <summary>
        /// Quarantine
        /// </summary>
        Quarantine = 3,
        /// <summary>
        /// Going to delete
        /// </summary>
        GoingToDelete = 4,
        /// <summary>
        /// Coming soon
        /// </summary>
        ComingSoon = 5
    }
}
