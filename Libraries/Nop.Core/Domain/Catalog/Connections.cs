using Nop.Core.Domain.Media;
using System;

namespace Nop.Core.Domain.Catalog
{
    /// <summary>
    /// Represents a connections
    /// </summary>
    public partial class Connections : BaseEntity
    {
        /// <summary>
        /// Gets or sets the type
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// Gets or sets the entity identifier
        /// </summary>
        public int From { get; set; }

        /// <summary>
        /// Gets or sets the entity identifier
        /// </summary>
        public int To { get; set; }

        /// <summary>
        /// Gets or sets the css class
        /// </summary>
        public string CssClass { get; set; }

        /// <summary>
        /// Gets or sets the display order
        /// </summary>
        public int DisplayOrder { get; set; }

        /// <summary>
        /// Gets or sets the from type
        /// </summary>
        public string FromType { get; set;  }

        /// <summary>
        /// Gets or sets the to type
        /// </summary>
        public string ToType { get; set; }

        /// <summary>
        /// Gets or sets the date and time of instance creation
        /// </summary>
        public DateTime CreatedOnUtc { get; set; }

        /// <summary>
        /// Gets or sets the date and time of instance update
        /// </summary>
        public DateTime UpdatedOnUtc { get; set; }

        /// <summary>
        /// Gets or sets the picture identifier
        /// </summary>
        public int? PictureId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to show the short description
        /// </summary>
        public bool ShowDescription { get; set; }

        /// <summary>
        /// Gets or sets the picture
        /// </summary>
        public virtual Picture Picture { get; set; }

        /// <summary>
        /// Gets or sets the connection type
        /// </summary>
        public ConnectionType ConnectionType
        {
            get => (ConnectionType)Enum.Parse(typeof(ConnectionType), Type);
            set => Type = value.ToString();
        }

        /// <summary>
        /// Gets or sets the connection from type
        /// </summary>
        public ConnectionFromType ConnectionFromType
        {
            get => (ConnectionFromType)Enum.Parse(typeof(ConnectionFromType), FromType);
            set => FromType = value.ToString();
        }

        /// <summary>
        /// Gets or sets the connection to type
        /// </summary>
        public ConnectionToType ConnectionToType
        {
            get => (ConnectionToType)Enum.Parse(typeof(ConnectionToType), ToType);
            set => ToType = value.ToString();
        }
    }
}