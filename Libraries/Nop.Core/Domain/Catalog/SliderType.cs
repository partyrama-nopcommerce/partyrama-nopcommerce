﻿namespace Nop.Core.Domain.Catalog
{
    /// <summary>
    /// Represents the slider type
    /// </summary>
    public enum SliderType
    {
        /// <summary>
        /// General
        /// </summary>
        General = 1,

        /// <summary>
        /// Text
        /// </summary>
        Text = 2,

        /// <summary>
        /// Mix (products and cats)
        /// </summary>
        Mix = 3,

        /// <summary>
        /// New (auto-build)
        /// </summary>
        New = 4,

        /// <summary>
        /// Sale (auto-build)
        /// </summary>
        Sale = 5
    }
}
