namespace Nop.Core.Domain.Catalog
{
    /// <summary>
    /// Represents a product type
    /// </summary>
    public enum ProductType
    {
        /// <summary>
        /// Simple
        /// </summary>
        SimpleProduct = 1,

        /// <summary>
        /// Pack
        /// </summary>
        Pack = 2,

        /// <summary>
        /// Simple
        /// </summary>
        Separator = 3,

        /// <summary>
        /// Personalized
        /// </summary>
        Personalized = 4,

        /// <summary>
        /// Grouped (product with variants)
        /// </summary>
        GroupedProduct = 10
    }
}
