using System;
using System.Collections.Generic;

namespace Nop.Core.Domain.Catalog
{
    /// <summary>
    /// Represents a menu
    /// </summary>
    public partial class Menu : BaseEntity
    {
        private ICollection<MenuItem> _menuItems;

        /// <summary>
        /// Gets or sets the name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether set countdown
        /// </summary>
        public bool SetCountDown { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the entity is visible
        /// </summary>
        public bool Visible { get; set; }

        /// <summary>
        /// Gets or sets the start date
        /// </summary>
        public DateTime? StartDateUtc { get; set; }

        /// <summary>
        /// Gets or sets the end date
        /// </summary>
        public DateTime? EndDateUtc { get; set; }

        /// <summary>
        /// Gets or sets the connection type identifier
        /// </summary>
        public int? TabConnectionTypeId { get; set; }

        /// <summary>
        /// Gets or sets the connection (product/category) identifier
        /// </summary>
        public int? TabConnectionId { get; set; }

        /// <summary>
        /// Gets or sets the date and time of instance creation
        /// </summary>
        public DateTime CreatedOnUtc { get; set; }

        /// <summary>
        /// Gets or sets the date and time of instance update
        /// </summary>
        public DateTime UpdatedOnUtc { get; set; }

        /// <summary>
        /// Gets or sets the mega menu tab connection type
        /// </summary>
        public RowConnectionType? TabConnectionType
        {
            get => (RowConnectionType?)TabConnectionTypeId;
            set => TabConnectionTypeId = (int?)value;
        }

        /// <summary>
        /// Gets or sets the collection of Menutems
        /// </summary>
        public virtual ICollection<MenuItem> MenuItems
        {
            get => _menuItems ?? (_menuItems = new List<MenuItem>());
            protected set => _menuItems = value;
        }
    }
}