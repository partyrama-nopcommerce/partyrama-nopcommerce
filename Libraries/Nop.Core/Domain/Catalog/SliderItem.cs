using Nop.Core.Domain.Media;
using System;

namespace Nop.Core.Domain.Catalog
{
    /// <summary>
    /// Represents a slider item
    /// </summary>
    public partial class SliderItem : BaseEntity
    {
        /// <summary>
        /// Gets or sets the slider identifier
        /// </summary>
        public int SliderId { get; set; }

        /// <summary>
        /// Gets or sets the column identifier
        /// </summary>
        public int ColumnId { get; set; }

        /// <summary>
        /// Gets or sets the display order
        /// </summary>
        public int DisplayOrder { get; set; }

        /// <summary>
        /// Gets or sets the row type identifier
        /// </summary>
        public int RowTypeId { get; set; }

        /// <summary>
        /// Gets or sets the css class
        /// </summary>
        public string CssClass { get; set; }

        /// <summary>
        /// Gets or sets the color (HEX)
        /// </summary>
        public string Color { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the entity is visible
        /// </summary>
        public bool Visible { get; set; }

        /// <summary>
        /// Gets or sets the start date
        /// </summary>
        public DateTime? StartDateUtc { get; set; }

        /// <summary>
        /// Gets or sets the end date
        /// </summary>
        public DateTime? EndDateUtc { get; set; }

        /// <summary>
        /// Gets or sets the connection type identifier
        /// </summary>
        public int? RowConnectionTypeId { get; set; }

        /// <summary>
        /// Gets or sets the connection (product/category) identifier
        /// </summary>
        public int? RowConnectionId { get; set; }

        /// <summary>
        /// Gets or sets the date and time of instance creation
        /// </summary>
        public DateTime CreatedOnUtc { get; set; }

        /// <summary>
        /// Gets or sets the date and time of instance update
        /// </summary>
        public DateTime UpdatedOnUtc { get; set; }

        /// <summary>
        /// Gets or sets the picture identifier
        /// </summary>
        public int? PictureId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to show the description
        /// </summary>
        public bool ShowDescription { get; set; }

        /// <summary>
        /// Gets the slider
        /// </summary>
        public virtual Slider Slider { get; set; }

        /// <summary>
        /// Gets or sets the picture
        /// </summary>
        public virtual Picture Picture { get; set; }

        /// <summary>
        /// Gets or sets the mega menu row type
        /// </summary>
        public RowType RowType
        {
            get => (RowType)RowTypeId;
            set => RowTypeId = (int)value;
        }

        /// <summary>
        /// Gets or sets the mega menu tab item connection type
        /// </summary>
        public RowConnectionType? RowConnectionType
        {
            get => (RowConnectionType?)RowConnectionTypeId;
            set => RowConnectionTypeId = (int?)value;
        }
    }
}