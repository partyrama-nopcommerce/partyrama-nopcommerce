using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Discounts;
using Nop.Core.Domain.Localization;
using Nop.Core.Domain.Security;
using Nop.Core.Domain.Seo;
using Nop.Core.Domain.Stores;

namespace Nop.Core.Domain.Catalog
{
    /// <summary>
    /// Represents a category
    /// </summary>
    public partial class Category : BaseEntity, ILocalizedEntity, ISlugSupported, IAclSupported, IStoreMappingSupported, IDiscountSupported
    {
        private ICollection<DiscountCategoryMapping> _discountCategoryMappings;
        private ICollection<CategoryPicture> _categoryPictures;

        /// <summary>
        /// Gets or sets the name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the short description
        /// </summary>
        public string ShortDescription { get; set; }

        /// <summary>
        /// Gets or sets a value of used category template identifier
        /// </summary>
        public int CategoryTemplateId { get; set; }

        /// <summary>
        /// Gets or sets the meta keywords
        /// </summary>
        public string MetaKeywords { get; set; }

        /// <summary>
        /// Gets or sets the meta description
        /// </summary>
        public string MetaDescription { get; set; }

        /// <summary>
        /// Gets or sets the meta title
        /// </summary>
        public string MetaTitle { get; set; }

        /// <summary>
        /// Gets or sets the parent category identifier
        /// </summary>
        public int ParentCategoryId { get; set; }

        /// <summary>
        /// Gets or sets the page size
        /// </summary>
        public int PageSize { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether customers can select the page size
        /// </summary>
        public bool AllowCustomersToSelectPageSize { get; set; }

        /// <summary>
        /// Gets or sets the available customer selectable page size options
        /// </summary>
        public string PageSizeOptions { get; set; }

        /// <summary>
        /// Gets or sets the available price ranges
        /// </summary>
        public string PriceRanges { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to show the category on home page
        /// </summary>
        public bool ShowOnHomePage { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to include this category in the top menu
        /// </summary>
        public bool IncludeInTopMenu { get; set; }
        
        /// <summary>
        /// Gets or sets a value indicating whether the entity is subject to ACL
        /// </summary>
        public bool SubjectToAcl { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the entity is limited/restricted to certain stores
        /// </summary>
        public bool LimitedToStores { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the entity is published
        /// </summary>
        public bool Published { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the entity has been deleted
        /// </summary>
        public bool Deleted { get; set; }

        /// <summary>
        /// Gets or sets the display order
        /// </summary>
        public int DisplayOrder { get; set; }

        /// <summary>
        /// Gets or sets css class
        /// </summary>
        public string CssClass { get; set; }

        /// <summary>
        /// Gets or sets the short name
        /// </summary>
        public string ShortName { get; set; }

        /// <summary>
        /// Gets or sets the sidebar identifier
        /// </summary>
        public int? SidebarId { get; set; }

        /// <summary>
        /// Gets or sets the collapse identifier
        /// </summary>
        public int? CollapseId { get; set; }

        /// <summary>
        /// Gets or sets the date and time of instance creation
        /// </summary>
        public DateTime CreatedOnUtc { get; set; }

        /// <summary>
        /// Gets or sets the date and time of instance update
        /// </summary>
        public DateTime UpdatedOnUtc { get; set; }

        /// <summary>
        /// Gets or sets the meta index type identifier
        /// </summary>
        public int? MetaIndexTypeId { get; set; }

        /// <summary>
        /// Gets or sets the category identifier
        /// </summary>
        public int? MetaCanonical { get; set; }

        /// <summary>
        /// Gets or sets the sidebar
        /// </summary>
        public virtual Sidebar Sidebar { get; set; }

        /// <summary>
        /// Gets or sets collapse
        /// </summary>
        public virtual SidebarItem Collapse { get; set; }

        /// <summary>
        /// Gets or sets the meta index type
        /// </summary>
        public MetaIndexType? MetaIndexType
        {
            get => (MetaIndexType?)MetaIndexTypeId;
            set => MetaIndexTypeId = (int?)value;
        }

        /// <summary>
        /// Gets or sets the collection of applied discounts
        /// </summary>
        public virtual IList<Discount> AppliedDiscounts => DiscountCategoryMappings.Select(mapping => mapping.Discount).ToList();

        /// <summary>
        /// Gets or sets the discount-category mappings
        /// </summary>
        public virtual ICollection<DiscountCategoryMapping> DiscountCategoryMappings
        {
            get => _discountCategoryMappings ?? (_discountCategoryMappings = new List<DiscountCategoryMapping>());
            set => _discountCategoryMappings = value;
        }

        /// <summary>
        /// Gets or sets the collection of CategoryPicture
        /// </summary>
        public virtual ICollection<CategoryPicture> CategoryPictures
        {
            get => _categoryPictures ?? (_categoryPictures = new List<CategoryPicture>());
            protected set => _categoryPictures = value;
        }
    }
}