using System;
using System.Collections.Generic;

namespace Nop.Core.Domain.Catalog
{
    /// <summary>
    /// Represents a slider
    /// </summary>
    public partial class Slider : BaseEntity
    {
        private ICollection<SliderItem> _sliderItems;

        /// <summary>
        /// Gets or sets the name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether set countdown
        /// </summary>
        public bool SetCountDown { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the entity is visible
        /// </summary>
        public bool Visible { get; set; }

        /// <summary>
        /// Gets or sets the start date
        /// </summary>
        public DateTime? StartDateUtc { get; set; }

        /// <summary>
        /// Gets or sets the end date
        /// </summary>
        public DateTime? EndDateUtc { get; set; }

        /// <summary>
        /// Gets or sets the connection type identifier
        /// </summary>
        public int? TabConnectionTypeId { get; set; }

        /// <summary>
        /// Gets or sets the connection (product/category) identifier
        /// </summary>
        public int? TabConnectionId { get; set; }

        /// <summary>
        /// Gets or sets the date and time of instance creation
        /// </summary>
        public DateTime CreatedOnUtc { get; set; }

        /// <summary>
        /// Gets or sets the date and time of instance update
        /// </summary>
        public DateTime UpdatedOnUtc { get; set; }

        /// <summary>
        /// Gets or sets the slider type identifier
        /// </summary>
        public int SliderTypeId { get; set; }

        /// <summary>
        /// Gets or sets the slider speed
        /// </summary>
        public int? Speed { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether arrows are visible
        /// </summary>
        public bool VisibleArrows { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether dots are visible
        /// </summary>
        public bool VisibleDots { get; set; }

        /// <summary>
        /// Gets or sets the css class
        /// </summary>
        public string CssClass { get; set; }

        /// <summary>
        /// Gets or sets the border style identifier
        /// </summary>
        public int BorderStyleId { get; set; }

        /// <summary>
        /// Gets or sets the border height
        /// </summary>
        public int? BorderHeight { get; set; }

        /// <summary>
        /// Gets or sets the color (HEX)
        /// </summary>
        public string BorderColor { get; set; }

        /// <summary>
        /// Gets or sets the border placement identifier
        /// </summary>
        public int BorderPlacementId { get; set; }

        /// <summary>
        /// Gets or sets the number of items
        /// </summary>
        public int? Number { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to show buy button
        /// </summary>
        public bool ShowBuyButton { get; set; }

        /// <summary>
        /// Gets or sets the mega menu tab connection type
        /// </summary>
        public RowConnectionType? TabConnectionType
        {
            get => (RowConnectionType?)TabConnectionTypeId;
            set => TabConnectionTypeId = (int?)value;
        }

        /// <summary>
        /// Gets or sets the slider type
        /// </summary>
        public SliderType SliderType
        {
            get => (SliderType)SliderTypeId;
            set => SliderTypeId = (int)value;
        }

        /// <summary>
        /// Gets or sets the border style
        /// </summary>
        public BorderStyle BorderStyle
        {
            get => (BorderStyle)BorderStyleId;
            set => BorderStyleId = (int)value;
        }

        /// <summary>
        /// Gets or sets the border placement
        /// </summary>
        public BorderPlacement BorderPlacement
        {
            get => (BorderPlacement)BorderPlacementId;
            set => BorderPlacementId = (int)value;
        }

        /// <summary>
        /// Gets or sets the collection of slider items
        /// </summary>
        public virtual ICollection<SliderItem> SliderItems
        {
            get => _sliderItems ?? (_sliderItems = new List<SliderItem>());
            protected set => _sliderItems = value;
        }
    }
}