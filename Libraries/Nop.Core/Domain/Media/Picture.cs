namespace Nop.Core.Domain.Media
{
    /// <summary>
    /// Represents a picture
    /// </summary>
    public partial class Picture : BaseEntity
    {
        /// <summary>
        /// Gets or sets the picture mime type
        /// </summary>
        public string MimeType { get; set; }

        /// <summary>
        /// Gets or sets the SEO friendly filename of the picture
        /// </summary>
        public string SeoFilename { get; set; }

        /// <summary>
        /// Gets or sets the "alt" attribute for "img" HTML element. If empty, then a default rule will be used (e.g. product name)
        /// </summary>
        public string AltAttribute { get; set; }

        /// <summary>
        /// Gets or sets the "title" attribute for "img" HTML element. If empty, then a default rule will be used (e.g. product name)
        /// </summary>
        public string TitleAttribute { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the picture is new
        /// </summary>
        public bool IsNew { get; set; }

        /// <summary>
        /// Gets or sets the picture binary
        /// </summary>
        public virtual PictureBinary PictureBinary { get; set; }

        /// <summary>
        /// Gets or sets the picture kind identifier
        /// </summary>
        public int PictureKindId { get; set; }

        /// <summary>
        /// Gets or sets the original filename of the picture
        /// </summary>
        public string OriginalName { get; set; }

        /// <summary>
        /// Gets or sets the picture type
        /// </summary>
        public PictureKind PictureKind
        {
            get => (PictureKind)PictureKindId;
            set => PictureKindId = (int)value;
        }

    }
}
