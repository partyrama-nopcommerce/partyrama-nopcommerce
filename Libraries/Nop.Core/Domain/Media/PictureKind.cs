namespace Nop.Core.Domain.Media
{
    /// <summary>
    /// Represents a picture item kind
    /// </summary>
    public enum PictureKind
    {
        /// <summary>
        /// Basic
        /// </summary>
        Basic = 1,

        /// <summary>
        /// SEO meta tag
        /// </summary>
        InMeta = 2,

        /// <summary>
        /// Mega menu
        /// </summary>
        MegaMenu = 3,

        /// <summary>
        /// Slider
        /// </summary>
        Slider = 4,

        /// <summary>
        /// Sidebar
        /// </summary>
        Sidebar = 5,

        /// <summary>
        /// Home page
        /// </summary>
        HomePage = 6,

        /// <summary>
        /// Connection
        /// </summary>
        Connection = 7
    }
}
