﻿namespace Nop.Core.Domain.Common
{
    /// <summary>
    /// Represents the meta index type
    /// </summary>
    public enum MetaIndexType
    {
        /// <summary>
        /// Index
        /// </summary>
        Index = 1,

        /// <summary>
        /// NoIndex
        /// </summary>
        NoIndex = 2,

        /// <summary>
        /// Follow
        /// </summary>
        Follow = 3,

        /// <summary>
        /// NoFollow
        /// </summary>
        NoFollow = 4
    }
}
